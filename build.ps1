$VerbosePreference="Continue"
set-psdebug -trace 1


$script = ((resolve-path .tools\buildmodule.ps1).path)
write-verbose "Script path is $script"

write-verbose "Path exists: $(test-path $script)"
. "$script"

if(-not (test-path deploy)) { mkdir deploy}
New-PSModulePackage -path "$((resolve-path .\src).path)" -name "eus-modules" -output "$((resolve-path .\deploy).path)"