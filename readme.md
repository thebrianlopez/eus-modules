# EUS Modules #

Branch | Build Status
:--- | :---
```master```| [![Logo][master]](https://ci.appveyor.com/project/thebrianlopez/eus-modules/branch/master)
```development```| [![Logo][development]](https://ci.appveyor.com/project/thebrianlopez/eus-modules/branch/development)

Used by the End User Solutions team to manage Windows Client Operating Systems as well as interface with the following services:

+ System Center Configuration Manager (**SCCM**)
+ Microsoft Deployment Toolkit (**MDT**)
+ Microsoft Windows Active Directory (**AD**)
+ SNOW EUC Web Service (**eucwebserv.nestle.xxx**)
+ SNOW EUC Redirection Service (**eucredserv.nestle.xxx**)
+ Windows Management Instrumentation (**WMI**)
+ Windows Remote Management (**WinRM**)

For more information refer to the [eus-modules][eus-modules wiki article]

## Usage ##
See the [documentation]

## Documentation ##
See the [documentation]

## Governance ##
See the [documentation]

## Issues ##
Please file an issue on Gitlab

## Installation ##

### Using Nuget (Recommended) ###

1. Login to node
1. Open a normal DOS command prompt (cmd.exe)
1. Run the command below to install all pre-reqs:
```powershell 
powershell.exe -exec Unrestricted -nopro -c "iex ((new-object net.webclient).DownloadString('https://usstni9989.nestle.com:4881/nuget/install.ps1'))"
```
1. Close the dos prompt
1. Open a PowerShell Prompt
1. Confirm the following
```powershell
import-module eus-modules
get-module eus-modules | select version
```

### From source using Git ###

1. Login to node
1. Open a normal (non-elevated) PowerShell Console
1. Copy/Paste the following commands into PowerShell console window to download the source files

```powershell
git clone git@gitlab.nestle.qa:gtseus-clients-team/eus-modules.git
cd .\eus-modules
git submodule update --init --recursive
Import-Module .\src\eus-modules.psd1 -verbose
```

## Appendix ##
### Appendix A: CI Build Infrastructure ###
The CI Build Infra for this module consists of the following:

1. Git Repository: ```master``` branch is used for production releases. When Adding/
1. TeamCity Build Server: Monitors the Git repository for changes, checks out latest commit, builds source using msbuild, deploys artifacts if build is successful.
1. TeamCity Artifact Managaer: upon successful builds, generates artifacts and makes them availible for deployment (e.g., Nuget Package, ZIP file, ISO File)
1. OctopusDeploy: Gathers the artifacts generated from team city (e.g., Nuget Package, ZIP file, IIS Website) and deploys to its target location

### Appendix B: Unit Testing ###
1. Login to node
1. Open a normal (non-elevated) PowerShell Console
1. Copy/Paste the following commands into PowerShell console window to initiate the unit test

```powershell
#
cd ~/
$verbosePreference="Continue"
Import-Module ".\eus-modules" -verbose
#
```
If no error messages appears then tests have been successful.

### Appendix C: Git Submodules ###
This project contains submodules that must be initialized before using the git repository. These submodules are:

1. [Jump-Location](https://github.com/tkellogg/Jump-Location) : A ```cd``` command that learns your frequent directories
1. [PoshGit](https://github.com/dahlbyk/posh-git) : A set of PowerShell scripts which provide Git/PowerShell integration 


```powershell
git clone https://thebrianlopez@bitbucket.org/thebrianlopez/eus-modules.git
cd eus-modules
git submodule update --init --recursive

```
Note: This should be done only when a submodule gets disconnected from the parent repository.

```powershell
git clone https://thebrianlopez@bitbucket.org/thebrianlopez/eus-modules.git
cd eus-modules
del .gitmodules
git rm --cached content\posh-git
rd /q /s content\posh-git
git add --all
git commit -m "Attempting to add submodule"
git submodule add https://github.com/dahlbyk/posh-git.git content/Posh-Git
git clone https://github.com/dahlbyk/posh-git.git content/Posh-Git
```

### Appendix D: Nuget Overview ###
1.  **Nuget** package is a [CI build][CI Overview] running from the master branch of the latest git commit. All Nuget packages are passing the TeamCity build before being published.
1. **Git** repository should be used to checkout a seperate feature of bugfix branch from the project. As a manner of standard and good houesekeeping all Git branches should be passing the build at all times. 



[snowwiki]: http://snowwiki.nestle.com/wiki
[snowget]: http://usstni9989.nestle.com/snowget
[eus-modules wiki article]: http://snowwiki.nestle.com/wiki/index.php/EUS_Modules
[eus nuget repository]: http://snowwiki.nestle.com/wiki/index.php/EUS_Nuget_Repository
[TeamCity]: http://1021tues-pkg.cloudapp.net
[CI Overview]: http://en.wikipedia.org/wiki/Continuous_integration
[Git]: http://git-scm.com/
[Internal Gitlab Repository]: http://gitlab.nestle.com/
[Documentation]: http://snowwiki.nestle.com/wiki/index.php/EUS_Modules
[development]:  https://ci.appveyor.com/api/projects/status/d7miepgbtdkqf824/branch/development?svg=true
[master]: https://ci.appveyor.com/api/projects/status/d7miepgbtdkqf824/branch/master?svg=true