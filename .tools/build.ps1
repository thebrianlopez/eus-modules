$VerbosePreference="Continue"
set-psdebug -trace 1
set-psbreakpoint -variable boom
set-alias -name wr -value write-host

$modulename = "eus-modules"
$modulesToLoad = @()
$modulesToLoad += $modulename
# $modulesToLoad += "pester"
#$modulesToLoad += "testmod"

wr "Step 1: Import script to build module"
    . "$((resolve-path .tools\New-PSModulePackage.ps1).path)"

wr "Step 2: Validate cmdlet is accessible after import"
    get-command New-PSModulePackage -ea Stop | out-null

wr "Step 3: Now cmdlet is visible, lets cleanup and prepare the folder structure to make our module"
    @("deploy",$modulename) |% {if(test-path $_) {rm -rec -for $_ };mkdir $_ | out-null;}
    copy -rec -force "src\*" $modulename | out-null

wr "Step 4: Since the folder structure is now prepared lets add the path to psmodules path for easy import"
    $src = $([io.directoryinfo]$MyInvocation.MyCommand.Definition).parent.parent.fullname
    $env:PSModulePath += ";$src"


wr "Step 5: Lets import all of our modules and validate they exist $src"
$boom = 0
    try {
        $modulesToLoad | %{ import-module $_ }
    }
    catch {$error;exit $error.Count;}

wr "Step 6: generate manifest, no nuget package yet"
    New-PSModulePackage -path "$((resolve-path .\$modulename).path)" -name $modulename -output "$((resolve-path .\deploy).path)" 

wr "Step 7: run pester tests, including manifest check with the one we just created"
    #invoke-pester -verbose -outputfile "build.log" -outputformat NUnitXml

wr "Step 7b: Import modules and get-command count"
    $modulesToLoad | %{ get-command -module $_ | select -Property CommandType,Name,Version,Source | ft}
if($error.Count -gt 0){$error;exit $error.Count;}

wr "Step 8: All items ran successfully, starting nuget package build to artifacts folder."
    New-PSModulePackage -path "$((resolve-path .\$modulename).path)" -name $modulename -output "$((resolve-path .\deploy).path)" -package

if($error.Count -gt 0){$error;exit $error.Count;}

write-host "Script completed successfully with return code $($error.Count) " -ForegroundColor Green
#$script = ((resolve-path .tools\buildmodule.ps1).path)
#wr "Script path is $script"
#
#wr "Path exists: $(test-path $script)"
#. "$script"
