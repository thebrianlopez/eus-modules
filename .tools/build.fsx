// include Fake lib
#r @"..\packages\FAKE\tools\FakeLib.dll"
#r "System.Management.Automation"

open Fake
open Fake.AssemblyInfoFile
open System.Management.Automation
open Fake.ProcessHelper

RestorePackages()
// app properties
let appName = "eus-modules"
let appTitle = "eus-modules"
let author = ["Brian Lopez (brian.lopez@purina.nestle.com"]
let orgName = "GLOBE Technology Solutions"
let year = System.DateTime.Now.ToUniversalTime().ToString("yyyy")
let day = System.DateTime.Now.DayOfYear.ToString()
let time = System.DateTime.Now.ToUniversalTime().ToString("HHmm")
let version = @"1." + year + @"." + day + @"." + time
let summary = ""
let description = """PowerShell Module for performing SNOW Build related admin functions."""
let tags = "snow osd mdt sccm"

// Directories
let buildDir  = @".\build\"
let packagesDir = @".\packages\"

// Targets
Target "Clean" (fun _ ->
    CleanDirs [buildDir]
)

Target "SetVersions" (fun _ ->
    CreateCSharpAssemblyInfo ("./AssemblyInfo.cs")
        [Attribute.Title appName
         Attribute.Description description
         Attribute.Guid "70935524-5799-4E34-A1F3-331D09809F42"
         Attribute.Product appName
         Attribute.Version version
         Attribute.FileVersion version]
)

Target "CompileModule" (fun _ ->
    !! @"*.*proj"
      |> MSBuildRelease buildDir "Build"
      |> Log "Build-Output: "
)

Target "CreatePackage" (fun _ ->
    let properties =
        [ ("DeployOnBuild", "false"); ("RunOctoPack","true") ]
    !! @"*.*proj"
      |> MSBuildReleaseExt buildDir properties "build"
      |> Log "Build-Output: "
)

// Default target
Target "Default" DoNothing

// Dependencies
"Clean"
    //==> "SetVersions"
    ==> "CompileModule"
    // ==> "CreatePackage"
    ==> "Default"

// start build
RunTargetOrDefault "Default"


