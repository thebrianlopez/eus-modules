# set-psdebug -trace 1
# set-psbreakpoint -variable boom
[io.fileinfo]$solutionDir = "$($solutionDir.Path)"
$psake.use_exit_on_error = $true
$psake.build_script_dir = $solutionDir

properties {
    $currentDir = resolve-path .
    $solutionDir = $psake.build_script_dir
    $sourceDir = "$($solutionDir.FullName)\src"
    $toolsDir = "$($solutionDir.FullName)\.tools"
    $buildDir = "$($solutionDir.FullName)\build"
    $packagesDir = "$($solutionDir.FullName)\packages"
    $projectName = $solutionDir.Name
    $Invocation = (Get-Variable MyInvocation -Scope 1).Value
    $nugetExe = "$solutionDir\.nuget\nuget.exe"
    $targetBase = "tools"
    $nuspec = "$sourceDir\$projectName.$version.nuspec"
}

# Task default -depends Test,Generate-NuSpec
Task default -depends GatherBuildInfo, Test, Generate-NuSpec

Task GatherBuildInfo {
  Set-Location $sourceDir
  exec {
    #$files = (Get-ChildItem -Recurse | select FullName)
    #echo "Files found: $($files.length)"
    #$files
    #if ($files.length -eq 0) {
    #    $testResults | Format-List
    #    Write-Error -Message 'No files found on file system!'
    #  }
    echo "Hi from gatherbuildinfo"
    # rm -rec -for $buildDir
    Get-ChildItem $packagesDir -Directory -Recurse | select FullName
  }
  Set-Location $solutionDir
}

Task Test {
    Set-Location "$sourceDir"
    exec {
      # rm -rec -for "$buildDir\Pester" -errorAction SilentlyContinue
      copy -rec -for "$packagesDir\Pester" "$buildDir\Pester" #becuase sometimes the nuget package is wrong
      $env:PSModulePath += ";$packagesDir"
      Import-Module Pester
      $testResults = Invoke-Pester -PassThru
      if ($testResults.FailedCount -gt 0) {
        $testResults | Format-List
        Write-Error -Message 'One or more Pester tests failed. Build cannot continue!'
      }
    }
    Set-Location $currentDir
}


Task Generate-NuSpec {
  exec {

    . "$toolsDir\New-PSModulePackage.ps1" # To use out custom function to generate a nuget package
    $version = (git.exe describe --abbrev=0 --tags)
    $hash="hash"
    
    New-PSModulePackage -path $sourceDir -name $projectName -output $buildDir -octo $packagesDir\octotools\Octo.exe -version $version -package
    #$testresults = (dir "$buildDir\*.nupkg")

    #if ($testResults.length -eq 0) {
    #    $testResults | Format-List
    #    Write-Error -Message 'One or more Pester tests failed. Build cannot continue!'
    #}
  }
}

Task Push-Nuget {
    $pkg = Get-Item -path $solutionDir\build\$projectName.$version.nupkg
    exec { .$nugetExe push $pkg.FullName }
}
