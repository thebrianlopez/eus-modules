@echo off

"%~dp0..\.nuget\NuGet.exe" "sources" "disable" "-name" "https://www.nuget.org/api/v2/" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
"%~dp0..\.nuget\NuGet.exe" "sources" "disable" "-name" "Microsoft Visual Studio Offline Packages" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
:: "%~dp0..\.nuget\NuGet.exe" "sources" "enable" "-name" "thebrianlopez" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
"%~dp0..\.nuget\NuGet.exe" "sources" "list" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed

::IF NOT EXIST "%~dp0..\packages\fake" (
:: 	echo "Restoring packages"
::	"%~dp0..\.nuget\NuGet.exe" "Install" "FAKE" "-OutputDirectory" "%~dp0..\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
::)

:: rd /q /s "%~dp0..\packages"

IF NOT EXIST "%~dp0..\packages\psake" (
	echo "Restoring packages"
	"%~dp0..\.nuget\NuGet.exe" "Install" "psake" "-OutputDirectory" "%~dp0..\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
)
IF NOT EXIST "%~dp0..\packages\octotools" (
	echo "Restoring packages"
	"%~dp0..\.nuget\NuGet.exe" "Install" "octotools" "-OutputDirectory" "%~dp0..\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
)
IF NOT EXIST "%~dp0..\packages\pester" (
	echo "Restoring packages"
	"%~dp0..\.nuget\NuGet.exe" "Install" "pester" "-OutputDirectory" "%~dp0..\packages" "-ExcludeVersion" "-configfile" "%~dp0..\.nuget\NuGet.Config" -verbosity detailed
)
if exist "%~dp0..\packages\pester\tools" (
	echo "renaming tools"
	ren "%~dp0..\packages\pester\tools" "Pester"
)
if exist "%~dp0..\packages\psake\tools" (
	echo "renaming tools"
	ren "%~dp0..\packages\psake\tools" "psake"
)

@SET cmd= $solutionDir=(resolve-path """%~dp0\..""");$psakeDir=(resolve-path $solutionDir\packages\psake*);".$psakeDir\psake.ps1" .tools\build.psake.ps1 %* -ScriptPath "$psakeDir" ; if ($psake.build_success -eq $false) { exit 1 } else { exit 0 }
powershell -version 3.0 -NoProfile -ExecutionPolicy Bypass -Command ^ "%cmd%"

echo %errorlevel%
goto :eof

dir /a /b /s 
git status
git remote -v 
git log -3
::git describe

echo %errorlevel%