
function New-PSModulePackage{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.IO.DirectoryInfo]$path,
        [Parameter(Mandatory=$false)][System.IO.DirectoryInfo]$output,
        [Parameter(Mandatory=$false)][System.IO.FileInfo]$octo=".tools\octo.exe",
        [Parameter(Mandatory=$false)][String]$name,
        [Parameter(Mandatory=$false)][String]$version
        )
    
    # Test if folder exists
    if(-not(test-path $path)){write-error "$path does not exist." -ea Stop}

    # If output parameter is passed to fucntion, then we need to ensure it exists
    if(-not ($output)){$output=$path}
    if(-not(test-path $output)){mkdir $output}
  
    # Get the paths name sinec all powershell module names must match the name of the folder
    if(-not ($name)){$name = $($path.Name)}
    
    # Since all folders exists lets set the file paths of what is expected to be in a powershell module
    [System.IO.FileInfo]$modulepath = "$path\$name.psm1"
    [System.IO.FileInfo]$manifestpath = "$path\$name.psd1"
    
    # Test is modules is present, we dont case about manifest path until we make it
    if(-not(test-path "$modulepath")){write-error "$modulepath does not exist." -ea Stop}

    # contruct the default version string
    $year = ([System.DateTime]::Now).ToUniversalTime().ToString("yyyy")
    $day = ([System.DateTime]::Now).ToUniversalTime().DayOfYear
    $time = ([System.DateTime]::Now).ToUniversalTime().ToString("HHmm")

    # If version is not passed to the function, then use the default one
    if(-not ($version)){[string]$version = "1.$year.$day$time"}
    
    $author = "GTS Clients Team"
    $description = "PowerShell module library for the End User Solutions team. Containing a number of submodules pertaining to operational activities surrounding System Center Configuration Manager (SCCM), Microsoft Deployment Toolkit (MDT), Active Directory (AD), and the EUC Webservice."
    $company="GTS Clients Team"
    $copyright="GNU General Public License v3.0"
    $releasenotes= "Updated on $(([System.DateTime]::Now).ToUniversalTime()) UTC by $author"
    $releasenotesfile= "readme.md"

    write-host "Using the version $version"

    $p=@{
        Path="$($manifestpath.fullname)";
        ModuleToProcess = "$($modulepath.Name)";
        ModuleVersion = "$version";
        Author = "$author";
        CompanyName = "$company"
        Copyright = "$copyright"
        Description = "$description"
        RequiredAssemblies = @()
        FunctionsToExport = '*'
        NestedModules = @()
        FileList = @()
        TypesToProcess = @()
        FormatsToProcess = @()
        CmdletsToExport = '*'
        VariablesToExport = '*'
        AliasesToExport = '*'
    }
    
    # Delete any previous module manifests
    (gci "$path\*$name*.psd1") | %{rm -for $_}    
    
    # Create the module manifest
    new-modulemanifest @p

    # confirm new manifest exists
    if(-not (test-path "$path\*$name*.psd1")){write-error "unable to validate manifest exists" -ea Stop}
    # Delete any previous nuget packages

    (gci -recurse $path\*.nupkg) | %{rm -for $_}


    # Create the nuget package, check if octo exists
    # $cmd = (resolve-path "$octo").path
    
    
    if(-not(test-path $octo)){write-error "Octo.exe was not found." -ea Stop}
    $cmd = (resolve-path $octo)
    $arg = "pack -id `"$name`""
    $arg += " -version `"$version`""
    $arg += " -basepath `"$($path.FullName)`""
    $arg += " -outfolder `"$output`""
    $arg += " -author `"$author`""
    $arg += " -title `"$name`""
    $arg += " -description `"$description`""
    $arg += " -releasenotes `"$releasenotes`""
    $arg += " -releasenotesfile `"$releasenotesfile`""
    $arg += " --overwrite"
    #$boom = 0
    $StartInfo = new-object System.Diagnostics.ProcessStartInfo
    $StartInfo.FileName = "$cmd"
    $StartInfo.Arguments = "$arg"
    $StartInfo.UseShellExecute = $false
    $StartInfo.WorkingDirectory = "$($path.FullName)"
    write-verbose "Running: $cmd $arg"

    $StartInfo | foreach-object {
        if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
            [int]$exitCode = [Diagnostics.Process]::Start($StartInfo).WaitForExit()
            if($exitCode -eq 0){
                write-verbose "command complete successfully."
                $results = $true
            }
            else{
                write-error "command existed with code $($_.Exception.Message.toString())" -ErrorAction Stop
                throw
            }
        }
    }
    $cmd = $null 
    $arg = $null 
}


# Example command line
# New-PSModulePackage -path "C:\Users\skittles\eus-modules\src" -name "eus-modules" -output "C:\Users\skittles\Dropbox\packages"
