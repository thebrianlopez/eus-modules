﻿$here = (split-path -parent $MyInvocation.MyCommand.Definition)

function New-ZipFile{
    #[CmdletBinding()]

        ##############################################################################
        ##
        ## New-ZipFile
        ##
        ## From Windows PowerShell Cookbook (O'Reilly)
        ## by Lee Holmes (http://www.leeholmes.com/guide)
        ##
        ##############################################################################

    <#

    .SYNOPSIS

    Create a Zip file from any files piped in. Requires that
    you have the SharpZipLib installed, which is available from
    http://www.icsharpcode.net/OpenSource/SharpZipLib/

    .EXAMPLE

    dir *.ps1 | New-ZipFile scripts.zip d:\bin\ICSharpCode.SharpZipLib.dll
    Copies all PS1 files in the current directory to scripts.zip
    dir -recurse "C:\temp\clear-pxe-tool\src\app\ClearPxeFlag" | New-ZipFile "build.zip"

    .EXAMPLE

    "readme.txt" | New-ZipFile docs.zip d:\bin\ICSharpCode.SharpZipLib.dll
    Copies readme.txt to docs.zip
    dir -recurse "C:\temp\clear-pxe-tool\src\app\ClearPxeFlag" | New-ZipFile "build.zip"

    #>

    param(
        ## The name of the zip archive to create
        #[Parameter(Mandatory=$false,HelpMessage="Enter the path to ICSharpCode.SharpZipLib.dll",ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=1)]
        [string]$out="$env:userprofile\Desktop\files.zip",

        # [Parameter(Mandatory=$false,HelpMessage="Enter the path to ICSharpCode.SharpZipLib.dll")]
        [string]$lib=((Resolve-Path "$here\ICSharpCode.SharpZipLib.dll").Path)

        )

    Set-StrictMode -Version Latest

    

    try{
        write-verbose "Attempting to load library."
        if(-not $lib){
            write-error "Library not found" -ea Stop
        }
        ## Load the Zip library
        [void] [Reflection.Assembly]::LoadFile($lib)

    }
    catch{
        throw
    }
    
    $namespace = "ICSharpCode.SharpZipLib.Zip.{0}"

    ## Create the Zip File
    $out = $executionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($out)
    $zipFile = New-Object ($namespace -f "ZipOutputStream") ([IO.File]::Create($out))
    $zipFullName = (Resolve-Path $out).Path

    [byte[]] $buffer = New-Object byte[] 4096

    ## Go through each file in the input, adding it to the Zip file
    ## specified
    foreach($file in $input)
    {
        ## Skip the current file if it is the zip file itself
        if($file.FullName -eq $zipFullName)
        {
            continue
        }

        ## Convert the path to a relative path, if it is under the
        ## current location
        $replacePath = [Regex]::Escape( (Get-Location).Path + "\" )
        $out = ([string] $file) -replace $replacePath,""

        ## Create the zip entry, and add it to the file
        $zipEntry = New-Object ($namespace -f "ZipEntry") $out
        $zipFile.PutNextEntry($zipEntry)
        $fileStream = [IO.File]::OpenRead($file.FullName)
        [ICSharpCode.SharpZipLib.Core.StreamUtils]::Copy($fileStream, $zipFile, $buffer)
        $fileStream.Close()
    }

    # 
    ## Close the file
    $zipFile.Close()
}
