function New-GitRepoSubdirectories
{
  # % { Push-Location $_.FullName; $(git init) -and $(git add --all) -and $(git commit -m "Initial commit from script");Pop-Location }
  # Get-ChildItem | ? { $_.PSIsContainer } | % { Push-Location $_.FullName; Write-Host $_.FullName; Pop-Location }
  Get-ChildItem | ? { $_.PSIsContainer } | % { Push-Location $_.FullName; git init; Pop-Location }
  Get-ChildItem | ? { $_.PSIsContainer } | % { Push-Location $_.FullName; git add --all; Pop-Location }
  Get-ChildItem | ? { $_.PSIsContainer } | % { Push-Location $_.FullName; git commit -m "Initial commit from script"; Pop-Location }
}