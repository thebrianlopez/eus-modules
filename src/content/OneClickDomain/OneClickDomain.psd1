@{

    ModuleToProcess = 'OneClickDomain.psm1'
    ModuleVersion = '1.0'
    GUID = '41f3f676-1781-42fb-bf86-e20e393e43de'
    Author = '@StephenWThomas'
    CompanyName = 'Company'
    Copyright = '(c) 2014 Administrator. All rights reserved.'
    Description = 'Company module'
    # RequiredAssemblies = @()
    FileList = @(
        'OneClickDomain.psm1',
        'content\Invoke-OCDCreateNetwork.ps1',
        'content\Invoke-OCDCreatePDC.ps1',
        'content\Invoke-OCDCreateServers.ps1',
        'content\Invoke-OCDLaunchRDP.ps1',
        'content\Invoke-OCDMaster.ps1',
        'content\Invoke-OCDRDP.ps1',
        'content\Invoke-OCDRdpSettings.ps1',
        'content\SaveMoney_StopAll.ps1',
        'content\SpendMoney_StartAll.ps1'
        )
    FunctionsToExport = '*'
    CmdletsToExport = '*'
    VariablesToExport = '*'
    AliasesToExport = '*'
}



