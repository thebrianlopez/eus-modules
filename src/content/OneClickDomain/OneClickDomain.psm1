$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)

. "$thismoddir\content\Invoke-OCDCreateNetwork.ps1"
. "$thismoddir\content\Invoke-OCDCreatePDC.ps1"
. "$thismoddir\content\Invoke-OCDCreateServers.ps1"
. "$thismoddir\content\Invoke-OCDLaunchRDP.ps1"
. "$thismoddir\content\Invoke-OCDMaster.ps1"
. "$thismoddir\content\Invoke-OCDRDP.ps1"
. "$thismoddir\content\Invoke-OCDRdpSettings.ps1"
#. "$thismoddir\content\SaveMoney_StopAll.ps1"
#. "$thismoddir\content\SpendMoney_StartAll.ps1"


function Import-WinRMCertificate($serviceName, $vmname)
{   
    Write-verbose "Installing WinRM Certificate for remote access: $serviceName $vmname"
    $WinRMCert = (Get-AzureVM -ServiceName $serviceName -Name $vmname | select -ExpandProperty vm).DefaultWinRMCertificateThumbprint
    $AzureX509cert = Get-AzureCertificate -ServiceName $serviceName -Thumbprint $WinRMCert -ThumbprintAlgorithm sha1

    $certTempFile = [IO.Path]::GetTempFileName()
    $AzureX509cert.Data | Out-File $certTempFile

    # Target The Cert That Needs To Be Imported
    $CertToImport = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 $certTempFile

    $store = New-Object System.Security.Cryptography.X509Certificates.X509Store "Root", "LocalMachine"
    $store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite)
    $store.Add($CertToImport)
    $store.Close()
    
    Remove-Item $certTempFile
}

function New-OneClickDomain
{
    param( 
    [Parameter(Position=0,Mandatory=$false,HelpMessage="Enter an 8 character string as a base node name.")]
    [ValidateLength(0,8)][string]$basename=(((Get-AzureSubscription) | select -First 1).SubscriptionId).Substring(0,8),
    [Parameter(Mandatory=$false)][string]$serveros="Windows Server 2008 R2 SP1", # 'Windows Server 2008 R2 SP1','Windows Server 2012 R2 Datacenter'
    [Parameter(Mandatory=$false)][string]$clientos="Windows 7 Enterprise SP1 (x64)",
    [Parameter(Mandatory=$false)][string]$pdcos=$serveros,
    [Parameter(Mandatory=$false)][int]$servercount=1,
    [Parameter(Mandatory=$false)][int]$clientcount=1,
    [Parameter(Mandatory=$false)][string]$defaultStorageAccount="$basename",
    [Parameter(Mandatory=$false)][string]$affinityGroup = "$basename",
    [Parameter(Mandatory=$false)][string]$subscriptionName = (Get-AzureSubscription).SubscriptionName,
    [Parameter(Mandatory=$false)][string]$domainName = 'company.com',
    [Parameter(Mandatory=$false)][string]$domainNamePreFix = 'company',
    [Parameter(Mandatory=$false)][string]$domainAdminUserName = 'dadmin',
    [Parameter(Mandatory=$false)][string]$password = 'th3l33t3st!',
    [Parameter(Mandatory=$false)][switch]$nosleep=$false,
    [Parameter(Mandatory=$false)][string]$rdpfiles=(Get-Location)
    )

    $IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") 

    if(($IsAdmin) -eq $false){
        Write-Error "Must run elevated PowerShell to install WinRM certificates used for Remote PowerShell."
    }

    # $thismoddir=(split-path -parent $scriptpath)

    # $callstack = Get-PSCAllStack | select-object -property *
    # wsub rite-verbose "Starting $($callstack.Command[0])"
    

    if((Get-AzureSubscription).count -eq 1){
        
    }
    else{
        write-error "Please specify an azure subscription."
    }

    $operatingsystem = Get-AzureVMImage | where {$_.imagefamily -eq "$os"} | sort -property PublishedDate -Descending | select -first 1
    $clientos = (Get-AzureVMImage | where {$_.imagefamily -eq "$clientos"} | sort -property PublishedDate -Descending | select -first 1).ImageName
    $serveros = (Get-AzureVMImage | where {$_.imagefamily -eq "$serveros"} | sort -property PublishedDate -Descending | select -first 1).ImageName
    $workStorgeAccount = '<storage account used for auto config>'   # OPTIONAL UPDATE - Update with the storage location of the Helper Files.  Needs to have public access.  Example: https://<storage name>.blob.core.windows.net/<container>
    $basePathToScripts = '<path to these files>'    # OPTIONAL UPDATE - this is used if you run the scripts through an IDE not a Command Windows
    $installCU2 = 'false'                           # OPTIONAL UPDATE - Set to 'true' - Must upload BizTalk CU2 exe to the root of the Storage Account above
    $installClassicShell = 'false'                  # OPTIONAL UPDATE - Set to 'true' - Must upload classic shell exe to the root of the Storage Account above
    $installBizTalkProv = 'false'                   # OPTIONAL UPDATE - Set to 'true' - Must upload 5 files from BizTalk 2013 Dev box to the root of the Storage Account above
    $adminUserName = 'ladmin'                       # OPTIONAL UPDATE
    $sizePDC = 'Basic_A2'                             # OPTIONAL UPDATE for the Domain Controller
    $sizeSrv = 'Basic_A2'                             # OPTIONAL UPDATE for SQL Server
    $sizeCli = 'Basic_A2'                             # OPTIONAL UPDATE for the BizTalk Servers
    $virtualNetwork = "$basename"            # OPTIONAL UPDATE
    $subNetName = 'App'                             # OPTIONAL UPDATE
    $virtualNetworkConfig = "$thismoddir\content\Configs"     # OPTIONAL UPDATE
    $virtualNetworkDNSName = 'Google'               # OPTIONAL UPDATE
    $virtualNetworkDNSIP = '8.8.8.8'                # OPTIONAL UPDATE
    $dnsName = 'companydns'
    $dnsLoopBackIP = '127.0.0.1'
    $dataCenter = 'West US'

    try{
        $envobj = (Get-Content "$thismoddir\config.json") -join "`n" | convertfrom-json
    }
    catch {
        write-error "[$($_.Exception.GetType().FullName)] : $($_.Exception.Message)"
    }

    $global:pdc = @()
    $global:vms = @()

    # provision other computers next
    foreach($vm in $envobj.node){
        $vmprops = @{
            "Computername"=$vm.ComputerName + "-$basename";
            "Size"=$vm.size;
            "Image"=(Get-AzureVMImage | where {$_.imagefamily -eq "$($vm.Image)"} | sort -property PublishedDate -Descending | select -first 1).ImageName;
            "IPAddress"=$vm.IPAddress;"Role"=$vm.Role
        }
        $thisvm = New-Object -TypeName PSOBject -Property $vmprops
        write-verbose "Provisioning $($thisvm.Computername) with role $($thisvm.Role)"
        $global:vms += $thisvm
        if($thisvm.Role -eq "PDC"){
            $global:pdc += $thisvm
            $global:dnsIP = $($global:pdc.IPAddress)
        }
    }

    # Main Method
    $params = @{
        "nosleep"=$nosleep;
    }
    Invoke-OCDMaster @params

}
