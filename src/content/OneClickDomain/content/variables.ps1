<#
Written by: Stephen W Thomas
Email: me@biztalkguru.com
Twitter: @stephenwthomas
Community Site: www.BIzTalkGurus.com
Personal Site: www.BizTalkGuru.com

Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

This script is the main variable script for the entire process.  Charges should only be needed in this script.  
Look for the items at top marked as “Update”.  Only 3 values are required to be changed.  
Currently, I’m hosting the Helper files on my storage account.  
If you run into errors with storage account downloads you make need to host them yourself and update the value for $workingStorageAccount.
If you are running the start, stop, or remove scripts ensure the baseVMName is set correctly.

Change Log:
1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
1.1 (6/10/2014) - Added support for per-server sizing, VNet configuration
#>

# MUST UPDATE - BASE Name
$baseVMName = '20140805'                        # UPDATE - Must be Globally Unique. This value must be 8 characters or less and all lower case. I usually use initials and numbers.

# MUST UPDATE - Subscription Details 
$subscriptionName = 'Visual Studio Premium with MSDN'       # UPDATE - From publishing file

# Storage Account Details - This is the location of the Helper files used by the scripts.  Only needed if any Flag below is set to true
$workStorgeAccount = '<storage account used for auto config>'   # OPTIONAL UPDATE - Update with the storage location of the Helper Files.  Needs to have public access.  Example: https://<storage name>.blob.core.windows.net/<container>
$setupDir = "C:\BizTalkGurus"                   # OPTIONAL UPDATE - Used for files on the remote computer running the script and remove BizTalk, only used when a flag below is true
$basePathToScripts = '<path to these files>'    # OPTIONAL UPDATE - this is used if you run the scripts through an IDE not a Command Windows

# Flags
$installCU2 = 'false'                           # OPTIONAL UPDATE - Set to 'true' - Must upload BizTalk CU2 exe to the root of the Storage Account above
$installClassicShell = 'false'                  # OPTIONAL UPDATE - Set to 'true' - Must upload classic shell exe to the root of the Storage Account above
$installBizTalkProv = 'false'                   # OPTIONAL UPDATE - Set to 'true' - Must upload 5 files from BizTalk 2013 Dev box to the root of the Storage Account above

# Default Storage account
$defaultStorageAccount = $baseVMName + "store"

# Affinity Group Details
$affinityGroup = "$baseVMName-ag"
$dataCenter = 'West US'                         # From Get-AzureLocation

# VM Details
$adminUserName = 'ladmin'                       # OPTIONAL UPDATE
$password = 'th3l33t3st!'                       # OPTIONAL UPDATE
$sizePDC = 'Medium'                             # OPTIONAL UPDATE for the Domain Controller
$sizeSQL = 'Medium'                             # OPTIONAL UPDATE for SQL Server
$sizeBTS = 'Medium'                             # OPTIONAL UPDATE for the BizTalk Servers

$virtualNetwork = "$baseVMName-vnet"            # OPTIONAL UPDATE
$subNetName = 'App'                             # OPTIONAL UPDATE
$virtualNetworkConfig = "$pathName\Configs"     # OPTIONAL UPDATE
$virtualNetworkDNSName = 'Google'               # OPTIONAL UPDATE
$virtualNetworkDNSIP = '8.8.8.8'                # OPTIONAL UPDATE

# Domain Details
$domainName = 'company.com'
$domainNamePreFix = 'company'
$domainAdminUserName = 'dadmin'

# DNS Details
$dnsName = 'companydns'
$dnsLoopBackIP = '127.0.0.1'

# Host Names of the 2 hosts auto-created by the deployment process
# To add more hosts the MultiConfig file in the storage account needs to be changed
$hostName1 = 'TrackingHost'                    # OPTIONAL UPDATE - Host1 is set for tracking
$hostName2 = 'ReceiveHost'                     # OPTIONAL UPDATE
$hostName3 = 'SendHost'                        # OPTIONAL UPDATE
$hostName4 = 'ProcessingHost'                  # OPTIONAL UPDATE

# Image Names from Get-AzureVMImage
$imageWindows = 'bd507d3a70934695bc2128e3e5a255ba__RightImage-Windows-2012-x64-v13.5'
$imageSQLEnt = 'fb83b3509582419d99629ce476bcb5c8__Microsoft-SQL-Server-2008R2SP2-Enterprise-CY13SU04-SQL2008-SP2-10.50.4021.0'
$imageSQL12Ent = 'fb83b3509582419d99629ce476bcb5c8__SQL-Server-2012-SP1-11.0.3430.0-Enterprise-ENU-Win2012-cy14su05'
$imageBTSEnt = '2cdc6229df6344129ee553dd3499f0d3__BizTalk-Server-2013-Enterprise'
$imageWindows7='03f55de797f546a1b29d1b8d66be687a__Windows-7-Enterprise-N-x64-en.us-201405.01'
$imageWindows2008R2='a699494373c04fc0bc8f2bb1389d6106__Win2K8R2SP1-Datacenter-201407.01-en.us-127GB.vhd'
$imgWindowsServer2012R2="a699494373c04fc0bc8f2bb1389d6106__Windows-Server-2012-R2-201407.01-en.us-127GB.vhd"
$imgVS2013UltimateWS2012R2="03f55de797f546a1b29d1b8d66be687a__Visual-Studio-2013-Ultimate-Update3-AzureSDK-2.4-WS2012"
$imgVS2013ProfWS2012R2="03f55de797f546a1b29d1b8d66be687a__Visual-Studio-2013-Professional-Update3-AzureSDK-2.4-WS2012"
# Here is the more object oriented approach
$environment = @"
{
	"computers":
	[
		{
			"ComputerName":"$baseVMName-pdc",
			'Size':"Medium",
			'Image':"$imgWindowsServer2012R2",
			'IPAddress':"10.0.0.4",
			'Role':"PDC"
		},{
			"ComputerName":"$baseVMName-vs2013",
			'Size':"Medium",
			'Image':"$imgVS2013ProfWS2012R2",
			'IPAddress':"10.0.0.10"
		}
	]
}
"@

try{
	$env = convertfrom-json $environment
}
catch {
	write-error "[$($_.Exception.GetType().FullName)] : $($_.Exception.Message)"
}

$global:pdc = @()
$global:vms = @()

# provision other computers next
foreach($vm in $env.computers){
	$vmprops = @{"Computername"=$vm.ComputerName;"Size"=$vm.size;"Image"=$vm.Image;"IPAddress"=$vm.IPAddress;"Role"=$vm.Role}
	$thisvm = New-Object -TypeName PSOBject -Property $vmprops
	write-verbose "Provisioning $($thisvm.Computername) with role $($thisvm.Role)"
	$global:vms += $thisvm
	if($thisvm.Role -eq "PDC"){
		$global:pdc += $thisvm
		$global:dnsIP = $($global:pdc.IPAddress)
	}
}


