
function Start-OneClickDomain{

	$vm_up_status="ReadyRole"
	$int_azure_status_poll_time = 10 #seconds
	$int_azure_max_poll_time = 600 #seconds;
	$int_azure_current_poll_time = 0

	# Start the pdc
	Get-AzureVM | where {$_.ServiceName -like "*pdc*" -and $_.Status -ne "$vm_up_status"} | start-azurevm

	while(
		((Get-AzureVM | where {$_.ServiceName -like "*pdc*"}).Status -ne "$vm_up_status")
	){
		$n = $int_azure_status_poll_time
		write-verbose "Waiting $n seconds for PDC to be online"
		sleep $int_azure_status_poll_time
		if($n = $int_azure_max_poll_time){ write-error "Azure timed out"}
		else {$int_azure_current_poll_time = $int_azure_current_poll_time + $n}
	}


	write-verbose "PDC is up, starting other VM's"





	$start_all_vms = (Get-AzureVM | where {$_.ServiceName -notlike "*pdc*" -and $_.Status -ne "$vm_up_status"} | start-azurevm)


	while(
		((Get-AzureVM | where {$_.ServiceName -notlike "*pdc*"}).Status -ne "$vm_up_status")
	){
		$n = $int_azure_status_poll_time
		write-verbose "Waiting $n seconds for PDC to be online"
		sleep $int_azure_status_poll_time
		if($n = $int_azure_max_poll_time){ write-error "Azure timed out"}
		else {$int_azure_current_poll_time = $int_azure_current_poll_time + $n}
	}

}