function Invoke-OCDCreateServers{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false
    )


    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.com
    Twitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will create one SQL Enterprise and two BizTalk Enterprise servers
    It will join the three to the domain and disable the firewall
    For SQL, it will add the Domain Admin as a SQL Sys Admin

    WARNING - this script was only tested with a subscription that did not have any existing Virtual Networks.  It may fail if any VNets already exist.  

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    1.1 (6/10/2014) - Added support for per-server sizing
    #>

    write-verbose "Start - Create servers"

    Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccount $defaultStorageAccount -ErrorAction SilentlyContinue
    Select-AzureSubscription -SubscriptionName $subscriptionName

    # Set the DNS IP once the DC is setup and configured
    $myDNS = New-AzureDNS -Name $dnsName -IPAddress $dnsIP

    $i=1
    $items = @()
    $work = @()
    $work += $($($global:vms) | where {$_.Role -ne "PDC"})
    $boom = 1
    # Provision all normal virtual machines

    foreach($item in $work){
        write-verbose "Provisioning vm $i of $($work.count)"
        $thisvm = $item
        $objvm = New-AzureVMConfig -name $thisvm.ComputerName -InstanceSize $thisvm.Size -ImageName $thisvm.Image `
            | Add-AzureProvisioningConfig -WindowsDomain -AdminUsername $adminUserName  -Password $password -Domain $domainNamePreFix -DomainPassword $password -DomainUserName $domainAdminUserName -JoinDomain $domainName `
            | Set-AzureSubnet -SubnetNames $subNetName `
            | Set-AzureStaticVNetIP -IPAddress $thisvm.IPAddress

        New-AzureVM -ServiceName $thisvm.ComputerName -AffinityGroup $affinityGroup -VMs $objvm -DnsSettings $myDNS -VNetName $virtualNetwork -WaitForBoot -ErrorAction SilentlyContinue | out-null

        # Using generated certs – use helper function to download and install generated cert.
        Import-WinRMCertificate $thisvm.ComputerName $thisvm.ComputerName
        

        $i += 1

    }

    write-verbose "End - Create servers"

}