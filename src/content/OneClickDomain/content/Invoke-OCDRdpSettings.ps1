﻿function Invoke-OCDRdpSettings{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false
    )
    
    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.com
    Twitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will prep auto configure the new domain using remote PowerShell and tasks on the BizTalk Servers.

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    #>
    write-verbose "Start - Setup variables"

    Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccount $defaultStorageAccount -ErrorAction SilentlyContinue
    Select-AzureSubscription -SubscriptionName $subscriptionName

    # Create the credentialss for Remote PowerShell
    # Note we are using the Domain Admin
    $strPassword = ConvertTo-SecureString $password -AsPlainText -Force
    $credentials = New-Object System.Management.Automation.PScredential ($adminUserName, $strPassword)
    $credentialsDomain = New-Object System.Management.Automation.PScredential ("$domainNamePreFix\$domainAdminUserName", $strPassword)

    # Configure all virtual machines
    foreach($item in $($global:vms)){
        $thisvm = $item
        $uri = Get-AzureWinRMUri -ServiceName $thisvm.ComputerName -Name $thisvm.ComputerName
        
        Import-WinRMCertificate $thisvm.ComputerName $thisvm.ComputerName

        
        $computername = $thisvm.ComputerName
        write-verbose "Configuring $($thisvm.computername) with creds [$credentialsDomain] and keyword [$strPassword]"

        # Moving Remote PowerShell calls to the end to see if they run better - getting random errors
        # Moving the SQL Remote Powershell to the end of the file since it sometimes does not work right away
        write-verbose "Disabling the NLA component of RDP to allow connectivity from non-Windows operating systems."

        Invoke-Command -ConnectionUri $uri.ToString() -credential $credentialsDomain -ScriptBlock {
            
            (Get-WmiObject -class "Win32_TSGeneralSetting" -Namespace root\cimv2\terminalservices -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0) | out-null

        }


       

        write-verbose "Successfully configured $($thisvm.computername)"
    }

    write-verbose "Downloading all RDP files to $rdpfiles"
    
    foreach($item in $($global:vms)){

        Get-AzureRemoteDesktopFile -ServiceName $item.ComputerName -Name $item.ComputerName -LocalPath "$rdpfiles\$($item.ComputerName).rdp"

    }

    write-verbose "Done downloading all rdp files"
}