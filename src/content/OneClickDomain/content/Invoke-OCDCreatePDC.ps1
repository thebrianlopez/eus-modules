function Invoke-OCDCreatePDC{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false
    )
    
    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.com
    Twitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will create a new VM using the Windows 2012 Image from the variables file.
    It will install ADDS and create a new domain.
    It also creates all the BizTalk users and adds the Domain Admin as a BizTalk and SSO Admin.

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    #>

    write-verbose "Start - creating the PDC"
    Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccount $defaultStorageAccount -ErrorAction SilentlyContinue
    Select-AzureSubscription -SubscriptionName $subscriptionName

    # User Loopback
    $myDNS = New-AzureDNS -Name $dnsName -IPAddress $dnsLoopBackIP

    $strPassword = ConvertTo-SecureString $password -AsPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential ($domainAdminUserName, $strPassword)

    # provision primary domain controllers first
    foreach($item in $($global:pdc)){

        write-verbose "Creating the primary domain controller with DNS ip addrres as $($global:pdc.IpAddress). Total $($global:pdc.count)"
        $thisvm = $item

        # if((Get-AzureVM | where {$_.Name -eq "$($thisvm.ComputerName)" -and $_.Status -eq "ReadyRole"})){
            # write-verbose "Vm already exists."
        #}
        #else{

            $objvm = New-AzureVMConfig -name $thisvm.ComputerName -InstanceSize $thisvm.Size -ImageName $thisvm.Image `
                | Add-AzureProvisioningConfig -Windows -AdminUsername $domainAdminUserName -Password $password `
                | Set-AzureSubnet -SubnetNames $subNetName `
                | Set-AzureStaticVNetIP -IPAddress $thisvm.IPAddress
            
            New-AzureVM -ServiceName $thisvm.ComputerName -AffinityGroup $affinityGroup -VMs $objvm -DnsSettings $myDNS -VNetName $virtualNetwork -WaitForBoot -ErrorAction SilentlyContinue | out-null
            
            # Confirm ready role status before running winrm
            if(-not (Get-AzureVM | where {$_.Name -eq "$($thisvm.ComputerName)" -and $_.Status -eq "ReadyRole"})){
                write-verbose "Waiting for provision to complete."
                if(-not($nosleep)){Start-Sleep -s 60}
            }
            if(-not($nosleep)){Start-Sleep -s 240}
            write-verbose "ComputerName: $($thisvm.ComputerName)"
            write-verbose "ServiceName: $($thisvm.ComputerName)"
            # Get the RemotePS/WinRM Uri to connect to
            $uri = Get-AzureWinRMUri -ServiceName $($thisvm.ComputerName) -Name $($thisvm.ComputerName)
         
            # Using generated certs � use helper function to download and install generated cert.
            Import-WinRMCertificate $thisvm.ComputerName $thisvm.ComputerName
         
            write-verbose "Start - AD Install"

            # test connection to see if machine is alrady on this domain
            while (-not (Invoke-Command -ErrorAction SilentlyContinue -ConnectionUri $uri -Credential $credential -ScriptBlock { if((1+1) -eq 2) { return $true } })) {
                write-verbose "Wating for winrm connection to become availible before moving on ..."
                Start-Sleep -s 15
            }

            Invoke-Command -ConnectionUri $uri.ToString() -Credential $credential -ScriptBlock {
                param($domainName, $password)
                $strPassword = ConvertTo-SecureString $password -AsPlainText -Force
                Install-windowsfeature -name AD-Domain-Services -IncludeManagementTools | out-null
                Install-ADDSForest -SkipPreChecks -DomainName $domainName -SafeModeAdministratorPassword $strPassword -Force -ErrorAction SilentlyContinue | out-null
            } -ArgumentList $domainName, $password
            write-verbose "End - AD Install"

            # Wait for the services to start
            if(-not($nosleep)){Start-Sleep -s 240}

            # test connection to see if machine is alrady on this domain
            while (-not (Invoke-Command -ErrorAction SilentlyContinue -ConnectionUri $uri -Credential $credential -ScriptBlock { if((1+1) -eq 2) { return $true } })) {
                write-verbose "Wating for winrm connection to become availible before moving on ..."
                Start-Sleep -s 15
            }

            Invoke-Command -ConnectionUri $uri.ToString() -Credential $credential -ScriptBlock {
                param($password, $domainAdminUserName)
                $strPassword = ConvertTo-SecureString $password -AsPlainText -Force

                # Wait for ADWS to start just in case
                # Sometimes this gives a thread error
                while ((get-service -Name 'ADWS').Status -ne "Running") {
                    write-verbose "Waiting for ADWS to start ..."
                    if(-not($nosleep)){Start-Sleep -s 15}
                }
            
                # These domain groups are hard coded inside the Multi Server Configuration file
                # Changes here will break the auto config process
                New-ADGroup -groupscope Global -Name "CASSQLServerAdmins"
                New-ADGroup -groupscope Global -Name "CASServerAdmins"
                New-ADGroup -groupscope Global -Name "CASWorkstationAdmins"
                New-ADGroup -groupscope Global -Name "CASReportingUsers"
                New-ADGroup -groupscope Global -Name "BldServerAdmins"
                New-ADGroup -groupscope Global -Name "BldServerUsers"
                New-ADGroup -groupscope Global -Name "BldServerReporting"
                New-ADUser -ea SilentlyContinue -SamAccountName "CASServerService" -AccountPassword $strPassword -name "CASServerService" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "CASSQLService" -AccountPassword $strPassword -name "CASSQLService" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "CASNetworkAccess" -AccountPassword $strPassword -name "CASNetworkAccess" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "CASReporting" -AccountPassword $strPassword -name "CASReporting" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "CASConsoleUsers" -AccountPassword $strPassword -name "CASConsoleUsers" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "EUCWebService" -AccountPassword $strPassword -name "EUCWebService" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "BldServerAgent" -AccountPassword $strPassword -name "BldServerAgent" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                New-ADUser -ea SilentlyContinue -SamAccountName "BldServerService" -AccountPassword $strPassword -name "BldServerService" -enabled $true -PasswordNeverExpires $true -CannotChangePassword $true  -ChangePasswordAtLogon $false
                Add-ADPrincipalGroupMembership -ea SilentlyContinue -Identity "BldServerAgent" -MemberOf "BldServerAdmins"
                Add-ADPrincipalGroupMembership -ea SilentlyContinue -Identity "BldServerService" -MemberOf "BldServerAdmins"
                
                # $de = [ADSI]"WinNT://localhost/Administrators","group" 
                # $de.psbase.Invoke("Add",([ADSI]"WinNT://$domainName/BldServerAdmins").path)
            } -ArgumentList $password, $domainAdminUserName

        #}
        
        write-verbose "End - User / Domain Group setup"
    }

}