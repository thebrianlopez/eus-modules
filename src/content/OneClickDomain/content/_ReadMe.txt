True One-click End-to-End BizTalk Domain Setup
Script by Stephen W. Thomas - www.BizTalkGuru.com
Get the latest scripts at www.BizTalkGurus.com
Version 1.0.0 
June 4th, 2014

NOTICE � you will be charged inside Microsoft Azure when using these scripts to create a VM.  
Please understand the charges before you run these scripts.
Script are provided AS-IS without any warranty or guarantee.'

Updates are needed to variables.ps1 before running any scripts.

See this post for more details: http://www.biztalkgurus.com/r.ashx?L

Change Log:
1.0 - (6/2/2014) Initial release
1.1 - (6/10/2014) Better VNet support, per VM size config, log off when done with auto config

