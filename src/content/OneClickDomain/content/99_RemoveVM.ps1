﻿function Remove-OneClickDomain{

    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.com
    Twitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will remove the VM's created, try to remove the disks, storage accounts, and VNet.  
    This is a hard delete.
    If you just want to STOP the VMs use the SaveMoney script to turn off the VMs.

    WARNING - this script was only tested with a subscription that did not have any existing Virtual Networks.  It may fail if any VNets already exist.  

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    #>


    # This needs to be set if running from and IDE otherwise the script will determine the correct path
    $pathForIDERun = ""

    if($MyInvocation.MyCommand.Path -eq $null) { $pathName = $pathForIDERun } else {$pathName = Split-Path $MyInvocation.MyCommand.Path -ErrorAction SilentlyContinue}
    "Script Path is " + $pathName

    . $pathName\variables.ps1

    Set-AzureSubscription -SubscriptionName $subscriptionName
    Select-AzureSubscription -SubscriptionName $subscriptionName

    Remove-AzureService -ServiceName $vmnameBTS02 -DeleteAll -Force -ErrorAction SilentlyContinue
    Remove-AzureService -ServiceName $vmnameBTS01 -DeleteAll -Force -ErrorAction SilentlyContinue
    Remove-AzureService -ServiceName $vmnameSQL01 -DeleteAll -Force -ErrorAction SilentlyContinue
    Remove-AzureService -ServiceName $vmnamePDC -DeleteAll -Force -ErrorAction SilentlyContinue

    # Remove the Storage Account
    $hasStorageAccount = Get-AzureStorageAccount | Where-Object {$_.StorageAccountName.contains("$defaultStorageAccount")}

    # Remove the storage account
    do {
        "Waiting to try to remove the storage account"
        Start-Sleep -s 30
        Remove-AzureStorageAccount -StorageAccountName $defaultStorageAccount -ErrorAction SilentlyContinue
        $hasStorageAccount = Get-AzureStorageAccount | Where-Object {$_.StorageAccountName.contains("$defaultStorageAccount")}
    } while($hasStorageAccount)

    # Remote the Virtual Network
    Remove-AzureVNetConfig -ErrorAction Continue

    # Give is a few seconds
    Start-Sleep -s 60

    # Remove the Affinity Group
    Remove-AzureAffinityGroup -Name $affinityGroup -ErrorAction Continue

    # Remove Director with RDP files
    if(Test-Path -Path "$pathName\RemoteDesktop"){
        Remove-Item "$pathName\RemoteDesktop" -Force -Recurse 
    }

    #Get-AzureVM -ServiceName $vmnamePDC -Name $vmnamePDC | Get-AzureStaticVNetIP
    #Get-AzureVM -ServiceName $vmnamePDC -Name $vmnamePDC | Get-AzureStaticVNetIP
    #Test-AzureStaticVNetIP -VNetName $virtualNetwork -IPAddress 10.0.0.4

}