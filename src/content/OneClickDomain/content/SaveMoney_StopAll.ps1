﻿<#
Written by: Stephen W Thomas
Email: me@biztalkguru.com
Twitter: @stephenwthomas
Community Site: www.BIzTalkGurus.com
Personal Site: www.BizTalkGuru.com

Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

This script will stop and deallocate the four VMs created in this process using the name provided in the variables file.

Change Log:
1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
#>

# This needs to be set if running from an IDE otherwise the script will determine the correct path
$pathForIDERun = ""

if($MyInvocation.MyCommand.Path -eq $null) { $pathName = $pathForIDERun } else {$pathName = Split-Path $MyInvocation.MyCommand.Path -ErrorAction SilentlyContinue}
"Script Path is " + $pathName

. $pathName\variables.ps1

Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccountName $defaultStorageAccount -ErrorAction SilentlyContinue
Select-AzureSubscription -SubscriptionName $subscriptionName

Stop-AzureVM -Name $vmnameBTS02 -ServiceName $vmnameBTS02 -Force -ErrorAction Continue
Stop-AzureVM -Name $vmnameBTS01 -ServiceName $vmnameBTS01 -Force -ErrorAction Continue
Stop-AzureVM -Name $vmnameSQL01 -ServiceName $vmnameSQL01 -Force -ErrorAction Continue
Stop-AzureVM -Name $vmnamePDC -ServiceName $vmnamePDC -Force -ErrorAction Continue

