﻿function Invoke-OCDMaster{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false,
    [Parameter(Mandatory=$false)][int]$version=0
    )


<#
Written by: Stephen W Thomas
Email: me@biztalkguru.com
Twitter: @stephenwthomas
Community Site: www.BIzTalkGurus.com
Personal Site: www.BizTalkGuru.com

Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

This is the Master Script.  Run from an elevated command promote.  Ensure all updates are make to variables.ps1 beforehand.
Also ensure your environment is correctly working with Azure.  I test with Get-AzureLocation.  It should return values.

Change Log:
1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
#>

Invoke-OCDCreateNetwork

# Give it a few minutes or so - things in Azure IaaS sometimes need to settle a bit
write-verbose "Sleeping for 90 seconds to let things in Azure IaaS settle"
if(-not($nosleep)){Start-Sleep -s 90}


Invoke-OCDCreatePDC


# Give it a few minutes or so - things in Azure IaaS sometimes need to settle a bit
write-verbose "Sleeping for 90 seconds to let things in Azure IaaS settle"
if(-not($nosleep)){Start-Sleep -s 90}

Invoke-OCDCreateServers


# Give it a few minutes or so - things in Azure IaaS sometimes need to settle a bit
write-verbose "Sleeping for 240 seconds to let things in Azure IaaS settle"
if(-not($nosleep)){Start-Sleep -s 240}


Invoke-OCDRdpSettings


write-verbose "Start - Master Script" # This script will try to configure BizTalk using the settings provided.  




# Only needed if Auto Configure is enabled
# To enable Auto Configure the updated BizTalk Provisioning tools are needed from a Azure BizTalk 2013 Dev VM
<#
if($installBizTalkProv -eq "true")
{
    write-verbose "Start - Configure servers" # This script will try to configure BizTalk using the settings provided.  
    . $pathName\04_Configure_Servers_LaunchRDP.ps1
    write-verbose "Endt - Configure servers"
}

write-verbose "Start - Get RDP Files"
. $pathName\05_RemoteDesktop.ps1
write-verbose "Endt - Get RDP Files"
#>

#write-verbose "You can login to each server with the Domain Admin account: $domainNamePreFix\$domainAdminUserName and Password: $password"

}
