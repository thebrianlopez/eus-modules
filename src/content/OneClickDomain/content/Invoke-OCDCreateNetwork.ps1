function Invoke-OCDCreateNetwork{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false
    )
    
    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.com
    Twitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will create vNetwork, Affinity Group, and Storage account.  If these already exist this script can be skipped.

    WARNING - this script was only tested with a subscription that did not have any existing Virtual Networks.  It may fail if any VNets already exist.  

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    1.1 (6/10/2014) - Support for subscriptions that already have a VNet
    #>
    
    # Check if there is one azure account assiciated here
    $mainacct = (Get-AzureSubscription | sort -descending $_.NotBefore | select -first 1)

    if($mainacct){ 
        write-verbose "Found Azure subscription $mainacct $($mainacct.SubscriptionName) with id $($mainacct.SubscriptionId)"
    }
    else{
        write-error "Unable to find valid windows Azure account. Please add one." -ErrorAction Stop
    }

    # Cheack if the Affinity Group Exists if not create it
    if(!(Get-AzureAffinityGroup -Name $affinityGroup -ErrorAction SilentlyContinue))
    {
        write-verbose "Creating Affinity Group $affinityGroup"
        # Create Addinity Group 
        New-AzureAffinityGroup -Name $affinityGroup -Location $dataCenter -ErrorAction Stop | out-null
        write-verbose "Done Creating Affinity Group $affinityGroup"
    }
    
    # Cheack if the Storage Account Exists if not create it
    
    if(!(Get-AzureStorageAccount -StorageAccountName $defaultStorageAccount -ErrorAction SilentlyContinue))
    {
        write-verbose "Creating Storage Account $defaultStorageAccount"
        New-AzureStorageAccount -StorageAccountName $defaultStorageAccount -Label "Development Storage" -AffinityGroup $affinityGroup -ErrorAction Stop | out-null
        Set-AzureStorageAccount -StorageAccountName $defaultStorageAccount -GeoReplicationEnabled $false -ErrorAction Stop | out-null
        write-verbose "Done Creating Storage Account $defaultStorageAccount"
    }

    # Check is the subscription already has a VNet 
    $hasVNetAlready = Get-AzureVNetSite

    # Check is the subscription has this specific VNet
    $hasThisVNetAlready = Get-AzureVNetSite | Where-Object {$_.Name.contains("$virtualNetwork")}

    # Cheack if the Virtual Network Exists if not create it
    if(!($hasThisVNetAlready))
    {
        if(!($hasVNetAlready))
        {
            . "$thismoddir\content\Invoke-OCDRdpSettings.ps1"
            write-verbose "Creating Virtual Network $virtualNetwork"
            write-verbose "Config file location $virtualNetworkConfig\NetworkConfig.xml"
            # Update the config file
            $configFile = Get-Content("$virtualNetworkConfig\NetworkConfig.xml")
            $configFile = $configFile.Replace("VNETNAME", $virtualNetwork);
            $configFile = $configFile.Replace("AFFINITYNAME", $affinityGroup);
            $configFile = $configFile.Replace("SUBNETNAME", $subNetName);
            $configFile | out-file "$virtualNetworkConfig\NetworkConfig_new.netcfg"
            Set-AzureVNetConfig -ConfigurationPath $virtualNetworkConfig\NetworkConfig_new.netcfg -ErrorAction Stop | out-null
            write-verbose "Done Creating Virtual Network $virtualNetwork"    
        }
        else
        {
            # If a VNet exists, add a new one.  Sure wish PowerShell did this better.
            # From http://blogs.blackmarble.co.uk/blogs/rhepworth/post/2014/03/03/Creating-Azure-Virtual-Networks-using-Powershell-and-XML.aspx
            # Get current config
            $currentVNetConfig = get-AzureVNetConfig
            [xml]$workingVnetConfig = $currentVNetConfig.XMLConfiguration   

            $dnsExists = $workingVnetConfig.GetElementsByTagName("DnsServer") | where {$_.name -eq $virtualNetworkDNSName}
            
            if ($dnsExists.Count -ne 0)
            {    
                write-verbose "DNS Server $virtualNetworkDNSName already exists"    
            }
            else
            {
                $workingNode = $workingVnetConfig.GetElementsByTagName("Dns") 
                
                $dnsServers = $workingVnetConfig.GetElementsByTagName("DnsServers")
                
                if ($dnsServers.count -eq 0)
                {
                    $newDnsServers = $workingVnetConfig.CreateElement("DnsServers","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
                    $dnsServers = $workingNode.appendchild($newDnsServers)
                } 
                
                $singleDNS = $workingVnetConfig.GetElementsByTagName("DnsServers")
                $addDNS = $workingVnetConfig.CreateElement("DnsServer","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
                $addDNS.SetAttribute("name",$virtualNetworkDNSName) 
                $addDNS.SetAttribute("IPAddress",$virtualNetworkDNSIP) 
                $DNS = $singleDNS.appendchild($addDNS)
            }    

            $virtNetCfg = $workingVnetConfig.GetElementsByTagName("VirtualNetworkSites")

            $newNetwork = $workingVnetConfig.CreateElement("VirtualNetworkSite","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $newNetwork.SetAttribute("name",$virtualNetwork) 
            $newNetwork.SetAttribute("AffinityGroup",$affinityGroup) 
            $Network = $virtNetCfg.appendchild($newNetwork)

            $newAddressSpace = $workingVnetConfig.CreateElement("AddressSpace","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $AddressSpace = $Network.appendchild($newAddressSpace) 
            $newAddressPrefix = $workingVnetConfig.CreateElement("AddressPrefix","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $newAddressPrefix.InnerText="10.0.0.0/8"
            $AddressSpace.appendchild($newAddressPrefix)

            $newSubnets = $workingVnetConfig.CreateElement("Subnets","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $Subnets = $Network.appendchild($newSubnets) 
            $newSubnet = $workingVnetConfig.CreateElement("Subnet","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $newSubnet.SetAttribute("name", $subNetName) 
            $Subnet = $Subnets.appendchild($newSubnet) 
            $newAddressPrefix = $workingVnetConfig.CreateElement("AddressPrefix","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration")
            $newAddressPrefix.InnerText="10.0.0.0/11"
            $Subnet.appendchild($newAddressPrefix)

            $newItemDNS = $workingVnetConfig.CreateElement("DnsServersRef","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $ItemDNS = $Network.appendchild($newItemDNS) 
            $newItemSingleDNS = $workingVnetConfig.CreateElement("DnsServerRef","http://schemas.microsoft.com/ServiceHosting/2011/07/NetworkConfiguration") 
            $newItemSingleDNS.SetAttribute("name", $virtualNetworkDNSName) 
            $ItemDNS.appendchild($newItemSingleDNS) 

            $tempFileName = "$virtualNetworkConfig\NetworkConfig_new.netcfg"
            $workingVnetConfig.save($tempFileName)
      
            set-AzureVNetConfig -configurationpath $tempFileName -ErrorAction Stop | out-null
        }
    }

}