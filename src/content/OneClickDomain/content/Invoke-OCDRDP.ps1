﻿function Invoke-OCDRDP{
    param( 
    [Parameter(Mandatory=$false)][switch]$nosleep=$false
    )
    
    <#
    Written by: Stephen W Thomas
    Email: me@biztalkguru.comTwitter: @stephenwthomas
    Community Site: www.BIzTalkGurus.com
    Personal Site: www.BizTalkGuru.com

    Content provided as-is.  Use at your own risk.  Charges may/will apply when creating VMs inside Windows Azure. 

    This script will download all the Remote Desktop clients for the new domain.

    WARNING - this script was only tested with a subscription that did not have any existing Virtual Networks.  It may fail if any VNets already exist.  

    Change Log:
    1.0 (6/2/2014) - Initial release (Tested with May 2014 PowerShell Commands - older versions may not work)
    #>
    "Start - Setup variables"

    Set-AzureSubscription -SubscriptionName $subscriptionName -CurrentStorageAccount $defaultStorageAccount -ErrorAction SilentlyContinue
    Select-AzureSubscription -SubscriptionName $subscriptionName

    # Set path
    if($MyInvocation.MyCommand.Path -eq $null) { $pathName = $pathForIDERun } else {$pathName = Split-Path $MyInvocation.MyCommand.Path -ErrorAction SilentlyContinue}

    # Make Director
    if(!(Test-Path -Path "$pathName\RemoteDesktop" )){
        New-Item -ItemType directory -Path "$pathName\RemoteDesktop"
    }

    "End - Setup variables"
    "Start - Generate Remote Desktop Files"

    # Get the RDP files
    Get-AzureRemoteDesktopFile -ServiceName $vmnamePDC -Name $vmnamePDC -LocalPath "$thismoddir\RemoteDesktop\$vmnamePDC.rdp"
    Get-AzureRemoteDesktopFile -ServiceName $vmnameSQL01 -Name $vmnameSQL01 -LocalPath "$thismoddir\$vmnameSQL01.rdp"
    Get-AzureRemoteDesktopFile -ServiceName $vmnameBTS01 -Name $vmnameBTS01 -LocalPath "$thismoddir\$vmnameBTS01.rdp"
    Get-AzureRemoteDesktopFile -ServiceName $vmnameBTS02 -Name $vmnameBTS02 -LocalPath "$thismoddir\$vmnameBTS02.rdp"

    "End - Generate Remote Desktop Files"
}