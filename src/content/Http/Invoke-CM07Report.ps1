set-alias wr write-verbose

function Invoke-CM07Report{
	param(
		[parameter()][Alias("i")][string]$uri,
		[parameter()][Alias("o")][string]$outfile
	)
	$result = $false
	wr "Starting function"

    wr "Step 1: Parse URL and create connection"
    wr "Step 2: Breakdown url delimted by / and ? and ="
    wr "Step 3: Extract page html into a generic powershell object"
    wr "Step 4: Confirm http status code is correct or catch error depending on status code, such as 200, 404, 500, etc"
    wr "Step 5: Export geenric powershell object to html, txt, json, datatable (ftw)"
	wr "Ending function"
	return $result
}


$params = @{
    "i"="http://usphxm0100.nestle.com:4880/SMSReporting_AMS/Report.asp?ReportID=298&UpdateSourceID=%7BC9BB3A37-DC2B-45B0-8C17-1AC05207AC2A%7D&CollID=AMS004BF";
    "o"=".\cm07-report.html";
}
Invoke-CM07Report @params