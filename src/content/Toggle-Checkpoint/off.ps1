Import-Module .\DeviceManagement.psd1 -verbose
Get-Device | where {$_.Name -like "*Check*"} | disable-Device
net stop TracSrvWrapper
Get-Service | where {$_.Name -like "*TracSrvWrapper*"}