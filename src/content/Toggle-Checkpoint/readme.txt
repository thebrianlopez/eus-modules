Toggle-Checkpoint


Usage:

== Disable Checkpoint ==
1. Right-click 'disable.cmd' and select 'Run as Administrator' -- Authenticate as needed
2. Verify command window does not show any errors



== Enable Checkpoint ==
1. Right-click 'enable.cmd' and select 'Run as Administrator' -- Authenticate as needed
2. Verify command window does not show any errors


