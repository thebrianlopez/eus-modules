Import-Module .\DeviceManagement.psd1 -verbose
Get-Device | where {$_.Name -like "*Check*"} | enable-Device
net start TracSrvWrapper
Get-Service | where {$_.Name -like "*TracSrvWrapper*"}