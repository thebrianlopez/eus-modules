$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)

. "$thismoddir\content\Import-CMGopher.ps1"
. "$thismoddir\content\Update-TftpRoot.ps1"
. "$thismoddir\content\New-CMBootDisk.ps1"
. "$thismoddir\content\Test-CustomCMBuildPackages.ps1"
. "$thismoddir\content\Export-PXEBootImage.ps1"

function Invoke-RobocopyUNCToLocal
{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$false)][PSCredential]$cred=$false,
        [Parameter(Mandatory=$true)][string]$srcuncpath,
        [Parameter(Mandatory=$true)][string]$dst
    )
    $exitcode = $false
    $parent = (split-path -parent $srcuncpath)

    write-verbose "Copying $src to the unc path $unc using the credentials $cred"
    write-debug "Step 1: Source path will always be the unc path."
    write-debug "Step 2: Testing if path already exists, else remap and rerun function"
    write-debug "Step 3: Testing if path already exists"
    write-debug "Step 4: Start Robocopy process"

    if(test-path -path $dst){
        write-debug "Destination already exists"
    }
    else {
        mkdir $dstpath
    }

    if(test-path -path $srcuncpath){
        write-verbose "Source already exists"
    }
    else{
        write-verbose "Path doesnt exist, will remap drive"
        new-psdrive -Name "TSExport$n" -psprovider filesystem -root "$parent" -credential $cred

        if(test-path -path $srcuncpath){
            write-verbose "Drive mapped"
        }
        else{
            write-error "Drive failed to map"
        }
    }

    if(test-path -path $parent){
        write-verbose "Able to locate path $parent"

        $cmd = "$env:systemroot\system32\robocopy.exe"
        $arg = " /v /e /z `"$srcuncpath`" `"$dst`" /w:1 /r:1 "
        $newprocess = [Diagnostics.Process]
        $newprocess = new-object System.Diagnostics.Process
        $newprocess.StartInfo.FileName = $cmd
        $newprocess.StartInfo.Arguments = $arg
        #$newprocess.StartInfo.RedirectStandardOutput = $true
        #$newprocess.StartInfo.UseShellExecute = $true

        if ($pscmdlet.ShouldProcess($newprocess)) {
            write-verbose "Starting copy operation"
            try{
                $newprocess.Start()
                $exitcode = $newprocess.WaitforExit()
            }
            catch [Exception]{
                write-error "Process exited with code $($_.Exception.Message)"
            }
        }
        else {
            write-verbose "Not starting copy operation as we are using the -whatif switch"
        }
    }
    else{
        write-error "Unable to locate source directory, $($_.Exception.Message)"
    }

    return $exitcode
}
function Get-GenericCimInstance
{
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$false)][Alias("cred")][PSCredential]$credential,
    [Parameter(Mandatory=$false)][string]$filter=$false,
    [Parameter(Mandatory=$false)][string]$classname=$false,
    [Parameter(Mandatory=$false)][Alias("cas")][string]$computername,
    [Parameter(Mandatory=$false)][switch]$uselegacywmi=$false,
    [Parameter(Mandatory=$false)]$CimSession,
    [Parameter(Mandatory=$false)][string]$namespace="root\cimv2"
  )
  <#
  $params = @{
      "filter"="PackageID='$srctspackageid'";
      "classname"="SMS_TaskSequenceReferencesInfo";
      "computername"=$srccas;
      "credential"=$srccred;
      "uselegacywmi"=$uselegacywmi;
      "CimSession"=(Get-CMCimSession -cas "$srccas" -cred $srccred);
    } 
  #>

  # write-host "typename = $(($CimSession | gm | select typename -first 1).TypeName)"
  # write-host "uselegacywmi=  $uselegacywmi "
  
  
  $results = 0
  
  $conxparams = @{
    "ComputerName"="$computername";
    "erroraction"="Stop";
    "credential"=$credential;
  }

  $instparams = @{
      "Namespace"="$namespace";
  }

  
  
  if($classname -ne $false){$instparams.Add("ClassName",$classname)}
  if($filter -ne $false){$instparams.Add("Filter",$filter)}

  try {

    if($uselegacywmi){

      write-debug "Using old skool wmi"
      $results = Get-WMIObject @conxparams @instparams 

    }
    else{
      write-debug "Using wsman"
      $so = New-CimSessionOption -Protocol DCOM      
      $connection = New-CimSession @conxparams -SessionOption $so

      switch($($CimSession.CimSession)){
        0 {$connection = New-CimSession -computername $computername -erroraction "Stop" -SessionOption $so -credential $credential}
        1 {$connection = $CimSession.CimSession }
      }

      $results = Get-CimInstance -CimSession $connection @instparams # 01 - get the cm site code
    }
  }
  catch{
    write-debug $_.Exception.Message
  }
  write-debug "Completed function 'Get-GenericCimInstance' "
  return $results
}
function Get-CMCimSession
{
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
    [PSCredential]$credential,
    [Parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)][Alias("cas")]
    [string]$computername=($env:computername),
    [Parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
    [switch]$uselegacywmi
  )

  $sitecode = ""
  # 1st test to see if the root\sms namespace even exists else abort

  if($uselegacywmi){
    write-debug "Using old skool wmi"
    $wbemlocator = Get-WMIObject -computername $computername -erroraction "Stop" -credential $credential -Namespace "root\sms" -ClassName "SMS_ProviderLocation" # 01 - get the cm site code
  }
  else{
    write-debug "Using wsman"
    $so = New-CimSessionOption -Protocol DCOM
    $connection = New-CimSession -computername "$computername" -erroraction "Stop" -SessionOption $so -credential $credential
    $wbemlocator = Get-CimInstance -CimSession $connection -Namespace "root\sms" -ClassName "SMS_ProviderLocation" -errorAction "Continue" # 01 - get the cm site code
  }

  if($($wbemlocator.ProviderForLocalSite)) {
    $sitecode = $wbemlocator.SiteCode
    write-verbose "Successfully connected to $sitecode"
  }
  else {
    write-error "Unable to determine site code"
  }

  $results = @{
    "CimSession"=[Microsoft.Management.Infrastructure.CimSession]$connection;
    "Namespace"="root\sms\site_$sitecode";
  }


  return $results
}

function Invoke-CMGopherConfigExport
{
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$false)][PSCredential]$cred,
    [Parameter(Mandatory=$false)][string]$cas,
    [Parameter(Mandatory=$false)][string]$tsid
  )

  $n = 0
  $hPackageInfo = @()
  $envparams = Get-CMCimSession -cas "$cas" -cred $cred
  $params = @{
    "Filter"="PackageID='$tsid'";
    "Classname"="SMS_TaskSequenceReferencesInfo"
  }
    
  foreach ($refpackage in (Get-CimInstance @params @envparams)){
        
    switch ($($refpackage.ReferencePackageType)){
      0 {$tso=$(Get-CimInstance @envparams -classname "SMS_Package" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
      3 {$tso=$(Get-CimInstance @envparams -classname "SMS_DriverPackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
      257 {$tso=$(Get-CimInstance @envparams -classname "SMS_ImagePackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
      258 {$tso=$(Get-CimInstance @envparams -classname "SMS_BootImagePackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
    }
      
    $props = @{
      'PackageName'=$refpackage.ReferenceName;
      'PackageVersion'=$refpackage.ReferenceVersion;
      'PackageManufacturer'=$tso.Manufacturer;
      'Index'=$n;
      'SourcePackageID'=$refpackage.ReferencePackageID;
      'DestinationPackageID'='';
      'PackageType'=$refpackage.ReferencePackageType;
      'PkgSourcePath'=$tso.PkgSourcePath;
    }

    write-verbose "This package $($refpackage.ReferenceName) is of type $($refpackage.ReferencePackageType) with path $($tso.PkgSourcePath) "
        
    $hPackageInfo += (New-Object -TypeName PSOBject -Property $props)
    $n = $n + 1
  }
  return $hPackageInfo
}

function Invoke-CMGopher
{
  <# 
  .SYNOPSIS 
  CM Gopher is your best friend! CM Gopher will:

  1. Take a look at your CM12 CAS server, query all task sequences availible, list all reference packages, and will export all settings to a .json file
  2. Next CM Gopher will then import all of the packages.json file settings into a new evironment -- NEATTTO !

  .DESCRIPTION 
  Not responsible for data loss, loss of hair, decreased appetite, increased despair and sadness.

  .INPUTS 
  String

  .OUTPUTS 
  String

  .EXAMPLE 

  // This is an export cmd to dump all ts info into a package.config file
  EUCPkgMgr -export -tasksequencezip build.zip -cas "hqaaam0300.nestle.w2k" -config package.config

  // This is the import command
  EUCPkgMgr -import -tasksequencezip build.zip -cas "hqbusm9200.nestle.w2k" -config package.config

  .NOTES 
  Initial Creation
  .LINK 
  http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$false)][Alias("credential")][PSCredential]$srccred,
    [Parameter(Mandatory=$false)][PSCredential]$dstcred,
    [Parameter(Mandatory=$true)][Alias("o")][string]$config=$false,
    [Parameter(Mandatory=$false)][Alias("cas")][string]$srccas,
    [Parameter(Mandatory=$false)][string]$dstcas,
    [Parameter(Mandatory=$false)][Alias("usexml","i")][string]$tasksequencexml=$false,
    [Parameter(Mandatory=$true)][Alias("srcts","ts")][string]$srctspackageid,
    [Parameter(Mandatory=$false)][Alias("dstts")][string]$dsttspackageid,
    [Parameter()][string][Alias("sourcefiles","sourcepath")]$content=$false,
    [Parameter()][string]$objectxml=$false,
    [Parameter()][switch]$export=$false,
    [Parameter()][switch]$import=$false,
    [Parameter()][switch]$console=$false,
    [Parameter()][switch]$checkonly=$false,
    [Parameter()][switch]$uselegacywmi=$false
  )

  if($import -eq $export){
    write-error "Please use the correct switch, eirther '-import' or '-export'"
    return 500
  }

  # Global variables
  $thispath=Get-Location
  $scriptpath = $MyInvocation.MyCommand.Definition
  $scriptname = $MyInvocation.MyCommand.Name
  $temp = "$env:temp"

  if($config -eq $false){
    write-debug "no out path specified, going to use this directory"
    $config = (Get-Location)
    $config="$($srctspackageid).dependencies.json"
  }
  else {
    write-debug "Output path specified, using $config"
      
  }

  $deps="$config\$($srctspackageid).dependencies.json"
  $xmlts="$config\$($srctspackageid).tasksequence.xml"


  # Initiate environmental connections
  
  $genericObjectToStorePackageInfo = @()
  $n = 0

  if(($export -eq $true) -and ($content -eq $false)){
   # Export Operation: Export config file with NO Content
    write-verbose "Exporting config to $config (no binaries)"

    $params = @{
      "uselegacywmi"=$uselegacywmi;
    }

    $cs = Get-CMCimSession @params -cas $srccas -cred $srccred
    
    $params = @{
      "computername"=$srccas;
      "credential"=$srccred;
      "uselegacywmi"=$uselegacywmi;
      "Namespace"=$cs.namespace;
    }

    if(($($cs.cimsession).length) -gt 0) {$params.Add("CimSession","$($cs.CimSession)")}
    # write-host "typename = $(( $cs | gm | select typename -first 1).typename)"
    $cimobject = Get-GenericCimInstance @params -classname "SMS_TaskSequenceReferencesInfo" -filter "PackageID='$srctspackageid'"
    $cimobject = @()
    
    write-debug "Step 1: Export all task seuqence references packages"

    foreach ($refpackage in ($cimobject)){

      switch ($($refpackage.ReferencePackageType)){
        0 {$tso=$(Get-GenericCimInstance @params -CimSession ($cs.cimsession) -classname "SMS_Package" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
        3 {$tso=$(Get-GenericCimInstance @params -CimSession ($cs.cimsession) -classname "SMS_DriverPackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
        257 {$tso=$(Get-GenericCimInstance @params -CimSession ($cs.cimsession) -classname "SMS_ImagePackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
        258 {$tso=$(Get-GenericCimInstance @params -CimSession ($cs.cimsession) -classname "SMS_BootImagePackage" -filter "PackageID='$($refpackage.ReferencePackageID)' ")}
      }
      
      $props = @{
        'PackageName'=$refpackage.ReferenceName;
        'PackageManufacturer'=$tso.Manufacturer;
        'PackageVersion'=$refpackage.ReferenceVersion;
        'PackageType'=$refpackage.ReferencePackageType;
        'Index'=$n;
        'SourcePackageID'=$refpackage.ReferencePackageID;
        'DestinationPackageID'='';
        'PkgSourcePath'=$tso.PkgSourcePath;
      }

      write-verbose "This package $($refpackage.ReferenceName) is of type $($refpackage.ReferencePackageType) with path $($tso.PkgSourcePath) "

      $genericObjectToStorePackageInfo += (New-Object -TypeName PSOBject -Property $props)
      $n = $n + 1
    }

    write-debug "Step 2: Export task sequernce xml file from server"
    write-debug "Since task seuqence export desnot work well using cimsession we need to use get-wmiobject"

    $params.Remove("uselegacywmi")
    $params.Add("uselegacywmi",$true)
    
    $TsList = Get-GenericCimInstance @params -CimSession ($cs.cimsession) -classname "SMS_TaskSequencePackage" -filter "PackageID='$srctspackageid'"
    
    $params.Remove("uselegacywmi")
    $params.Add("uselegacywmi",$uselegacywmi)
    
    try{
      $res = $TsList
      $res.Get()
      write-verbose "Exporting task seuqence to $($srctspackageid).xml "
      $pretransformed = $res.Sequence 

        $text = $pretransformed
        foreach ($match in "GLB","000","QA0","Q00","DV0","D00"){
            write-verbose "Performing regex operations on strings matching $match"
            $me = new-object System.Text.RegularExpressions.RegEx('<reference package="' + $match + '.....');$text = $me.Replace($text,'<reference package="XXXXXXXX')
            $me = new-object System.Text.RegularExpressions.RegEx('">'+ $match +'.....</variable>');$text = $me.Replace($text,"`">XXXXXXXX</variable>")
            $me = new-object System.Text.RegularExpressions.RegEx('/run:'+ $match +'.....');$text = $me.Replace($text,"/run:XXXXXXXX")
            $me = new-object System.Text.RegularExpressions.RegEx('/install:'+ $match +'.....');$text = $me.Replace($text,"/install:XXXXXXXX")
            $me = new-object System.Text.RegularExpressions.RegEx('/image:'+ $match +'.....');$text = $me.Replace($text,"/image:XXXXXXXX")
            $me = new-object System.Text.RegularExpressions.RegEx('/pkg:'+ $match +'.....');$text = $me.Replace($text,"/pkg:XXXXXXXX")
            $me = new-object System.Text.RegularExpressions.RegEx('/config:'+ $match +'.....');$text = $me.Replace($text,'/config:XXXXXXXX')
        }

        $posttransformed = $text
        $posttransformed | out-file "$xmlts"
    }
    catch{
      write-error "Unable to locate task sequence $srctspackageid"

    }

    

    
    #$params = @{
    #  "computername"=$srccas;
    #  "credential"=$srccred;
    #  "uselegacywmi"=$uselegacywmi;
    #}


    $props = @{
      "TaskSequence"=$genericObjectToStorePackageInfo
    }
    $jsonwrapper = (New-Object -TypeName PSOBject -Property $props)

    write-verbose "Exporting task sequence dependencies to xml file $config"
    $jsonwrapper | ConvertTo-json | out-file "$deps"
    return 200
  }

  if(($export -eq $true) -and ($content -ne $false)){ # Export Operation: Export config file including package Content
    write-verbose "Exporting config to $config along with binaries to $content"
    
    # Get JSON Object payload
    $genericObjectToStorePackageInfo = (Invoke-CMGopherConfigExport -ts $srctspackageid -cred $srccred -cas $srccas)  

    # Create json object
    $jsonwrapper = (New-Object -TypeName PSOBject -Property @{"TaskSequence"=$genericObjectToStorePackageInfo;"LastUpdateTime"=""})

    # output the results to a file
    write-verbose "Exporting task sequence dependencies to xml file $config"
    $jsonwrapper | ConvertTo-json | out-file "$xmlts"


    # export the packaeg content source files using the newly created config file
    $env = (Get-Content $config) -join "`n" | convertfrom-json

    # Todo: Invoke-CMGopherSourceExport -configfile -path 
    foreach($job in $env.TaskSequence){
      $srcpath = $job.PkgSourcePath
      $parent = (split-path -parent $srcpath)
      $dstpath = "$content\$($job.PackageName) $($job.PackageVersion)"
      write-verbose "Copying $srcpath to $dstpath"

      switch ($($job.PackageType)){
        0 {$exitcode = Invoke-RobocopyUNCToLocal -srcunc $srcpath -dst $dstpath -cred $srccred}
        3 {$exitcode = Invoke-RobocopyUNCToLocal -srcunc $srcpath -dst $dstpath -cred $srccred}
        257 {$exitcode = Invoke-RobocopyUNCToLocal -srcunc $parent -dst $dstpath -cred $srccred}
        258 {$exitcode = Invoke-RobocopyUNCToLocal -srcunc $parent -dst $dstpath -cred $srccred}
      }
      write-verbose "Exit code of $exitcode"
    }
    write-verbose "Data export job complete"
  }

  if(($import -eq $true) -and ($config -ne $false) -and ($objectxml -eq $false) -and ($content -eq $false)){ # Import Operation: Import config with NO Content
    write-verbose "Importing configuration with NO content."
    if(test-path -path $config){
      write-verbose "Found config file"
    }
    else{
      write-error "Unable to find config file"
      write-error $_.Exception.Message
      return 500
    }

    $env = (Get-Content $config) -join "`n" | convertfrom-json

    foreach($pkg in $env.TaskSequence){
      $srcpath = $pkg.PkgSourcePath
      $dstpath = "$($pkg.PackageName) $($pkg.PackageVersion)" 
      write-host $dstpath    
      Invoke-CMGopherCreatePackage -application
      Invoke-CMGopherCreateEmptyTaskSequence -packages
      Invoke-CMGopherCreateCMObject -package $pkg
      # Todo: make this function , input is an generic psobject , output is create a cm package/driverpackage/wim package/bootimage/pxeimage/etc
      # $exitcode = Invoke-CM12SopurceImport -package $job
      write-verbose "Exit code of $exitcode"
    }
    return 999
  }

  if(($import -eq $true) -and ($config -ne $false) -and ($objectxml -eq $false) -and ($content -ne $false)){ # Import Operation: Import config including content from path
    write-verbose "Importing configuration including package source content from path $content"
    # export the packaeg content source files using the newly created config file

    if(test-path -path $config){
      write-verbose "Found config file"
    }
    else{
      write-error "Unable to find config file"
      write-error $_.Exception.Message
      return 500
    }

    $env = (Get-Content $config) -join "`n" | convertfrom-json

    foreach($job in $env.TaskSequence){
      $srcpath = $job.PkgSourcePath
      $dstpath = "$content\$($job.PackageName) $($job.PackageVersion)"
      write-verbose "Copying $srcpath to $dstpath"
      $exitcode = Invoke-RobocopyUNCToLocal -srcunc $srcpath -dst $dstpath -cred $creds
      write-verbose "Exit code of $exitcode"
    }
  }

  if(($import -eq $true) -and ($config -eq $false) -and ($objectxml -ne $false) -and ($content -eq $false)){ # Import operation: Use pre-defined xml file from task seuqence with NO Content import
    write-verbose "Using an existing object xml to transform"
    write-verbose "tasksequencexml is defined as $objectxml"
    
    [xml] $tsxml = Get-content $objectxml
    $xpath = "//Instance/Property[@Name]/@Value"
    $sequence = $tsxml.SelectNodes("INSTANCE/PROPERTY[@NAME=""Sequence""]/VALUE")
    $actualxml = $sequence."#cdata-section"
    [xml]$val = $actualxml
    $srcpackages = $val.sequence.referenceList.reference.package
    foreach($refpackage in $srcpackages)
    {
        write-verbose "ref: $refpackage"
      $props = @{
        'Index'=$n;
        'SourcePackageID'=$refpackage;
        'DestinationPackageID'='';
        'PackageName'='';
        'PackageVersion'='';
        'PackageType'='';
        'PkgSourcePath'='';
      }
      $genericObjectToStorePackageInfo += (New-Object -TypeName PSOBject -Property $props)
      $n = $n + 1
    }
    return 69

    $dstenv = Get-CMCimSession -cas "$dstcas" -cred $dstcred
    foreach($item in $genericObjectToStorePackageInfo)
    {
      $filter = "Name='$($item.PackageName)' and Version='$($item.PackageVersion)'"
      foreach($sitem in (Get-CimInstance @srcenv -Classname "SMS_Package" -filter $filter)){
        write-verbose "Found $($sitem.Name) ; Updating $($item.DestinationPackageID) to $($sitem.PackageID)"
        $item.DestinationPackageID = $sitem.PackageID
        $item.PkgSourcePath = $sitem.PkgSourcePath
      }

      foreach($sitem in (Get-CimInstance @srcenv -Classname "SMS_ImagePackage" -filter $filter)){
        write-verbose "Found $($sitem.Name) ; Updating $($item.DestinationPackageID) to $($sitem.PackageID)"
        $item.DestinationPackageID = $sitem.PackageID
        $item.PkgSourcePath = $sitem.PkgSourcePath
      }

      foreach($sitem in (Get-CimInstance @srcenv -Classname "SMS_DriverPackage" -filter $filter)){
        write-verbose "Found $($sitem.Name) ; Updating $($item.DestinationPackageID) to $($sitem.PackageID)"
        $item.DestinationPackageID = $sitem.PackageID
        $item.PkgSourcePath = $sitem.PkgSourcePath
      }

      foreach($sitem in (Get-CimInstance @srcenv -Classname "SMS_BootImagePackage" -filter $filter)){
        write-verbose "Found $($sitem.Name) ; Updating $($item.DestinationPackageID) to $($sitem.PackageID)"
        $item.DestinationPackageID = $sitem.PackageID
        $item.PkgSourcePath = $sitem.PkgSourcePath
      }
    }

    $props = @{"TaskSequence"=$genericObjectToStorePackageInfo}
    $jsonwrapper = (New-Object -TypeName PSOBject -Property $props)
    $jsonwrapper | ConvertTo-json | out-file "$deps"
  }
}

Set-Alias -name cmexport -value Invoke-CMGopher
Set-Alias -name cmgopher -value Invoke-CMGopher
Set-Alias -name cmcmd -value Invoke-CMGopher
set-alias -name test-buildpackages -value Test-CustomCMBuildPackages
Export-ModuleMember -Function * -Alias *