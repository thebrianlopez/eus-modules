function New-CMBootDisk{
    #[cmdletBinding(SupportsShouldProcess=$true)]
    [cmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$true)][string]$cas,
        [Parameter(Mandatory=$true)][string]$bootimageid,
        [Parameter(Mandatory=$false)][string]$startupscript="powershell.exe -nopro -exec Unrestricted -f X:\SNOWMediaHook.ps1",
        [Parameter(Mandatory=$true)][string]$DPContainingBootImage,
        [Parameter(Mandatory=$true)][string]$preexecutionpackage,
        [Parameter(Mandatory=$true)][string]$encryptedUserName,
        [Parameter(Mandatory=$true)][string]$encryptedPassword,
        [Parameter(Mandatory=$true)][string]$encryptedDomain,
        [Parameter(Mandatory=$true)][System.IO.FileInfo]$destination,
        [Parameter(Mandatory=$false)][string]$eucwebservice="https://eucwebserv.$($env:USERDNSDOMAIN)/eucwebserv"
        )

    process{

        # check if the output folder exists
        resolve-path $destination.directoryname

        # check if 

        $startupscript = "powershell.exe -nopro -exec Unrestricted -c "
        $startupscript += "`$uri`='$eucwebservice/InstallOperatingSystem.ps1';"
        $startupscript += "[Net.ServicePointManager]::ServerCertificateValidationCallback `= {`$true};"
        $startupscript += "write-host -ForegroundColor Green 'Starting SNOW Media Hook from `$uri';"
        $startupscript += "start-sleep 30;"
        $startupscript += "iex (new-object net.webclient).downloadstring(`$uri);"

        $dp = $DPContainingBootImage
        # tests before running script


        # 0: Confirm environment has all prereq libraries
        if(-not (test-path "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe")){write-error "Unable to find CreateMedia.exe. Is the CM console installed?" -ea Stop}      

        # 1: CAS must be pingable and actually a CAS        
        if(-not(test-connection -count 1 -quiet $cas)){write-error "Unable to connect to the CAS $cas" -ea Stop}
        if(-not(Get-wmiobject -ComputerName $cas -classname "SMS_ProviderLocation" -Namespace "root\sms" -ea SilentlyContinue )){write-error "Unable to connect to the namespace root\SMS. Is this a ConfigMgr CAS?" -ea Stop}
        
        # 2: Bootimage must exist - but we need the sitecode first
        $sitecode = (Get-wmiobject -ComputerName $cas -ClassName SMS_ProviderLocation -Namespace root\sms).Sitecode
        write-verbose "Sitecode is $Sitecode"

        $s = @(Get-wmiobject -ComputerName $cas -ClassName "SMS_PackageBaseclass" -Namespace root\sms\site_$sitecode -Filter "PackageID='$bootimageid'" | select -first 1)
        if(-not($s.count -gt 0)){write-error "Unable to locate bootimage $bootimageid" -ea Stop}
        [IO.FileInfo]$bootimagepath = $s.PkgSourcePath
        if(-not(test-path $bootimagepath)){write-error "Cannot find the path $bootimagepath" -ea Stop;}
        write-verbose "Bootimage path is $bootimagepath"

        # Get the management point URL
        $params = @{
            "ComputerName"="$cas" 
            "ClassName"="SMS_SystemResourceList"
            "Namespace"="root\sms\site_$SiteCode"
            "Filter"="RoleName`='SMS Management Point'"
        }
        $mp = "http://"
        $mp += ((Get-wmiobject @params ) | select -first 1).ServerName
        $mp += ":4880"

        # Get dp that conatains the boot image that is close    
        $disklabel="Configuration Manager 2012"

        # delete the boot image it it alread yexists
        if(test-path "$($destination.fullname)"){rm -rec -for "$($destination.fullname)";write-verbose "Deleting the file $($destination.fullname)"; }


        # items passed to createbootmedia.exe
        $cmd = "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe"
        $arg = " /K:boot"
        $arg +=" /P:$cas"
        $arg +=" /S:$sitecode"
        $arg +=" /D:$dp"
        $arg +=" /L:`"$disklabel`""
        $arg +=" /U:True"
        $arg +=" /J:False" 
        $arg +=" /Z:False" 
        $arg +=" /1:30447808`;496062832"
        $arg +=" /2:31549994`;-623013520"
        $arg +=" /5:2"
        $arg +=" /X:pUserName=$encryptedUserName"
        $arg +=" /X:pUserPassword=$encryptedPassword"
        $arg +=" /X:pUserDomain=$encryptedDomain"
        $arg +=" /X:SMSTSMP=$mp"
        $arg +=" /B:$bootimageid"
        $arg +=" /T:CD"
        $arg +=" /F:$destination"
        #$arg +=" /G:`"$startupscript`""
        $arg +=" /E:$preexecutionpackage"
        $s = new-object System.Diagnostics.ProcessStartInfo
        $s.FileName = "$cmd"
        $s.Arguments = "$arg"
        $s.UseShellExecute = $false
        $cmd = "`"$cmd`""
        write-verbose "Running $cmd"
        write-verbose "Running $arg"

        $s | foreach-object {
            if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();
            }
        }
        $arg = $null;$cmd=$null;$s=$null;
        if(-not (test-path "$destination")){
            write-error "ISO creation failed. Has the iso been replicated to the specified DP?" -ea Stop;
        } else {
            write-verbose "All done"
        }
    }
}

<#
 - resolves a boot image from the environment
 - resovled environmental parameters
 - constructs a command wrapper for createmedia.exe
 - generates an iso to the specified output path

Measure-Command{
    $bdparams = @{
        "cas"="hqaaam0300";
        "DPContainingBootImage"="hqaaam0322.nestle.w2k"
        # "D0000290" -- Windows 8.1 x86
        # "D0000291" -- Windows 8.1 amd64
        "bootimageid"="D0000290";
        "encryptedPassword"="TmVzdGxlMTIz"
        "encryptedDomain"="bmVzdGxlLncyaw=="
        "destination"="$($env:userprofile)\desktop\D0000290.iso"
    }
    New-CMBootDisk @bdparams -verbose
}
#>

