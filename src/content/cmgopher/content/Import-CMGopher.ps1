function Import-CMGopher
{
  <# 
  .SYNOPSIS 
  CM Gopher is your best friend! CM Gopher will:

  1. Take a look at your CM12 CAS server, query all task sequences availible, list all reference packages, and will export all settings to a .json file
  2. Next CM Gopher will then import all of the packages.json file settings into a new evironment -- NEATTTO !

  .DESCRIPTION 
  Not responsible for data loss, loss of hair, decreased appetite, increased despair and sadness.

  .INPUTS 
  String

  .OUTPUTS 
  String

  .EXAMPLE 

  // This is an export cmd to dump all ts info into a package.config file
  EUCPkgMgr -export -tasksequencezip build.zip -cas "hqaaam0300.nestle.w2k" -config package.config

  // This is the import command
  EUCPkgMgr -import -tasksequencezip build.zip -cas "hqbusm9200.nestle.w2k" -config package.config

  .NOTES 
  Initial Creation
  .LINK 
  http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 
  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$true)][Alias("cred")][PSCredential]$credential,
    [Parameter(Mandatory=$true)][Alias("server","computername")][string]$cas,
    [Parameter(Mandatory=$false)][Alias("d")][string]$dependencies=$false,
    [Parameter(Mandatory=$true)][Alias("i")][string]$tasksequencexml=$false,
    [Parameter(Mandatory=$false)][Alias("srcts","ts")][string]$taskseuqenceid,
    [Parameter()][string][Alias("sourcefiles","sourcepath")]$packagesourcelocation=$false,
    [Parameter()][switch]$import,
    [Parameter()][switch]$uselegacywmi=$false
  )

  # Global variables
  $thispath=Get-Location
  $scriptpath = $MyInvocation.MyCommand.Definition
  $scriptname = $MyInvocation.MyCommand.Name
  $temp = "$env:temp"

  write-verbose "Step 1: Parse dependency file and import any missing packages"
  
  $packagearray = @()
  $deps = (Get-Content $dependencies) -join "`n" | convertfrom-json
  $envparams =(Get-CMCimSession -cas $cas -cred $credential)

  $cs = Get-CMCimSession -cas $cas -cred $credential -uselegacywmi $uselegacywmi

  $params = @{
    "computername"=$cas;
    "credential"=$credential;
    "uselegacywmi"=$uselegacywmi;
    "Namespace"=$cs.namespace;
  }
  
  foreach($pkg in $deps.TaskSequence){
      switch ($($pkg.PackageType)){
        0 {$classname="SMS_Package";$filter="Name='$($pkg.PackageName)' and Version='$($pkg.PackageVersion)' and Manufacturer='$($pkg.PackageManufacturer)'";}
        3 {$classname="SMS_DriverPackage";$filter="Name='$($pkg.PackageName)' and Version='$($pkg.PackageVersion)' and Manufacturer='$($pkg.PackageManufacturer)'";}
        257 {$classname="SMS_ImagePackage";$filter="Name='$($pkg.PackageName)' and Version='$($pkg.PackageVersion)' and Manufacturer='$($pkg.PackageManufacturer)'";}
        258 {$classname="SMS_BootImagePackage";$filter="Name='$($pkg.PackageName)' and Version='$($pkg.PackageVersion)' and Manufacturer='$($pkg.PackageManufacturer)'";}
      }
      write-verbose "querying the class: $classname with filter $filter"

      $packageExists=(Get-GenericCimInstance @params -classname $classname -filter $filter)
      
      $props = @{
        'PackageName'=$pkg.PackageName;
        'PackageVersion'=$pkg.PackageVersion;
        'PackageManufacturer'=$pkg.PackageManufacturer;
        'PackageType'=$pkg.PackageType;
        'Index'=$n;
        'SourcePackageID'=$pkg.SourcePackageID;
        'DestinationPackageID'=$pkg.DestinationPackageID;
        'PkgSourcePath'=$pkg.PkgSourcePath;
      }

      $packagearray += (New-Object -TypeName PSOBject -Property $props)
      write-verbose "This package $($refpackage.ReferenceName) is of type $($refpackage.ReferencePackageType) with path $PkgSourcePath "
      $n = $n + 1
  }

  $boom = 9

  write-verbose "Step 1a: If including source files: "
  write-verbose "Step 1a: - Source files are present and valid "
  write-verbose "Step 1a: - Copy source files to current path (optionally overwrite current path, rename existing to .old, or rename new path to append date"

  write-verbose "Step 2: Verify packages were imported successfully by querying name/manufacterer/version/date/etc"
  write-verbose "Step 3: Parse task sequence xml to ensure its valid"
  write-verbose "Step 4: Import task sequence xml file into infrastructure"
  write-verbose "Step 5: Confirm task sequence was imported successfully and return ts name,id,and folder location in SCCM Console."


  

}