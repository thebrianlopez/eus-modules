function Export-PXEBootImage{
    [cmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$true)][string]$cas,
        [Parameter(Mandatory=$true)][string[]]$bootimageid,
        [Parameter(Mandatory=$true)][string]$DPContainingBootImage,
        [Parameter(Mandatory=$true)][string]$preexecutionpackage,
        [Parameter(Mandatory=$true)][string]$encryptedUserName,
        [Parameter(Mandatory=$true)][string]$encryptedPassword,
        [Parameter(Mandatory=$true)][string]$encryptedDomain,
        [Parameter(Mandatory=$true)][IO.DirectoryInfo]$destination
        )


    begin{

        $a = @()

        # 0: Confirm environment has all prereq libraries
        if(-not (test-path "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe")){write-error "Unable to find CreateMedia.exe. Is the CM console installed?" -ea Stop}      
        if(-not (test-path "$env:systemroot\system32\dism.exe")){write-error "Unable to find Dism.exe. It the Windows ADK Installed?" -ea Stop}
        if(-not (test-path "$env:appdata\nuget\programs\7zip\7z.exe")){write-error "Unable to find 7z.exe. Might need to run 'nuget install 7zip -o `"%appdata%\nuget\programs`" -verbosity detailed -excludeversion' " -ea Stop}
        if(-not (test-path "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe")){write-error "Unable to find nuget.exe. Might need to run 'nuget install nuget -o `"%appdata%\nuget\programs`" -verbosity detailed -excludeversion' " -ea Stop}       
        # 1: CAS must be pingable and actually a CAS        
        if(-not(test-connection -count 1 -quiet $cas)){write-error "Unable to connect to the CAS $cas" -ea Stop}
        if(-not(Get-wmiobject -ComputerName $cas -classname "SMS_ProviderLocation" -Namespace "root\sms" -ea SilentlyContinue )){write-error "Unable to connect to the namespace root\SMS. Is this a ConfigMgr CAS?" -ea Stop}
        
        # 2: Bootimage must exist - but we need the sitecode first
        $sitecode = (Get-wmiobject -ComputerName $cas -ClassName SMS_ProviderLocation -Namespace root\sms).Sitecode
        write-verbose "Sitecode is $Sitecode"

         # Get the management point URL
        $params = @{
            "ComputerName"="$cas" 
            "ClassName"="SMS_SystemResourceList"
            "Namespace"="root\sms\site_$SiteCode"
            "Filter"="RoleName`='SMS Management Point'"
        }
        $mp = "http://"
        $mp += ((Get-wmiobject @params ) | select -first 1).ServerName
        $mp += ":4880"
        

    }
    

    process{

        $dp = $DPContainingBootImage

        foreach($image in $bootimageid){
            if(test-path "$env:temp\iso"){write-verbose "Removing $env:temp\iso";rm -rec -for "$env:temp\iso"}
            if(test-path "$env:temp\mount"){write-verbose "Removing $env:temp\mount";rm -rec -for "$env:temp\mount"}
            if(test-path "$env:temp\boot.iso"){ write-verbose "Removing previous boot.iso";rm -rec -for "$env:temp\boot.iso" }

            $s = @(Get-wmiobject -ComputerName $cas -ClassName "SMS_PackageBaseclass" -Namespace root\sms\site_$sitecode -Filter "PackageID='$image'" | select -first 1)
            if(-not($s.count -gt 0)){write-error "Unable to locate bootimage $image" -ea Stop;}
            [IO.FileInfo]$srcbootimagepath = $s.PkgSourcePath
            $wmi = $s
            if(-not(test-path $srcbootimagepath)){write-error "Cannot find the path $srcbootimagepath" -ea Stop;}
            write-verbose "Bootimage path is $srcbootimagepath"
            $dst = $destination
            if (-not $dst.Exists){write-verbose "Creating directory $($dst.fullname)";$dst.Create()}
            [IO.DirectoryInfo]$dst = "$dst\Images\" 
            if (-not $dst.Exists){write-verbose "Creating directory $($dst.fullname)";$dst.Create()}
            [IO.DirectoryInfo]$dst = "$dst\$image" 
            if (-not $dst.Exists){write-verbose "Creating directory $($dst.fullname)";$dst.Create()}
            [IO.FileInfo]$dstbootimagepath = "$dst\boot.$image.wim"

            # Get dp that conatains the boot image that is close    

            # items passed to createbootmedia.exe
            $cmd = "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe"
            $arg = " /K:boot"
            $arg +=" /P:$cas"
            $arg +=" /S:$sitecode"
            $arg +=" /D:$dp"
            $arg +=" /L:`"Configuration Manager 2012`""
            $arg +=" /U:True"
            $arg +=" /J:False" 
            $arg +=" /Z:False" 
            $arg +=" /1:30447808`;496062832"
            $arg +=" /2:31549994`;-623013520"
            $arg +=" /5:2"
            $arg +=" /X:pUserName=$encryptedUserName"
            $arg +=" /X:pUserPassword=$encryptedPassword"
            $arg +=" /X:pUserDomain=$encryptedDomain"
            $arg +=" /X:SMSTSMP=$mp"
            $arg +=" /B:$image"
            $arg +=" /T:CD"
            $arg +=" /F:$env:temp\boot.iso"
            $arg +=" /E:$preexecutionpackage"
            $s = new-object System.Diagnostics.ProcessStartInfo
            $s.FileName = "$cmd"
            $s.Arguments = "$arg"
            $s.UseShellExecute = $false
            write-verbose "Running $cmd$arg"
            $s | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                    [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();
                }
            }
            $arg = $null;$cmd=$null;$s=$null;
            if(-not (test-path "$env:temp\boot.iso")){write-error "ISO creation failed. Has the iso been replicated to the specified DP?" -ea Stop;}


            # 7zip extract the iso image to $renv:iso
            # extract variables from iso file 
            $cmd = [System.IO.FileInfo]"$env:appdata\nuget\programs\7zip\7z.exe"
            $arg = " "
            $arg += " x"
            $arg += " $env:temp\boot.iso"
            #$arg += " sms\data"
            $arg += " -o$env:temp\iso"
            $arg += " -y"
            if(-not (test-path $cmd)){write-error "Unable to find 7zip. Please install with nuget" -ea "Stop"}
            if(test-path $env:temp\iso){ rm -rec -for $env:temp\variables}
            $s = (new-object System.Diagnostics.ProcessStartInfo)
            $s.FileName = $cmd
            $s.Arguments = $arg
            $s.UseShellExecute = $false
            write-verbose "Running $cmd$arg"
            if(-not (test-path $env:temp\iso)){mkdir $env:temp\iso;}
            $s | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                    [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit()
                    write-verbose "Completed with exite code $exitCode"
                }
            }
            $arg = $null;$cmd=$null;$s=$null;

            
            # mount new wim as read/write
            $cmd = "$env:systemroot\system32\dism.exe"
            $arg  = " /Mount-Wim"
            $arg += " /WimFile:$env:temp\iso\sources\boot.wim"
            $arg += " /index:1"
            $arg += " /MountDir:$env:temp\mount"
            $arg += " /LogPath:$env:temp\runit.log"
            $s = new-object System.Diagnostics.ProcessStartInfo
            $s.FileName = "$cmd"
            $s.Arguments = "$arg"
            $s.UseShellExecute = $false
            write-verbose "Running $cmd$arg"        

            if(-not (test-path $env:temp\mount)){mkdir $env:temp\mount;}
            $s | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                    [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();
                }
            }

            $arg = $null;$cmd=$null;$s=$null;

            write-verbose "Injecting variables into boot.wim"
            ([io.directoryinfo]"$env:temp\iso\sms\data").MoveTo("$env:temp\mount\sms\data")

            # commit boot image
            $cmd = "$env:systemroot\system32\dism.exe"
            $arg  = " /unmount-wim"
            $arg += " /MountDir:$env:temp\mount"
            $arg += " /commit"
            $arg += " /LogPath:$env:temp\runit.log"
            $s = new-object System.Diagnostics.ProcessStartInfo
            $s.FileName = "$cmd"
            $s.Arguments = "$arg"
            $s.UseShellExecute = $false
            write-verbose "Running $cmd$arg"
            
            $s | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                    [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();$arg = $null;$cmd=$null;$s=$null;
                }
            }


            # copy finiziled boot image to our destination
            
            if (([io.fileinfo]"$dstbootimagepath").exists){([io.fileinfo]"$dstbootimagepath").Delete()}
            ([io.fileinfo]"$env:temp\iso\sources\boot.wim").MoveTo("$dstbootimagepath")

            $bcd = "$($($dstbootimagepath.fullname).Replace(".wim",".bcd"))"
            $device = "[boot]\Images\$image\boot.$image.wim"
            # generate BCD File using new boot image
            $params = @{
                bcd=$bcd;
                wim="$($dstbootimagepath.fullname)";
                device="$device";
            }
            write-verbose "Updating bcd"
            update-bcd @params
            $p = @{
                "Name"="$($wmi.Name)";
                "PackageID"="$($wmi.packageid)";
                "Description"="$($wmi.Description)";
                "LastRefreshTime"="$($wmi.lastrefreshtime.tostring())";
                "PackageSize"="$($wmi.packagesize)";
                "PkgSourcePath"="$($wmi.pkgsourcepath)";
                "ImagePath"="$($wmi.imagepath)";
                "BCDPath"="$bcd";
                "FinializedBootImage"="$dstbootimagepath";
                "Device"="$device";
            }
            write-verbose "Populating custom properties"
            $a += (new-object -type psobject -property $p )


        }
    }

    end {return $a }
    
}