function update-bcd{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.IO.FileInfo]$bcd,
        [Parameter(Mandatory=$true)][System.IO.FileInfo]$wim,
        [Parameter(Mandatory=$true)][string]$device
        )
    
    if(-not($wim.Exists)){write-error "Unable to find wim file $wim" -ea Stop}

    rm -rec -for "$bcd*"


    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-createstore `"$bcd`""


    # Ramdisk
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -create {ramdiskoptions} /d `"Ramdisk Options`""
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {ramdiskoptions} ramdisksdidevice boot"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {ramdiskoptions} ramdisksdipath \boot.sdi"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 4096"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 8192"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 16384"

    # debug
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -create {dbgsettings} /d `"Debugger Settings`""
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {dbgsettings} debugtype serial"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {dbgsettings} baudrate 115200"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {dbgsettings} debugport 1"



    # OSLoader
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -create /d `"WinPE 6.2`" /application osloader"
    $guid = (iex "bcdedit.exe -store `"$bcd`" " | select-string "identifier").tostring().replace("identifier","").trim()
    write-verbose "My guid is $guid"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid systemroot \Windows"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid detecthal Yes"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid winpe yes"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid osdevice ramdisk=$device,{ramdiskoptions}"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid device ramdisk=$device,{ramdiskoptions}"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid systemroot \Windows"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set $guid debug on"


    # Boot Manager
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -create {bootmgr} /d `"Boot Manager`" "
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -create {bootmgr} /d `"Boot Manager`" inherit {dbgsettings}"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {bootmgr} timeout 30"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {bootmgr} inherit {dbgsettings}"
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {bootmgr} bootdebug on"
    # Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -set {dbgsettings} advancedoptions true"

    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -displayorder $guid"


    # Dump settings
    Start-CustomProcess "$env:systemroot\system32\bcdedit.exe" "-store `"$bcd`" -enum all"
}

function Start-CustomProcess {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][System.IO.FileInfo]$cmd,
        [Parameter(Mandatory=$true)][string]$arg
        )
    if(-not (test-path $cmd)){write-error "Unable to find $cmd" -ea Stop}
    $StartInfo = new-object System.Diagnostics.ProcessStartInfo
    $StartInfo.FileName = "$cmd"
    $StartInfo.Arguments = "$arg"
    $StartInfo.UseShellExecute = $false
    $StartInfo.WorkingDirectory = "$env:systemdrive"
    write-verbose "Running: $cmd $arg"
    
    $StartInfo | foreach-object {
        if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
            [int]$exitCode = [Diagnostics.Process]::Start($StartInfo).WaitForExit()
            if($exitCode -eq 0){
                write-verbose "command complete successfully."
                $results = $true
            }
            else{
                write-error "command existed with code $($_.Exception.Message.toString())" -ErrorAction Stop
                throw
            }
        }
    }
    $cmd = $null 
    $arg = $null 
}

function Update-TftpRoot{
    [cmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(Mandatory=$true)][string]$cas,
        [Parameter(Mandatory=$true)][string]$bootimageid,
        [Parameter(Mandatory=$true)][string]$tftprootpackage,
        [Parameter(Mandatory=$false)][string]$startupscript="powershell.exe -nopro -exec Unrestricted -f X:\SNOWMediaHook.ps1",
        [Parameter(Mandatory=$true)][string]$DPContainingBootImage,
        [Parameter(Mandatory=$true)][string]$preexecutionpackage,
        [Parameter(Mandatory=$true)][string]$encryptedUserName,
        [Parameter(Mandatory=$true)][string]$encryptedPassword,
        [Parameter(Mandatory=$true)][string]$encryptedDomain,
        [Parameter(Mandatory=$false)][string]$eucwebservice="https://eucwebserv/eucwebserv"
        )

    process{


        $dp = $DPContainingBootImage
        # tests before running script

        # 0: Confirm environment has all prereq libraries
        if(-not (test-path "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe")){write-error "Unable to find CreateMedia.exe. Is the CM console installed?" -ea Stop}      
        if(-not (test-path "$env:systemroot\system32\dism.exe")){write-error "Unable to find Dism.exe. It the Windows ADK Installed?" -ea Stop}
        if(-not (test-path "$env:appdata\nuget\programs\7zip\7z.exe")){write-error "Unable to find 7z.exe. Might need to run 'nuget install 7zip -o `"%appdata%\nuget\programs`" -verbosity detailed -excludeversion' " -ea Stop}
        if(-not (test-path "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe")){write-error "Unable to find nuget.exe. Might need to run 'nuget install nuget -o `"%appdata%\nuget\programs`" -verbosity detailed -excludeversion' " -ea Stop}

        # 1: CAS must be pingable and actually a CAS        
        if(-not(test-connection -count 1 -quiet $cas)){write-error "Unable to connect to the CAS $cas" -ea Stop}
        if(-not(Get-cimclass -ComputerName $cas -classname "SMS_ProviderLocation" -Namespace "root\sms" -ea SilentlyContinue )){write-error "Unable to connect to the namespace root\SMS. Is this a ConfigMgr CAS?" -ea Stop}
        
        # 2: Bootimage must exist - but we need the sitecode first
        $sitecode = (Get-CimInstance -ComputerName $cas -ClassName SMS_ProviderLocation -Namespace root\sms).Sitecode
        write-verbose "Sitecode is $Sitecode"

        $s = @(Get-CimInstance -ComputerName $cas -ClassName "SMS_PackageBaseclass" -Namespace root\sms\site_$sitecode -Filter "PackageID='$bootimageid'" | select -first 1)
        if(-not($s.count -gt 0)){write-error "Unable to locate bootimage $bootimageid" -ea Stop}
        [IO.FileInfo]$bootimagepath = $s.PkgSourcePath
        if(-not(test-path $bootimagepath)){write-error "Cannot find the path $bootimagepath" -ea Stop;}
        write-verbose "Bootimage path is $bootimagepath"
        
        
        # 3: tftproot package must exist
        $t = @(Get-CimInstance -ComputerName $cas -ClassName "SMS_PackageBaseclass" -Namespace root\sms\site_$sitecode -Filter "PackageID='$tftprootpackage'" | select -first 1)
        if(-not($t.count -gt 0)){write-error "Unable to locate tftp root package $tftprootpackage" -ea Stop}
        # This is a mandatory folder structure required by 1e PXE
        [IO.DirectoryInfo]$tftprootSource = "$($t.PkgSourcePath)Images\$bootimageid" 
        [IO.FileInfo]$finalizedBootImage = "$($t.PkgSourcePath)Images\$bootimageid\boot.$bootimageid.wim" 
        write-verbose "TFTPRoot source path is $tftprootSource"
        

        # 4: tftp root source path must be writeable and exists
        if(-not(test-path $tftprootSource)){write-warning "Cannot find the path $tftprootSource, creating"; mkdir $tftprootSource;}
        
        # 5: tftproot source must be of type folder
        #if((gci $tftprootSource | gm | select -first 1).TypeName -eq "System.IO.DirectoryInfo"){write-error "Tftprootpath is not a directory" -ea Stop}

        write-verbose "Copying the wim file $bootimagepath to $tftprootSource"  

        try{
            $bootimagepath | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Copy Bootimage to tftp root path?")){
                    copy "$bootimagepath" "$finalizedBootImage" -force
                }
            }
        }
        catch{
            throw;
        }

        # Get the management point URL
        $params = @{
            "ComputerName"="$cas" 
            "ClassName"="SMS_SystemResourceList"
            "Namespace"="root\sms\site_$SiteCode"
            "Filter"="RoleName`='SMS Management Point'"
        }
        $mp = "http://"
        $mp += ((Get-CimInstance @params ) | select -first 1).ServerName
        $mp += ":4880"

        # Get dp that conatains the boot image that is close    
        $disklabel="Configuration Manager 2012"

        # delete the boot image it it already exists
        if(test-path "$env:temp\boot.iso"){rm -rec -for "$env:temp\boot.iso" }

        # items passed to createbootmedia.exe
        $cmd = "$env:SMS_ADMIN_UI_PATH\CreateMedia.exe"
        $arg = " /K:boot"
        $arg +=" /P:$cas"
        $arg +=" /S:$sitecode"
        $arg +=" /D:$dp"
        $arg +=" /L:`"$disklabel`""
        
        $arg +=" /U:True"
        $arg +=" /J:False" 
        $arg +=" /Z:False" 
        $arg +=" /1:30447808`;496062832"
        $arg +=" /2:31549994`;-623013520"
        $arg +=" /5:2"
        $arg +=" /X:pUserName=$encryptedUserName"
        $arg +=" /X:pUserPassword=$encryptedPassword"
        $arg +=" /X:pUserDomain=$encryptedDomain"
        $arg +=" /X:SMSTSMP=$mp"
        $arg +=" /B:$bootimageid"
        $arg +=" /T:CD"
        $arg +=" /F:$env:temp\boot.iso"
        # $arg += " /G:`"$startupscript`""
        $arg +=" /E:$preexecutionpackage"
        $s = new-object System.Diagnostics.ProcessStartInfo
        $s.FileName = "$cmd"
        $s.Arguments = "$arg"
        $s.UseShellExecute = $false
        write-verbose "Running $cmd$arg"
        $s | foreach-object {
            if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();
            }
        }
        $arg = $null;$cmd=$null;$s=$null;
        if(-not (test-path "$env:temp\boot.iso")){write-error "ISO creation failed. Has the iso been replicated to the specified DP?" -ea Stop;}
        
        

        # mount new wim as read/write
        $cmd = "$env:systemroot\system32\dism.exe"
        $arg  = " /Mount-Wim"
        $arg += " /WimFile:$finalizedBootImage"
        $arg += " /index:1"
        $arg += " /MountDir:$env:temp\mount"
        $arg += " /LogPath:$env:temp\runit.log"
        $s = new-object System.Diagnostics.ProcessStartInfo
        $s.FileName = "$cmd"
        $s.Arguments = "$arg"
        $s.UseShellExecute = $false
        write-verbose "Running $cmd$arg"

        

        if(test-path $env:temp\mount){
            rm -rec -for $env:temp\mount 
            #; mkdir $env:temp\mount;
        }

        if(-not (test-path $env:temp\mount)){
            mkdir $env:temp\mount;
        }
        $s | foreach-object {
            if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();
            }
        }

        $arg = $null;$cmd=$null;$s=$null;

        # copy iso to tftproot path
        try{
            "$env:temp\boot.iso" | foreach-object {
                if($PSCmdlet.ShouldProcess($_,"Copy the iso $($_) to tftp root path?")){
                    copy "$env:temp\boot.iso" "$tftprootSource\$bootimageid.iso" -force
                }
            }
        }
        catch{
            throw;
        }

        # extract variables from iso file 
        $cmd = [System.IO.FileInfo]"$env:appdata\nuget\programs\7zip\7z.exe"
        $arg = " "
        $arg += " x"
        $arg += " $env:temp\boot.iso"
        $arg += " sms\data"
        $arg += " -o$env:temp\mount"
        $arg += " -y"
        if(-not (test-path $cmd)){write-error "Unable to find 7zip. Please install with nuget" -ea "Stop"}
        if(test-path $env:temp\variables){ rm -rec -for $env:temp\variables}
        $s = (new-object System.Diagnostics.ProcessStartInfo)
        $s.FileName = $cmd
        $s.Arguments = $arg
        $s.UseShellExecute = $false
        write-verbose "Running $cmd$arg"

        $s | foreach-object {
            if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit()
                write-verbose "Completed with exite code $exitCode"
            }
        }
        $arg = $null;
        $cmd=$null;
        $s=$null;
        
        # confirm the folder now exists
        if(-not (test-path "$env:temp\mount")){write-error "Variables injection failed" -ea Stop;}
        <#
        write-verbose "Injecting SNOW MediaHook Script"

        [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
        $text = (new-object net.webclient).DownloadString("$eucwebservice/InstallOperatingSystem.ps1")
        $text | Out-File "$env:temp\mount\SNOWMediaHook.ps1"

        "[CustomHook]" | out-file "$env:temp\mount\tsconfig.ini"
        $cmd = "CommandLine=$startupscript"
        #$cmd = "powershell.exe -nopro -exec Unrestricted -c `"`$uri='https://eucwebserv.nestle.w2k/eucwebserv/InstallOperatingSystem.ps1';[Net.ServicePointManager]::ServerCertificateValidationCallback = {$true};write-host -ForegroundColor Green `"Starting SNOW Media Hook from $uri`";iex (new-object net.webclient).downloadstring($uri);`""
        "$cmd" | out-file -append "$env:temp\mount\tsconfig.ini"
        
        write-verbose "install some nuget packages"

        # iex (new-object net.webclient).downloadstring('https://usstni9989.nestle.com:4881/nuget/install.ps1') > out-null
        # Start-CustomProcess "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe" "install firefox -excludeversion -verbosity detailed -o $env:temp\mount"
        # Start-CustomProcess "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe" "install npp -excludeversion -verbosity detailed -o $env:temp\mount"
        # Start-CustomProcess "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe" "install 7zip -excludeversion -verbosity detailed -o $env:temp\mount"
        Start-CustomProcess "$env:appdata\Nuget\Programs\Nuget\content\Nuget.exe" "install ccmtools -excludeversion -verbosity detailed -o $env:temp\mount"
        
        #>

        # confirm the folder now exists
        if(-not (test-path "$env:temp\mount")){write-error "Variables injection failed" -ea Stop;}

        # commit boot image
        $cmd = "$env:systemroot\system32\dism.exe"
        $arg  = " /unmount-wim"
        $arg += " /MountDir:$env:temp\mount"
        # $arg += " /discard"
        $arg += " /commit"
        $arg += " /LogPath:$env:temp\runit.log"
        $s = new-object System.Diagnostics.ProcessStartInfo
        $s.FileName = "$cmd"
        $s.Arguments = "$arg"
        $s.UseShellExecute = $false
        write-verbose "Running $cmd$arg"
        
        $s | foreach-object {
            if($PSCmdlet.ShouldProcess($_,"Run command? $($_.FileName) $($_.Arguments)")){
                [int]$exitCode = [Diagnostics.Process]::Start($s).WaitForExit();$arg = $null;$cmd=$null;$s=$null;
            }
        }

        # generate BCD File using new boot image
        $params = @{
            bcd="$(([string]$finalizedBootImage).Replace(".wim",".bcd"))";
            wim="$finalizedBootImage";
            device="[boot]\Images\$bootimageid\boot.$bootimageid.wim";
        }

        update-bcd @params
        # profit
    }
}



<# 
# gets a boot imaege
# creates an iso
# extracts the variables
# mounts the boot image
# then adds the variables and other files required
# copies the completed bootimage to the 1ftproot path
# generates the BCD file

# Sample command 
Measure-Command{
    $params = @{
        "cas"="hqaaam0300";
        "DPContainingBootImage"="hqaaam0322.nestle.w2k"
        "bootimageid"="D0000282";
        "tftprootpackage"="D000026F";
        "encryptedPassword"="TmVzdGxlMTIz"
        "encryptedDomain"="bmVzdGxlLncyaw=="
    }

    Update-TftpRoot @params -verbose
}
#>