
function Test-CustomCMBuildPackages{
    param(
    [Parameter(Mandatory=$true)][string[]]$computername,
    [Parameter(Mandatory=$true)][string]$ipaddress,
    [Parameter(Mandatory=$false)][string]$webserviceuri="https://eucwebserv/eucwebserv",
    [Parameter(Mandatory=$false)][string]$make="VMWare, Inc.",
    [Parameter(Mandatory=$false)][string]$model="VMWare Virtual Platform"

    )


begin{
    write-verbose "Starting script"
}

process{
    foreach($computer in $computername){

        write-verbose "Step 0: Launch webservice functions"
        $ad = (new-webserviceproxy -uri "$webserviceuri/ad.asmx")
        $mdt = (new-webserviceproxy -uri "$webserviceuri/mdt.asmx")
        $sccm = (new-webserviceproxy -uri "$webserviceuri/sccm.asmx")

        write-verbose "Step 1: Find adsitename from ip address"
        $res = $ad.GetADSiteManual($ipaddress)
        switch($res.Status){
            "NotFound" {write-error "Unable to find adsite for ip address $ipaddress" -ea Stop;}
            "Success" {$adsitename = $($res.Result);write-verbose "Found ip address $ipaddress";}
            default {write-error "Failed bruhhh" -ea Stop;}
        }
        $res = $null

        write-verbose "Step 2: find uuid of the computer record, or throw an error that it does not exist"
        $res = $mdt.GetComputerSettingsProperty($computer)
        switch($res.Status){
            "NotFound" {write-error "Unable to find a mdt record for the computer $computer" -ea Stop;}
            "RecordsForKeyNotFound" {write-error "Unable to find a mdt record for the computer $computer" -ea Stop;}
            "Success" {$uuid = $($res.ComputerSettingResult.UUID);write-verbose "Found uuid $uuid";}
            default {write-error "Failed bruhhh" -ea Stop;}
        }
        $res = $null


        write-verbose "Step 3: query mdt packages step and generate a list"
        $res = $mdt.GetMDTPackages($uuid,$make,$model,$false)
        switch($res.Status){
            "NotFound" {write-error "Unable to find packages for uuid $uuid" -ea Stop;}
            "RecordsForKeyNotFound" {write-error "Unable to find packages for uuid $uuid" -ea Stop;}
            "Success" {$packages = $($res.PackagesResult.PackageID);write-verbose "Found $($packages.count) packages for uuid $uuid";}
            default {write-error "Failed bruhhh" -ea Stop;}
        }
        $res.PackagesResult
        $res = $null
        
        write-verbose "Step 4: Get list of mdt packages and check the availibility"
        $res = $sccm.GetPackageReplicationStatus($packages,$adsitename)
        switch($res.Status){
            "NotFound" {write-error "Unable to determine replication status of the packages for uuid $uuid at the ad site $adsitename" -ea Stop;}
            "Failure" {write-error "The following packages are missing from the site $adsitename, $([string]$res.packages). Please have someone replicate them." -ea Stop;}
            "RecordsForKeyNotFound" {write-error "Unable to find packages for uuid $uuid" -ea Stop;}
            "Success" {write-host "Successfully found all packages ad the site $adsitename" -ForegroundColor Green;}
            default {write-error "Failed bruhhh" -ea Stop;}
        }
        $res = $null


        write-verbose "Check Make/Model to ensure it is valid"
        $res = $mdt.CheckMakeAndModel($make,$model)
        switch($res.Status){
            "NotFound" {write-error "Unable to find Make/Model for uuid $uuid" -ea Stop;}
            "Failure" {write-error "Unable to find Make/Model for uuid $uuid" -ea Stop;}
            "RecordsForKeyNotFound" {write-error "Unable to find Make/Model for uuid $uuid" -ea Stop;}
            "Success" {write-host "Found the make $make and the model $model in the MDT database for uuid $uuid" -ForegroundColor Green;}
            default {write-error "Failed bruhhh" -ea Stop;}
        }
        $res = $null

    }
    
}

end{

    write-verbose "Script completed"
    
}

    
    
}

