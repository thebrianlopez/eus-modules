cls
ipmo  ..\PSLog.psm1 -Force

$target = New-Object NLog.Targets.FileTarget

$target.Layout       = '${longdate} ${logger} ${message}'
$target.FileName     = 'C:\DougFinke\PowerShell\NLog\examples\logs\logfile.txt'
$target.KeepFileOpen = $false
$target.Encoding     = "iso-8859-2"

[NLog.Config.SimpleConfigurator]::ConfigureForTargetLogging($target, [NLog.LogLevel]::Debug)

$logger = [NLog.LogManager]::GetLogger("Example")
$logger.Debug("log message")