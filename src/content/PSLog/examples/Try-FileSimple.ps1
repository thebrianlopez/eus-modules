cls
ipmo  ..\PSLog.psm1 -Force

$target = New-NLogFileTarget 

$target.Layout       = '${longdate} ${logger} ${message}'
$target.FileName     = "$pwd\logs\logfile.txt"
$target.KeepFileOpen = $false
$target.Encoding     = "iso-8859-2"

Set-ConfigureForTargetLogging $target (Get-LogLevelFatal)

$logger = Get-NLogLogger Example

$logger.Debug("log message") # doesn't get logged
$logger.Fatal("fatal message")