cls

Import-Module  ..\PSLog.psm1 -Force

$config        = New-NLogLoggingConfiguration
$consoleTarget = New-NLogColoredConsoleTarget
$config.AddTarget( "console", $consoleTarget )

$fileTarget = New-NLogFileTarget
$config.AddTarget( "file", $fileTarget )

$consoleTarget.Layout = '${date:format=HH\:MM\:ss} ${logger} ${message}'
$fileTarget.FileName  = "$pwd\log\file.txt"
$fileTarget.Layout    = '${message}'

$rule1 = New-Object NLog.Config.LoggingRule("*", [NLog.LogLevel]::Debug, $consoleTarget)
$config.LoggingRules.Add($rule1)

$rule2 = New-Object NLog.Config.LoggingRule("*", [NLog.LogLevel]::Debug, $fileTarget)
$config.LoggingRules.Add($rule2)

[NLog.LogManager]::Configuration = $config

$logger = Get-NLogLogger "Example"
$logger.Trace("trace log message")
$logger.Debug("debug log message")
$logger.Info("info log message")
$logger.Warn("warn log message")
$logger.Error("error log message")
$logger.Fatal("fatal log message")