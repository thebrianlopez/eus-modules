Function My-Logger {
    <#
    .Synopsis
        Sets up 2 logs for NLog. One to the console and one to a file.
        The log remains open and does not pay the tax of setup for each message logged.
    .Example
        . .\My-Logger.ps1
        $logger = My-Logger
        1..1000 | % { $logger.Info("Message $($_)") }
    #>
    param (
        $logFileName = "$pwd\log\file.txt",
        $loggerName  = "Example"        
    )
    
    Import-Module  ..\PSLog.psm1 -Force

    $config        = New-NLogLoggingConfiguration
    $consoleTarget = New-NLogColoredConsoleTarget
    $config.AddTarget( "console", $consoleTarget )

    $fileTarget = New-NLogFileTarget
    $config.AddTarget( "file", $fileTarget )

    $consoleTarget.Layout = '${date:format=HH\:MM\:ss} ${logger} ${message}'
    $fileTarget.FileName  = $logFileName
    $fileTarget.Layout    = '${message}'

    $rule1 = New-Object NLog.Config.LoggingRule("*", [NLog.LogLevel]::Debug, $consoleTarget)
    $config.LoggingRules.Add($rule1)

    $rule2 = New-Object NLog.Config.LoggingRule("*", [NLog.LogLevel]::Debug, $fileTarget)
    $config.LoggingRules.Add($rule2)

    [NLog.LogManager]::Configuration = $config

    Get-NLogLogger $loggerName
}