cls
Start-Job -ScriptBlock {
     cd C:\pslog\trunk\examples
     
    while (1) {
        .\Try-FileSimple.ps1
        Start-Sleep 1
    }
}

Start-Job -ScriptBlock {
    cd C:\pslog\trunk\examples

    while (1) {
        .\Test-NewLogger.ps1
        Start-Sleep 1
    }
}
