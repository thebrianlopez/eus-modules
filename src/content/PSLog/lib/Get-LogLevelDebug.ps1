Function Get-LogLevelDebug { [NLog.LogLevel]::Debug }
Function Get-LogLevelFatal { [NLog.LogLevel]::Fatal }
Function Get-LogLevelInfo  { [NLog.LogLevel]::Info  }
Function Get-LogLevelWarn  { [NLog.LogLevel]::Warn  }
Function Get-LogLevelError { [NLog.LogLevel]::Error }