#[NLog.Config.SimpleConfigurator]::ConfigureForTargetLogging($target, (Get-LogLevelFatal))
Function Set-ConfigureForTargetLogging {
    param (
        $fileTarget,
        $logLevel
    )
    
    #$logLevel = Get-LogLevelInfo
    
    #if($Debug) {
    #    $logLevel = Get-LogLevelDebug
    #}
    #elseif($Fatal) {
    #    $logLevel = Get-LogLevelFatal
    #}
    
    [NLog.Config.SimpleConfigurator]::ConfigureForTargetLogging($fileTarget, $loglevel)
}