Function New-Logger {
    param (
        [string]$loggerName="Example",
        [NLog.LogLevel]$logLevel = ([NLog.LogLevel]::Debug)
    )
    
    $target = New-NLogFileTarget 

    $target.Layout       = '${longdate} ${logger} ${message}'
    $target.FileName     = "$pwd\logs\logfile.txt"
    $target.KeepFileOpen = $false
    $target.Encoding     = "iso-8859-2"

    Set-ConfigureForTargetLogging $target $logLevel
    Get-NLogLogger $loggerName
}