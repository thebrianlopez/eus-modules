$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)
$thisdir = (Get-Location)
$assem = "$thismoddir\nlog.dll"

function Write-NLog
{
    param (
        [parameter(Mandatory=$true)] [string] $name,
        [parameter(Mandatory=$true)] [string] $logfile,
        [parameter(Mandatory=$true)] [string] $config
    )
    
    try{
        
        write-verbose "Loading the dll $assem"
        [Reflection.Assembly]::LoadFile($assem)  


        write-verbose "Creating config object using the file $config"
        $ne = new-object NLog.Config.XmlLoggingConfiguration($config)
            
        write-verbose "Attempting to load the config file"
        ([NLog.LogManager]::Configuration)=$ne
        
        # Change filename
        write-verbose "Using the specified log file $logfile"
        [NLog.Targets.FileTarget]$fileTarget = [NLog.Targets.FileTarget]([NLog.LogManager]::Configuration.FindTargetByName("file"));

        write-verbose "Current log file defined is $fileTarget.FileName -- changing to $logfile"
        $fileTarget.FileName = $logfile;

        write-verbose "Creating instance of the GetLogger class named $name"
        $logger = [NLog.LogManager]::GetLogger($name)
        
        write-verbose "Class was successfully created"
    }
    catch{
        $ex = $_.Exception 
        Write-Error "$ex.Message" 
        exit
    }
    return $logger
}
# 
# $logger = Write-NLog -name "TestLogger" -config "$thisdir\config\nlog.config" -logfile "C:\\test.log"
#$logger = Write-NLog -name "TestLogger" -logfile "C:\\test.log"
# $logger.Info("Logging test")