# Script Name: Push-WinEventss.ps1
# Date Created: 2014-02-27 15:54PM PST
# Description: Script polls a list of client workstations for offline sync status and logs all data to a database
# Revisions: 
#    2014-02-27: Created. 
#    2014-04-17: Added logging support
#    2014-04-17: Added support to report selective providers and lognames based on passed string values.
#    2014-04-17: Added standalone support to allow clients to run individually   
#    2014-04-17: Added support to push events in json format, opening up the ability for an HTTP based service to consume this data at scale
#
#
# ToDo:
#    2014-04-17: Phase out sql bulk-insert method and use something more scalable like HTTP webservice
#    2014-04-17: Add some framework configure which providers/lognames get reported , maybe webservice or static webconfig file
#    2014-04-17: Report the current version of this script being used to track the correct version on clients.
#    2014-04-17: Add support to push invidual state messages to SCCM 2012, assuming this is a good idea and SCCMDB can support this much traffic from every client
#    2014-04-17: Experimental support to push json events to REST API such as Elasticsearch , or Logstash
#
#
Function Write-Loggly{
  <# 
.SYNOPSIS 
Write a specified string to Loggly
.DESCRIPTION 
Writes a string to the Loggly webservice
.INPUTS 
String

.OUTPUTS 
String
.EXAMPLE 
$params = @{
              "header"=$global:header;  # HTTP Header for Rest Request
              "apikey"=$global:apikey;  # APIKey for webservice
              "url"=$global:url;        # Webservice URL
              "tag"="SNOW-Powershell";  # URL attribut to identify tag
              "data"=$json;             # Data to be posted to HTTP webservice
            }
            
            write-loggly @params

This example 'splatts' all parameters and passes it to the Write-Loggly function.
.NOTES 
Initial Creation
.LINK 
http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
#> 

[CmdletBinding()] 
param(
  [Parameter(Position=0, Mandatory=$true, ValueFromPipeline = $true)] [PSObject[]]$data,
  [Parameter(Mandatory=$true, ValueFromPipeline = $true)] [string]$tag,
  [Parameter(Mandatory=$true)][string]$header,
  [Parameter(Mandatory=$true)][string]$apikey,
  [Parameter(Mandatory=$true)][string]$url,
  [Parameter(Mandatory=$false)][string]$defaultproxy,
  [Parameter()][switch]$bulk=$false,
  [Parameter()][switch]$useproxy=$false
  )


if($bulk -eq $true){
  Write-verbose "Using Bulk upload api"
  foreach($item in $data){
    $json = $item | ConvertTo-Json
    write-loggly -data $json -tag $tag
  } 

        <#
        write-verbose "Using Bulk upload API"
        $tag="bulk"
        $body = $data
        $header = $global:header
        $apikey=$global:apikey
        $uri = $global:bulkurl
        $uri = "$url$apikey/tag/$tag"
        #>
      }
      else
      {
        write-verbose "Using singular event api"
        $body = $data
        #$header = $global:header
        #$apikey=$global:apikey
        #$uri = $global:url
        $uri = "$url$apikey/tag/$tag"
      }

      [System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }


      # Shamelessly stolen from Stackoverflow - http://stackoverflow.com/questions/20471486/how-can-i-make-invoke-restmethod-use-the-default-web-proxy
      $proxyUri = [Uri]$url
      $proxy = [System.Net.WebRequest]::GetSystemWebProxy()
      #write-host "proxy is set to [$proxy]" -backgroundcolor white -foregroundcolor red
      if ($proxy)
      {
        $proxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials
        $proxyUri = $proxy.GetProxy($proxyUri)
      }

      # if proxy server matches the url then there is not proxy server detected - crap !
      if ("$proxyUri" -eq "$url"){
        $proxyUri = $global:defaultproxy
        write-verbose "Unable to determine proxy server, going with the default one."
        $params = @{
          "Method"="Post";
          "Uri"="$uri";
          "Body"="$body";
          "SessionVariable"="Session";
          "Proxy"=$proxyUri;
        }

      }
      else{
        write-verbose "Was able to detect proxy settings on system."
        $params = @{
          "Method"="Post";
          "Uri"="$uri";
          "Body"="$body";
          "SessionVariable"="Session";
          "Proxy"=$proxyUri;
        }
      }

      write-verbose "Using the url: $url"
      write-verbose "Using the proxy: $proxyUri"
      try{
        $response = Invoke-RestMethod @params
      }
      catch{
        write-error $_.Exception.Message
        
      }
      

      $status = $($response.response)
      if($response.response -eq "OK"){
        write-verbose "Web response $status received"
      }
      else
      {
        write-error "Web response $status received"
      }
    }

    ####################### 
    function Get-Type 
    { 
      param($type) 

      $types = @( 
        'System.Boolean', 
        'System.Byte[]', 
        'System.Byte', 
        'System.Char', 
        'System.Datetime', 
        'System.Decimal', 
        'System.Double', 
        'System.Guid', 
        'System.Int16', 
        'System.Int32', 
        'System.Int64', 
        'System.Single', 
        'System.UInt16', 
        'System.UInt32', 
        'System.UInt64') 

      if ( $types -contains $type ) { 
        Write-Output "$type" 
      } 
      else { 
        Write-Output 'System.String' 

      } 
      } #Get-Type 

      ####################### 
<# 
.SYNOPSIS 
Creates a DataTable for an object 
.DESCRIPTION 
Creates a DataTable based on an objects properties. 
.INPUTS 
Object 
Any object can be piped to Out-DataTable 
.OUTPUTS 
System.Data.DataTable 
.EXAMPLE 
$dt = Get-psdrive| Out-DataTable 
This example creates a DataTable from the properties of Get-psdrive and assigns output to $dt variable 
.NOTES 
Adapted from script by Marc van Orsouw see link 
Version History 
v1.0  - Chad Miller - Initial Release 
v1.1  - Chad Miller - Fixed Issue with Properties 
v1.2  - Chad Miller - Added setting column datatype by property as suggested by emp0 
v1.3  - Chad Miller - Corrected issue with setting datatype on empty properties 
v1.4  - Chad Miller - Corrected issue with DBNull 
v1.5  - Chad Miller - Updated example 
v1.6  - Chad Miller - Added column datatype logic with default to string 
v1.7 - Chad Miller - Fixed issue with IsArray 
.LINK 
http://thepowershellguy.com/blogs/posh/archive/2007/01/21/powershell-gui-scripblock-monitor-script.aspx 
#> 
function Out-DataTable 
{ 
  [CmdletBinding()] 
  param([Parameter(Position=0, Mandatory=$true, ValueFromPipeline = $true)] [PSObject[]]$InputObject) 

  Begin 
  { 
    $dt = new-object Data.datatable   
    $First = $true  
  } 
  Process 
  { 
    foreach ($object in $InputObject) 
    { 
      $DR = $DT.NewRow()   
      foreach($property in $object.PsObject.get_properties()) 
      {   
        if ($first) 
        {   
          $Col =  new-object Data.DataColumn   
          $Col.ColumnName = $property.Name.ToString()   
          if ($property.value) 
          { 
            if ($property.value -isnot [System.DBNull]) { 
              $Col.DataType = [System.Type]::GetType("$(Get-Type $property.TypeNameOfValue)") 
            } 
          } 
          $DT.Columns.Add($Col) 
        }   
        if ($property.Gettype().IsArray) { 
          $DR.Item($property.Name) =$property.value | ConvertTo-XML -AS String -NoTypeInformation -Depth 1 
        }   
        else { 
          $DR.Item($property.Name) = $property.value 
        } 
      }   
      $DT.Rows.Add($DR)   
      $First = $false 
    } 
  }  

  End 
  { 
    Write-Output @(,($dt)) 
  } 

  } #Out-DataTable


  ####################### 
    <# 
    .SYNOPSIS 
    Writes data only to SQL Server tables. 
    .DESCRIPTION 
    Writes data only to SQL Server tables. However, the data source is not limited to SQL Server; any data source can be used, as long as the data can be loaded to a DataTable instance or read with a IDataReader instance. 
    .INPUTS 
    None 
    You cannot pipe objects to Write-DataTable 
    .OUTPUTS 
    None 
    Produces no output 
    .EXAMPLE 
    $dt = Invoke-Sqlcmd2 -ServerInstance "Z003\R2" -Database pubs "select *  from authors" 
    Write-DataTable -ServerInstance "Z003\R2" -Database pubscopy -TableName authors -Data $dt 
    This example loads a variable dt of type DataTable from query and write the datatable to another database 
    .NOTES 
    Write-DataTable uses the SqlBulkCopy class see links for additional information on this class. 
    Version History 
    v1.0   - Chad Miller - Initial release 
    v1.1   - Chad Miller - Fixed error message 
    .LINK 
    http://msdn.microsoft.com/en-us/library/30c3y597%28v=VS.90%29.aspx 
    #> 
    function Write-DataTable 
    { 
      [CmdletBinding()] 
      param( 
        [Parameter(Position=0, Mandatory=$true)] [string]$xstring, 
        [Parameter(Position=2, Mandatory=$true)] [string]$TableName, 
        [Parameter(Position=3, Mandatory=$true)] [Data.DataTable] $Data, 
        [Parameter(Position=0, Mandatory=$false)] [string]$ServerInstance, 
        [Parameter(Position=1, Mandatory=$false)] [string]$Database, 
        [Parameter(Position=4, Mandatory=$false)] [string]$Username, 
        [Parameter(Position=5, Mandatory=$false)] [string]$Password, 
        [Parameter(Position=6, Mandatory=$false)] [Int32]$BatchSize=50000, 
        [Parameter(Position=7, Mandatory=$false)] [Int32]$QueryTimeout=0, 
        [Parameter(Position=8, Mandatory=$false)] [Int32]$ConnectionTimeout=15 
        ) 


      $conn=new-object System.Data.SqlClient.SQLConnection 

      if ($Username) 
      { $ConnectionString = "Server={0};Database={1};User ID={2};Password={3};Trusted_Connection=False;Connect Timeout={4}" -f $ServerInstance,$Database,$Username,$Password,$ConnectionTimeout } 
      else 
      { $ConnectionString = "Server={0};Database={1};Integrated Security=True;Connect Timeout={2}" -f $ServerInstance,$Database,$ConnectionTimeout } 

      # force to use passed xstring
      $ConnectionString = $xstring

      $conn.ConnectionString=$ConnectionString    

      try 
      { 
        $conn.Open() 
        $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionString 
        $bulkCopy.DestinationTableName = $tableName 
        $bulkCopy.BatchSize = $BatchSize 
        $bulkCopy.BulkCopyTimeout = $QueryTimeOut 
        $bulkCopy.WriteToServer($Data) 
        $conn.Close() 
      } 
      catch 
      { 
        $ex = $_.Exception 
        Write-Error "$ex.Message" 
        continue 
      } 

      } #Write-DataTable

      function Push-WinEvents
      {
        # [CmdletBinding()] 
        param( 
          [Parameter(Mandatory=$false)][string]$ComputerName="127.0.0.1",
          [Parameter(Mandatory=$false)][string]$ProviderName,
          [Parameter(Mandatory=$false)][string]$LogName,
          [Parameter(Mandatory=$false)][string]$StartTime,
          [Parameter(Mandatory=$true)][string]$config
          ) 

        # Parse Config file and import variables
        if(test-path "$config"){
          Write-verbose "Using config file $config"
        }
        else{
          write-error "Invalid config file specified"
          
        }

        [xml]$secret = (get-content "$config")
        $global:xstring = $($secret.package.config.sql.xstring)    
        $global:table = $($secret.package.config.sql.table)    
        $global:header = $($secret.package.config.loggly.header)
        $global:apikey = $($secret.package.config.loggly.apikey)
        $global:url = $($secret.package.config.loggly.url)
        $global:bulkurl = $($secret.package.config.loggly.bulkurl)
        $global:tag = $($secret.package.config.loggly.tag)
        $global:defaultproxy = $($secret.package.config.defaultproxy)
        $global:starttime = $($secret.package.config.starttime)
        $starttime = $global:StartTime

        write-verbose "Starting Push-WinEvents on $ComputerName"
        write-verbose "LogName: $LogName ,ProviderName:  $ProviderName"

        try
        {

          ############################################## of both are null
          if(!($ProviderName) -and !($LogName)) {
            write-verbose "No filter values provided. Grabbing all operational windows events."
            $params = @{
              ProviderName="*";
              LogName="*";
              StartTime = (Get-Date).AddDays($StartTime);
            }
          }
          ############################################## of provider is null
          if(!($ProviderName) -and ($LogName)) { 
            write-verbose "Logname parameter has been defined. Grabbing all logs from `"$LogName`"" 
            $params = @{
              LogName=$LogName;
              StartTime = (Get-Date).AddDays($StartTime); 
            }
          }
          ############################################## if log is null
          if(($ProviderName) -and !($LogName)) { 
            write-verbose "Logname parameter has been defined. Grabbing all logs from `"$ProviderName`""
            $params = @{
              ProviderName=$ProviderName;
              StartTime = (Get-Date).AddDays($StartTime); 
            } 
          }
          ############################################## if neither are null
          if(($ProviderName) -and ($LogName)) { 
            write-verbose "both are defined"
            $params = @{
              ProviderName=$ProviderName;
              LogName=$LogName;
              StartTime = (Get-Date).AddDays($StartTime)
            }
          }
          ############################################## Main ##############################################

          # Step 1: Get parameters to Get-Event and store in object
          $eventparams = @{
            ErrorAction="SilentlyContinue";
            ComputerName=$computername;
            FilterHashTable=$params;
          }


          # Step 2: Perform Get-WinEvents
          try{
            #$ErrorAction="SilentlyContinue"
            $ErrorAction="Stop";
            $result = Get-WinEvent @eventparams | Select-object ProviderName,LogName,MachineName,TimeCreated,ID,Message,LevelDisplayName,OpcodeDisplayName
          }
          catch
          {
            write-verbose "Failed to perform Get-WinEvent: " $_.Exception.message
          }
          $ErrorAction="Stop";

          write-host "$($result.count)"
          

          # Step 3: Iterate through each event, convert to json, then write to log aggregator service          
          foreach($item in $result){
            write-verbose "$item"
            $json = $item | ConvertTo-Json

            $params = @{
              "header"=$global:header;  # HTTP Header for Rest Request
              "apikey"=$global:apikey;  # APIKey for webservice
              "url"=$global:url;        # Webservice URL
              "tag"="SNOW-Powershell";  # URL attribut to identify tag
              "data"=$json;             # Data to be posted to HTTP webservice
            }
            
            write-loggly @params
            Write-Debug "Successfully sent $json"

          }

          # Step 4: Write To Internal SQL Database (to be depricated due to compliance issues with plain text passwords)
          <#
          $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
          $sqlConnection.ConnectionString = $global:xstring
          $sqlConnection.Open()
          if ($sqlConnection.State -ne [Data.ConnectionState]::Open) {
            write-error "Connection to DB is not open."
            
          }

          # Step 4: Call the function that does the inserts.
          Write-DataTable -xstring $global:xstring -TableName $global:table -Data $($result | Out-DataTable)

          # Close the connection.
          if ($sqlConnection.State -eq [Data.ConnectionState]::Open) {
            $sqlConnection.Close()
          }
          #>

        }
        catch [Exception]
        {
          #write-verbose $_.Exception.Type
          write-verbose $_.Exception.ToString()
        }
        write-verbose "Script complete"
      }
<#
      $amount = (Measure-Command{
        Push-WinEvents -LogName "Application" -config ".\package.config"
        Push-WinEvents -LogName "System" -config ".\package.config"
        Push-WinEvents -LogName "Setup" -config ".\package.config"
        #Push-WinEvents -LogName "Security" -config ".\package.config"
        }).TotalSeconds
      write-verbose "Completed in $amount seconds"
      #>
      
