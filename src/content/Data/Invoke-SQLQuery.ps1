function Invoke-SQLquery{
    [CmdletBinding()] 
    param(
        [Parameter()][Alias("q")][string]$query,
        [Parameter()][Alias("xstring")][string]$connectionString
        ) 
    write-verbose "Starting function Invoke-SQLQuery"

    $connection = New-Object System.Data.SqlClient.SqlConnection
    $connection.ConnectionString = $connectionString

    $command = New-Object System.Data.SqlClient.SqlCommand
    $command.CommandText = $query     
    $command.Connection = $connection

    $sqladapter = new-object System.Data.SqlClient.SqlDataAdapter
    $sqladapter.SelectCommand = $command

    $dataset = New-Object System.Data.DataSet
    $sqladapter.fill($dataset)

    $results = $dataset.Tables[0]  
    $connection.Close()

    write-verbose "Ending function Invoke-SQLQuery"
    return $results
}

Set-Alias -name sqlquery -value Invoke-SQLquery
Export-ModuleMember -Function * -Alias *
