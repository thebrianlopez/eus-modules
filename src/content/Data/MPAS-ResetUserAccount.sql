USE [MPAS]
GO

/****** Object:  StoredProcedure [dbo].[sp_MPAS_ResetUserAccount]    Script Date: 09/29/2014 12:24:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MPAS_ResetUserAccount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MPAS_ResetUserAccount]
GO

USE [MPAS]
GO

/****** Object:  StoredProcedure [dbo].[sp_MPAS_ResetUserAccount]    Script Date: 09/29/2014 12:03:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Creates a new record in the [dbo].[Asset_Hardware] table.
CREATE PROCEDURE [dbo].[sp_MPAS_ResetUserAccount]
    @UserName nvarchar(250)
AS
DECLARE
    @statement nvarchar(4000),
    @waiting int
BEGIN
 
   
    ------------------ No need to modify below ---------------------

    -- 01 - Get the integer value of the 'waiting for approval' status, this is usually 1
    set @waiting=(select [id] from PolicyStatus where Name = 'Waiting for approval')

    -- 02 - Delete the user's security role if it exists already
    delete FROM [MPAS].[dbo].[UserAccount_SecurityRole] where UserAccountID in (select id from [MPAS].[dbo].[UserAccount] where UserName=@UserName)

    -- 03 - Delete the user's changelog otherwise the policy deletion will fail
    delete from [MPAS].[dbo].[UserAccount_Policy_ChangeLog] where UserAccountId in (select [id] from [MPAS].[dbo].[UserAccount] where UserName=@UserName)

    
    -- 04 - Set the user status back to 'waiting' as the delete will fail if the status is set to 'accepted'
    update [MPAS].[dbo].[UserAccount_Policy] 
    set PolicyStatusId=@waiting 
    where UserAccountID=(select [id] from [MPAS].[dbo].[UserAccount] where UserName=@UserName) 
    and PolicyStatusId!=@waiting

    -- 05 - find all users that have this person as a manager and reset their managers to 'admin'
    -- If their managers changed it is not an issue as they have alread yaccepted the mobile policy
    -- in the future if we need to resend the policy informaiton then we can reset *all* user accounts
    update [MPAS].[dbo].[UserAccount_Policy] 
    set Manager_UserAccountId=(select [id] from [MPAS].[dbo].[UserAccount] where UserName='Admin')
    where Manager_UserAccountId=(select [id] from [MPAS].[dbo].[UserAccount] where UserName=@UserName) 

    -- 06 - Delete the user's policy
    delete FROM [MPAS].[dbo].[UserAccount_Policy] where UserAccountId in (select [id] from [MPAS].[dbo].[UserAccount] where UserName=@UserName)
    
    -- 07 - Then finally, Delete the user account 
    delete from [MPAS].[dbo].[UserAccount] where UserName=@UserName

    -- 07a. since this is a stored procedure we will need to pass an exit code back to the user to tell
    -- if the deletion was successful or if it failed. We use the execute() function for this.
    SET @statement ='delete from [MPAS].[dbo].[UserAccount] where UserName=' + '''' + @UserName + '''';
    

    -- 07b. Run the execute
    EXECUTE (@statement)

    -- 07c. Use the global variable rowcount to pass back the number of affected rows
    print N'Running the statement: ' + @statement + ' completed with row changed: ' + cast(@@ROWCOUNT as nvarchar(250));

    ---- Confirm record deletion by doing a select, should always result to '0' ----
    -- select count(*) as 'Result' from [MPAS].[dbo].[UserAccount] where UserName=@UserName

END

GO

-- Test Code --
-- exec [MPAS].[dbo].[sp_MPAS_ResetUserAccount] @UserName='usscottjo'







