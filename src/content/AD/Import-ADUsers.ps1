# Script Name: Import-ADUsers.ps1
# Date Created: 2013-03-11 14:08PM PST
# Description: Imports a list of AD users into a SQL table
# Revisions: 
#    2014-02-27: Created. 
#    
#
#
# ToDo:
#   2014-04-14-2014: Update function to gather all user data then do 1 bulk insert. today it does one insert per user which is horribly inefficient
#       
#
#

####################### 
function Get-Type 
{ 
    param($type) 
 
$types = @( 
'System.Boolean', 
'System.Byte[]', 
'System.Byte', 
'System.Char', 
'System.Datetime', 
'System.Decimal', 
'System.Double', 
'System.Guid', 
'System.Int16', 
'System.Int32', 
'System.Int64', 
'System.Single', 
'System.UInt16', 
'System.UInt32', 
'System.UInt64') 
 
    if ( $types -contains $type ) { 
        Write-Output "$type" 
    } 
    else { 
        Write-Output 'System.String' 
         
    } 
} #Get-Type 
 
####################### 
<# 
.SYNOPSIS 
Creates a DataTable for an object 
.DESCRIPTION 
Creates a DataTable based on an objects properties. 
.INPUTS 
Object 
    Any object can be piped to Out-DataTable 
.OUTPUTS 
   System.Data.DataTable 
.EXAMPLE 
$dt = Get-psdrive| Out-DataTable 
This example creates a DataTable from the properties of Get-psdrive and assigns output to $dt variable 
.NOTES 
Adapted from script by Marc van Orsouw see link 
Version History 
v1.0  - Chad Miller - Initial Release 
v1.1  - Chad Miller - Fixed Issue with Properties 
v1.2  - Chad Miller - Added setting column datatype by property as suggested by emp0 
v1.3  - Chad Miller - Corrected issue with setting datatype on empty properties 
v1.4  - Chad Miller - Corrected issue with DBNull 
v1.5  - Chad Miller - Updated example 
v1.6  - Chad Miller - Added column datatype logic with default to string 
v1.7 - Chad Miller - Fixed issue with IsArray 
.LINK 
http://thepowershellguy.com/blogs/posh/archive/2007/01/21/powershell-gui-scripblock-monitor-script.aspx 
#> 
function Out-DataTable 
{ 
    [CmdletBinding()] 
    param([Parameter(Position=0, Mandatory=$true, ValueFromPipeline = $true)] [PSObject[]]$InputObject) 
 
    Begin 
    { 
        $dt = new-object Data.datatable   
        $First = $true  
    } 
    Process 
    { 
        foreach ($object in $InputObject) 
        { 
            $DR = $DT.NewRow()   
            foreach($property in $object.PsObject.get_properties()) 
            {   
                if ($first) 
                {   
                    $Col =  new-object Data.DataColumn   
                    $Col.ColumnName = $property.Name.ToString()   
                    if ($property.value) 
                    { 
                        if ($property.value -isnot [System.DBNull]) { 
                            $Col.DataType = [System.Type]::GetType("$(Get-Type $property.TypeNameOfValue)") 
                         } 
                    } 
                    $DT.Columns.Add($Col) 
                }   
                if ($property.Gettype().IsArray) { 
                    $DR.Item($property.Name) =$property.value | ConvertTo-XML -AS String -NoTypeInformation -Depth 1 
                }   
               else { 
                    $DR.Item($property.Name) = $property.value 
                } 
            }   
            $DT.Rows.Add($DR)   
            $First = $false 
        } 
    }  
      
    End 
    { 
        Write-Output @(,($dt)) 
    } 
 
} #Out-DataTable


####################### 
<# 
.SYNOPSIS 
Writes data only to SQL Server tables. 
.DESCRIPTION 
Writes data only to SQL Server tables. However, the data source is not limited to SQL Server; any data source can be used, as long as the data can be loaded to a DataTable instance or read with a IDataReader instance. 
.INPUTS 
None 
    You cannot pipe objects to Write-DataTable 
.OUTPUTS 
None 
    Produces no output 
.EXAMPLE 
$dt = Invoke-Sqlcmd2 -ServerInstance "Z003\R2" -Database pubs "select *  from authors" 
Write-DataTable -ServerInstance "Z003\R2" -Database pubscopy -TableName authors -Data $dt 
This example loads a variable dt of type DataTable from query and write the datatable to another database 
.NOTES 
Write-DataTable uses the SqlBulkCopy class see links for additional information on this class. 
Version History 
v1.0   - Chad Miller - Initial release 
v1.1   - Chad Miller - Fixed error message 
.LINK 
http://msdn.microsoft.com/en-us/library/30c3y597%28v=VS.90%29.aspx 
#> 
function Write-DataTable 
{ 
    [CmdletBinding()] 
    param( 
    [Parameter(Position=0, Mandatory=$true)] [string]$xstring, 
    [Parameter(Position=2, Mandatory=$true)] [string]$TableName, 
    [Parameter(Position=3, Mandatory=$true)] [Data.DataTable] $Data, 
    [Parameter(Position=0, Mandatory=$false)] [string]$ServerInstance, 
    [Parameter(Position=1, Mandatory=$false)] [string]$Database, 
    [Parameter(Position=4, Mandatory=$false)] [string]$Username, 
    [Parameter(Position=5, Mandatory=$false)] [string]$Password, 
    [Parameter(Position=6, Mandatory=$false)] [Int32]$BatchSize=50000, 
    [Parameter(Position=7, Mandatory=$false)] [Int32]$QueryTimeout=0, 
    [Parameter(Position=8, Mandatory=$false)] [Int32]$ConnectionTimeout=15 
    ) 
    
    $conn=new-object System.Data.SqlClient.SQLConnection 
 
    if ($Username) 
    { $ConnectionString = "Server={0};Database={1};User ID={2};Password={3};Trusted_Connection=False;Connect Timeout={4}" -f $ServerInstance,$Database,$Username,$Password,$ConnectionTimeout } 
    else 
    { $ConnectionString = "Server={0};Database={1};Integrated Security=True;Connect Timeout={2}" -f $ServerInstance,$Database,$ConnectionTimeout } 
 
    # force to use passed xstring
    $ConnectionString = $xstring
    
    $conn.ConnectionString=$ConnectionString    
 
    try 
    { 
        $conn.Open() 
        $bulkCopy = new-object ("Data.SqlClient.SqlBulkCopy") $connectionString 
        $bulkCopy.DestinationTableName = $tableName 
        $bulkCopy.BatchSize = $BatchSize 
        $bulkCopy.BulkCopyTimeout = $QueryTimeOut 
        $bulkCopy.WriteToServer($Data) 
        $conn.Close() 
    } 
    catch 
    { 
        $ex = $_.Exception 
        Write-Error "$ex.Message" 
        continue 
    } 
 
} #Write-DataTable

function Import-ActiveDirectory
{

    # Global GTS Users
    # $ou="OU=AMS,OU=Organizations,DC=nestle,DC=com" #ou to limit query to otherwise it will take forever
    $gtsgroup = "GLOBEGTSAll" #win2000 name of adgroup
    # $xstring = "Server=usstni9989.nestle.com\pspac01,40031;Database=SMS_GLB;Integrated Security=false;" #remote db connection string using built-in auth
    # $sqltablename="nestle_activedirectory_gtsna" #sql table name


    # NAR GTS Users
    $ou="OU=AMS,OU=Organizations,DC=nestle,DC=com" #ou to limit query to otherwise it will take forever
    # $gtsgroup = "AMSNAITAllPeopleInternalandExternalContractors" #win2000 name of adgroup
    $xstring = "Server=usstni9989.nestle.com\pspac01,40031;Database=SMS_GLB;Integrated Security=false;User ID=migrationwizard;Password=migrationwizard;"
    $sqltablename="nestle_activedirectory_gtsna" #sql table name
    
    # Step 1: Get list adusers that are in the said group that are of class 'user'
    $users = Get-ADGroupMember $gtsgroup -recursive | where {$_.objectclass-eq "user"} 

    foreach($s in $users)
    {
        try
        {
            # Step 2: Get each user in the collection and gather these properties
            $dt = Get-ADUser $s -Properties * | Select-Object -Property cn,displayName,distinguishedName,givenName,sn,userPrincipalName,description,OfficePhone,initials,mail,title,physicalDeliveryOfficeName,wWWHomePage,homePhone,facsimileTelephoneNumber,mobile,pager,streetAddress,l,st,postalCode,profilePath,scriptPath,info,department,company,postOfficeBox,SiteCode,homeDirectory | out-DataTable

            # Step 3 : Setup db to write data
            $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
            $sqlConnection.ConnectionString = $xstring
            
            $sqlConnection.Open()
            if ($sqlConnection.State -ne [Data.ConnectionState]::Open) {
                "Connection to DB is not open."
                Exit
            }
        
            # Step 4: Call the function that does the inserts.
             Write-DataTable -xstring $xstring -TableName $sqltablename -Data $dt
        
            # Close the connection.
            if ($sqlConnection.State -eq [Data.ConnectionState]::Open) {
                $sqlConnection.Close()
            }    
        }
        catch [Exception]
        {
            Write-Host $_.Exception.ToString()
        }
    }
    
}

Import-ActiveDirectory

# Get-ADGroupMember $gtsgroup -recursive | Get-ADUser -Filter * -SearchBase $ou -Properties * | Select-Object -Property cn,displayName,distinguishedName,givenName,sn,userPrincipalName,description,OfficePhone,initials,mail,title,physicalDeliveryOfficeName,wWWHomePage,homePhone,facsimileTelephoneNumber,mobile,pager,streetAddress,l,st,postalCode,profilePath,scriptPath,info,department,company,postOfficeBox,SiteCode,homeDirectory | Out-File c:\Temp\ADUsers.csv

