. ".\DataTable.ps1"
. ".\SQLTable.ps1"

function Export-ADSitesSubnets{
    param(
        [Parameter()][string]$context=(Get-ADRootDSE).ConfigurationNamingContext
    )    

    $results = @()
    $siteContainerDN = ("CN=Sites," + $context)
    $sites = Get-ADObject -SearchBase $siteContainerDN -filter { objectClass -eq "site" } -properties "siteObjectBL", "location", "description" | select -property Name,DistinguishedName,siteObjectBL 
    foreach ($site in $sites){ #each site in sites
        foreach ($ipobject in $site.siteObjectBL){ #each subnet in sites
            $props = @{
                "Subnet"=((Get-adobject -SearchBase $ipobject -filter { objectClass -eq "subnet" } -properties * ).cn);
                "ADSiteName"=$site.Name;
            }
            $results += New-Object -TypeName PSOBject -Property $props
            $props = $null
        }
    }
    return $results
}
