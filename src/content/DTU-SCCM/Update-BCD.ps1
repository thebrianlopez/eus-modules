$VerbosePreference="Continue"
set-alias -name wr -value write-host


function update-bcd{
    param(
        [string]$bcd,
        [string]$wim,
        [string]$device
        )
    rm -rec -for "$bcd*"
    Start-CustomProcess "-createstore `"$bcd`""


    # Ramdisk
    Start-CustomProcess "-store `"$bcd`" -create {ramdiskoptions} /d `"Ramdisk Options`""
    Start-CustomProcess "-store `"$bcd`" -set {ramdiskoptions} ramdisksdidevice boot"
    Start-CustomProcess "-store `"$bcd`" -set {ramdiskoptions} ramdisksdipath \boot.sdi"
    # Start-CustomProcess "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 4096"
    # Start-CustomProcess "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 8192"
    Start-CustomProcess "-store `"$bcd`" -set {ramdiskoptions} ramdisktftpblocksize 16384"

    # debug
    Start-CustomProcess "-store `"$bcd`" -create {dbgsettings} /d `"Debugger Settings`""
    # Start-CustomProcess "-store `"$bcd`" -set {dbgsettings} debugtype serial"
    # Start-CustomProcess "-store `"$bcd`" -set {dbgsettings} baudrate 115200"
    # Start-CustomProcess "-store `"$bcd`" -set {dbgsettings} debugport 1"



    # OSLoader
    Start-CustomProcess "-store `"$bcd`" -create /d `"WinPE 6.2`" /application osloader"
    $guid = (iex "bcdedit.exe -store `"$bcd`" " | select-string "identifier").tostring().replace("identifier","").trim()
    wr "My guid is $guid"
    Start-CustomProcess "-store `"$bcd`" -set $guid systemroot \Windows"
    Start-CustomProcess "-store `"$bcd`" -set $guid detecthal Yes"
    Start-CustomProcess "-store `"$bcd`" -set $guid winpe yes"
    Start-CustomProcess "-store `"$bcd`" -set $guid osdevice ramdisk=$device,{ramdiskoptions}"
    Start-CustomProcess "-store `"$bcd`" -set $guid device ramdisk=$device,{ramdiskoptions}"
    Start-CustomProcess "-store `"$bcd`" -set $guid systemroot \Windows"
    Start-CustomProcess "-store `"$bcd`" -set $guid debug on"


    # Boot Manager
    Start-CustomProcess "-store `"$bcd`" -create {bootmgr} /d `"Boot Manager`" "
    # Start-CustomProcess "-store `"$bcd`" -create {bootmgr} /d `"Boot Manager`" inherit {dbgsettings}"
    Start-CustomProcess "-store `"$bcd`" -set {bootmgr} timeout 30"
    Start-CustomProcess "-store `"$bcd`" -set {bootmgr} inherit {dbgsettings}"
    Start-CustomProcess "-store `"$bcd`" -set {bootmgr} bootdebug on"
    Start-CustomProcess "-store `"$bcd`" -set {dbgsettings} advancedoptions true"

    Start-CustomProcess "-store `"$bcd`" -displayorder $guid"


    # Dump settings
    Start-CustomProcess "-store `"$bcd`" -enum all"
}

function Start-CustomProcess {
    param(
        [string]$arg
        )
    $cmd = "$env:systemroot\system32\bcdedit.exe"
    $StartInfo = new-object System.Diagnostics.ProcessStartInfo
    $StartInfo.FileName = "$cmd"
    $StartInfo.Arguments = "$arg"
    $StartInfo.UseShellExecute = $false
    $StartInfo.WorkingDirectory = "$env:systemdrive"
    write-verbose "Running: $cmd $arg"
    [int]$exitCode = [Diagnostics.Process]::Start($StartInfo).WaitForExit();$arg = $null 

    if($exitCode -eq 0){
        write-verbose "command complete successfully."
        $results = $true
    }
    else{
        write-error "command existed with code $($_.Exception.Message.toString())" -ErrorAction Stop
        throw
    }
}

#update-bcd -bcd "P:\ZTI\ci\1e-tftp-root\Images\D00001A4\boot.D00001A4.bcd" -wim "P:\ZTI\ci\1e-tftp-root\Images\D00001A4\boot.D00001A4.wim" -device "[boot]\Images\D00001A4\boot.D00001A4.wim"
