# Name: Nestle-Modules
# Author: Brian Lopez (brian.lopez@purina.nestle.com)
# Description: Performs helper options used for managing the nestle.cloud environment
# Added pending reboot script
# $thispath=Get-Location
$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)


function Get-ScriptDirectory
{ 
    if($hostinvocation -ne $null)
    {
        Split-Path $hostinvocation.MyCommand.path
    }
    else
    {
        Split-Path $script:MyInvocation.MyCommand.Path
    }
}
 
 
 
 
function New-CustomIISSite{
    PARAM(
        [Parameter(Mandatory=$True)][string]$SiteName,
        [Parameter(Mandatory=$True)][string]$ServiceAccountName,
        [Parameter(Mandatory=$false)][string]$IdentityPassword,
        [Parameter(Mandatory=$True)][string]$AuthRoleName,
        [Parameter(Mandatory=$True)][string]$folderpath,
        [Parameter(Mandatory=$True)][string]$uri,
        [Parameter(Mandatory=$True)][int]$httpport,
        [Parameter(Mandatory=$True)][int]$sslport,
        [Parameter(Mandatory=$True)][string]$ipaddress,
        [Parameter(Mandatory=$True)][string]$CertName="*EUCREDServ*",
        [Parameter(Mandatory=$false)][string]$managedRunTimeVersion="v4.0",
        [Parameter(Mandatory=$True)][string]$AppPoolName="My App Pool"
    )

    $results = $false
    $uncUri = $uri.replace("/","\")
 
    try{
        write-verbose "Entered Function: New-CustomIISSite"
 
        #Create App Pool
        write-verbose "Attempting to create Application Pools"

        write-verbose "Checking if App Pool: $AppPoolName already exists"
        $CheckAppPool = Get-ChildItem IIS:\AppPools | where{$_.name -eq $AppPoolName}

        if(-not $CheckAppPool){
            write-verbose "Creating App Pool: $AppPoolName"
            new-item IIS:\AppPools\$AppPoolName |Out-Null
        }
        

        write-verbose "Checking to see if the apppool is already configured with this user account"
        $config = (Get-ChildItem IIS:\AppPools | where{$_.name -eq $AppPoolName})


        if($config.processmodel.userName -eq "$($env:USERDOMAIN)\$ServiceAccountName" ){
            write-verbose "Apppool $AppPoolName is already configured for specific user $($config.processmodel.username) , skipping ..."
        }
        else{
            write-verbose "Setting/overriding Identity and Runtime version"
            Set-ItemProperty IIS:\AppPools\$AppPoolName -name processModel -value @{userName="$($env:USERDOMAIN)\$ServiceAccountName";password=$IdentityPassword;identityType=3}
        }

        if($config.managedRunTimeVersion -eq "$managedRunTimeVersion" ){
            write-verbose ".NET runtime is already set to $managedRunTimeVersion for the app pool $AppPoolName"

        }
        else{
            write-verbose "Setting .NET runtime to $managedRunTimeVersion for the app pool $AppPoolName"
            Set-ItemProperty IIS:\AppPools\$AppPoolName -name managedRunTimeVersion -value $managedRunTimeVersion
        }

        #Create Websites
        write-verbose "Attempting to create Websites"
        $BoolBug = $false
        if(-not $(get-website)){
            write-verbose "No websites exist. Creaing temp website due to PSH bug"
            new-website -name BUG -id 0 -physicalpath "C:\inetpub\temp" |Out-Null
            $BoolBug = $true
        }
 
        write-verbose "Checking if Website: $SiteName already exists"
        $CheckWebSite1 = Get-Website| where{$_.name -eq $SiteName}
        if(-not $CheckWebSite1){
            write-verbose "Creating website: $SiteName"
            #new-website -name $SiteName -physicalpath "$rootpath\EUCWebServ" -IPAddress 10.54.248.71 -Port $httpport -ApplicationPool $SiteName
            new-item iis:\Sites\$SiteName -bindings @{protocol="http";bindinginformation=$IPAddress + "`:$httpport`:"} -physicalpath "$folderpath" |Out-Null
            new-webbinding -Name $SiteName -IP $IPAddress -port $sslport -protocol https
            Set-ItemProperty iis:\Sites\$SiteName -Name applicationPool -value $AppPoolName
        }
 
        #Create Web Applications
        write-verbose "Attempting to create Web Applications"
        write-verbose "Checking if Webapp: `'$uri`' already exists"

        $CheckApplication1 = Get-WebApplication |where{$_.path -eq $uri}

        if(-not $CheckApplication1){
            write-verbose "Creating webapp: $SiteName"
            new-item IIS:\Sites\$SiteName\$uncuri -physicalpath $folderpath -type Application |out-null
        }

        write-verbose "Setting/overriding ApplicationPool setting for webapp"
        Set-ItemProperty IIS:\Sites\$SiteName\$uncuri -name applicationPool -value $AppPoolName
 
        #Set Authentication rules
        write-verbose "Configuring Authentication rules for websites" 
        Set-WebConfigurationProperty -pspath iis:\ -Filter /system.webServer/security/authentication/AnonymousAuthentication -name enabled -value true -Location $SiteName
        Set-WebConfigurationProperty -pspath iis:\ -Filter /system.webServer/security/authentication/WindowsAuthentication -name enabled -value false -Location $SiteName
        Set-WebConfigurationProperty -pspath iis:\ -Filter /system.webServer/security/authentication/basicAuthentication -name enabled -value false -Location $SiteName
 
        #Create Authorization rules
        write-verbose "Creating Authorization rules for websites"
        clear-WebConfiguration -filter '/system.webserver/security/authorization' -pspath IIS:\Sites\$SiteName
        Add-WebConfiguration -pspath iis:\Sites\$SiteName "/system.webServer/security/authorization" -Value @{accessType="Allow";roles="$AuthRoleName"} 
        
        write-verbose "Creating MIMTypes rules for websites"
        clear-WebConfiguration -filter '/system.webserver/staticContent' -pspath IIS:\Sites\$SiteName
        Add-WebConfiguration -pspath iis:\Sites\$SiteName "/system.webserver/staticContent" -Value @{fileExtension=".md";mimeType="text/plain"}
        Add-WebConfiguration -pspath iis:\Sites\$SiteName "/system.webserver/staticContent" -Value @{fileExtension=".log";mimeType="text/plain"}

        # removed as this isnt working
        $msp = Get-WebConfiguration -pspath iis:\Sites\$SiteName "/system.webserver/directoryBrowse" | where {$_.enabled -eq 'true'}
        if ($msp -eq $null) 
        {
            write-verbose "Enabling directory browsing"
            clear-WebConfiguration -filter '/system.webserver/directoryBrowse' -pspath IIS:\Sites\$SiteName
            #Add-WebConfiguration -pspath iis:\Sites\$SiteName "/system.webserver/directoryBrowse" -Value @{enabled="true"}
            Set-WebConfigurationProperty -pspath iis:\ -Filter /system.webServer/directoryBrowse -name enabled -value true -Location $SiteName
        }
        

     

        write-verbose "Checking if Certicate is already bound to $SiteName with ip $ipaddress"
        $cert = (dir IIS:\SslBindings | where {$_.ipaddress -eq $ipaddress})
        if($cert){
            write-verbose "A certificate is already associated with the IP Address: $ipaddress"
        }
        else{
            write-verbose "Certficate not found, attempting to find certificate with subject like $CertName"
            $cert = (dir Cert:\LocalMachine\My | where {$_.subject -like "$CertName"} | Select-Object -first 1) | out-null
            if($cert){
                write-verbose "Certificate found. Binding to website"
                new-item -path "iis:\sslbindings\$ipaddress!$(sslport)" -value $EUCREDcert  |out-null
            }
            else{
                write-warning "Unable to bind to SSL certificate, please assign manually." -erroraction SilentlyContinue
            }
        }

       $CheckBUG = Get-Website| where{$_.name -eq "BUG"}
       if($CheckBUG){Remove-Website -Name BUG}
       $results = $true

    }
    catch{
        write-error $_.exception.message -erroraction Stop
        $results = $False
    }
    finally{
        write-verbose "Exiting function: New-CustomIISSite"
    }

    return $results
 
}
 
Function New-LocalGroup{
    PARAM(
        $GroupName
            )
 
    try{
        write-verbose "Entered Function: New-LocalGroup"
        $comp = [ADSI]"WinNT://$($env:computername)"
 
        $testgroup = $comp.Children | where {$_.class -eq "group" -and $_.name -eq $GroupName}
        if($testgroup){
            write-verbose "Group already exists"
            return $true
        }
        else{
            $oUser = $comp.create("Group",$GroupName)
            $oUser.Put("Description","Use this group to control access to the EUC web services hosted on IIS")
            $oUser.SetInfo()
            write-verbose "Succesfully created group"
        }
        return $true
        }
    catch{
        write-error $_.exception.message -erroraction Stop
        return $false
    }
    finally{
        write-verbose "Exising function: New-LocalGroup"
    }
}
 
Function Invoke-AddtoLocalGroup{
    PARAM(
        [string]$LocalGroup,
        [string[]]$Members,
        [switch]$IsGroup
    )
    
    foreach($user in $Members){
        write-verbose "Confiming user account $user exists is AD"

        if((Get-AdUser -erroraction Stop -filter { name -eq $user} | measure-object | select-object count).count -gt 0){
            write-verbose "Account exists"
        }
        else {
            write-error "The ad account $user does not exist." -erroraction Stop
        }
    }
   

    try{

        write-verbose "Entered Function: Invoke-AddtoLocalGroup"

        foreach($Member in $members){
            write-verbose "Found $member in $members"
            $boolfound = $false
            $testmembership = [ADSI]"WinNT://$($env:COMPUTERNAME)/$Localgroup"
            foreach($user in $testmembership.psbase.Invoke("Members")){
                write-verbose "Found the user $user in the local group $localgroup"
                if($($user.gettype().invokemember("Name","GetProperty",$null,$user,$null)) -eq $Member){
                    write-verbose "$Member is already a member of the group $localgroup"
                    $boolfound = $true
                    break
                }
            }

            if($boolfound -eq $false){
                write-verbose "Confirming user $Members exists in Active Directory"
                if($IsGroup){
                    $NAA = [ADSI]"WinNT://$($env:USERDOMAIN)/$Member,Group"
                    }
                else{
                    $NAA = [ADSI]"WinNT://$($env:USERDOMAIN)/$Member,User"
                    }
                $testmembership.Add($NAA.path)
                write-verbose "Successfully added $Member to the group $localgroup"
 
            }
        }
       return $true
    }
    catch{
        write-error $_.exception.message -erroraction Stop
        return $false
    }
    finally{
        write-verbose "Exising function: Invoke-AddtoLocalGroup"
    }
 
}
 
Function Set-StringsInFile{
    PARAM(
        $File,
        $readstring,
        $writestring
        )
    (Get-Content $File) | 
    Foreach-Object {$_ -replace $readstring, $writestring} | 
    Set-Content $File
 
 
}
 
 
Function New-IISWebsite{
    Param(
        [Parameter(Mandatory=$True,HelpMessage="The IP address to be associated with EUC Redirector IIS SIte")][string]$ipaddress,
        [Parameter(Mandatory=$false,HelpMessage="The password for the EUCWebServ account that will be used by IIS when connecting to MDT,SCCM and AD")][string]$IdentityPassword,
        [Parameter(Mandatory=$False,HelpMessage="Users or groups to add to be granted permissions to access the webservices")][string[]]$members = "GLBSMSNetworkAccess",
        [Parameter(Mandatory=$False,HelpMessage="Path to app source containing web.config file")][string]$folderpath = "c:\inetpub\app",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$websitename = "I Forgot to specify the app name",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$localgroup = "TestApplication",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$uri = "app/foo/bar",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$authrolename = "WebAuthRole",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$CertName = "CertName",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][string]$AppPoolName = "AppPoolName",
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][bool]$CreateSamplePage = $false,
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][int]$httpport = 80,
        [Parameter(Mandatory=$False,HelpMessage="Website Name")][int]$sslport = 443
    )
    $ScriptDirectory = Get-ScriptDirectory
 

    write-verbose "*** Checking if WebAdministration module is available on this system ***"
 
    #Check if IIS Module is available
    if(get-module -listavailable | where{$_.name -eq "WebAdministration"}){
        write-verbose "Module found. Importing"
        Import-module WebAdministration
    }
    else{
        write-error "WebAdministration Module is not available on this system. Cannot continue" -erroraction Stop
    }

    #Check if IIS Module is available
    if(get-module -listavailable | where{$_.name -eq "ActiveDirectory"}){
        write-verbose "Module found. Importing"
        Import-module ActiveDirectory
    }
    else{
        write-error "ActiveDirectory Module is not available on this system. Cannot continue" -erroraction Stop
    }
 
    write-verbose "*** Creating folder structure for webservices: $rootpath ***"
 
    #Create Folder structure
    if($(test-path $folderpath) -eq $false){
        write-verbose "$folderpath does not exist. Creating..."
        new-item -path $folderpath -itemtype Directory
    }
    else{write-verbose "$folderpath already exists"}
 
    write-verbose "*** Creating local group: EUCWebServiceAccess ***"
    
    $boolcreategroup = New-LocalGroup $localgroup
    if($boolcreategroup -eq $true){
        write-verbose "Successfully created local group"
    }
    else{
        write-error "There was an error when creating the local group." -erroraction Stop
    }
     
    write-verbose "*** Adding account to the local EUCWebServiceAccess group ***"
    write-verbose "Adding the following members $Members to the local group $localgroup"
    $booladdtogroup = Invoke-AddtoLocalGroup -LocalGroup $localgroup -Members $Members
    if($booladdtogroup -eq $true){
        write-verbose "Successfully added the NAA to the local group"
    }
    else{
        write-error "There was an error adding members to local group." -erroraction Stop
    }

    $params = @{
        "SiteName"="$websitename"
        "ServiceAccountName"="$members"
        "IdentityPassword"="$IdentityPassword"
        "folderpath"="$folderpath"
        "httpport"="$httpport"
        "sslport"="$sslport"
        "uri"="$uri"
        "authrolename"="$authrolename"
        "ipaddress"="$ipaddress"
        "CertName"="$CertName"
        "AppPoolName"="$AppPoolName"
    }
        
    write-verbose "*** Configuring IIS ***"
    $boolIISConfig = New-CustomIISSite @params


    if($boolIISConfig -eq $true){
        write-verbose "Successfully configured IIS"
        if($CreateSamplePage -eq $true){
            write-verbose "Creating sample web page"
            $contents = "`<`%`@ Page Trace`=`"true`"  Language`=`"C`#`" ContentType`=`"text`/html`" ResponseEncoding`=`"utf-8`" `%`>"
            $contents | out-file "$folderpath\default.aspx"

        }
    }
    else{
        write-error "There was an error configuring IIS." -erroraction Stop
    }


    #Check if IIS Module is available
    if(get-module -listavailable | where{$_.name -eq "NetSecurity"}){
	    # Open http Firewall Ports
	    write-verbose "Opening Windows Firewall to allow ports out"
	    $res = (get-NetFirewallRule | where { ($_.DisplayName  -like "*$httpport*") -and($_.DisplayName  -like "*$websitename*")})


	    if($res.count -gt 0){
	        write-verbose "firewall is already configured for port $httpport"
	    }
	    else {
	        write-verbose "Configuring firewall rule for $websitename"
	        New-NetFirewallRule -DisplayName "Allow $websitename through port $httpport" -Protocol tcp -localport $httpport -action block | out-null
	        Set-NetFirewallRule -DisplayName "Allow $websitename through port $httpport" -protocol tcp -localport $httpport -action allow | out-null
	    }

	    # Open http Firewall Ports
	    write-verbose "Opening Windows Firewall to allow ports out"
	    $res = (get-NetFirewallRule | where { ($_.DisplayName  -like "*$sslport*") -and($_.DisplayName  -like "*$websitename*")})
	    
	    if($res.count -gt 0){
	        write-verbose "firewall is already configured for port $sslport"
	    }
	    else {
	        write-verbose "Configuring firewall rule for $websitename"
	        New-NetFirewallRule -DisplayName "Allow $websitename through port $sslport" -Protocol tcp -localport $sslport -action block | out-null
	        Set-NetFirewallRule -DisplayName "Allow $websitename through port $sslport" -protocol tcp -localport $sslport -action allow | out-null
	    }
    }
    else{
    	write-verbose "Unable to find netsecurity modules, skipping firewall configuration."
    }

}

