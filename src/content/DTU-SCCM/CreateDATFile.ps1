﻿$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)

Function Export-CustomCMIso{
    PARAM(
        [string]$ProviderName,
        [string]$ProviderSiteCode,
        [string]$DistributionPoint,
        [string]$Password,
        [string]$PackageID
    )

    Write-Verbose "Parsing params"
    write-verbose "ProviderName = $ProviderName"
    write-verbose "ProviderSiteCode = $ProviderSiteCode"
    write-verbose "DistributionPoint = $DistributionPoint"
    write-verbose "Password = $Password"
    write-verbose "PackageID = $PackageID"
    $ErrorAction="Stop"
    $bootimgpath="$($env:temp)\bootimage.iso"
    write-verbose "bootimgpath = $bootimgpath"
    write-verbose "Calling CreateBootMedia method to create $bootimgpath"

    If(Test-Path "$bootimgpath"){
        Write-Verbose "File exists, removing"
	    Remove-Item "$bootimgpath"
    }
    else{
        Write-Verbose "File doesnt already exists, creating"
    }

    $chararr = [System.Text.Encoding]::UTF8.GetBytes($Password)
    $encodedpassword = [system.convert]::ToBase64String($chararr)
    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.ConfigMgr.TsMedia")
    $tsmedia = New-Object -ComObject Microsoft.ConfigMgr.TsMedia
    $VBControl = New-Object -ComObject ScriptControl


    $tsmedia.ProviderName = $ProviderName
    $tsmedia.SiteCode = $ProviderSiteCode
    $tsmedia.DistributionPoints = $DistributionPoint
    $tsmedia.ConnectionOptions = ""
    $tsmedia.MediaLabel = $encodedpassword
    $tsmedia
    
    $VBControl.language = "VBScript"
    $vbnull = $VBControl.Eval("NULL")
    $ErrorAction="Stop"
    
    [datetime]$startdate = (get-date)
    [datetime]$findate = $((get-date).addyears(10))
	write-verbose "Calling createbootmedia api"
    try{
        # $tsmedia.createbootmedia("CD","$($env:temp)\Bootimage.iso",$PackageID,"",$vbnull,$vbnull,$startdate,$findate,$true)
        $tsmedia.createbootmedia("CD",$bootimgpath,$PackageID,"",$vbnull,$vbnull,$startdate,$findate,$true)
    }
    catch{
        write-verbose "Thre was an exception calling CreateBootMedia COM Method."
        write-error $_.Exception.Message
        exit
    }

    
   
    do{
        write-verbose "$($tsmedia.CurrentStep)/$($tsmedia.numsteps) ($($tsmedia.StepProgress)) $($tsmedia.StepInfo)"
    }
    while($tsmedia.status -eq 0)
    
    If(Test-Path "$bootimgpath"){
        Write-Verbose "File exists, removing"
        Remove-Item "$bootimgpath"
    }
    else{
        Write-error "File generation failed"
        exit
    }

   write-verbose "Boot Media creation finished with result $($tsmedia.exitcode)"
   if($TSMedia.ExitCode -eq 0){return $true}
   else{$false}

    
   
}

Function Export-CustomCMISODatFile{
    PARAM(
        [string]$DestinationPath
    )

    If(Test-Path "$DestinationPath\SMS"){
	    Remove-Item -Recurse -Force "$DestinationPath\SMS"
    }

    $ps = new-object System.Diagnostics.Process
    $ps.StartInfo.Filename = "$($thismoddir)\7z.exe"
    $ps.StartInfo.Arguments = "x $($env:temp)\Bootimage.iso sms\data -o$DestinationPath"
    $ps.StartInfo.RedirectStandardOutput = $True
    $ps.StartInfo.UseShellExecute = $false
    $ps.start()
    $ps.WaitForExit()
    [string]$Out = $ps.StandardOutput.ReadToEnd();
    #write-verbose $Out
    if($ps.ExitCode -eq 0){
        if($out.contains("error")){return $false}
        else{return $true}
        }
    else{return $false}
}

Function Main
{
    <#

        .SYNOPSIS 
        This powershell script automates the creation of a VARIABLES.DAT (contained in the SMS\Data directory( file to be used by the SNOW5 OSD Deployment process.
        It replaces the 1e tool UpdateBootImage.exe as it provides support for the MediaLabel variable.

        .DESCRIPTION 
        Using the TSMediaAPI.dll file this script will generate SCCM Boot Media using the parameters defined on the commandline.
        Once the ISO Image is created the script will then use the GNU tool 7z.exe to extract the SMS\DATA Folder which contains the VARIABLES.DAT file (among others).
        This folder must then be manually injected into the root of the boot.wim file of the appropriate boot media package.

        Of particular impotance is the -PXEBootPassword switch. 
        This parameter contains the password for all webservice calls performed as part of the PXE Boot process.
        The script will encode the password into BASE64 and add it to the VARIABLES.DAT file.

        If no parameters are defined for this script the user will be prompted to enter the information at run time.

        Parameters:
            -ProviderName - The server to connect to when querying SCCM for the package information
            -PorviderSiteCode - The sitecode of the Providerserver
            -DistributionPoint - The location of the bootimage file
            -BootImagePackageID - The bootimage package ID to be used by the boot media
            -PXEBootPassword - THe password to be injected into the VARIABLES.DAT for webservice calls
            -DestinationPath - Location to store the SMS Folder which contains most importantly the VARIABLES.DAT File.
            -verbose - increases logging activity

        .EXAMPLE 
        ./CreateDATFile.PS1

        .NOTES 
        This script requires the following:
            1. 32bit command prompt or 32bit system
            2. A system with the SCCM Console installed

        This script supports the -verbose switch for logging.

        .LINK 
        http://www.neslte.com

        #>


        [CmdletBinding()]
        Param(
          [Parameter(Mandatory=$false)][string]$ProviderName="HQAAAM0300.nestle.w2k",
          [Parameter(Mandatory=$false)][string]$ProviderSiteCode="D00",
          [Parameter(Mandatory=$false)][string]$DistributionPoint="\\hqaaam0011.nestle.w2k\zti$\PXEBoot\5.1.20140829\WinPE.wim",
          [Parameter(Mandatory=$false)][string]$BootImagePackageID="D0000067",
          [Parameter(Mandatory=$false)][string]$PXEBootPassword="Nestle1234",
          [Parameter(Mandatory=$false)][string]$DestinationPath="\\hqaaam0011.nestle.w2k\zti$\pxeboot\5.1.20140829\test"

        )

        $verbosepreference="Continue"
        $ErrorAction="Stop"



    $dllpath="$thismoddir\tsmediaapi.dll"

    write-verbose "Loading $dllpath"
    try{
    	# [System.Reflection.Assembly]::LoadFile("$dllpath")
    	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.ConfigMgr.TsMedia")
        $global:TSMedia = New-Object -ComObject Microsoft.ConfigMgr.TsMedia
        $VBControl = New-Object -ComObject ScriptControl

        if(!($TSMedia) -or !($VBControl)){
            Write-Error "COM Objects could not be loaded"
        }
    }
    catch{
        Write-Error "COM Objects could not be loaded"
        Write-Error $_.Exception.Message
        exit
    }

    try{
    	$params = @{
            ProviderName=$ProviderName;
            ProviderSiteCode=$ProviderSiteCode;
            DistributionPoint=$DistributionPoint;
            PackageID=$BootImagePackageID;
            Password=$PXEBootPassword;
        }
    	   Export-CustomCMIso @params
    	}
    catch [Exception]
            {
              #write-verbose $_.Exception.Type
              write-verbose $_.Exception.ToString()
              throw
    }

    if($returnISOGen -eq $false){
        write-verbose "There was an error generating ISO Image"
        exit
    }
    else{write-verbose "Succesfully created media"}
    
    try{$returnExtract = Export-CustomCMISODatFile -DestinationPath $DestinationPath}
    catch{
    	write-verbose "There was an error extracting DAT File"
    	write-error $_.Exception.message
    	exit
    	}
    if($returnExtract -eq $false){
        write-verbose "There was an error extracting DAT File"
    	exit
    }
    else{
    	write-verbose "Succesfully extracted DAT File"
    }

    write-verbose "Tidying up..."
    If(Test-Path "$($env:temp)\Bootimage.iso"){
    	    Remove-Item "$($env:temp)\Bootimage.iso"
    }

    write-verbose "Script completed succesfully"
}

Main
# SIG # Begin signature block
# MIITrAYJKoZIhvcNAQcCoIITnTCCE5kCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUUAfCq0eFZa3l9+4rzC9PwM36
# MSWgghDGMIIIETCCBfmgAwIBAgIKQsvxoQADAAAykDANBgkqhkiG9w0BAQUFADCB
# nzELMAkGA1UEBhMCQ0gxDTALBgNVBAgTBFZhdWQxDjAMBgNVBAcTBVZldmV5MQ8w
# DQYDVQQKEwZOZXN0bGUxDjAMBgNVBAsTBUlTL0lUMSgwJgYDVQQDEx9OZXN0bGUg
# SW50ZXJuYWwgU3Vib3JkaW5hdGUgQ0ExMSYwJAYJKoZIhvcNAQkBFhdwa2ktc2Vj
# dXJpdHlAbmVzdGxlLmNvbTAeFw0xMDEyMTcxNTAxMjVaFw0xNTEyMTYxNTAxMjVa
# MIGTMQswCQYDVQQGEwJDSDELMAkGA1UECBMCVkQxDjAMBgNVBAcTBVZldmV5MQ8w
# DQYDVQQKEwZOZXN0bGUxEjAQBgNVBAsTCUdMT0JFIEJUQzEbMBkGA1UEAxMSU05P
# VyBCdWlsZCBTaWduaW5nMSUwIwYJKoZIhvcNAQkBFhZXaW5EZXYuU05PV0BuZXN0
# bGUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp7uaKQz9puCY
# Ffv4wSi/nBF1nE+tS8yqMcBTocOvWUQw4wLcs7+uORhRZNc6593b30RfPYVpb/SX
# AQ/2QqWvDTiZhDrvaY84E7NzUgDQckvqysK/pXnsZ7Jm1WWqKWP6qjEbOjj3tRp9
# zVSxjmafVJU0XyPQlbrOWzgOp2m8tcr6sFZ+HpJOeY21uo0SA3OfO5GgqbMx+F0s
# iWcw58TdEyxVAuZDx5VLdKfN+h4JkpMAqI+iWacXX5CEqjZpoUca768sTGMNCWH8
# 2UWb+KLelq6xa8vsU5EYQHmNrmy2RkHeN6cnAT5KtlheBeb3pfZsfQYn26vo4Hm8
# kdiu38VLwwIDAQABo4IDVzCCA1MwCwYDVR0PBAQDAgeAMB0GA1UdDgQWBBQe+Dlm
# OGRZiHmJzPz1LOQg64v83TA9BgkrBgEEAYI3FQcEMDAuBiYrBgEEAYI3FQiHptAi
# goyYCobViwyFnrobhNOVDXaErI8+gqvYNwIBZAIBAzAfBgNVHSMEGDAWgBSTZh+f
# Tkgry34qa4t8AqIeQiKR8zCCAUAGA1UdHwSCATcwggEzMIIBL6CCASugggEnhoHR
# bGRhcDovLy9DTj1OZXN0bGUlMjBJbnRlcm5hbCUyMFN1Ym9yZGluYXRlJTIwQ0Ex
# KDMpLENOPUhRQlVTQTAwMDMsQ049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZp
# Y2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9bmVzdGxlLERDPWNv
# bT9jZXJ0aWZpY2F0ZVJldm9jYXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JM
# RGlzdHJpYnV0aW9uUG9pbnSGUWh0dHA6Ly9pbnRyYW5ldC5uZXN0bGUuY29tL3No
# LWRiL3BraS9OZXN0bGUlMjBJbnRlcm5hbCUyMFN1Ym9yZGluYXRlJTIwQ0ExKDMp
# LmNybDCCAU0GCCsGAQUFBwEBBIIBPzCCATswgcMGCCsGAQUFBzAChoG2bGRhcDov
# Ly9DTj1OZXN0bGUlMjBJbnRlcm5hbCUyMFN1Ym9yZGluYXRlJTIwQ0ExLENOPUFJ
# QSxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25m
# aWd1cmF0aW9uLERDPW5lc3RsZSxEQz1jb20/Y0FDZXJ0aWZpY2F0ZT9iYXNlP29i
# amVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRob3JpdHkwcwYIKwYBBQUHMAKGZ2h0
# dHA6Ly9pbnRyYW5ldC5uZXN0bGUuY29tL3NoLWRiL3BraS9IUUJVU0EwMDAzLm5l
# c3RsZS5jb21fTmVzdGxlJTIwSW50ZXJuYWwlMjBTdWJvcmRpbmF0ZSUyMENBMSgz
# KS5jcnQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwGwYJKwYBBAGCNxUKBA4wDDAKBggr
# BgEFBQcDAzANBgkqhkiG9w0BAQUFAAOCAgEAeQ/teFLm8VEoagRoi0vQfjamt9ky
# AwZpzSixfgSSpUgdEk7hv4zX/39g1zNUImIK73qGrqFPrlgosLwVJE4e09oMfy3a
# QtqZQzBvXdURwa9cJOSxZeX1Prq0IkbTeBxE3U2dv2o2wAhPsAQfy9ob+80NXMmM
# ZnqvdKlH2mX/CJiC/eIDTLXF2+Jc5oZVXJhZ/kpFWiO1tq87R9kUUNMATq1+KhW8
# m8xvocUItpSvIxWbLcRc75CukXXi2jqa+LnP40JAk9fY7iE6YXGA2Ph1/zYhIx7R
# ApDDkGEXJPutosYcSWgo/RG1XwaF80SaYGhhpgiVmFzKUSOhCNJEU9L2PEzK5J23
# 7Vkk2Xfna1aGrPNUyRQqm5gUDxCG3gWukrVfZ4dJrN6gJnLbFa7+mQe8tbYSC8d+
# Gm78UUKVhemcsbOdwaWxW55iSH5oWECt1j2880zZN7OzimuwnD1ZxbuuT1PDnxA6
# XswRdEv5xp4wubaCNfok5/Htkji0hNVC1rJLUv0fCvvvUv/w27pzXoztOXJNgFCu
# oocaHSpk2nYFdh2jMZtiLRuum0kQQ69Cj2fxjZvAPqoh/nWmwt3r/PHHpNchV4hY
# xxhFg32ihdxG+0Btd84SOGAOrM14+DHnqQ+wRKryYBvN4E0pE+80X21eOyteATeK
# 99t45rOWUpEQnXcwggitMIIGlaADAgECAgpDPl50AAAAAAAJMA0GCSqGSIb3DQEB
# BQUAMIGOMSYwJAYJKoZIhvcNAQkBFhdwa2ktc2VjdXJpdHlAbmVzdGxlLmNvbTEL
# MAkGA1UEBhMCQ0gxDTALBgNVBAgTBFZhdWQxDjAMBgNVBAcTBVZldmV5MQ8wDQYD
# VQQKEwZOZXN0bGUxDjAMBgNVBAsTBUlTL0lUMRcwFQYDVQQDEw5OZXN0bGUgUm9v
# dCBDQTAeFw0xMDEwMTYwNjA3NDNaFw0yMDEwMTYwNjE3NDNaMIGfMQswCQYDVQQG
# EwJDSDENMAsGA1UECBMEVmF1ZDEOMAwGA1UEBxMFVmV2ZXkxDzANBgNVBAoTBk5l
# c3RsZTEOMAwGA1UECxMFSVMvSVQxKDAmBgNVBAMTH05lc3RsZSBJbnRlcm5hbCBT
# dWJvcmRpbmF0ZSBDQTExJjAkBgkqhkiG9w0BCQEWF3BraS1zZWN1cml0eUBuZXN0
# bGUuY29tMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAsT5GT2WalV2C
# 5EeGii0biXbwcfy6Vo4ubpYF0Djtz6HAou1zZ0P2uZxv1hyUnu9uFHohJyUHAcKn
# Zye3QRMMegyy55poASBiYPGrL0Z6O1QkHDsrV5ziTnRRY/4enCZfHNBkfXfGPtqE
# 2caIfhjkSyIOxMou9RjsoyrGEoTxlP7Hp+rnfPUnP2zAfBvkXTaDVO5J/wQaAArU
# EzHKP4DrOXgrULWkFyPz+h7oRC/dJ5v1vq/KMi7abCFBBX+bU3dfAsIsCblXTqzX
# VoVP4NxyVVDvrD9HB7tqR6IGIsUIJvq+r8Ka3aU2kDXaHvKT6sKmEmS+9BGnW1QF
# j4affJMGy03XvUl8dhN7l6UcbNgKlxDqRwaeGDfgHy631tvxPJVVtUFYn8FJcof/
# gZsOHs8c1JsOPWDKTrbkcdRhy3tfZxpOlf9QmMTzt4i4zMsgAMP2+SeCTPoIQMEG
# I7rByhH93u3CvFKytkrddUfRf7rxZNEYlk05NyPKk1lK/zvfCnpTgNwkIHIiwFUK
# /Thegs9Zb6jJhnPVIU8B74dKEjg9w28PnAoizrDuKHhx96M0+nsx9fmTr9Nylk3/
# vyug1StVDB41AqDtM7rfQExna2mdBm4x/1miQ8+uYpofw357VsWiWNHq0xeTbOg8
# Fi5tg7IIuKSeKyg0r6M8G7AlEKbcWp0CAwEAAaOCAvgwggL0MA8GA1UdEwEB/wQF
# MAMBAf8wHQYDVR0OBBYEFJNmH59OSCvLfipri3wCoh5CIpHzMAsGA1UdDwQEAwIB
# hjASBgkrBgEEAYI3FQEEBQIDAwADMCMGCSsGAQQBgjcVAgQWBBTCC58a/DPFrelH
# ieNKHz+lL2+ABzAZBgkrBgEEAYI3FAIEDB4KAFMAdQBiAEMAQTAfBgNVHSMEGDAW
# gBTXTxtseuVqFfFimt05dV4egwRodDCCARIGA1UdHwSCAQkwggEFMIIBAaCB/qCB
# +4aBu2xkYXA6Ly8vQ049TmVzdGxlJTIwUm9vdCUyMENBLENOPUhRQlVTQTAwMDEs
# Q049Q0RQLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENO
# PUNvbmZpZ3VyYXRpb24sREM9TmVzdGxlLERDPWNvbT9jZXJ0aWZpY2F0ZVJldm9j
# YXRpb25MaXN0P2Jhc2U/b2JqZWN0Q2xhc3M9Y1JMRGlzdHJpYnV0aW9uUG9pbnSG
# O2h0dHA6Ly9pbnRyYW5ldC5uZXN0bGUuY29tL3NoLWRiL3BraS9OZXN0bGUlMjBS
# b290JTIwQ0EuY3JsMIIBKAYIKwYBBQUHAQEEggEaMIIBFjCBsAYIKwYBBQUHMAKG
# gaNsZGFwOi8vL0NOPU5lc3RsZSUyMFJvb3QlMjBDQSxDTj1BSUEsQ049UHVibGlj
# JTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixE
# Qz1OZXN0bGUsREM9Y29tP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1j
# ZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MGEGCCsGAQUFBzAChlVodHRwOi8vaW50cmFu
# ZXQubmVzdGxlLmNvbS9zaC1kYi9wa2kvSFFCVVNBMDAwMS5jdHIubmVzdGxlLmNv
# bV9OZXN0bGUlMjBSb290JTIwQ0EuY3J0MA0GCSqGSIb3DQEBBQUAA4ICAQBslxBf
# f3jjm8ChlAN1m4/HPvhdgeNmdQfopm9VKchQkz/q+9d01vyaH8EaQ7dHQb18pkVJ
# 3x5a/AXxPFGOoe3JiiuK0xXNTe80SjKBj4v095qvmGIFAcXww72nA4Ij1s6NzPVJ
# h4n3Fw+eCbHOpJNT34iiMCajRh0cKi1h16pVZKFSDSLAU9FgQD2EjlbvNJ0o+z3h
# vjK0JxmWKNgQGy6AEfrRGCDCKrSKfASrKaApzy+TqCNHLJEeA6cJhNxBg5Bl/8U9
# LxDAKmslVb3ljA0+kyH7suse1tv+r1md2o3WxtusyqUHbDcfk7NeuFHYXpngbWHh
# A3jZitjY+uKkIX5A7ire3WUUINbuz5E93ppKcXumqlLtYe+sirLf4Tjk/I6u1oEl
# yxIOQ5/1R8tmtodJJ6HDXLm+p2z0ZAHT7+B2hNUICEDNsVrs0qq1Z7RdMTT4oKeI
# JR3JTeqG1nwBTNVGqM7nCPI4nIod7vVKzLNAJNonJV2+522TXMOd7E1xK6xVhDHO
# K645OyIfQP9xDT/SHzs2iET+NgIZAxxu5I6YwgoRLhH1Jp4GR6VU4YWfNAcB7NIN
# 6Nj32h300uD1Tz+6nR0uHk3oxg47GmfDq8xmil5m6LtdkSALpmVfpMIYqCDLKXyT
# iNFbzz1yn4ZsWVVrZMkCqEKNc92ju/v82/I8JTGCAlAwggJMAgEBMIGuMIGfMQsw
# CQYDVQQGEwJDSDENMAsGA1UECBMEVmF1ZDEOMAwGA1UEBxMFVmV2ZXkxDzANBgNV
# BAoTBk5lc3RsZTEOMAwGA1UECxMFSVMvSVQxKDAmBgNVBAMTH05lc3RsZSBJbnRl
# cm5hbCBTdWJvcmRpbmF0ZSBDQTExJjAkBgkqhkiG9w0BCQEWF3BraS1zZWN1cml0
# eUBuZXN0bGUuY29tAgpCy/GhAAMAADKQMAkGBSsOAwIaBQCgeDAYBgorBgEEAYI3
# AgEMMQowCKACgAChAoAAMBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3AgEEMBwGCisG
# AQQBgjcCAQsxDjAMBgorBgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBQwkFsWLOen
# RIWocv0AmcramSFa5jANBgkqhkiG9w0BAQEFAASCAQCdZTabJfkk+pGUksTnZwv4
# 2YgpMjKuxh7yIuvP5RnHV5Zb7VRcWzp9oJKuzHsEuwshB3efbi++g1M9hW+aKy9+
# yp3phohYPj8bfFY2b3uk7hqrdeOKWJY2sm3JkrhGNqVumzXND2bKazC3Bfk8pKEQ
# CGbFnRAp3UYS6pm7JZVUHmTebvIzPPtW55NHGeHjnim1T9EiDvK/ZNWUSJDb3na/
# C6927sTGB+tQ4qfido3osD7xidM2K+2XIVzPz+nFFMLlG8HM5jQ4col/9aij8+T9
# cuECZylnxiZ8eUCleC46qEUFuwyp5817sGHs3oxCxQh49ChQiyNE/jSt72CqJfKl
# SIG # End signature block
