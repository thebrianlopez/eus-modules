set-psdebug -trace 1
set-alias -name wr -value write-verbose
$VerbosePreference="Continue"


function Test-EUCWebService{
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$true,HelpMessage="Enter the base url for eucwebservice",ValueFromPipeline=$true)]$uri,
        [Parameter(Mandatory=$true,HelpMessage="Computername please",ValueFromPipeline=$true)]$computername,
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential]$credential=$null
        ) 
    $mdt=(New-WebServiceProxy -uri "$uri/mdt.asmx")
    $ad=(New-WebServiceProxy -uri "$uri/ad.asmx")
    $sccm=(New-WebServiceProxy -uri "$uri/sccm.asmx")
    $red=(New-WebServiceProxy -uri "$uri/redirector.asmx")

    # Get UUID for computer name
    $results = $($mdt.GetComputerSettingsProperty($computername))
    $results;if($($results.Status) -ne "Success"){throw}

    $uuid = $results.ComputerSettingResult.UUID
    # Check machine name of a uuid
    $results = $($mdt.GetSingleProperty_Priority($uuid,"ComputerName"))
    if($results.Result -ne $computername){throw}

    # Should Pass - provices EUC Webservice URL
    $results = $red.GetWebServiceRedirectorURL($uuid)
    if($($results.Status) -ne "Success"){throw}
    $redurl = $results.results

    # Should Pass
    [xml]$data = "<?xml version=`"1.0`"?><soap12:Envelope xmlns:xsi=`"http://www.w3.org/2001/XMLSchema-instance`" xmlns:xsd=`"http://www.w3.org/2001/XMLSchema`" xmlns:soap12=`"http://www.w3.org/2003/05/soap-envelope`"><soap12:Body><GetPackageReplicationStatus xmlns=`"http://www.nestle.com/`"><Packages><string>QA000590</string><string>QA000532</string><string>QA00048C</string><string>QA0000DC</string><string>QA00055C</string><string>QA0005C4</string><string>QA00052F</string><string>QA000531</string><string>QA00055D</string><string>QA0002D8</string><string>QA0002AA</string><string>QA000591</string><string>QA0006C3</string><string>QA00058E</string><string>QA000564</string><string>QA000593</string><string>QA0005BE</string><string>QA0005E5</string><string>QA0006EC</string><string>QA0007B2</string><string>QA0007AB</string><string>QA00053D</string></Packages><ADSiteName>HQ-Bussigny-DataCenterClients</ADSiteName></GetPackageReplicationStatus></soap12:Body></soap12:Envelope>"
    $packages = $data.Envelope.Body.GetPackageReplicationStatus.Packages.string
    $adsite = "US-Glendale-QA"
    # $results = $sccm.GetPackageReplicationStatus($packages,$adsite)
    #$results

    # Should Pass
    [xml]$data = "<?xml version=`"1.0`"?><soap12:Envelope xmlns:xsi=`"http://www.w3.org/2001/XMLSchema-instance`" xmlns:xsd=`"http://www.w3.org/2001/XMLSchema`" xmlns:soap12=`"http://www.w3.org/2003/05/soap-envelope`"><soap12:Body><GetPackageReplicationStatus xmlns=`"http://www.nestle.com/`"><Packages><string>QA000590</string><string>QA000532</string><string>QA00048C</string><string>QA0000DC</string><string>QA00055C</string><string>QA0005C4</string><string>QA00052F</string><string>QA000531</string><string>QA00055D</string><string>QA0002D8</string><string>QA0002AA</string><string>QA000591</string><string>QA0006C3</string><string>QA00058E</string><string>QA000564</string><string>QA000593</string><string>QA0005BE</string><string>QA0005E5</string><string>QA0006EC</string><string>QA0007B2</string><string>QA0007AB</string><string>QA00053D</string></Packages><ADSiteName>HQ-Bussigny-DataCenterClients</ADSiteName></GetPackageReplicationStatus></soap12:Body></soap12:Envelope>"
    $packages = $data.Envelope.Body.GetPackageReplicationStatus.Packages.string
    $adsite = "HQ-Bussigny-DataCenterClients"
    # $results = $sccm.GetPackageReplicationStatus($packages,$adsite)
    #$results

    # Should Fail
    [xml]$data = "<?xml version=`"1.0`"?><soap12:Envelope xmlns:xsi=`"http://www.w3.org/2001/XMLSchema-instance`" xmlns:xsd=`"http://www.w3.org/2001/XMLSchema`" xmlns:soap12=`"http://www.w3.org/2003/05/soap-envelope`"><soap12:Body><GetPackageReplicationStatus xmlns=`"http://www.nestle.com/`"><Packages><string>QA000590</string><string>QA000532</string><string>QA00048C</string><string>QA0000DC</string><string>QA00055C</string><string>QA0005C4</string><string>QA00052F</string><string>QA000531</string><string>QA00055D</string><string>QA0002D8</string><string>QA0002AA</string><string>QA000591</string><string>QA0006C3</string><string>QA00058E</string><string>QA000564</string><string>QA000593</string><string>QA0005BE</string><string>QA0005E5</string><string>QA0006EC</string><string>QA0007B2</string><string>QA0007AB</string><string>QA00053D</string></Packages><ADSiteName>HQ-Bussigny-DataCenterClients</ADSiteName></GetPackageReplicationStatus></soap12:Body></soap12:Envelope>"
    $packages = $data.Envelope.Body.GetPackageReplicationStatus.Packages.string
    $packages += "QA000XXX"
    $adsite = "HQ-Bussigny-DataCenterClients"
    #$results = $sccm.GetPackageReplicationStatus($packages,$adsite)
    # $results

    # Should fail
    $results = $sccm.GetMarketServer("0.0.0.0")
    $results;if($($results.Status) -ne "Failure"){throw}

    # Should fail
    $results = $sccm.GetMarketServer("999.999.999.999")
    $results;if($($results.Status) -ne "Failure"){throw}

    # Should fail
    $results = $sccm.GetMarketServer("hg.hh.hh.jj")
    $results;if($($results.Status) -ne "Failure"){throw}

    # Should Pass - Adds a machine to an AD group
    # $results = $ad.AddComputerToGroup("us16-0g32w33","USUSADesktopDevelopment2")
    # $results;if($($results.Status) -ne "Exception"){throw}

    # Should fail - machine does exist
    # $results = $ad.AddComputerToGroup("us16-0g32w33","USUSADesktopDevelopment2")
    # $results;if($($results.Status) -ne "Exception"){throw}
}

