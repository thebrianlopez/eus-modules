function Invoke-MDTPreReqCheck
{
  <# 
  .SYNOPSIS 
    Performs Pre-req check of workstation again the EUC WebService.
  .DESCRIPTION 
    Queries local WMI and Task Sequence variables and send the information to the EucWebService and waits for a response.
  .INPUTS 
    Task Sequence Object
  .OUTPUTS 
    Boolean
  .EXAMPLE 
    $params = @{
                "header"=$global:header;  # HTTP Header for Rest Request
                "apikey"=$global:apikey;  # APIKey for webservice
                "url"=$global:url;        # Webservice URL
                "tag"="SNOW-Powershell";  # URL attribut to identify tag
                "data"=$json;             # Data to be posted to HTTP webservice
    }
              
    write-loggly @params

    This example 'splatts' all parameters and passes it to the Write-Loggly function.
  .NOTES 
    Initial Creation
  .LINK 
    http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 
[CmdletBinding()] 
param(
  [Parameter()][string]$dbxstring,
  [Parameter()][string]$tasksequenceobject,
  [Parameter()][string]$eucwebservurl,
  [Parameter()][switch]$whatif=$false,
  [Parameter()][switch]$useproxy=$false
  )
  $result = $false
  Write-Verbose "Result is $([string]$results)"
}

Invoke-MDTPreReqCheck

