function Export-TasksequenceXML{
  <# 
  .SYNOPSIS 
  Exports a task sequence to an XML file
  .DESCRIPTION 
  Performs a query against SMS_TaskSequencePackage and executes the Sequence method to display xml data. Exports the xml into an xml file
  .INPUTS 
  String

  .OUTPUTS 
  XML file
  .EXAMPLE 
  Export-TaskSequenceXML -name "SNOW5 Deployment" -folderpath "H:\exported-tasksequence-files"


  .NOTES 
  Initial Creation
  .LINK 
  http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 

  [CmdletBinding()] 
  param(
    [Parameter(Mandatory=$true)][string]$name,
    [Parameter(Mandatory=$false)][string]$sitecode="D00",
    [Parameter(Mandatory=$false)][string]$folderpath = (split-path -parent $MyInvocation.MyCommand.Definition)
    )


    $ns = "root\sms\site_$sitecode"
    write-verbose "Connecting to $computername using namespace $ns"
    $TsList = Get-WmiObject SMS_TaskSequencePackage -Namespace "$ns"

    ForEach ($Ts in $TsList)
    {
        $name = "$($Ts.Name)"
        write-verbose "Found $name"
        if($name.contains("Deployment")){
          write-verbose "Matched $name , exporting task sequence to xml"
          $Ts = [wmi]"$($Ts.__PATH)"
          Set-Content -Path "$folderpath\$($ts.PackageId).xml" -Value $Ts.Sequence
        }
    }

}

function Test-ExportTasksequenceXML{
  Export-TasksequenceXML -computername "usglnm0100.nestle.com" -sitecode "U07" -name "SNOW5 Deployment(3.1)" -folderpath "c:\temp\test" 
}

