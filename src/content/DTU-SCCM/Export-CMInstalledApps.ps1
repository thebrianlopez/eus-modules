Function Export-CMInstalledApps{
<#
  .SYNOPSIS
  Retrieves list if computers for clients with a given application.
  .PARAMETER ComputerName
  The name or IP address of one or more computers.
  .PARAMETER AppName
  Name or ProductName of an application.
  .PARAMETER AppCode
  MSI code of application
  #>
  [cmdletbinding()]
  param(
    [parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Mandatory=$true,HelpMessage='Computer or server name')]
    [string]$computername,
    [parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Mandatory=$true,HelpMessage='SCCM site code')]
    [string]$sitecode,
    [parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Mandatory=$false,HelpMessage='Application Name')]
    [string]$appname,
    [parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Mandatory=$false)]
    [System.Management.Automation.PSCredential]$cred
    )

  # if(!$msicode){$filter="ProductName like '$ProductName'"}

  process{

    try{

      $results = @()

      # application
      $params = @{
        "Credential"=$cred;
        "Namespace"="Root\SMS\site_$sitecode";
        "Class"="SMS_G_System_Installed_Software";
        "Computername"="$computername";
        "Filter"="ProductName like '%$appname%'";
        "ErrorAction"="Stop"
      }
      $apps = Get-WmiObject @params

      foreach ($item in $apps) {
        write-verbose $item.ProductName
        $props = @{
          "ProductName"=$item.ProductName
          "ProductCode"=$item.ProductCode
          "Publisher"=$item.Publisher
          "ResourceID"=$item.ResourceID
        }

        $result = New-Object -type PSObject -property $props
        $results += $result


      }

      $results

      } # End try block

      catch {

        write-verbose "Error running fucntion: $Error"

        } # end catch block

        }# end process block

        }# end function block

        Function Get-CMClearPXE{
  <#
  .SYNOPSIS
  Retrieves basic SCCM Information based on provided computernames or resourceids
  .PARAMETER ComputerName
  The name or IP address of one or more computers.
  .PARAMETER AppName
  Name or ProductName of an application.
  .PARAMETER AppCode
  MSI code of application
  #>
  [cmdletbinding()]
  param(
    [parameter(
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true,
      Mandatory=$true,
      HelpMessage='Computer or server name'
      )][int[]]$resourceid,
    [parameter(
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true,
      Mandatory=$true,
      HelpMessage='Computer or server name'
      )][string]$computername,
    [parameter(
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true,
      Mandatory=$true,
      HelpMessage='SCCM site code'
      )][string]$sitecode,
    [parameter(
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true,
      Mandatory=$false,
      HelpMessage='SCCM site code'
      )][System.Management.Automation.PSCredential]$cred,
    [Parameter()][string]$path
    )

  PROCESS {
    try{
      $results = @()

      foreach ($item in $resourceid) {

        write-verbose "Starting in $item"

        $params = @{
          "Credential"=$cred;
          "Namespace"="Root\SMS\site_$sitecode";
          "Class"="SMS_R_System";
          "Computername"="$computername";
          "ErrorAction"="Stop";
          "Filter"="Resourceid=$item";
        }
        $vrs = Get-WmiObject @params 


        $props = @{
          'resourceid'=[string]$vrs.resourceid;
          'ComputerName'=$vrs.name;
          'lastlogonuser'=$vrs.LastLogonUserName;
          'adsitename'=$vrs.adsitename;
          'agentsite'=$vrs.AgentSite[0];
          'agenttime'=$vrs.AgentTime[0];
          'IPAddress0'=$vrs.IPAddresses[0];
          'IPAddress1'=$vrs.IPAddresses[1];          
          'guid'=$vrs.SMSUniqueIdentifier;
        }
        $result = New-Object -type PSObject -property $props
        $results += $result
        }# end for loop

        $results

        }# end try block
        catch{write-verbose "Error running fucntion: $Error"
      }


      }# end process block

      }# end function

      # Export-CMInstalledApps -computername usglnm0100 -sitecode U07 -appname vlc
      Export-CMInstalledApps -computername usglnm0100 -sitecode U07 -appname vlc | select -property resourceid 
      # Get-CMClearPXE -computername usglnm0100 -sitecode U07 
      # Get-CMClearPXE -computername usglnm0100 -sitecode U07 -resourceid "12043","12039"