﻿# DTU-SCCM Modules

$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$thismoddir = (split-path -parent $scriptpath)

. "$thismoddir\Invoke-CMSearch.ps1"

Function Connect-SCCMServer 
{    
    <#
    .SYNOPSIS 
    Returns an object representing a connection to an SCCM server

    .DESCRIPTION
    Returns a system.object that contains the information necessary to connect to an SCCM server.

    .INPUTS
    Accepts input from the pipeline as a [string] as the Servername property

    .OUTPUTS
    Array of System.Object. 

    .EXAMPLE
    C:\PS> Connect-SCCMServer -Hostname "Server123"
   
    .EXAMPLE
    C:\PS> Connect-SCCMServer -Hostname "Server123" -sitecode "XXX"

    .EXAMPLE
    C:\PS> Connect-SCCMServer -Hostname "Server123" -sitecode "XXX" -credential $cred

    .LINK
    http://www.nestle.com

    .LINK
    Set-Item
    #>   
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$false,HelpMessage="SCCM Server Name or FQDN",ValueFromPipeline=$true)][Alias("ServerName","FQDN","ComputerName","Computer")][String] $HostName = (Get-Content env:computername),        
        [Parameter(Mandatory=$false,HelpMessage="Optional SCCM Site Code",ValueFromPipelineByPropertyName=$true )][String] $siteCode = $null,        
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null    
        )     
    PROCESS {       
         # Get the pointer to the provider for the site code        
         if ($siteCode -eq $null -or $siteCode -eq "") {            
            Write-Verbose "Getting provider location for default site on server $HostName"            
            if ($credential -eq $null) {                
                $sccmProviderLocation = Get-WmiObject -query "select * from SMS_ProviderLocation where ProviderForLocalSite = true" -Namespace "root\sms" -computername $HostName -errorAction Stop            
                } 
            else {                
                $sccmProviderLocation = Get-WmiObject -query "select * from SMS_ProviderLocation where ProviderForLocalSite = true" -Namespace "root\sms" -computername $HostName -credential $credential -errorAction Stop            
                }        
            } 
         else {            
            Write-Verbose "Getting provider location for site $siteCode on server $HostName"            
            if ($credential -eq $null) {                
                $sccmProviderLocation = Get-WmiObject -query "SELECT * FROM SMS_ProviderLocation where SiteCode = '$siteCode'" -Namespace "root\sms" -computername $HostName -errorAction Stop            
                } 
            else {
                $sccmProviderLocation = Get-WmiObject -query "SELECT * FROM SMS_ProviderLocation where SiteCode = '$siteCode'" -Namespace "root\sms" -computername $HostName -credential $credential -errorAction Stop            
                }        
         }         
         # Split up the namespace path        
         $parts = $sccmProviderLocation.NamespacePath -split "\\", 4     
         Write-Verbose "Provider is located on $($sccmProviderLocation.Machine) in namespace $($parts[3])"         
         # Create a new object with information        
         $retObj = New-Object -TypeName PsObject     
         $retObj | add-Member -memberType NoteProperty -name Machine -Value $HostName   
         $retObj | add-Member -memberType NoteProperty -name Namespace -Value $parts[3]
         #$retObj | add-Member -memberType NoteProperty -name SccmProvider -Value $sccmProviderLocation         
         $retObj    
         }
} 


Function Get-SCCMServerList{
    <#
    .SYNOPSIS 
    Returns a list of either Market or Regional primary servers in the hierarchy

    .DESCRIPTION
    Returns an array of System.Object for each SCCM Site server in the hierarchy based on whether the
    servers are Regional or Market level. The returned array can then be used to perform various functions.

    .INPUTS
    None. You cannot pipe objects to Get-SCCMServerList

    .OUTPUTS
    Array of System.Object. 

    .EXAMPLE
    C:\PS> Get-SCCMServerList -CentralPrimary "Server123"
   
    .EXAMPLE
    C:\PS> Get-SCCMServerList -CentralPrimary "Server123" -Scope "Market"

    .EXAMPLE
    C:\PS> Get-SCCMServerList -CentralPrimary "Server123" -Scope "Market" -credential $cred

    .LINK
    http://www.nestle.com

    .LINK
    Set-Item
    #>
    [CmdletBinding()]
    PARAM(
        [parameter(Mandatory=$false,HelpMessage="Name of Central Primary Site Server")][String]$CentralPrimary= (Get-Content env:computername),
        [parameter(Mandatory=$False,HelpMessage="Select 'Market' or 'Regional'")][ValidateSet("Market","Regional")][String]$Scope = "Market",
        [parameter(Mandatory=$False,HelpMessage="Select 'Market' or 'Regional'")]
        [String[]]$SiteCodeExclusions = @("DVD", "DVF", "DVG", "DVH", "QA6", "QA7", "QA8", "QAZ", "E50", "E51", "E52", "E53", "E54", "E60", "A50", "A60", "P50", "P60", "E70"), 
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null 
        )

    PROCESS{

    #$CentralServerSiteCodes = @("GLB","QA0","DV0") # Array of Site codes of the Central primaries in Dev,QA and Prod
    

    $ServerArray = @()
    if ($credential -eq $null) { 
        $centralsitecode = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $centralprimary -ErrorAction stop)[0].SiteCode
        $Sitecodes = Get-WmiObject -namespace "root\sms\site_$centralsitecode" -ComputerName $centralprimary -query "select * from SMS_Site where type='2'" -errorAction Stop  
        }
    else{
        $centralsitecode = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $centralprimary -Credential $credential -ErrorAction stop)[0].SiteCode
        $Sitecodes = Get-WmiObject -namespace "root\sms\site_$centralsitecode" -ComputerName $centralprimary -query "select * from SMS_Site where type='2'" -Credential $credential -errorAction Stop  
        }

   
   
     switch ($Scope){
        "Market"{
        write-verbose "Enumerating Market Site servers"
         foreach($site in $Sitecodes){
            
            $Found = $False
            #foreach($globalsitecode in $CentralServerSiteCodes){
                if($site.sitecode.toupper() -eq $centralsitecode.toupper() -or $site.reportingsitecode.toupper() -eq $centralsitecode.toupper()){
                    $Found = $True
                }
                
            #}
              if(!($Found)){
                    
                    #Filter out OASIS Server sitecodes
                    if($SiteCodeExclusions -contains $site.sitecode){
                        Write-Verbose "OASIS Server: $($site.servername) detected.Skipping"
                    }
                    Else{
                        write-verbose "Found site: $($site.servername)"
                        $retObj = New-Object -TypeName System.Object        
                        $retObj | add-Member -memberType NoteProperty -name Machine -Value $($site.servername)
                        $retObj | add-Member -memberType NoteProperty -name Namespace -Value "root\sms\site_$($site.sitecode)"        
                        #$retObj | add-Member -memberType NoteProperty -name SccmProvider -Value $sccmProviderLocation
                        $ServerArray += $retObj
                    }
                }
         }
        }
        "Regional"{
            write-verbose "Enumerating Regional Site servers"
             foreach($site in $Sitecodes){
                    
                    #foreach($globalsitecode in $CentralServerSiteCodes){
                        if($site.reportingsitecode.toupper() -eq $centralsitecode.toupper()){
                            #[void]$Servershash.add($site.servername,$site.sitecode)
                            write-verbose "Found site: $($site.servername)"
                            $retObj = New-Object -TypeName PsObject        
                            $retObj | add-Member -memberType NoteProperty -name Machine -Value $site.servername       
                            $retObj | add-Member -memberType NoteProperty -name Namespace -Value "root\sms\site_$($site.sitecode)"        
                            #$retObj | add-Member -memberType NoteProperty -name SccmProvider -Value $sccmProviderLocation
                            $ServerArray += $retObj
                       }
                    #}
          }
       }
          
     }
  
    return $ServerArray
    }

}



Function Get-SCCMObject {    
    #  Generic query tool    
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$true, HelpMessage="SCCM Server",ValueFromPipelineByPropertyName=$true)][Alias("Server","SmsServer")][System.Object] $SccmServer,        
        [Parameter(Mandatory=$true, HelpMessage="SCCM Class to query",ValueFromPipeline=$true)][Alias("Table","View")][String] $class,        
        [Parameter(Mandatory=$false,HelpMessage="Optional Filter on query")][String] $Filter = $null    )     
    PROCESS {        
        if ($Filter -eq $null -or $Filter -eq ""){            
            Write-Verbose "WMI Query: SELECT * FROM $class"            
            $retObj = get-wmiobject -class $class -computername $SccmServer.Machine -namespace $SccmServer.Namespace     
            }        
        else        
            {            
             Write-Verbose "WMI Query: SELECT * FROM $class WHERE $Filter"            
             $retObj = get-wmiobject -query "SELECT * FROM $class WHERE $Filter" -computername $SccmServer.Machine -namespace $SccmServer.Namespace        
             }         
        return $retObj    
        }
}

Function Get-SCCMCollection {
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$true, HelpMessage="SCCM Server",ValueFromPipeline=$true)][Alias("Server","SmsServer")][System.Object] $SccmServer,        
        [Parameter(Mandatory=$false, HelpMessage="Optional Filter on query")][String] $Filter = $null    
        )     
    PROCESS {        
        return Get-SCCMObject -sccmServer $SccmServer -class "SMS_Collection" -Filter $Filter    
            }
} 

Function Get-SCCMAdvertisement {    
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$true, HelpMessage="SCCM Server",ValueFromPipeline=$true)][Alias("Server","SmsServer")][System.Object] $SccmServer,        
        [Parameter(Mandatory=$false, HelpMessage="Optional Filter on query")][String] $Filter = $null    
        )     
    PROCESS {        
        return Get-SCCMObject -sccmServer $SccmServer -class "SMS_Advertisement" -Filter $Filter    
        }
}

Function Get-SCCMTaskSequence {    
    [CmdletBinding()]    
    PARAM (        
        [Parameter(Mandatory=$true, HelpMessage="SCCM Server",ValueFromPipeline=$true)][Alias("Server","SmsServer")][System.Object] $SccmServer,        
        [Parameter(Mandatory=$false, HelpMessage="Optional Filter on query")][String] $Filter = $null    
        )     
    PROCESS {        
        return Get-SCCMObject -sccmServer $SccmServer -class "SMS_TaskSequencePackage" -Filter $Filter    
        }
}


Function New-SCCMAdvertisementFuzzy{
  <#
    .SYNOPSIS 
    Creates an SCCM Advertisement on an SCCM server using Fuzzy Logic checks

    .DESCRIPTION
    Version 1.0

    Version History

    
    This function can can take the servername and namespace from the pipeline, including PSObjects returned by
    the  Get-SCCMServerList and Connect-SCCMServer functions.
    The script will then create an advertisment based on the defined command line parameters.
    
    By default this function will create advertisements as mandatory by using the default advertisement properties
    of a SNOW5 Deployment task sequence.

    The instanceusers and instancepermissions parameters allow instancepermissions to be set on the newly created
    or modified Advertisement. The default permissions is 7: Read,Write,Modify.
    When setting instance permissions, any existing instance permissions are removed.

    By using the -merge parameter it is possible to overwrite any existing Advertisement settings with new settings
    specified on the command line. If this parameter is not set it will be set to $false by default.

    NOTE: This version cannot create Advertisements which have scheduled assignments.

    * AdvertFlags *
    ---------------
    45350944 =  (default) - mandatory - pxe boot
    45088800 = mandatory - no pxe boot
    8650752  = non-mandatory - pxe boot
    8388608  = non-mandatory - no pxe boot

    * RemoteClientFlags *
    ---------------
    4864 = Download packages when needed (default)
    4232 = Run from DP
    4176 = Download all content locally before running (cannot be used with PXE flag)
       
   
    
    ###########FUZZY LOGIC##############
    Fuzzy logic is available to all parameters of type string as well as the instanceusers array
       

    Any string can contain wmi queries surrounded by the delimiters '##', i.e ##root\sms;select sitecode from SMS_ProviderLocation##
    The substring (within the delimeters) will be extracted and executed dynamically by the cmdlet. The result will be added to the
    original string.

    The substring can be further delimited using the ';' 
    The left of the delimeter is the WMI namespace and the query is to the right. 
    If this delimeter is not used then the SCCM namespace will be used.

    NOTE: The select statment must specify a property and not use the wildcard *

    ####################################
        
    
  
    This function can can take the servername from the pipeline 

    .INPUTS
    Accepts input from the pipeline as a [string[]] for the Servername property

    Accepts input from the pipeline for the servername and namespace parameters

    .OUTPUTS
    None. 

    .EXAMPLE
    C:\PS> New-SCCMAdvertisement -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -credential = $cred
  
    .EXAMPLE
    C:\PS> New-SCCMAdvertisement -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -merge

    .EXAMPLE
    C:\PS> New-SCCMAdvertisement -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -instanceusers "DOMAIN\UserName" -instancepermissions 7 (or leave this blank) -merge

   
     .EXAMPLE
    C:\PS> New-SCCMAdvertisement -ServerName "Server123" -CollectionName "My Fuzzy Dynamic Collection for site ##root\sms;select sitecode from SMS_ProviderLocation##" -PackageID "XXX010101" --AdvertisementName "My Advert" -instanceusers "DOMAIN\UserName" -instancepermissions 7 (or leave this blank) -merge
  
  
    .LINK
    http://www.nestle.com

    #> 
[CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,  ValueFromPipeline=$true, ValueFromPipelineByPropertyName = $true,Position=0)][Alias("FQDN","ComputerName","Computer","Machine")]
        [String[]] $ServerName,
        [Parameter(Mandatory=$false,  ValueFromPipelineByPropertyName = $true)]
        [String] $NameSpace,
        [Parameter(Mandatory=$false, ParameterSetName="Collection ID",Position=1)]
        [String]$CollectionID,
        [Parameter(Mandatory=$false, ParameterSetName="Collection Name",Position=1)]
        [String]$CollectionName,
        [parameter(Mandatory=$true)][string]$PackageID,
        [parameter(Mandatory=$true)][string]$ProgramName,
        [parameter(Mandatory=$true)][string]$AdvertisementName,
        [parameter(Mandatory=$false)][string]$Comment = "",
        [parameter(Mandatory=$false)][int]$AdvertFlags = 45350944, #Default for SNOW5 Deployment Task sequences (Download packages when needed)
        [parameter(Mandatory=$false)][int]$RemoteClientFlags = 4864, #Default for SNOW5 Deployment Task sequences (Download packages when needed)
        [parameter(Mandatory=$False)][string]$PresentTime = "20110718092000.000000+***",
        [parameter(Mandatory=$False)][boolean]$PresentTimeEnabled = $true,
        [parameter(Mandatory=$False)][string[]]$InstanceUsers,
        [parameter(Mandatory=$False)][Long[]]$InstancePermissions, # default to read, write and modify: 3 bit
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null,
        [Parameter(Mandatory=$false)][switch]$Merge
        )

        
    BEGIN{
        # Check for missing parameters


       if($psboundparameters.ContainsKey("instanceusers") -eq $true -and $psboundparameters.ContainsKey("instancepermissions") -eq $false){
            Write-verbose "instancePermissins parameter has not been specified. Cannot set permissions" -ForegroundColor Red -BackgroundColor black
            break
        }

    }


    PROCESS{

    $namespace = $null
    $SC = $null
    $par = $null
    $pos1 = $null
    $pos2 = $null
    $newvalue = $null
    $tempvalue = $null
    

    foreach($server in $ServerName){
        $commandparameters = @{}
    
        Write-verbose "Connecting to server: $server"
        #if (!($NameSpace)){
            $SC = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $server -ErrorAction silentlycontinue -ErrorVariable errtest)[0].SiteCode
            if($errtest){ #Error test
                    Write-verbose "There was an error attempting to enumerate the Sitecode for Server: $Server" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
            } # End of error check
            Write-Verbose "SiteCode: $SC"
            $Namespace = "root\sms\site_$($SC)"
        #}


        foreach($par in $psBoundParameters.keys){
            $tempvalue =$psboundparameters.Item($par)
            if($psboundparameters.Item($par).gettype() -ne [string]){
                
                switch($par){
                "servername"{
                                $commandparameters += @{$par = $server}
                            }
                "instanceusers"  {

                                [string[]]$tempaccountsarr = @()
                                    foreach($account in $instanceusers){
                        
                                        if($account.Contains("##")){
                                            $pos1 = $account.IndexOf("##")
                                            $pos2 = $account.LastIndexOf("##")
                                            if($pos1 -ne $pos2){
                                                $newvalue = FuzzyLogic -Inputstring $account -servername $server -Namespace $namespace
                                                if($newvalue -eq $null){
                                                    Write-verbose "Failed to execute Fuzzy Logic query" -ForegroundColor Red -BackgroundColor black
                                                    continue
                                                }
                                                else{
                                                    $tempaccountsarr += @($newvalue)
                                                    }
                                            }
                                            else{
                                                $tempaccountsarr += @($account)
                                                }

                                        }
                                        else{
                                              $tempaccountsarr += @($account)
                                            }

                                    }
                                    $commandparameters += @{$par = $tempaccountsarr}
                                }

                           
                default     {
                                $commandparameters += @{$par = $tempvalue}
                            }


                }
               }

            else{
                
                if($tempvalue.Contains("##")){
                    $pos1 = $tempvalue.IndexOf("##")
                    $pos2 = $tempvalue.LastIndexOf("##")
                    if($pos1 -ne $pos2){
                        $newvalue = FuzzyLogic -Inputstring $tempvalue -servername $server -Namespace $namespace
                        if($newvalue -eq $null){
                            Write-verbose "Failed to execute Fuzzy Logic query" -ForegroundColor Red -BackgroundColor black
                            continue
                        }
                        else{
                            $commandparameters += @{$par = $newvalue}
                            }
                    }
                    else{
                        $commandparameters += @{$par = $tempvalue}
                        }

       
                }
                else{
               
       
                        $commandparameters += @{$par = $tempvalue}
                    }
            }
            

        }  
 
        New-SCCMAdvertisement @commandparameters 

    }

}


}




  Function New-SCCMAdvertisement
{
    <#
    .SYNOPSIS 
    Creates an SCCM Advertisement on an SCCM server

    .DESCRIPTION
    Version 1.2

    Version History
        12-02-2013 BTC - Added parameters to set instance permissions
                         Created new function to handle instance permissions
                         When searching for adverts we now check the 'sourcecode' field.
        07-05-2013 BTC - Fixed issue when setting instance permissions

    This function can can take the servername and namespace from the pipeline, including PSObjects returned by
    the  Get-SCCMServerList and Connect-SCCMServer functions.
    The script will then create an advertisment based on the defined command line parameters.
    
    By default this function will create advertisements as mandatory by using the default advertisement properties
    of a SNOW5 Deployment task sequence.

    The instanceusers and instancepermissions parameters allow instancepermissions to be set on the newly created
    or modified Advertisement. The default permissions is 7: Read,Write,Modify.
    When setting instance permissions, any existing instance permissions are removed.

    By using the -merge parameter it is possible to overwrite any existing Advertisement settings with new settings
    specified on the command line. If this parameter is not set it will be set to $false by default.

    NOTE: This version cannot create Advertisements which have scheduled assignments.

    * AdvertFlags *
    ---------------
    45350944 =  (default) - mandatory - pxe boot
    45088800 = mandatory - no pxe boot
    8650752  = non-mandatory - pxe boot
    8388608  = non-mandatory - no pxe boot

    * RemoteClientFlags *
    ---------------
    4864 = Download packages when needed (default)
    4232 = Run from DP
    4176 = Download all content locally before running (cannot be used with PXE flag)


    .INPUTS
    Accepts input from the pipeline as a [string] for the Servername property

    Accepts input from the pipeline for the servername and namespace parameters

    .OUTPUTS
    None. 

    .EXAMPLE
    C:\PS> New-SCCMAdvertisment -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -credential = $cred
  
    .EXAMPLE
    C:\PS> New-SCCMAdvertisment -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -merge

    .EXAMPLE
    C:\PS> New-SCCMAdvertisment -ServerName "Server123" -CollectionID "XXX00001" (OR -CollectionName "My Collection") -PackageID "XXX010101" -ProgramName "*" -AdvertisementName "My Advert" -instanceusers "DOMAIN\UserName" -instancepermissions 7 (or leave this blank) -merge

    .LINK
    http://www.nestle.com

    #>   
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,  ValueFromPipeline=$true, ValueFromPipelineByPropertyName = $true,Position=0)][Alias("FQDN","ComputerName","Computer","Machine")]
        [String[]] $ServerName,
        [Parameter(Mandatory=$false,  ValueFromPipelineByPropertyName = $true)]
        [String] $NameSpace,
        [Parameter(Mandatory=$false, ParameterSetName="Collection ID",Position=1)]
        [String]$CollectionID,
        [Parameter(Mandatory=$false, ParameterSetName="Collection Name",Position=1)]
        [String]$CollectionName,
        [parameter(Mandatory=$true)][string]$PackageID,
        [parameter(Mandatory=$true)][string]$ProgramName,
        [parameter(Mandatory=$true)][string]$AdvertisementName,
        [parameter(Mandatory=$false)][string]$Comment = "",
        [parameter(Mandatory=$false)][int]$AdvertFlags = 45350944, #Default for SNOW5 Deployment Task sequences (Download packages when needed)
        [parameter(Mandatory=$false)][int]$RemoteClientFlags = 4864, #Default for SNOW5 Deployment Task sequences (Download packages when needed)
        [parameter(Mandatory=$False)][string]$PresentTime = "20110718092000.000000+***",
        [parameter(Mandatory=$False)][boolean]$PresentTimeEnabled = $true,
        [parameter(Mandatory=$False)][string[]]$InstanceUsers,
        [parameter(Mandatory=$False)][Long[]]$InstancePermissions, # default to read, write and modify: 3 bit
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null,
        [Parameter(Mandatory=$false)][switch]$Merge
        )

     BEGIN{
        # Check for missing parameters


        if($psboundparameters.ContainsKey("instanceusers") -eq $true -and $psboundparameters.ContainsKey("instancepermissions") -eq $false){
            Write-verbose "Instancepermissions parameter has not been specified. Cannot set permissions" -ForegroundColor Red -BackgroundColor black
            break
        }

    }

    PROCESS{
        $Server = $null
        $NameSpace = $null
        $CollID = $null
        $testAdvert=$null
        $AdvertInstance = $null
        $checkad = $false

        foreach($server in $ServerName){

            #$Server = $ServerName
            Write-Verbose "ServerName: $Server"
            #if (!($NameSpace)){
                Write-Verbose "Namespace parameter not specified. Resolving"
                $SC = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $server -ErrorAction silentlycontinue -ErrorVariable errtest)[0].SiteCode
                if($errtest){ #Error test
                    Write-verbose "There was an error attempting to enumerate the Sitecode for Server: $Server" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
                } # End of error check

                $Namespace = "root\sms\site_$($SC)"
            #}


            switch ($PsCmdlet.ParameterSetName) 
            { 
                "Collection ID"  {
                    Write-Verbose "-CollectionID parameter used"
                    $CollID = $CollectionID
                    } 
                "Collection Name"  {
                    Write-Verbose "-Collection Name parameter used"
                    Write-Verbose "Resolving CollectionName using ownedbythissite=1"
                    if($credential -eq $null){
                        $CollID = @(Get-WmiObject -computer $Server -namespace $namespace -class "SMS_Collection" -Filter "name='$CollectionName' and ownedbythissite=1" -ErrorAction silentlycontinue -ErrorVariable errtest)[0].CollectionID
                        if($errtest){ #Error test
                            Write-verbose "There was an error resolving the collection: $collectionname" -ForegroundColor Red -BackgroundColor black
                            Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                        } # End of error check

                        if(!($CollID)){
                            Write-verbose "Could not resolve collection name."
                            continue
                            }
                        }
                    else{
                        $CollID = @(Get-WmiObject -computer $Server -namespace $namespace -class "SMS_Collection" -Filter "name='$CollectionName' and ownedbythissite=1" -Credential $credential -ErrorAction silentlycontinue -ErrorVariable errtest)[0].CollectionID
                         if($errtest){ #Error test
                            Write-verbose "There was an error resolving the collection: $collectionname" -ForegroundColor Red -BackgroundColor black
                            Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                        } # End of error check
                       if(!($CollID)){
                            Write-verbose "Could not resolve collection name."
                            continue
                            }
                        }
                   }
              
            }
        
            Write-Verbose "CollectionID: $CollID"
                
            $AdvertProperties = @{
                CollectionID = $CollID.ToUpper();
                PackageID = $PackageID.ToUpper();
                ProgramName = $ProgramName;
                AdvertisementName = $AdvertisementName;
                Comment = $Comment;
                AdvertFlags = $AdvertFlags;
                RemoteClientFlags = $RemoteClientFlags;
                PresentTime = $PresentTime;
                PresentTimeEnabled = $PresentTimeEnabled
                }

            Write-verbose "Checking if Advertisment already exists"
            if($credential -eq $null){
                $testAdvert = Get-WmiObject -ComputerName $Server -Class SMS_Advertisement -Namespace $Namespace -Filter "AdvertisementName='$AdvertisementName' AND packageid='$PackageID' AND SourceSite='$($SC)'" -ErrorAction silentlycontinue -ErrorVariable errtest
            }
            else{
                $testAdvert = Get-WmiObject -ComputerName $Server -Class SMS_Advertisement -Namespace $Namespace -Filter "AdvertisementName='$AdvertisementName' AND packageid='$PackageID' AND SourceSite='$($SC)'" -Credential $credential -ErrorAction silentlycontinue -ErrorVariable errtest
            }
            if($errtest){ #Error test
                Write-verbose "There was an error determining if Advert: $AdvertisementName already exists" -ForegroundColor Red -BackgroundColor black
                Write-Verbose $errtest.exception.message
                $errtest = $null
                continue
            } # End of error check

            if($testAdvert){
                Write-verbose "An Advertisment already exists with the same name and PackageID"
                if($Merge -eq $false){
                    Write-Verbose "Merge flag set to false. Exiting"
                    continue 
                    }
                else{
                    Write-Verbose "Merge flag set to True. Overwriting properties"
                    if($credential -eq $null){
                        $AdvertInstance = Set-WmiInstance -Path $testAdvert.__PATH -Arguments $AdvertProperties -ErrorAction silentlycontinue -ErrorVariable errtest
                        if($errtest){ #Error test
                            Write-verbose "There was an error creating advert" -ForegroundColor Red -BackgroundColor black
                            Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                        } # End of error check

                        if($InstanceUsers){
                            Write-Verbose "Setting instance permissions"
                            Set-InstancePermissions -username $instanceusers -instancekey $($testadvert.advertisementid) -objectkey 3 -instanceperms $instancepermissions -servername $ServerName -namespace $NameSpace -ErrorAction silentlycontinue -ErrorVariable errtest
                            if($errtest){ #Error test
                                Write-verbose "There was an error when setting permissions" -ForegroundColor Red -BackgroundColor black
                                Write-Verbose $errtest.exception.message
                                $errtest = $null
                                continue
                            } # End of error check
                        }
                    
                    }
                    else{
                        $AdvertInstance =  Set-WmiInstance -Path $testAdvert.__PATH -Arguments $AdvertProperties  -ErrorAction silentlycontinue -ErrorVariable errtest -Credential $credential
                         if($errtest){ #Error test
                            Write-verbose "There was an error creating advert" -ForegroundColor Red -BackgroundColor black
                            Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                        } # End of error check
                        if($InstanceUsers){
                            Write-Verbose "Setting instance permissions"
                            Set-InstancePermissions -username $instanceusers -instancekey $($testadvert.advertisementid) -objectkey 3 -instanceperms $instancepermissions -servername $ServerName -namespace $NameSpace -credential $credential -ErrorAction silentlycontinue -ErrorVariable errtest
                            if($errtest){ #Error test
                                Write-verbose "There was an error when setting permissions" -ForegroundColor Red -BackgroundColor black
                                Write-Verbose $errtest.exception.message
                                $errtest = $null
                                continue
                            } # End of error check
                       }
                    
                    }
               
                    Write-verbose "Sucessfully modified advert with AdvertisementID:$($AdvertInstance.AdvertisementID)"
                
                }
            } 
            else{
                Write-verbose "Attempting to create advertisement on Server: $Server"

                if($credential -eq $null){
                    $AdvertInstance = Set-WmiInstance -class SMS_Advertisement -Arguments $AdvertProperties -Namespace $NameSpace -ComputerName $Server -ErrorAction silentlycontinue -ErrorVariable errtest
                    if($errtest){ #Error test
                            Write-verbose "There was an error creating advert" -ForegroundColor Red -BackgroundColor black
                           Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                        } # End of error check
                    $checkad = $false
                    do{ # wait for instance to commit
                        $testAdvert = @(Get-WmiObject -ComputerName $Server -Class SMS_Advertisement -Namespace $Namespace -Filter "AdvertisementName='$AdvertisementName' AND packageid='$PackageID' AND SourceSite='$($namespace.substring($NameSpace.Length -3,3))'" )[0]
                        if($testAdvert.AdvertisementID -ne $null -and $testAdvert.AdvertisementID -ne ""){$checkad = $true} 
                        }
                    until($checkad = $true)
                    if($InstanceUsers){
                            Write-Verbose "Setting instance permissions"
                            $permreturn = Set-InstancePermissions -username $instanceusers -InstanceKey $($testadvert.AdvertisementID) -objectkey 3 -instanceperms $instancepermissions -servername $Server -namespace $NameSpace
                            if($permreturn -eq $false){ 
                                Write-verbose "There was an error when setting permissions" -ForegroundColor Red -BackgroundColor black
                                continue
                            } # End of error check
                        }
                
                }
                else{
                    $AdvertInstance = Set-WmiInstance -class SMS_Advertisement -Arguments $AdvertProperties -Namespace $NameSpace -ComputerName $Server -ErrorAction silentlycontinue -ErrorVariable errtest -Credential $credential
                    if($errtest){ #Error test
                            Write-verbose "There was an error creating advert" -ForegroundColor Red -BackgroundColor black
                            Write-Verbose $errtest.exception.message
                            $errtest = $null
                            continue
                     } # End of error check
                     $checkad = $false
                    do{ # wait for instance to commit
                        $testAdvert = @(Get-WmiObject -ComputerName $Server -Class SMS_Advertisement -Namespace $Namespace -Filter "AdvertisementName='$AdvertisementName' AND packageid='$PackageID' AND SourceSite='$($namespace.substring($NameSpace.Length -3,3))'" -Credential $credential )[0]
                        if($testAdvert.AdvertisementID -ne $null -and $testAdvert.AdvertisementID -ne ""){$checkad = $true} 
                        }
                    until($checkad = $true)
                    if($InstanceUsers){
                            Write-Verbose "Setting instance permissions"
                            $permreturn = Set-InstancePermissions -username $instanceusers -InstanceKey $($testadvert.AdvertisementID) -objectkey 3 -instanceperms $instancepermissions -servername $Server -namespace $NameSpace -credential $credential 
                            if($permreturn -eq $false){ 
                                Write-verbose "There was an error when setting permissions" -ForegroundColor Red -BackgroundColor black
                                continue
                            } # End of error check
                        }
                
                }
                Write-verbose "Sucessfully created advert with AdvertisementID:$($testadvert.AdvertisementID)"
                $testadvert #Not using $AdvertInstance as the AdvertisementID is not always available
           
            }
        }
       
    }
    }

function Set-InstancePermissions{
  <#
    .SYNOPSIS 
    Creates an SCCM Instance Permissionsr

    .DESCRIPTION
    Version 1.2

    Version History
        15-04-2013 BTC - Permissions are now passed in as an array
        07-05-2013 BTC - Changed the way the function returns values
                         
#>
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)][string[]]$Usernames,
        [parameter(Mandatory=$true)][string]$InstanceKey,
        [parameter(Mandatory=$true)][int]$ObjectKey,
        [parameter(Mandatory=$true)][string]$ServerName,
        [parameter(Mandatory=$true)][string]$NameSpace,
        [parameter(Mandatory=$true)][long[]]$instancePerms,
        [Parameter(Mandatory=$false)][System.Management.Automation.PSCredential] $credential = $null
        )
    BEGIN{
        Write-Verbose "Entered Function: Set-InstancePermissions"
    }
    PROCESS{

        $boolReturn = $false
               
        Try{          
            if($credential -eq $null){
                $checkperms = Get-WmiObject -Class SMS_UserInstancePermissions -Namespace $NameSpace -ComputerName $ServerName -filter "ObjectKey=$ObjectKey AND InstanceKey='$InstanceKey'" -ErrorAction Stop
                if($checkperms){
                    Write-Verbose "Existing instance permissions found"
                    foreach($o in $checkperms){
                        Write-Verbose "Removing instance for $($o.UserName)"
                        Remove-WmiObject -path $o.__PATH -ErrorAction stop
                    }
                }
            }
            else{
                $checkperms = Get-WmiObject -Class SMS_UserInstancePermissions -Namespace $Namespace -ComputerName $ServerName -filter "ObjectKey=$ObjectKey AND InstanceKey='$InstanceKey'" -Credential $credential -ErrorAction Stop
                if($checkperms){
                    Write-Verbose "Existing instance permissions found"
                    foreach($o in $checkperms){
                        Write-Verbose "Removing instance for $($o.UserName)"
                        Remove-WmiObject -path $o.__PATH -Credential $credential -ErrorAction Stop
                    }
                }

            }

        }
        Catch{
            Write-verbose "There was an error removing existing instance permissions. Continuing anyway"
            Write-Verbose "ERROR: $($_.Exception.Message)"  
            $error.clear()

        }

        try{

            foreach($username in $Usernames){
                if($usernames.count -gt 1 -and $instancePerms.count -eq 1){
                    $tempperms = $instancePerms[0]
                }
                else{
                    #$tempindex = $usernames.indexof($username)
                    $tempindex = [array]::indexof($usernames,$username)

                    $tempperms = $instanceperms[$tempindex]
                }
                $props = @{UserName=$UserName.ToUpper();ObjectKey=$ObjectKey;InstanceKey=$InstanceKey.toupper();InstancePermissions=$tempperms}
                if($credential -eq $null){
                    Write-Verbose "Creating instance of SMS_UserInstancePermissions"
                    $UserPermInstance = Set-WmiInstance -class sms_userinstancepermissions -arguments $props -namespace $namespace -ComputerName $ServerName -ErrorAction Stop
                    Write-verbose "Succesfully created instance of SMS_UserInstancePermissions for user $UserName"
                    write-verbose $UserPermInstance
                }
                else{
                   
                    Write-Verbose "Creating instance of SMS_UserInstancePermissions"
                    $UserPermInstance = Set-WmiInstance -class sms_userinstancepermissions -arguments $props -namespace $namespace -ComputerName $ServerName -Credential $credential -ErrorAction Stop
                    Write-verbose "Succesfully created instance of SMS_UserInstancePermissions for user $UserName"
                    write-verbose $UserPermInstance

                }
               
            }
            $boolReturn = $true
            return $boolReturn
            }
        catch{
            Write-verbose "ERROR: $($_.Exception.Message)"  
            return $boolReturn
        }


    }
    END{
        Write-Verbose "Exited Function: Set-InstancePermissions"
    }
    }

    Function New-SCCMCollectionFuzzy{
    <#
    .SYNOPSIS 
    Creates an SCCM Collection on an SCCM server using Fuzzy logic

    .DESCRIPTION
    Version 1.1

    Version History
       
   
    The script will then create a collection based on the defined command line parameters.
    
    There are two types of collections in Configuration Manager: Static and Dynamic

    ##############STATIC################
    Static collection creation only requires that the 
        -servername  
        -collectionname 
    parameters are specified.

    Optional parameters include 
        -parentcollectionname  - The collection the collection will be a member of (default is COLLROOT)
        -Accounts and -Permissions -  Are used to define the permissions that will be defined for the collection


   ###############DYNAMIC###############
   Dynamic Collection creation requires the following additional properties over and above those defined for static collections.

        -queryname - The name of the WQL query
        -queryexpression - The WQL expression that will be used to evaluate the collections members

    Optional parameters include
        -LimitTo - The collection that will be used to limit the results of the query expression. (Default is "All Systems")
        -RefreshMinutes -RefreshHours -RefreshDays - Used to set the interval when the collection query will be executed
    
    ###########FUZZY LOGIC##############
    Fuzzy logic is available to all parameters of type string
       

    Any string can contain wmi queries surrounded by the delimiters '##', i.e ##root\sms;select sitecode from SMS_ProviderLocation##
    The substring (within the delimeters) will be extracted and executed dynamically by the cmdlet. The result will be added to the
    original string.

    The substring can be further delimited using the ';' 
    The left of the delimeter is the WMI namespace and the query is to the right. 
    If this delimeter is not used then the SCCM namespace will be used.

    NOTE: The select statment must specify a property and not use the wildcard *

    ####################################
        
    
    NOTE: Although the -credential parameter is present it cannot be used due to a limitation in WMI for this WMI object.
          Instead the script must be run from an account with the permissions necessary to create collections in SCCM.

    This function can can take the servername from the pipeline 


    .INPUTS
    Accepts input from the pipeline as a [string] for the Servername property
    The Accounts and Permissions parameters accept arrays

    .OUTPUTS
    None. 

    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" 
  
    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" -Accounts "NESTLE\User1","NESTLE\User2" -Permissions 3,3

    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" -Accounts "NESTLE\User1","NESTLE\User2" -Permissions 3,3 -ParentCollectionName "Parent Collection"

     .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Dynamic Collection" -queryname "MyCollectionQuery" -queryexpression "select * from sms_r_system" -limitto "All Workstations" -refreshdays 1
  
     .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Fuzzy Dynamic Collection for site ##root\sms;select sitecode from SMS_ProviderLocation##" -queryname "MyCollectionQuery" -queryexpression "select * from sms_r_system" -limitto "All Workstations" -refreshdays 1
  
    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Fuzzy Static Collection for computer: ##root\cimv2:select name from win32_computersystem##" 
 
    .LINK
    http://www.nestle.com

    #> 

  [CmdletBinding()]
    param (
        [parameter(Mandatory=$true,ValueFromPipeline=$true,  ValueFromPipelineByPropertyName = $true,Position=0)][Alias("FQDN","ComputerName","Computer","Machine")]
        [string[]]$ServerName, 
        [Parameter(Mandatory=$false,  ValueFromPipelineByPropertyName = $true)]
        [string]$Namespace,
        [parameter(Mandatory=$true)] [string]$collectionname, 
        [parameter(Mandatory=$false)] [string]$parentCollectionName = "COLLROOT",
        [parameter(Mandatory=$false)] [string[]]$Accounts = $null,
        [parameter(Mandatory=$false)] [Long[]]$Permissions = $null,
        [Parameter(Mandatory=$false)] [ValidateRange(0, 59)] [int] $refreshMinutes = 0, 
        [Parameter(Mandatory=$false)] [ValidateRange(0, 23)] [int] $refreshHours = 0, 
        [Parameter(Mandatory=$false)] [ValidateRange(0, 31)] [int] $refreshDays = 0, 
        [parameter(Mandatory=$false)] [string]$queryname = $null, 
        [parameter(Mandatory=$false)] [string]$queryexpression = $null, 
        [parameter(Mandatory=$false)] [string]$LimitTo = $null,
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null
    )
    BEGIN{
        # Check for missing parameters

        if($psboundparameters.ContainsKey("queryname") -eq $true -and $psboundparameters.ContainsKey("queryexpression") -eq $false){
            Write-verbose "Query Expression has not been specified. Quitting." -ForegroundColor Red -BackgroundColor black
            break
        }
        if($psboundparameters.ContainsKey("queryexpression") -eq $true -and $psboundparameters.ContainsKey("queryname") -eq $false){
            Write-verbose "Query Expression has not been specified. Quitting." -ForegroundColor Red -BackgroundColor black
            break
        }
        if($psboundparameters.ContainsKey("Accounts") -eq $true -and $psboundparameters.ContainsKey("Permissions") -eq $false){
            Write-verbose "Permissins parameter has not been specified. Cannot set permissions" -ForegroundColor Red -BackgroundColor black
            break
        }

    }

    PROCESS{

    $namespace = $null
    $SC = $null
    $par = $null
    $pos1 = $null
    $pos2 = $null
    $newvalue = $null
    $tempvalue = $null
    

    foreach($server in $ServerName){
        $commandparameters = @{}
    
        Write-verbose "Connecting to server: $server"
        #if (!($NameSpace)){
            $SC = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $server -ErrorAction silentlycontinue -ErrorVariable errtest)[0].SiteCode
            if($errtest){ #Error test
                    Write-verbose "There was an error attempting to enumerate the Sitecode for Server: $Server" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
            } # End of error check
            Write-Verbose "SiteCode: $SC"
            $Namespace = "root\sms\site_$($SC)"
        #}


        foreach($par in $psBoundParameters.keys){
            $tempvalue =$psboundparameters.Item($par)
            if($psboundparameters.Item($par).gettype() -ne [string]){
                
                switch($par){
                "servername"{
                                $commandparameters += @{$par = $server}
                            }
                "accounts"  {

                                [string[]]$tempaccountsarr = @()
                                    foreach($account in $accounts){
                        
                                        if($account.Contains("##")){
                                            $pos1 = $account.IndexOf("##")
                                            $pos2 = $account.LastIndexOf("##")
                                            if($pos1 -ne $pos2){
                                                $newvalue = FuzzyLogic -Inputstring $account -servername $server -Namespace $namespace
                                                if($newvalue -eq $null){
                                                    Write-verbose "Failed to execute Fuzzy Logic query" -ForegroundColor Red -BackgroundColor black
                                                    continue
                                                }
                                                else{
                                                    $tempaccountsarr += @($newvalue)
                                                    }
                                            }
                                            else{
                                                $tempaccountsarr += @($account)
                                                }

                                        }
                                        else{
                                              $tempaccountsarr += @($account)
                                            }

                                    }
                                    $commandparameters += @{$par = $tempaccountsarr}
                                }

                           
                default     {
                                $commandparameters += @{$par = $tempvalue}
                            }


                }
               }

            else{
                
                if($tempvalue.Contains("##")){
                    $pos1 = $tempvalue.IndexOf("##")
                    $pos2 = $tempvalue.LastIndexOf("##")
                    if($pos1 -ne $pos2){
                        $newvalue = FuzzyLogic -Inputstring $tempvalue -servername $server -Namespace $namespace
                        if($newvalue -eq $null){
                            Write-verbose "Failed to execute Fuzzy Logic query" -ForegroundColor Red -BackgroundColor black
                            continue
                        }
                        else{
                            $commandparameters += @{$par = $newvalue}
                            }
                    }
                    else{
                        $commandparameters += @{$par = $tempvalue}
                        }

       
                }
                else{
               
       
                        $commandparameters += @{$par = $tempvalue}
                    }
            }
            

        }  
 
        New-SCCMCollection @commandparameters 

    }

}

}


Function New-SCCMCollectionFromXML{
  <#
    .SYNOPSIS 
    Creates SCCM Collections based on an XML file

    .DESCRIPTION
    Version 1.1

    Version History

    The script will then create a collection based on the defined command line parameters.
    
    Collection creation only requires that the following parameters are specified.
        -servername  
        -inputxml
      
    ###########FUZZY LOGIC##############

    Fuzzy logic is available to all parameters of type string

    Within any of the above mentioned parameters strings can contain wmi queries surrounded by the delimiters '##', i.e ##root\sms;select sitecode from SMS_ProviderLocation##
    The substring (within the delimeters) will be extracted and executed dynamically by the cmdlet. The result will be added to the
    original string.

    The substring can be further delimited using the ';' 
    The left of the delimeter is the WMI namespace and the query is to the right. 
    If this delimeter is not used then the SCCM namespace will be used.

    NOTE: The select statment must specify a property and not use the wildcard *

    #############XML SCHEMA##############

    The following schema must be used by the -inputXML parameter

    <?xml version="1.0"?>
                <Collections>
                               <Collection>
                                               <CollectionName>##root\sms;select sitecode from sms_providerlocation## - PSH Static Test</CollectionName>>
                                               <QueryName></QueryName>
                                               <QueryExpression></QueryExpression>
                                               <LimitedTo></LimitedTo>
                                               <RefreshDays></RefreshDays>
                                               <RefreshHours></RefreshHours>
                                               <RefreshMinutes></RefreshMinutes>
                                               <Accounts></Accounts>
                                               <Permissions></Permissions>
                                               <ParentCollectionName>COLLROOT</ParentCollectionName>
                               </Collection>
                               <Collection>
                                               <CollectionName>##root\sms;select sitecode from sms_providerlocation## - PSH Dynamic Test</CollectionName>
                                               <QueryName>PSH Dynamic Test</QueryName>
                                               <QueryExpression>select SMS_R_SYSTEM.ResourceID,SMS_R_SYSTEM.ResourceType,SMS_R_SYSTEM.Name,SMS_R_SYSTEM.SMSUniqueIdentifier,SMS_R_SYSTEM.ResourceDomainORWorkgroup,SMS_R_SYSTEM.Client from SMS_R_System inner join SMS_G_System_NESTLE_SYSTEM_INFO on SMS_G_System_NESTLE_SYSTEM_INFO.ResourceId = SMS_R_System.ResourceId where SMS_R_System.OperatingSystemNameandVersion like "%Workstation 6.2%" and (SMS_G_System_NESTLE_SYSTEM_INFO.Country is not Null or SMS_G_System_NESTLE_SYSTEM_INFO.region is not Null or SMS_G_System_NESTLE_SYSTEM_INFO.computerroles is not Null) and SMS_R_System.Client = 1 and SMS_R_System.Obsolete = 0 and SMS_R_System.Active = 1</QueryExpression>
                               <LimitedTo></LimitedTo>
                                               <RefreshDays>1</RefreshDays>
                                               <RefreshHours></RefreshHours>
                                               <RefreshMinutes></RefreshMinutes>
                                               <Accounts>
                                                               <Account>NESTLEW2k\GlobalSMSWorkstationLCMAdmins</Account>
                                                               <Account>NESTLEW2k\GlobalSMSWorkstationSWDAdmins</Account>
                                               </Accounts>
                                               <Permissions>
                                                               <Permission>2104039</Permission>
                                                               <Permission>2104039</Permission>
                                               </Permissions>
                                               <ParentCollectionName>COLLROOT</ParentCollectionName>
                               </Collection>
                </Collections>

    This function can can take the servername from the pipeline 

    .INPUTS
    Accepts input from the pipeline as a [string] for the Servername property
    An XML file with the appropriate schema must be provided to the InputXML property

    .OUTPUTS
    None. 

    .EXAMPLE
    C:\PS> New-SCCMCollectionFromXML -ServerName "Server123" -InputXML "C:\mycollectionlist.xml"
   
    .LINK
    http://www.nestle.com

    #> 
    [CmdletBinding()]
    param (
    [parameter(Mandatory=$true,ValueFromPipeline=$true,  ValueFromPipelineByPropertyName = $true,Position=0)][Alias("FQDN","ComputerName","Computer","Machine")]
    [string[]]$ServerName, 
    [Parameter(Mandatory=$false,  ValueFromPipelineByPropertyName = $true)]
    [String] $NameSpace,
    [parameter(Mandatory=$true)] [string]$InputXML
    )


    PROCESS{

        $namespace = $null
        $SC = $null
        $Coll = $null
        $CollList = $null
        
    
        foreach($server in $ServerName){
            $commandparameters = @{}
            Write-verbose "Connecting to server: $ServerName"
            #if (!($NameSpace)){
                $SC = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $server -ErrorAction silentlycontinue -ErrorVariable errtest)[0].SiteCode
                if($errtest){ #Error test
                    Write-verbose "There was an error attempting to enumerate the Sitecode for Server: $Server" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
                } # End of error check
                Write-Verbose "SiteCode: $SC"
                $Namespace = "root\sms\site_$($SC)"
            #}

            Write-Verbose "Reading XML File"
            [xml]$CollList = Get-Content $InputXML -ErrorAction silentlycontinue -ErrorVariable errtest
            if($errtest){ #Error test
               Write-verbose "There was an error when reading XML File" -ForegroundColor Red -BackgroundColor black
                Write-Verbose $errtest.exception.message
                $errtest = $null
                continue
            } # End of error check

            Foreach($Coll in $CollList.Collections.Collection){
                $commandparameters = @{}
                $commandparameters += @{ServerName = $Server; Namespace = $NameSpace}
                $arrColl = $Coll | get-member -MemberType Property | where{$_.name -ne "#text"} | select name
                foreach($prop in $arrColl){
                    write-verbose "$($prop.name) = $($coll.$($prop.name))"
                    switch($prop.name.tolower())
                    {
                        "accounts" {if($coll.$($prop.name) -ne ""){$commandparameters += @{Accounts = $($coll.$($prop.name).account)}}}
                        "permissions" {if($coll.$($prop.name) -ne ""){$commandparameters += @{Permissions = $($coll.$($prop.name).permission)}}}
                        default {if($coll.$($prop.name) -ne ""){$commandparameters += @{$prop.name = $coll.$($prop.name)}}}
                    }



                }
                New-SCCMCollectionFuzzy @commandparameters 
                  

            }
        }

}

}


Function New-SCCMCollection{
    <#
    .SYNOPSIS 
    Creates an SCCM Collection on an SCCM server

    .DESCRIPTION
    Version 1.0

    Version History
       
   
    The script will then create a collection based on the defined command line parameters.
    
    There are two types of collections in Configuration Manager: Static and Dynamic

    ########STATIC##########
    Static collection creation only requires that the 
        -servername  
        -collectionname 
    parameters are specified.

    Optional parameters include 
        -parentcollectionname  - The collection the collection will be a member of (default is COLLROOT)
        -Accounts and -Permissions -  Are used to define the permissions that will be defined for the collection


   #########DYNAMIC#########
   Dynamic Collection creation requires the following additional properties over and above those defined for static collections.

        -queryname - The name of the WQL query
        -queryexpression - The WQL expression that will be used to evaluate the collections members

    Optional parameters include
        -LimitTo - The collection that will be used to limit the results of the query expression. (Default is "All Systems")
        -RefreshMinutes -RefreshHours -RefreshDays - Used to set the interval when the collection query will be executed
    #########################
   
    NOTE: Although the -credential parameter is present it cannot be used due to a limitation in WMI for this WMI object.
          Instead the script must be run from an account with the permissions necessary to create collections in SCCM.

    This function can can take the servername from the pipeline 


    .INPUTS
    Accepts input from the pipeline as a [string] for the Servername property
    The Accounts and Permissions parameters accept arrays

    .OUTPUTS
    None. 

    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" 
  
    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" -Accounts "NESTLE\User1","NESTLE\User2" -Permissions 3,3

    .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Static Collection" -Accounts "NESTLE\User1","NESTLE\User2" -Permissions 3,3 -ParentCollectionName "Parent Collection"

     .EXAMPLE
    C:\PS> New-SCCMCollection -ServerName "Server123" -CollectionName "My Dynamic Collection" -queryname "MyCollectionQuery" -queryexpression "select * from sms_r_system" -limitto "All Workstations" -refreshdays 1
  
 
    .LINK
    http://www.nestle.com

    #> 
    [CmdletBinding()]
   param (
        [parameter(Mandatory=$true,ValueFromPipeline=$true,  ValueFromPipelineByPropertyName = $true,Position=0)][Alias("FQDN","ComputerName","Computer","Machine")]
        [string[]]$ServerName, 
        [Parameter(Mandatory=$false,  ValueFromPipelineByPropertyName = $true)][string]$namespace,
        [parameter(Mandatory=$true)] [string]$collectionname, 
        [parameter(Mandatory=$false)] [string]$parentCollectionName = "COLLROOT",
        [parameter(Mandatory=$false)] [string[]]$Accounts = $null,
        [parameter(Mandatory=$false)] [Long[]]$Permissions = $null,
        [Parameter(Mandatory=$false)] [ValidateRange(0, 59)] [int] $refreshMinutes = 0, 
        [Parameter(Mandatory=$false)] [ValidateRange(0, 23)] [int] $refreshHours = 0, 
        [Parameter(Mandatory=$false)] [ValidateRange(0, 31)] [int] $refreshDays = 0, 
        [parameter(Mandatory=$false)] [string]$queryname = $null, 
        [parameter(Mandatory=$false)] [string]$queryexpression = $null, 
        [parameter(Mandatory=$false)] [string]$LimitTo = $null,
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null
    )
    
    BEGIN{
        # Check for missing parameters

        if($psboundparameters.ContainsKey("queryname") -eq $true -and $psboundparameters.ContainsKey("queryexpression") -eq $false){
            Write-verbose "Query Expression has not been specified. Quitting." -ForegroundColor Red -BackgroundColor black
            break
        }
        if($psboundparameters.ContainsKey("queryexpression") -eq $true -and $psboundparameters.ContainsKey("queryname") -eq $false){
            Write-verbose "Query Expression has not been specified. Quitting." -ForegroundColor Red -BackgroundColor black
            break
        }
        if($psboundparameters.ContainsKey("Accounts") -eq $true -and $psboundparameters.ContainsKey("Permissions") -eq $false){
            Write-verbose "Permissins parameter has not been specified. Cannot set permissions" -ForegroundColor Red -BackgroundColor black
            break
        }

    }
    PROCESS{
        # Reset variables to rest the pipeline
        $boolNewColl = $false
        $CollectionCheck = $null
        $ParentCollectionID = $null
        $RelationshipCheck = $null
        $NewColl = $null
        $Dynamic = $false
        $newRelation = $null
        $intervalClass = $null
        $interval = $null
        $LimitCollectionCheck = $null
        $LimitToID = $null
        $collprops = $null
        $subarguments = $null
        $ruleclass = $null
        $newrule = $null
        $path = $null
        $namespace = $null
        $SC = $null

        if($psboundparameters.ContainsKey("queryname") -eq $true -and $psboundparameters.ContainsKey("queryexpression") -eq $true){
            $Dynamic = $true
        }

        Foreach($Server in $ServerName){

            Write-Verbose "ServerName: $Server"
            #if (!($NameSpace)){
                $SC = @(Get-WmiObject -Namespace root\sms -Class SMS_ProviderLocation -ComputerName $server -errorvariable errtest -ErrorAction silentlycontinue)[0].SiteCode
                if($errtest){ #Error test
                    Write-verbose "There was an error attempting to enumerate the Sitecode for Server: $Server" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
                } # End of error check
                Write-Verbose "SiteCode: $SC"
                $Namespace = "root\sms\site_$($SC)"
                Write-Verbose "Namespace: root\sms\site_$($SC)"
            #}
        

            if($Dynamic -eq $false){
                write-verbose "Creating Static collection"
            }
            else{
                write-verbose "Creating Dynamic collection"
            }
  
             
            if ($parentCollectionName -eq "COLLROOT"){
                $ParentCollectionID = "COLLROOT"
            }
            else{
                Write-Verbose "Resolving parent collectionID"
                $parentCollectionCheck = Get-WmiObject -Namespace $Namespace -ComputerName $Server -query "Select * from SMS_Collection where name = '$parentCollectionName' and ownedbythissite=1" -erroraction silentlycontinue -ErrorVariable errtest
                if($errtest){ #Error test
                    Write-verbose "There was an error attempting to resolve the parent collection: $parentCollectionName" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
                } # End of error check
                if(!($parentCollectionCheck)){
                    Write-verbose "No results returned for collection: $parentCollectionName" -ForegroundColor Red -BackgroundColor black
                   continue
                }
                $ParentCollectionID = $parentCollectioNCheck.CollectionID
           }


            Write-verbose "Checking if the collection '$collectionname' already exists"
            $CollectionCheck = Get-WmiObject -Namespace $Namespace -ComputerName $Server -query "Select * from SMS_Collection where name = '$collectionname' and ownedbythissite=1" -erroraction silentlycontinue -ErrorVariable errtest
            if($errtest){ #Error test
                    Write-verbose "There was an error attempting to check if the collection: $CollectionName already exists" -ForegroundColor Red -BackgroundColor black
                    Write-Verbose $errtest.exception.message
                    $errtest = $null
                    continue
            } # End of error check
            
            if (!($CollectionCheck)) {
                Write-verbose "Collection does not exists Going to create it"
                $boolNewColl = $true
            }
            else{
                $boolFound = $false
                foreach($collinst in $CollectionCheck){
                    $RelationshipCheck = Get-WmiObject -Namespace $Namespace -ComputerName $Server -query "Select * from SMS_CollectToSubCollect where parentCollectionID = '$ParentCollectionID' AND subCollectionID = '$($collinst.CollectionID)'" -erroraction silentlycontinue -ErrorVariable errtest 
                    if($errtest){ #Error test
                        Write-verbose "There was an error attempting to determine the location of the collection: $($collinst.CollectionID)" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check
                    
                    if($RelationshipCheck){
                        $boolFound = $true
                    }
                }
                if($boolFound -eq $false){$boolNewColl = $true}
           }
    
            If($boolNewColl -eq $false){
                Write-verbose "A Collection with the same name and same location (Parent Collection ID) already exists. Cannot proceed" -ForegroundColor Red -BackgroundColor black
                continue
            
            }
            else{
                write-verbose "Creating Collection: $CollectionName"

               $collprops = @{Name = $CollectionName; OwnedByThisSite = $true}
                if ($credential -eq $null) { 
                    $newColl = Set-WmiInstance -class SMS_Collection -arguments $collprops -computername $Server -namespace $Namespace -erroraction silentlycontinue -ErrorVariable errtest
                    if($errtest){ #Error test
                        Write-verbose "There was an error creating collection: $CollectionName" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check
                }
                else{
                    $newColl = Set-WmiInstance -class SMS_Collection -arguments $collprops -computername $Server -namespace $Namespace -Credential $credential -Erroraction silentlycontinue -ErrorVariable errtest
                     if($errtest){ #Error test
                        Write-verbose "There was an error creating collection: $CollectionName" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check
                }
                $hack = $newColl.PSBase | select * | Out-Null # Taken from Micahel Niehaus blog to ensure WMI info is updated
                
                $subArguments = @{SubCollectionID = $newColl.CollectionID; ParentCollectionID = $parentCollectionID}
                if ($credential -eq $null) {
                    $newRelation = Set-WmiInstance -class SMS_CollectToSubCollect -arguments $subArguments -computername $Server -namespace $Namespace -erroraction silentlycontinue -ErrorVariable errtest
                     if($errtest){ #Error test
                        Write-verbose "There was an error creating SMS_CollectToSubCollect for Collection: $($newColl.CollectionID)" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check
                }
                else{
                    $newRelation = Set-WmiInstance -class SMS_CollectToSubCollect -arguments $subArguments -computername $Server -namespace $Namespace -Credential $credential -erroraction silentlycontinue -ErrorVariable errtest
                     if($errtest){ #Error test
                        Write-verbose "There was an error creating SMS_CollectToSubCollect for Collection: $($newColl.CollectionID)" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check
                }
                Write-verbose "Successfully created collection"
                write-verbose $newcoll
       
                if($psboundparameters.ContainsKey("Accounts") -eq $true){
   
           
                    Write-verbose "Adding permissons to the collection" 
          
            
                    #$index = $Accounts.IndexOf($Account) #get corresponding permissions
                    if ($credential -eq $null) {
                        $PermReturn = Set-InstancePermissions -ServerName $Server -NameSpace $namespace -InstanceKey $newColl.CollectionID -ObjectKey 1 -Usernames $Accounts -instancePerms $permissions -verbose 

                    }
                    else{
                        $PermReturn = Set-InstancePermissions -ServerName $Server -NameSpace $namespace -InstanceKey $newColl.CollectionID -ObjectKey 1 -Usernames $Accounts -instancePerms $permissions -credential $credential -verbose 

                    }
                   
                    if($PermReturn -eq $false){ 
                        Write-verbose "There was an error when setting permissions" -ForegroundColor Red -BackgroundColor black
                        continue
                    } # End of error check

                    Write-verbose "Permissions successfully created"
                    }
        
          

                if($Dynamic -eq $true){
                    Write-verbose "Creating Query properties for collection"
                    Write-Verbose "Creating instance of RecurInterval embedded class"
                    try{
                        $intervalClass = [WMICLASS]"\\$Server\$($Namespace):SMS_ST_RecurInterval"            
                        $interval = $intervalClass.CreateInstance()            
                        if($refreshMinutes -eq 0 -and $refreshHours -eq 0 -and $refreshDays -eq 0){$refreshDays = 1}
                        if ($refreshMinutes -gt 0) {                
                            $interval.MinuteSpan = $refreshMinutes            
                            }            
                        if ($refreshHours -gt 0) {                
                            $interval.HourSpan = $refreshHours            
                            }            
                        if ($refreshDays -gt 0) {                
                            $interval.DaySpan = $refreshDays            
                            }


                        #  Set the refresh schedule to collection
                        $newColl.RefreshSchedule = $interval 
                        $newColl.RefreshType=2
                        #save the changes 
                        $path = $newColl.Put()
                        Write-verbose "Successfully created interval instance"
                    }
                    catch{
                        Write-verbose "There was an error creating RecurInterval instance" -ForegroundColor Red -BackgroundColor black
                        Write-verbose "ERROR: $($_.exception.message)" -ForegroundColor Red -BackgroundColor black
                        continue
                    }

                    Write-verbose "Resolving Limiting collection"
                    if($LimitTo -eq "" -or $LimitTo -eq $null){$LimitTo = "All Systems"}
                    $LimitCollectionCheck = Get-WmiObject -Namespace $Namespace -ComputerName $Server -query "Select * from SMS_Collection where name = '$LimitTo'" -ErrorAction silentlycontinue -ErrorVariable errtest
                    if($errtest){ #Error test
                        Write-verbose "There was an error resolving Limiting collection: $LimitTo" -ForegroundColor Red -BackgroundColor black
                        Write-Verbose $errtest.exception.message
                        $errtest = $null
                        continue
                    } # End of error check

                    if(!($LimitCollectionCheck)){
                        Write-verbose "Limiting collection cannot be found." -ForegroundColor Red -BackgroundColor black
                        #return
                    }
                    else{
                        $LimitToID = $LimitCollectionCheck.CollectionID
                    }

                    try{
                        Write-verbose "Creating CollectionRuleQuery instance"
                        $ruleClass = [wmiclass]"\\$Server\$($Namespace):SMS_CollectionRuleQuery" 
                        $newRule = $ruleClass.CreateInstance() 
                        $newRule.RuleName = $queryname
                        $newRule.LimitToCollectionID = $LimitToID
                        $newRule.QueryExpression = $queryexpression 
                        $newColl.AddMembershipRule($newRule) | Out-Null
                        Write-verbose "Successfully created CollectionRuleQuery"
                        }
                    catch{
                        Write-verbose "There was an error creating an instance of the CollectionRuleQueryClass" -ForegroundColor Red -BackgroundColor black
                        Write-verbose "ERROR: $($_.exception.message)" -ForegroundColor Red -BackgroundColor black
                        contunue
                
                    }

                }
            }
        }
    }

}


Function FuzzyLogic{
    param(
        [parameter(Mandatory=$true)] [string]$ServerName,
        [parameter(Mandatory=$true)] [string]$Namespace,
        [parameter(Mandatory=$true)] [string]$InputString
    )
    
    #Fuzzy name logic tests
    Write-verbose "Entered Function: FuzzyLogic"
    $substr = $InputString.split("##")[2]
    if(!($substr.contains(";"))){
        write-verbose "Delimeter missing in Fuzzy string. Using default SCCM namspace."
        $subnamespace = $Namespace
        $subquery = $substr
    }
    else{
        $subnamespace = $substr.split(";")[0]
        $subquery = $substr.Split(";")[1]
    }
    Write-Verbose "Namespace: $subnamespace"
    Write-Verbose "Query: $subquery"
    Write-Verbose "Checking sub query string"
    if($subquery.tolower().contains("select") -eq $false -and $subquery.tolower().contains("from") -eq $false){
        Write-verbose "Invalid Fuzzy query" -ForegroundColor Red -BackgroundColor black
        return $null
    }

    try{

        $subprop = $subquery.split()[1]
        $subresult = @(Get-WmiObject -ComputerName $servername -Namespace $subnamespace -query $subquery -ErrorAction Stop)[0].$subprop
        $newresult = -join ($InputString.split("##")[0],$InputString.split("##")[1],$subresult,$InputString.split("##")[3],$InputString.split("##")[4])
        Write-verbose "Returning: $newresult"
        return $newresult

    }
   catch{
        Write-verbose "ERROR in Fuzzy Query function" -ForegroundColor Red -BackgroundColor black
        Write-Verbose "ERROR: $($_.exception.message)"
        return $null
    }

    Write-verbose "Exited Function: FuzzyLogic"
}


Export-ModuleMember -Function * -Alias *