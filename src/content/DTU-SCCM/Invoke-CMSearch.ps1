function Invoke-CMSearch{
  <# 
  .SYNOPSIS 
  Exports a task sequence to an XML file
  .DESCRIPTION 
  Performs a query against SMS_TaskSequencePackage and executes the Sequence method to display xml data. Exports the xml into an xml file
  .INPUTS 
  String

  .OUTPUTS 
  XML file
  .EXAMPLE 
  Export-TaskSequenceXML -name "SNOW5 Deployment" -folderpath "H:\exported-tasksequence-files"


  .NOTES 
  Initial Creation
  .LINK 
  http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 

  [CmdletBinding(SupportsShouldProcess=$true)]
  param(
    [Parameter(Mandatory=$false)][Alias("cred")][PSCredential]$global:credential,
    [Parameter(Mandatory=$false)][string]$config,
    [Parameter(Mandatory=$true)][Alias("cas")][string]$computername,
    [Parameter(Mandatory=$true)][string]$search,
    [Parameter(Mandatory=$false)][string]$tasksequencexml,
    [Parameter()][switch]$export=$true,
    [Parameter(Mandatory=$false)][switch]$uselegacywmi,
    [Parameter()][switch]$import=$false,
    [Parameter()][switch]$console=$false
  )


    
    


      # open a connection to the server
      $params=@{
        "computername"=$computername;
        "credential"=$credential;
        "uselegacywmi"=$uselegacywmi;
      }

      try {
        $so = Get-CMCimSession @params
        write-verbose "Successfully got the cim session"
        write-verbose "CIMSession = $($so.CimSession)"
        write-verbose "Namespace = $($so.Namespace)"
      }
      catch{
        write-error $_.Exception.Message

      }  

      $params = @{
          "computername"=$computername;
          "credential"=($global:credential);
          "uselegacywmi"=$uselegacywmi;
          # "CimSession"=$so.CimSession;
          "Namespace"=$so.Namespace;
          "Filter"="Name like '%$search%' or PackageID like '%$search%' or Description like '%$search%' ";
      }

      if(($($so.cimsession).length) -gt 0) {
        $params.Add("CimSession","$($so.CimSession)")
      }

      

    

    

    

      $genericObjectArray = @()    
      $tsequences = Get-GenericCimInstance @params -classname "SMS_Package" |  select -property PackageID,Name,Version,Description

      foreach ($property in $tsequences){
        write-verbose "$($property.Name) = $($property.Value)"
        write-verbose "$property"
      }

      $pkgs = Get-GenericCimInstance @params -classname "SMS_TaskSequencePackage" |  select -property PackageID,Name,Version,Description
      $advs = Get-GenericCimInstance @params -classname "SMS_Advertisement" |  select -property PackageID,AdvertisementName,AdvertisementID,AdvertFlags,Comment,ProgramName,AssignedScheduleEnabled,AssignedSchedule
    
      $genericObjectArray += $tsequences
      $genericObjectArray += $pkgs
      $genericObjectArray += $advs
      $genericObjectArray
}



Set-Alias -name cmsearch -value Invoke-CMSearch

# $params = @{
#     cas="hqbusm9902.nestle.qa";
#     sitecode="QA0";
#     search="-BL";
#     -credential (Get-Credential)
# }
$VerbosePreference="Continue"
cmsearch -cred (Get-Credential) -computername hqbusm9902.nestle.qa -search "SNOW5 Deployment" -uselegacywmi | ogv
