
function New-CMDriversFromInternet{
    $mdtserver="hqbusm9904.nestle.qa"
    $zti="\\$mdtserver\zti$"
    $infrastructure="\\$mdtserver\zti$"
    $packages="\\$mdtserver\packages$"
    $vendor="HP"
    $name = "HP EliteBook 820 G2"
    $shortname = $name.Replace(" ","")
    $platform = "SNOW6"
    $proc="x64"
    $originalpath="\\Hqbusm9904.nestle.qa\Infrastructure$\OSClients\Drivers\ToImport\$platform\$proc\$shortname"
    $driverpackagename="$platform-$proc $name"
    $driverpackagepath="\\HQBUSM9904\ZTI$\$platform-$proc\$vendor\$name"

    write-verbose "Importing drivers from $originalpath with full name of $name and driver package name $driverpackagename"

    if(test-path $originalpath){
        write-verbose "Yay"
    }
    else{
        write-verbose "Boo"
    }

}