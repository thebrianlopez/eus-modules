# This module serves as a wrapper for PowerShellGet and PackageManagement. This is intended to be used as a singular command that uses powershell package management and 
# degradesto using nuget if it exists on the compulter

function Install-CustomPackage{
    param(
        [Parameter(Mandatory=$true)][string[]]$searchterm,
        [Parameter(Mandatory=$false)][System.IO.DirectoryInfo]$destination="$env:appdata\nuget\programs",
        [Parameter(Mandatory=$false)][System.IO.FileInfo]$nuget="$env:appdata\nuget\programs\nuget\content\nuget.exe",
        [Parameter(Mandatory=$false)][switch]$forcenuget = ($psversiontable.PSVersion.Major -lt 5)
        )

    foreach($item in $searchterm){

      if($forcenuget){
        write-debug "Using Nuget"
        write-debug "Find-Module missing. Using nuget.exe to install package"
        Start-CustomProcess "$nuget" "install $item -excludeversion -verbosity detailed -o `"$destination`" "
      }
      else{
        write-debug "Using PowerShell5 Package management"
        write-debug "Find-package exists. Using native commandlets to install package"
        $fp = @{
            "name"="$item";
            "ProviderName"="Nuget"
        }
        $ip = @{
            "destination"="$destination"
            "excludeversion"=$true
            "verbose"=$true
        }
        find-package @fp | install-package @ip
      }
    }
}

function Install-CustomModule{
    param(
        [Parameter(Mandatory=$true)][string[]]$searchterm,
        [Parameter(Mandatory=$false)][switch]$user=$true,
        [Parameter(Mandatory=$false)][switch]$system=$false,
        [Parameter(Mandatory=$false)][System.IO.DirectoryInfo]$destination,
        [Parameter(Mandatory=$false)][System.IO.FileInfo]$nuget="$env:appdata\nuget\programs\nuget\content\nuget.exe",
        [Parameter(Mandatory=$false)][switch]$forcenuget = ($psversiontable.PSVersion.Major -lt 5)
      )

    write-debug "By default we assume the modules is going to install under the user context"

    switch($system){
      $true{$scope="AllUsers";$destination="$env:programfiles\WindowsPowerShell\Modules";}
      $false{$scope="CurrentUser";$destination="$env:userprofile\Documents\WindowsPowerShell\Modules"}
      default {write-error "Please specify the installation scope"}
    }

    foreach($item in $searchterm){
      if($forcenuget){
        write-debug "Using Nuget"
        write-debug "Find-Module missing. Using nuget.exe to install package"
        Start-CustomProcess "$nuget" "install $item -excludeversion -verbosity detailed -o `"$destination`" "
      }
      else{
        write-debug "Using PowerShell5 Package management"
        write-debug "Find-package exists. Using native commandlets to install package"
        $fp = @{
            "name"="$item";
        }
        $ip = @{
            "force"=$true;
            "scope"=$scope;
            "verbose"=$true;
        }
        find-module @fp | install-module @ip
      }
    }
}