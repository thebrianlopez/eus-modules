﻿Function Export-CustomCMISODatFile{
    PARAM(
        [string]$MountedWimDirectory
    )

    If(Test-Path "$MountedWimDirectory\SMS"){
        Remove-Item -Recurse -Force "$MountedWimDirectory\SMS"
    }

    $ps = new-object System.Diagnostics.Process
    $ps.StartInfo.Filename = "$($scriptdirectory)\7z.exe"
    $ps.StartInfo.Arguments = "x $($env:temp)\Bootimage.iso sms\data -o$MountedWimDirectory"
    $ps.StartInfo.RedirectStandardOutput = $True
    $ps.StartInfo.UseShellExecute = $false
    $ps.start()
    $ps.WaitForExit()
    [string]$Out = $ps.StandardOutput.ReadToEnd();
    write-verbose $Out
    if($ps.ExitCode -eq 0){
        if($out.contains("error")){return $false}
        else{return $true}
        }
    else{return $false}
}

Function Export-CustomCMIso{
    PARAM(
        [string]$ProviderName,
        [string]$ProviderSiteCode,
        [string]$DistributionPoint,
        [string]$Password,
        [string]$PackageID


    )
    $results = $false
	
	
	write-verbose "Detecting processor architecture..."
    $arch=[System.Environment]::Is64BitProcess
    write-verbose "Arch is set to $arch"

    if($arch){
        write-verbose "Running under amd64 is not supported due to tsmediaapi.dll, closing."
        write-error "Script must be run on i386 platform"
        throw
    }
    else {
        write-verbose "i386 session detected, continuing."
        $dll = "$scriptdirectory\content\tsmediaapi.dll"
        write-verbose "Checking of dll exists: $dll"
        Test-Path $dll -ea Stop
        regsvr32 $dll /s
    }
    
    write-verbose "Step 2 - Test Access to tsmediaapi"
		
    try{
        $tsmedia = New-Object -ComObject Microsoft.ConfigMgr.TsMedia
        $VBControl = New-Object -ComObject ScriptControl
        if(!($tsmedia) -or !($VBControl)){
            write-error "COM Objects could not be loaded"
            throw
        }
		write-verbose "Com objects loaded successfully."
	}
	catch{
        write-verbose "COM Objects could not be loaded: $_.Exception.Message "
        throw
    }

    write-verbose "Attempting to call CreateBootMedia method to create SCCM Boot Media"
	$bootimgpath = "$($env:temp)\Bootimage.iso"
	write-verbose "Checking if boot image exists at $bootimgpath"
     If(Test-Path -path $bootimgpath){
        Remove-Item "$bootimgpath"
		write-verbose "Removed previous path $bootimgpath"
    }

    $chararr = [System.Text.Encoding]::UTF8.GetBytes($Password)
    $encodedpassword = [system.convert]::ToBase64String($chararr)
    $tsmedia.ProviderName = $ProviderName
    $tsmedia.SiteCode = $ProviderSiteCode
    $tsmedia.DistributionPoints = $DistributionPoint
    $tsmedia.ConnectionOptions = ""
    $tsmedia.MediaLabel = $encodedpassword
    $VBControl.language = "VBScript"
    $vbnull = $VBControl.Eval("NULL")
    [datetime]$startdate = (get-date)
    [datetime]$findate = $((get-date).addyears(10))

	write-verbose "Attempting to run create method"

    try{
        # $hr = $tsmedia.createbootmedia("CD","$($env:temp)\Bootimage.iso",$PackageID,"",$vbnull,$vbnull,$startdate,$findate,$true)
        $hr = $tsmedia.createbootmedia("CD",$bootimgpath,$PackageID,"",$vbnull,$vbnull,$startdate,$findate,$true)
    }
    catch{
        write-verbose "Thre was an exception calling CreateBootMedia COM Method."
        throw $_.Exception
    }
   
   do{
        write-verbose "$($tsmedia.CurrentStep)/$($tsmedia.numsteps) ($($tsmedia.StepProgress)) $($tsmedia.StepInfo)"
   }
   while($tsmedia.status -eq 0)


    write-verbose "Boot Media creation finished with result $($tsmedia.exitcode)"

    if($TSMedia.ExitCode -eq 0){
        $results = $true
    }
    return $results;
	
	throw;
}

function Get-ScriptDirectory
{ 
    if($hostinvocation -ne $null)
    {
        Split-Path $hostinvocation.MyCommand.path
    }
    else
    {
        Split-Path $script:MyInvocation.MyCommand.Path
    }
}

function Invoke-CreateDatFile{
<#

.SYNOPSIS 
This powershell script automates the creation of a VARIABLES.DAT (contained in the SMS\Data directory( file to be used by the SNOW5 OSD Deployment process.
It replaces the 1e tool UpdateBootImage.exe as it provides support for the MediaLabel variable.

.DESCRIPTION 
Using the TSMediaAPI.dll file this script will generate SCCM Boot Media using the parameters defined on the commandline.
Once the ISO Image is created the script will then use the GNU tool 7z.exe to extract the SMS\DATA Folder which contains the VARIABLES.DAT file (among others).
This folder must then be manually injected into the root of the boot.wim file of the appropriate boot media package.

Of particular impotance is the -PXEBootPassword switch. 
This parameter contains the password for all webservice calls performed as part of the PXE Boot process.
The script will encode the password into BASE64 and add it to the VARIABLES.DAT file.

If no parameters are defined for this script the user will be prompted to enter the information at run time.

Parameters:
    -ProviderName - The server to connect to when querying SCCM for the package information
    -PorviderSiteCode - The sitecode of the Providerserver
    -DistributionPoint - The location of the bootimage file
    -BootImagePackageID - The bootimage package ID to be used by the boot media
    -PXEBootPassword - THe password to be injected into the VARIABLES.DAT for webservice calls
    -DestinationPath - Location to store the SMS Folder which contains most importantly the VARIABLES.DAT File.
    -verbose - increases logging activity

.EXAMPLE 
./CreateDATFile.PS1

.NOTES 
This script requires the following:
    1. 32bit command prompt or 32bit system
    2. A system with the SCCM Console installed

This script supports the -verbose switch for logging.

.LINK 
http://www.neslte.com

#>


[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True)][string]$SourceWimFile,
  [Parameter(Mandatory=$True)][string]$ProviderName,
  [Parameter(Mandatory=$True)][string]$ProviderSiteCode,
  [Parameter(Mandatory=$True)][string]$DistributionPoint,
  [Parameter(Mandatory=$True)][string]$BootImagePackageID,
  [Parameter(Mandatory=$True)][string]$PXEBootPassword,
  [Parameter(Mandatory=$True)][string]$DestinationPath

)
		$mountpath="$($env:temp)\wim-mount"
        $scriptdirectory = Get-ScriptDirectory
        if((test-path -path $SourceWimFile) -ne $true){write-error "Wim files $SourceWimFile does not exists.";throw;}


        write-verbose "Step 1: Verify dlls and pre-req files are present in module"
        


<#
        write-verbose "Step 3 - Mount Wim File to filesystem using dism commands: $SourceWimFile <> $mountpath"
		$filepath = "dism.exe"
		$args = "/Mount-Image /ImageFile:$SourceWimFile /index:1 /MountDir:$mountpath /LogPath:$($mountpath)_dism.log"
		write-verbose "Running command $filepath $args"
		if((test-path -path $mountpath) -eq $false) {mkdir $mountpath}
		# Dism /Mount-Image /ImageFile:"$SourceWimFile" /index:1 /MountDir:"$mountpath"
		$ps = new-object System.Diagnostics.Process
		$ps.StartInfo.Filename = $filepath
		$ps.StartInfo.Arguments = $args
		$ps.StartInfo.RedirectStandardOutput = $True
		$ps.StartInfo.UseShellExecute = $false
		$ps.start()
		$ps.WaitForExit()
		[string]$Out = $ps.StandardOutput.ReadToEnd();
		write-verbose $Out
		
		throw;
#>




        write-verbose "Step 4: Generate the ISO file using the ConfigMgr tsmediaapi"
        try{
            $params = @{
                "ProviderName"=$ProviderName;
                "ProviderSiteCode"=$ProviderSiteCode;
                "DistributionPoint"=$DistributionPoint;
                "PackageID"=$BootImagePackageID;
                "Password"=$PXEBootPassword;
            }
            $returnISOGen = Export-CustomCMIso @params

            write-verbose "Generating ISO Image function returned $returnISOGen"

            # if($returnISOGen){;throw; }
            # else{write-verbose "Succesfully created media"}
        }
        catch{
            write-verbose "There was an error generating ISO Image"
            write-verbose $_.Exception.message
            # throw
        }

        throw;





        write-verbose "Step 5: Once iso file is generated we now need to extract the dat files to a location, most likely the directory where the pxe boot image is mounted"
        try{
            $returnExtract = Export-CustomCMISODatFile -SourceWimFile $DestinationPath
            if($returnExtract -eq $false){write-verbose "There was an error extracting DAT File";throw;}
            else{write-verbose "Succesfully extracted DAT File"}
        }
        catch{
            write-verbose "There was an error extracting DAT File"
            write-verbose $_.Exception.message
            throw
        }





        write-verbose "Step 6 - Copy the extracted sms\data folder to the mounted iso path"





        write-verbose "Step 7 - Commit the iso file and dismount"

    



        
        
        

        

        write-verbose "Step 8 - Cleanup the temp files"
        If(Test-Path "$($env:temp)\Bootimage.iso"){
        	    Remove-Item "$($env:temp)\Bootimage.iso"
        }

        write-verbose "Script completed succesfully"
}

Function Test-CreateDatFile{
    Invoke-CreateDatFile -ProviderName hqaaam0303.nestle.w2k -ProviderSiteCode D01 -DistributionPoint hqaaam0322.nestle.w2k -BootImagePackageID D0000068 -PXEBootPassword Nestle1234 -DestinationPath c:\temp -SourceWimFile "c:\temp\test.wim"
}
