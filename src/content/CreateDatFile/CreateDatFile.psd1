@{
ModuleToProcess = 'CreateDatFile.psm1'
ModuleVersion = '1.0'
GUID = 'a91640fa-ddd3-4096-8635-c16709998bea'
Author = 'EUS Team'
CompanyName = 'Company'
Copyright = '(c) 2014 Administrator. All rights reserved.'
Description = 'Company module'
RequiredAssemblies = @(
    'content\7z.dll',
    'content\tsmediaapi.dll'
)
FileList = @(
    'CreateDatFile.psm1',
    'content\7z.exe'
)
FunctionsToExport = '*'
CmdletsToExport = '*'
VariablesToExport = '*'
AliasesToExport = '*'
}
