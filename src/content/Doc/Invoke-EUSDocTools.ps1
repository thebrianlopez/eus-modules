function Convert-PandocMarkdownToWiki{
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true,Position=0)][string]$oldfile=$false,
        [parameter(Mandatory=$true,Position=1)][string]$newfile=$false
    )
    
    set-alias -name wr -value write-verbose
    $oldfile = resolve-path $oldfile
    wr "step 1: convert the existing mediawiki document to markdown"


    pandoc -f mediawiki -t markdown -s "$oldfile" -o "$newfile"

    wr "Step 2: prepend the html to make it readable in web browser"
    $newbody=(get-content $newfile)
    $header="<!DOCTYPE html><html><title>GTS EUS Standard Routine</title><xmp theme=`"cerulean`" style=`"display:none;`">"
    $footer="</xmp><script src=`"http://strapdownjs.com/v/0.2/strapdown.js`"></script></html>"
    set-content $newfile -value $header,$newbody,$script,$footer
    wr "Step 3: Arepend the html to make it readable in web browser"
}