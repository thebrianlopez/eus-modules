Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim wbem :Set wbem = CreateObject("WbemScripting.SWbemLocator") 
Dim wmi :Set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
Dim reg :Set reg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\default:StdRegProv")
Dim scriptversion : scriptversion = "0.20120919"
Dim globalDomainName : globalDomainName = "domain.com"
Dim scriptname : scriptname = wscript.scriptname
Dim usertemp :usertemp = sh.ExpandEnvironmentStrings("%localappdata%") & "\temp"
Dim loglocation: loglocation = sh.ExpandEnvironmentStrings("%programfiles%") & "\Nestle\Logs\"
Dim appdata: appdata = sh.ExpandEnvironmentStrings("%appdata%") 
Dim localappdata: localappdata = sh.ExpandEnvironmentStrings("%localappdata%") 
Dim programdata: programdata = sh.ExpandEnvironmentStrings("%programdata%") 
Dim computername :computername = sh.ExpandEnvironmentStrings("%computername%")
Dim filename :filename = loglocation & scriptname & ".log"
Dim logfile :Set logfile = fso.OpenTextFile(filename, 2, True, 0) 
Dim thisfolder: thisfolder = fso.GetFile(Wscript.ScriptFullName).ParentFolder & "\"
Dim defaultcachepath :defaultcachepath="D:\ESDCache\SMS\ccmcache"
Dim defaultcachesize :defaultcachesize=8192
Dim inventorywaittime: inventorywaittime= 30000
Dim sProviderServer
Dim sSiteCode
Dim sglobalDomainName
Dim sMDTSQLServer
Dim sMDTDataBase
Dim sMDTUser
Dim sMDTPassword
Dim sMDTUser2
Dim sMDTPassword2
Dim strComputerName
Dim strUserName
Dim strPassword
Dim boolWinPE
Dim oSMS
Dim adUsername
Dim adKeyword
Dim smsServerName
Dim smsSiteCode
Dim smsnsuri
Dim smsadsitename
Dim smssUsername2
Dim smssKeyword2
Dim DebugEnabled :DebugEnabled = true
Dim strTaskID
Dim strProgramName
Dim intTimeoutValue :intTimeoutValue = 900000 '// 15 minutes time out value
Dim waitinterval : waitinterval = 30000
Dim globalConfigFile: globalConfigFile = thisfolder & "config.ini"
dim n: n = 0
Dim Location, newcode,command,strdeletecache,pCacheLocation,pCacheSize
Dim ad : set ad = CreateObject("ADSystemInfo")

'// Nestle.com //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
sProviderServer=ConfigFile("sProviderServer")
sSiteCode=ConfigFile("sSiteCode")
sMDTSQLServer=ConfigFile("sMDTSQLServer")
sMDTDataBase=ConfigFile("sMDTDataBase")
sMDTUser=ConfigFile("sMDTUser")
sMDTPassword=ConfigFile("sMDTPassword")
sMDTUser2=ConfigFile("sMDTUser2")
sMDTPassword2=ConfigFile("sMDTPassword2")
adUsername=ConfigFile("adUsername")
adKeyword=ConfigFile("adKeyword")
smsServerName=ConfigFile("smsServerName")
smssUsername2=ConfigFile("smssUsername2")
smssKeyword2=ConfigFile("smssKeyword2")

'// Main /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DbgMsg("****** " & scriptname & " ******")
DbgMsg "Current Sitecode: " & GetLocalSiteCode
DbgMsg "Current Management Point: " & GetLocalMP
DbgMsg "Local Cache Location: " & GetCacheLocation
DbgMsg "Local Cache Size: " & GetCacheSize
DbgMsg "AD Site: " & ad.SiteName


'DbgMsg "QueryResource: " & 
DbgMsg SCCMConfigurationValidated

Function SCCMConfigurationValidated
	Dim results: results = false
	if(GetCacheLocation <> defaultcachepath) then 
		DbgMsg "Changing cache location"
		if(SetCacheLocation(defaultcachepath)) then InitiateDiscoveryPolicies
	end if

	if (GetCacheSize<>defaultcachesize) then 
		DbgMsg "Changing cachle size"
		if(SetCacheSize(defaultcachesize))then InitiateDiscoveryPolicies
	end if

	if(ValidateBoundary(ad.SiteName,GetLocalSiteCode)) then
		DbgMsg "SCCM Configuration has been validated"
		results = true
	end if
	SCCMConfigurationValidated = results
End Function


'// Private Functions ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function ADSiteCode(connection,adsite) 
    On Error Resume next
    Dim resources, resource 
    Set resources = connection.ExecQuery("Select SiteCode FROM SMS_Boundary WHERE DisplayName = '" & adsite & "'")
    If Err.Number<>0 Then 
		Wscript.Echo "Couldn't get resources"
	End if
    For Each resource In resources 
		ADSiteCode = resource.SiteCode 
    Next 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
    End If
End Function

Function ADSiteSCCMServer(connection,code) 
    On Error Resume next
    Dim resources, resource 
    Set resources = connection.ExecQuery("SELECT ServerName FROM SMS_SystemResourceList WHERE RoleName = 'SMS Site Server' AND SiteCode = '" & code & "' ")
    If Err.Number<>0 Then 
		Wscript.Echo "Couldn't get resources"
	End if
    For Each resource In resources 
		ADSiteSCCMServer = resource.ServerName 
    Next 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
    End If
End Function

Function ADSite
	Dim objADSysInfo :Set objADSysInfo = CreateObject("ADSystemInfo")
	ADSite = objADSysInfo.SiteName
End Function

Function IsRunning(processname)
	Dim service :set service = GetObject ("winmgmts:")
	Dim Process
	IsRunning = false
	for each Process in Service.InstancesOf ("Win32_Process")
		If Process.Name = processname then
			IsRunning = true
		End If
	Next
End Function

Function CopySource
	dim src,dst
	src = "\\usglnw0012.domain.com\localinstall\Tools\SCCMClient\package"
	dst = "c:\progra~1\Nestle\Support\SCCMRefresh"
	
	if not fso.folderexists(programs) then
		fso.createfolder programs
	end if
	if not fso.folderexists(programs & "\Nestle") then
		fso.createfolder programs & "\Nestle"
	end if
	if not fso.folderexists(programs & "\Nestle\Support") then
		fso.createfolder programs & "\Nestle\Support"
	end if
	if not fso.folderexists(programs & "\Nestle\Support\SCCMRefresh") then
		fso.createfolder programs & "\Nestle\Support\SCCMRefresh"
	end if	
	if not fso.folderexists(dst) then
		fso.createfolder dst
	end if
	sh.run "robocopy.exe /v /e /z " & src & " " & dst,0
	DbgMsg("robocopy.exe /v /e /z " & src & " " & dst)
	WaitWhileThisIsFinishesRunning("robocopy.exe")
End Function

Function RemoveClient
	Dim pwd,cmd
	cmd = programs & globalccmsetuppath
	DbgMsg("Removing client with command line: " & cmd)
	sh.run cmd,0
	WaitWhileThisIsFinishesRunning("ccmsetup.exe")
	DbgMsg("Removal complete")
End function

Function WaitWhileThisIsFinishesRunning(psCmd)
	DbgMsg("Waiting for " & psCmd & " to finish running...")
	Do While IsRunning(psCmd)
		'DebugMsg "*"
		Wscript.sleep 5000
	Loop
End Function

Function BlowOutWMI
	Dim pwd,cmd
	DbgMsg("Blowing out wmi...")
	pwd = "C:\Windows\System32\wbem\"
	cmd = "WinMgmt.exe /resetrepository"
	DbgMsg("Current diectory: " & pwd)
	DbgMsg("Commandline: " & cmd)
	sh.CurrentDirectory = pwd
	sh.run cmd,0
	pwd = "C:\windows\system32\"
	cmd = "reg delete " & chr(34) & "HKLM\SOFTWARE\Microsoft\SMS\Mobile Client\Software Distribution\State" & chr(34) & " /v Paused /f"
	DbgMsg("Current diectory: " & pwd)
	DbgMsg("Commandline: " & cmd)
	sh.CurrentDirectory = pwd
	sh.run cmd,0	
	DbgMsg("Finished rebuiling wmi...")
End Function

Function RecompileMOFS
	Dim pwd,cmd,Folder,Files,file,ext
	DbgMsg("Recompiling mof files...")
	pwd = "C:\Windows\System32\wbem\"
	ext = ".dll"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "c:\Windows\System32\regsvr32.exe /s " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
		end if
	next
	pwd = "C:\Windows\System32\wbem\"
	ext = ".mof"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "mofcomp.exe " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
			WaitWhileThisIsFinishesRunning("mofcomp.exe")
		end if
	next
	pwd = "C:\Windows\System32\wbem\"
	ext = ".mfl"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "mofcomp.exe " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
			WaitWhileThisIsFinishesRunning("mofcomp.exe")
		end if
	next
	DbgMsg("Finished recompiling mof files")
End Function


Sub DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End sub

Function DeleteSCCMRecord
	Dim pwd,cmd
	DbgMsg("Deleting the sccm records.")
	DeleteMe("usphxm0110")
	DeleteMe("usphxm0100")
	DeleteMe("usglnm0000")
	DeleteMe("ussolm0000")
	DbgMsg("Deletion complete.")
End function

Function DeleteMe(psServer)
	Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
	Dim serverattrb,resources,resource,test,mymac,mac,query
	serverattrb = Chr(105) & Chr(120) & Chr(51) & Chr(48) & Chr(85) & Chr(85) & Chr(51) & Chr(120) & Chr(57) & Chr(56)
	DbgMsg("Deleting from " & psServer)
	Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(psServer, "root\sms", ,,128) 
	Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
	Dim filename, Location,i,hostname
	hostname = sh.ExpandEnvironmentStrings("%computername%")
	For Each Location In providerLoc 
		If location.ProviderForLocalSite = True Then 
			Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode,, ,,128)
			Exit For 
		End If 
	Next
	mac = ""
	query = "Select * From SMS_R_System where NetbiosName like '" & ucase(hostname)  & "' or MacAddresses like '" & ucase(hostname) & "' or SMSUniqueIdentifier like '" & ucase(hostname) & "' or ResourceID like '" & ucase(hostname) & "'"
	DbgMsg("WMI Query: " & query)
	err.clear
	Set resources = swbemServices.ExecQuery(query)
	If Err.Number<>0 Then 
		DbgMsg "Couldn't get resources" 
	End If 
	If resources.Count=0 Then 
		DbgMsg "No resources found" 
	End If
	For Each resource In resources 
		for each mac in resource.MacAddresses
			mymac = mac 
		next
		DbgMsg "Found [" & resource.NetbiosName & "] :: [" & resource.ResourceID & "] with ID [" & resource.SMSUniqueIdentifier & "] and mac [" & mymac & "] at site [" & Location.SiteCode & "] [" & Location.Machine & "] :: " & DeleteResource(swbemServices,resource.ResourceID) 
	Next 
End function

Function DeleteResource (connection, resourceID) 
		Dim res :Set res = connection.Get("SMS_R_System.ResourceID='" & resourceID & "'") 
		err.clear
		On error resume next
		res.Delete_
		DeleteResource = "Completed with exitcode: " & Err.Number & " #notarealwarning"
End Function
	
Function InitiateDiscoveryPolicies
	Dim sccmAction,action,srv,actions,strHardwareInventory,strSoftwareInventory,strMachinePolicyEvaluation,strSoftwareUpdateDeployment,strSoftwareUpdateScan
	Dim strRefreshDefaultMP,strRefreshLocation,strRefreshProxyMP
	'// *********** SCCM Actions ******************
	strHardwareInventory = "{00000000-0000-0000-0000-000000000001}"
	'//	   strSoftwareInventory = "{00000000-0000-0000-0000-000000000002}"
	'//    strDataDiscovery = "{00000000-0000-0000-0000-000000000003}"
	'//    strMachinePolicyAssignmentRequest = "{00000000-0000-0000-0000-000000000021}"
	strMachinePolicyEvaluation = "{00000000-0000-0000-0000-000000000022}"
	strRefreshDefaultMP = "{00000000-0000-0000-0000-000000000023}"
	strRefreshLocation = "{00000000-0000-0000-0000-000000000024}"
	'//    strSoftwareMeteringReporting = "{00000000-0000-0000-0000-000000000031}"
	'//    strSourcelistUpdateCycle = "{00000000-0000-0000-0000-000000000032}"
	strRefreshProxyMP = "{00000000-0000-0000-0000-000000000037}"
	'//    strCleanuppolicy = "{00000000-0000-0000-0000-000000000040}"
	'//    strValidateAssignments = "{00000000-0000-0000-0000-000000000042}"
	'//    strCertificateMaintenance = "{00000000-0000-0000-0000-000000000051}"
	'//    strBranchDPScheduledMaintenance = "{00000000-0000-0000-0000-000000000061}"
	'//    strBranchDPProvisioningStatu Reporting = "{00000000-0000-0000-0000-000000000062}"
	'//	   strSoftwareUpdateDeployment = "{00000000-0000-0000-0000-000000000108}"
	'//    strStateMessageUpload = "{00000000-0000-0000-0000-000000000111}"
	'//    strStateMessageCacheCleanup = "{00000000-0000-0000-0000-000000000112}"
	strSoftwareUpdateScan = "{00000000-0000-0000-0000-000000000113}"
	'//    strSoftwareUpdateDeploymentReEval = "{00000000-0000-0000-0000-000000000114}"
	'//    strOOBSDiscovery = "{00000000-0000-0000-0000-000000000120}"

	'// List all client actions availible
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
			DbgMsg "-> Availible actions: " & action.Name
			action.PerformAction
	Next
	
	'// Request & Evaluate Machine Policy
	sccmAction = strMachinePolicyEvaluation
	Set srv = wbem.ConnectServer( , "root\ccm\invagt") 
	On error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Request & Evaluate Machine Policy") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next

	'// Hardware Inventory Collection Cycle
	sccmAction = strHardwareInventory
	Set srv = wmi.ConnectServer( , "root\ccm\invagt") 
	on error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Hardware Inventory Collection Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
	
	'// Software Updates Assignments Evaluation Cycle
	sccmAction = strSoftwareUpdateScan
	Set srv = wmi.ConnectServer( , "root\ccm\invagt") 
	On error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Software Updates Assignments Evaluation Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
End Function 

Function GetLocalMP
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	mp = "fspserv.ams.domain.com"
	sitecode = "U01"
	if err.number = 0 then
		mp = sms.GetCurrentManagementPoint
		sitecode = sms.GetAssignedSite	
	end if
	GetLocalMP = mp
    Set sms=Nothing
End Function

Function GetLocalSiteCode
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	mp = "fspserv.ams.domain.com"
	sitecode = "U01"
	if err.number = 0 then
		mp = sms.GetCurrentManagementPoint
		sitecode = sms.GetAssignedSite	
	end if
	GetLocalSiteCode = sitecode
    Set sms=Nothing
End Function

Function SetLocalSiteCode(argSiteCode)
	Dim oldsite, newsite
	Dim results : results = false
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	newsite = argSiteCode
	if err.number = 0 then
		oldsite = sms.GetAssignedSite
	end if
	DbgMsg "Oldsite=" & oldsite & " and newsite=" & newsite
	'// compare
	if(oldsite = newsite) then
		'// do nothing
		DbgMsg "Site are the same, doing nothing."
		'wscript.quit
	else
		'// reassign to new site
		DbgMsg "Reassigning..."
		sms.SetAssignedSite(argSiteCode)
		if err.number = 0 then
			oldsite = sms.GetAssignedSite
			DbgMsg "Reassignment successful. Initiating inventory after 30 seconds."
			wscript.sleep(inventorywaittime)
			ForceHardwareInv
		else
			DbgMsg "Reassignment failed"
			wscript.quit
		end if
		results = true
	end if
	SetLocalSiteCode = results
    Set sms=Nothing
End Function

Function ChangeSiteCode(strHost)
	'Connect to the Remote Computer Namespace
	Dim oSCCMNamespace :set oSCCMNamespace = GetObject("winmgmts://" & sMachine & "/root/ccm") 
	Dim oInstance :Set oInstance = oSCCMNamespace.Get("SMS_Client") 
	Dim oParams :set oParams = oInstance.Methods_("SetAssignedSite").inParameters.SpawnInstance_() 
	'Change the Site Code
	oParams.sSiteCode = sSiteCode 
	oSCCMNamespace.ExecMethod "SMS_Client", "SetAssignedSite", oParams
	set oSCCMNamespace = Nothing
End Function

Function GetCacheLocation
	Dim mp,results
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		results = ocache.Location
	else
		results = "** unknown **"
	end if
	GetCacheLocation = results
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function GetCacheSize
	Dim mp,results
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		results = ocache.TotalSize
	else
		results = "** unknown **"
	end if
	GetCacheSize = results
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function SetCacheLocation(cachelocation)
	Dim mp,results
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		ocache.Location = cachelocation
		results = cachelocation
	else
		results = "** error **"
	end if
	SetCacheLocation = results
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function SetCacheSize(cachesize)
	Dim mp,results	
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	Dim oldsize :oldsize=cint(ocache.TotalSize)
	Dim oldpath :oldpath = ocache.Location
	Dim newsize :newsize = cint(cachesize)
	'Dim newpath :newpath = cachepath
	if err.number <> 0 then 
		DbgMsg "Unable to connect to local SMS client object. Quiting"
		results = false
		wscript.quit
	end if
	
	'if ((newsize=oldsize) and (oldpath=newpath)) then
	if (newsize=oldsize) then
		DbgMsg "Cache is correctly configured to " & oldpath & " with size " & oldsize 
		results = true
	else
		DbgMsg "Configuring cache size to size " & newsize 
		'ocache.Location = newpath
		ocache.TotalSize = newsize
		results = true
	end if
	SetCacheSize = results
    set sms=nothing
    set orm=nothing
	set ocache = nothing
End Function

Function DeleteCache
	Dim oCacheElement,pkgcount
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	Dim oCacheElements :set oCacheElements=oCache.GetCacheElements
	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002
	pkgcount = 1
	for each oCacheElement in oCacheElements
		'DbgMsg "************************* Package " & oCacheElement.contentid & " (" &  pkgcount & ") *******************************"
		if(strdeletecache = "-delete") then 
			DbgMsg "Deleteing Cache " & oCacheElement.contentid & " #warning"
			ocache.DeleteCacheElement(oCacheElement.CacheElementID)
		else
			'DbgMsg "Content id: " & oCacheElement.contentid
		end if
			DbgMsg "Size: " & oCacheElement.contentsize & " Version: " & oCacheElement.contentversion & " Location: " & oCacheElement.location
			pkgcount = pkgcount + 1
	next
	'DbgMsg "*************************    end    ************************************"
End Function


Function ForceHardwareInv
	dim cmd
	cmd = "WMIC.exe /node:localhost /namespace:\\root\ccm\invagt path inventoryActionStatus where InventoryActionID=" & chr(34) & "{00000000-0000-0000-0000-000000000001}" & chr(34) & " DELETE /NOINTERACTIVE"
	sh.run cmd 
	DbgMsg "Executed: " & cmd & " with exit code " & err.description & " (" & err.number & ") "
	cmd = "WMIC.exe /node:localhost /namespace:\\root\ccm\invagt path inventoryActionStatus where InventoryActionID=" & chr(34) & "{00000000-0000-0000-0000-000000000002}" & chr(34) & " DELETE /NOINTERACTIVE"
	sh.run cmd 
	DbgMsg "Executed: " & cmd & " with exit code " & err.description & " (" & err.number & ") "
End Function


Function ValidateBoundary(pstrADSite,pstrSiteCode)
	Dim results : results = false
	Dim oSMSClient :Set oSMSClient = CreateObject ("Microsoft.SMS.Client") '// Creates SMS object to query the closest DP
	Dim lLocator :Set lLocator = CreateObject("WbemScripting.SWbemLocator")
	Dim ad :Set ad = CreateObject("ADSystemInfo")
	Dim ServerName :ServerName = smsservername
	Dim SiteCode :SiteCode = sSiteCode
	Dim sUsername2 :sUsername2=smssUsername2
	Dim sKeyword2 :sKeyword2=smsskeyword2
	Dim clientsitecode :clientsitecode = oSMSClient.GetAssignedSite
	Dim desiredsite: desiredsite = pstrSiteCode
	Dim adsitename :adsitename =  pstrADSite
	Dim nsuri :nsuri = "Root\SMS\Site_" & SiteCode
	Dim item,sc,sHostSitecode,oPSC
	If Err.Number <>0 Then 
		DbgMsg "Could not create SMS Client Object - quitting"
	End If
	
	'// query central server for sitecode of boundary
	Dim gservices: Set gservices = lLocator.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	Dim query: query = "SELECT SiteCode FROM SMS_Boundary WHERE DisplayName = '"& adsitename &"' "
	DbgMsg query & " : Count " & gservices.ExecQuery(query).count

	Dim icount: icount = 0
	For each item in gservices.ExecQuery(query)
		icount = iCount + 1
		sHostSitecode = item.SiteCode
		DbgMsg "Found " & adsitename & " as belonging to " & sHostSitecode
	Next
	
	if icount = 0 Then
		DbgMsg "Error No ConfigMgr site exists for ADSite: " & adsitename
		Exit Function
	else
		If sHostSitecode = "" Then
			DbgMsg "Error SiteCode is blank"
			exit function
		Else
			DbgMsg "Sitecode for ADSite: " & adsitename & " is " & sHostSiteCode
		End If
	end if
	
	DbgMsg "Check if client site matches boundary site"
	if(clientsitecode <> sHostSitecode) then
		DbgMsg "Error, client site does not match actual site. Moving to " & sHostSiteCode
		if(SetSiteCode(sHostSiteCode)) then 
			DbgMsg "Site reassignment successful. Running inventory trigger schedules"
			DbgMsg "Inventory sucessful: " & InitiateDiscoveryPolicies
			DbgMsg "Validating machine appears in site database"
			if(QueryResource(computername,sHostSiteCode)) then 
				DbgMsg "Validated the host appears in site database. Adding to site new build collection"
			else
				DbgMsg "Failed to find machine in new site database"
			end if
		else
			DbgMsg "Site reassignment failed"
		end if
		
		'// To do: Reassign site automagically 
		'// To do: wait for assignment to complete succesfully
		'// To do: retrieve new resource ID from new server
		'// To do: add workstation to the new build collection 
		'// Wait for task seuqence to reappear in WMI for this workstation
	else
		DbgMsg "Sitecode is ok"
		results = true
	end if
	ValidateBoundary = results
End Function

Function ConfigFile(myKey)
	Dim myFilePath : myFilePath = globalConfigFile
	Dim mySection: mySection = globalDomainName
	'DbgMsg "mkey=" & mykey
    Const ForReading   = 1
    Const ForWriting   = 2
    Const ForAppending = 8
    Dim intEqualPos
    Dim objFSO, objIniFile
    Dim strFilePath, strKey, strLeftString, strLine, strSection
    Set objFSO = CreateObject( "Scripting.FileSystemObject" )
    ConfigFile     = ""
    strFilePath = Trim( myFilePath )
    strSection  = Trim( mySection )
    strKey      = Trim( myKey )
	'DbgMsg "Checking for " & strFilePath
    If objFSO.FileExists( strFilePath ) Then
        Set objIniFile = objFSO.OpenTextFile( strFilePath, ForReading, False )
        Do While objIniFile.AtEndOfStream = False
            strLine = Trim( objIniFile.ReadLine )

            ' Check if section is found in the current line
            If LCase( strLine ) = "[" & LCase( strSection ) & "]" Then
                strLine = Trim( objIniFile.ReadLine )

                ' Parse lines until the next section is reached
                Do While Left( strLine, 1 ) <> "["
                    ' Find position of equal sign in the line
                    intEqualPos = InStr( 1, strLine, "=", 1 )
                    If intEqualPos > 0 Then
                        strLeftString = Trim( Left( strLine, intEqualPos - 1 ) )
                        ' Check if item is found in the current line
                        If LCase( strLeftString ) = LCase( strKey ) Then
                            ConfigFile = Trim( Mid( strLine, intEqualPos + 1 ) )
                            ' In case the item exists but value is blank
                            If ConfigFile = "" Then
                                ConfigFile = " "
                            End If
                            ' Abort loop when item is found
                            Exit Do
                        End If
                    End If

                    ' Abort if the end of the INI file is reached
                    If objIniFile.AtEndOfStream Then Exit Do

                    ' Continue with next line
                    strLine = Trim( objIniFile.ReadLine )
                Loop
            Exit Do
            End If
        Loop
        objIniFile.Close
    Else
        WScript.Echo strFilePath & " doesn't exists. Exiting..."
        Wscript.Quit 1
    End If
End Function

Function SetSiteCode(argSiteCode)
	Dim oldsite, newsite
	Dim results : results = false
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	newsite = argSiteCode
	if err.number = 0 then
		oldsite = sms.GetAssignedSite
	end if
	DbgMsg "Oldsite=" & oldsite & " and newsite=" & newsite
	'// compare
	if(oldsite = newsite) then
		'// do nothing
		DbgMsg "Site are the same, doing nothing."
		'wscript.quit
	else
		'// reassign to new site
		DbgMsg "Reassigning..."
		sms.SetAssignedSite(argSiteCode)
		if err.number = 0 then
			oldsite = sms.GetAssignedSite
			DbgMsg "Reassignment successful. Initiating inventory after 30 seconds."
			wscript.sleep(inventorywaittime)
			ForceHardwareInv
		else
			DbgMsg "Reassignment failed"
			wscript.quit
		end if
		results = true
	end if
	SetSiteCode = results
    Set sms=Nothing
End Function

Function InitiateDiscoveryPolicies
	Dim results: results = false
	Dim sccmAction,action,srv,actions,strHardwareInventory,strSoftwareInventory,strMachinePolicyEvaluation,strSoftwareUpdateDeployment,strSoftwareUpdateScan
	Dim strRefreshDefaultMP,strRefreshLocation,strRefreshProxyMP
	'// *********** SCCM Actions ******************
	strHardwareInventory = "{00000000-0000-0000-0000-000000000001}"
	'//	   strSoftwareInventory = "{00000000-0000-0000-0000-000000000002}"
	'//    strDataDiscovery = "{00000000-0000-0000-0000-000000000003}"
	'//    strMachinePolicyAssignmentRequest = "{00000000-0000-0000-0000-000000000021}"
	strMachinePolicyEvaluation = "{00000000-0000-0000-0000-000000000022}"
	strRefreshDefaultMP = "{00000000-0000-0000-0000-000000000023}"
	strRefreshLocation = "{00000000-0000-0000-0000-000000000024}"
	'//    strSoftwareMeteringReporting = "{00000000-0000-0000-0000-000000000031}"
	'//    strSourcelistUpdateCycle = "{00000000-0000-0000-0000-000000000032}"
	strRefreshProxyMP = "{00000000-0000-0000-0000-000000000037}"
	'//    strCleanuppolicy = "{00000000-0000-0000-0000-000000000040}"
	'//    strValidateAssignments = "{00000000-0000-0000-0000-000000000042}"
	'//    strCertificateMaintenance = "{00000000-0000-0000-0000-000000000051}"
	'//    strBranchDPScheduledMaintenance = "{00000000-0000-0000-0000-000000000061}"
	'//    strBranchDPProvisioningStatu Reporting = "{00000000-0000-0000-0000-000000000062}"
	'//	   strSoftwareUpdateDeployment = "{00000000-0000-0000-0000-000000000108}"
	'//    strStateMessageUpload = "{00000000-0000-0000-0000-000000000111}"
	'//    strStateMessageCacheCleanup = "{00000000-0000-0000-0000-000000000112}"
	strSoftwareUpdateScan = "{00000000-0000-0000-0000-000000000113}"
	'//    strSoftwareUpdateDeploymentReEval = "{00000000-0000-0000-0000-000000000114}"
	'//    strOOBSDiscovery = "{00000000-0000-0000-0000-000000000120}"

	'// List all client actions availible
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
			DbgMsg "-> Availible actions: " & action.Name
			action.PerformAction
			results = true
	Next
	
	'// Request & Evaluate Machine Policy
	sccmAction = strMachinePolicyEvaluation
	Set srv = wbem.ConnectServer( ".", "root\ccm\invagt") 
	On error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Request & Evaluate Machine Policy") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next

	'// Hardware Inventory Collection Cycle
	sccmAction = strHardwareInventory
	Set srv = wbem.ConnectServer( , "root\ccm\invagt") 
	on error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Hardware Inventory Collection Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
	
	'// Software Updates Assignments Evaluation Cycle
	sccmAction = strSoftwareUpdateScan
	Set srv = wbem.ConnectServer( , "root\ccm\invagt") 
	On error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Software Updates Assignments Evaluation Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
	InitiateDiscoveryPolicies = results
End Function 

Function QueryResource(pHostname,pSitecode)
	Dim results : results = false
	Dim oSMSClient :Set oSMSClient = CreateObject ("Microsoft.SMS.Client") '// Creates SMS object to query the closest DP
	'Dim lLocator :Set lLocator = CreateObject("WbemScripting.SWbemLocator") -- replace by wbem global object
	'Dim ad :Set ad = CreateObject("ADSystemInfo")
	Dim ServerName :ServerName = smsservername
	Dim SiteCode :SiteCode = sSiteCode
	Dim sUsername2 :sUsername2=smssUsername2
	Dim sKeyword2 :sKeyword2=smsskeyword2
	'Dim clientsitecode :clientsitecode = oSMSClient.GetAssignedSite
	Dim desiredsite: desiredsite = pSitecode
	'Dim adsitename :adsitename =  pstrADSite
	Dim nsuri :nsuri = "Root\SMS\Site_" & SiteCode
	Dim item,sc,sHostSitecode,oPSC,gservices,query,icount,primarysiteserver
	
	'// query site server using sitecode
	Set gservices = wbem.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "SELECT ServerName FROM SMS_SystemResourceList WHERE RoleName = 'SMS Site Server' AND SiteCode = '" & desiredsite & "'"
	DbgMsg query & " : Count " & gservices.ExecQuery(query).count
	icount = 0
	For each item in gservices.ExecQuery(query)
		icount = iCount + 1
		primarysiteserver = item.ServerName
		DbgMsg "Found " & primarysiteserver & " as belonging to " & desiredsite
	Next
	
	ServerName = primarysiteserver
	nsuri = "Root\SMS\Site_" & desiredsite
	
	'// query clients in site database
	DbgMsg "Connecting to " & ServerName & "\" & nsuri & " as " & sUsername2
	Dim cmprov: Set cmprov = wbem.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "SELECT * FROM SMS_R_System WHERE NetbiosName like '%"& pHostname &"%' "
	DbgMsg query & " : Count " & cmprov.ExecQuery(query).count
	
	if(cmprov.ExecQuery(query).count < 1)  then
		DbgMsg "Unable to locate hostname in database"
	else
		DbgMsg "Found hostname "& pHostname &" in database"
		results = true
	end if
	
	QueryResource = results
End Function

sub break
	wscript.quit
end sub
