' Set up a connection to the local provider.
Option Explicit
dim fso :set fso = createobject("scripting.filesystemobject")
dim osh :set osh = createobject("wscript.shell")
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 
Dim smsserver :smsserver = wscript.arguments(0)
Dim search :search = wscript.arguments(1)
Dim forcedelete :forcedelete = wscript.arguments(2)
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator")
Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(smsserver, "root\sms")
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation")
Dim Location

For Each Location In providerLoc
    If location.ProviderForLocalSite = True Then
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode)
        Exit For
    End If
Next

Call EnumerateCollectionMembers(swbemServices,forcedelete)

sub EnumerateCollectionMembers(connection,booldelete)
	dim ind,collection,sublist,item,rume,ruleset
	Dim query1 :Query1 = "SELECT * FROM SMS_Collection WHERE OwnedByThisSite='1' and Name like '%" & search & "%' OR CollectionID Like '" & search & "' "
	DbgMsg query1
	Dim listOfCollections :set listOfCollections = connection.ExecQuery(Query1)
	for each collection In listOfCollections
			DbgMsg "" & collection.Name & " ("& collection.CollectionID & ")"
			set sublist = connection.Get("SMS_Collection='" & collection.CollectionID & "'")
			ruleset = sublist.CollectionRules
			if(TypeName(sublist.CollectionRules) = "Variant()") then
				for each item in ruleset
					if (item.Path_.Class = "SMS_CollectionRuleDirect" and item.ResourceID<>0) then
						DbgMsg MachineName(item.ResourceID,connection) &" from collection [" & collection.Name & " ("& collection.CollectionID & ")] = " & booldelete
						if(booldelete) then
							sublist.DeleteMembershipRule item
						end if
					end if
				next
			end if
    next
end sub

Function MachineName(strResourceID,xcon)
	Dim query,count,resource,results,smshostname,SMSUniqueIdentifier
	query = "Select * From SMS_R_System where ResourceID='" & ucase(strResourceID) & "'"
	'DbgMsg query
	count = 1
	Dim resources: Set resources = xcon.ExecQuery(query)
    If (Err.Number<>0 or resources.Count=0)  Then 
        'DbgMsg "Couldn't get resources" 
    else
		'DbgMsg "Found: " & resources.count
		For Each resource In resources
			'results=resource.Netbiosname & "(" & resource.SMSUniqueIdentifier & ")" 
			smshostname = resource.Netbiosname
			SMSUniqueIdentifier = resource.SMSUniqueIdentifier
			results = smshostname & " [" & SMSUniqueIdentifier & "]"
			count = count + 1
		Next 
	end if
	MachineName = results
End Function

Sub DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End sub
