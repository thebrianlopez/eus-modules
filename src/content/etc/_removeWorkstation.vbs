' Set up a connection to the local provider.
Option Explicit
dim fso :set fso = createobject("scripting.filesystemobject")
dim osh :set osh = createobject("wscript.shell")
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator")
Dim Location

if(wscript.arguments.count <> 4) then
	DbgMsg "Incorrect number of arguments. Usage: cscript.exe " & wscript.scriptname & " usphxm0110 "& chr(34) &"U01 - GLB0169B"& chr(34) &" US43-0DWI448 false|true"
	wscript.quit(0)
end if

Dim smsserver :smsserver = wscript.arguments(0)
Dim collectionitem :collectionitem = wscript.arguments(1)
Dim computername :computername = wscript.arguments(2)
Dim forcedelete :forcedelete = wscript.arguments(3)

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(smsserver, "root\sms")
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation")

For Each Location In providerLoc
    If location.ProviderForLocalSite = True Then
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode)
        Exit For
    End If
Next


Dim gresourceid: gresourceid = GetResourceID(swbemServices,computername)
if(RemoveWorkstationFromCollection(swbemServices,gresourceid,collectionitem,forcedelete)) then
	DbgMsg "Deletion was successful"
else
	DbgMsg "No items removed."
end if

wscript.quit(999)

Function RemoveWorkstationFromCollection(connection,presourceid,pcollectionname,booldelete)
	dim ind,collection,sublist,item,rume,ruleset,results
	Dim query :query = "SELECT * FROM SMS_Collection WHERE OwnedByThisSite='1' and Name like '%" & pcollectionname & "%' OR CollectionID Like '" & pcollectionname & "' "
	'DbgMsg query
	Dim listOfCollections :set listOfCollections = connection.ExecQuery(query)
	for each collection In listOfCollections
			'DbgMsg "" & collection.Name & " ("& collection.CollectionID & ")"
			set sublist = connection.Get("SMS_Collection='" & collection.CollectionID & "'")
			ruleset = sublist.CollectionRules
			if(TypeName(sublist.CollectionRules) = "Variant()") then
				for each item in ruleset
					if (item.Path_.Class = "SMS_CollectionRuleDirect" and item.ResourceID=presourceid) then
						DbgMsg "Found " & MachineName(item.ResourceID,connection) &" in [" & collection.Name & " ("& collection.CollectionID & ")] Delete: " & booldelete
						if(booldelete) then
							sublist.DeleteMembershipRule item
							results= true
						end if
					end if
				next
			end if
    next
	RemoveWorkstationFromCollection = results
End function

Function MachineName(strResourceID,xcon)
	Dim query,count,resource,results,smshostname,SMSUniqueIdentifier
	query = "Select * From SMS_R_System where ResourceID='" & ucase(strResourceID) & "'"
	count = 1
	Dim resources: Set resources = xcon.ExecQuery(query)
    If (Err.Number<>0 or resources.Count=0)  Then 
        'DbgMsg "Couldn't get resources" 
    else
		'DbgMsg "Found: " & resources.count
		For Each resource In resources
			'results=resource.Netbiosname & "(" & resource.SMSUniqueIdentifier & ")" 
			smshostname = resource.Netbiosname
			SMSUniqueIdentifier = resource.SMSUniqueIdentifier
			results = smshostname & " [" & SMSUniqueIdentifier & "]"
			count = count + 1
		Next 
	end if
	MachineName = results
End Function

Sub DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End sub

Function GetResourceID(pconnection,strSearchCriteria)
	dim item
	GetResourceID = "0"
	Dim query: query = "Select * From SMS_R_System where MacAddresses like '%"& strSearchCriteria &"%' or NetbiosName like '%" & strSearchCriteria & "%' or SMSUniqueIdentifier like '%" & strSearchCriteria & "%' or SMBIOSGUID like '%" & strSearchCriteria & "%' or LastLogonUserName like '%" & strSearchCriteria & "%' or ADSiteName='" & strSearchCriteria & "' "
	'DbgMsg query
	Dim results :set results = pconnection.ExecQuery(query)
	'DbgMsg "Records returned: " & results.count
	For each item In results
		GetResourceID = item.ResourceID
		DbgMsg "GetResourceID: found " & item.Name & " with resource id = " & item.ResourceID 
	Next
End Function


