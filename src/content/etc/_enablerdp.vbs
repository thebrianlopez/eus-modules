Option Explicit
Dim objWMIService, objItem, objService,colListOfServices, strComputer, strService, intSleep,colItems,errResult,xstring
 
strComputer = "."
strComputer = wscript.arguments(0)
intSleep = 15000
StartServ(strComputer)
StartRDP(strComputer)
AddUsertoRDPGroup strComputer,ADDomain 

wscript.quit
 
sub StartServ(thismachine)
	strService = " 'Remote Registry' "
	xstring = "winmgmts:" & "{impersonationLevel=impersonate}!\\" & thismachine & "\root\cimv2"
	DbgMsg "Starting " & strService & xstring
	Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & thismachine & "\root\cimv2")
	Set colListOfServices = objWMIService.ExecQuery ("Select * from Win32_Service Where Name =" & strService & " ")
		For Each objService in colListOfServices
			DbgMsg "Service: " & objService
			objService.StartService()
		Next
	DbgMsg strService & " service is running on " & thismachine
 
End Sub
 
sub StartRDP(thismachine)
	Const ENABLE_CONNECTIONS = 1
	Set objWMIService = GetObject("WinMgmts:{impersonationLevel=impersonate, authenticationLevel=PktPrivacy}!\\" & thismachine & "\root\cimv2/TerminalServices")
	Set colItems = objWMIService.ExecQuery ("Select * from Win32_TerminalServiceSetting")
	For Each objItem in colItems
		errResult = objItem.SetAllowTSConnections(ENABLE_CONNECTIONS)
	Next
End sub
 
sub AddUsertoRDPGroup(thismachine,myadsite)
	Dim objLocalGroup
	Dim objDomainUser
	Set objLocalGroup = GetObject("WinNT://" & thismachine & "/Remote Desktop Users,group")
	Set objDomainUser = GetObject("WinNT://"& myadsite &"/AMSWorkstationAdmins,group")
	DbgMsg "Member of group? : " & objLocalGroup.IsMember(objDomainUser.Name)
	If not (objLocalGroup.IsMember(objDomainUser.ADsPath)) Then
		on error resume next
		objLocalGroup.Add(objDomainUser.ADsPath)
		DbgMsg "Adding user " & objDomainUser.ADsPath & " to " & objLocalGroup.Name & " group"
	End If
	Set objDomainUser = GetObject("WinNT://"& myadsite &"/AOAWorkstationAdmins,group")
	DbgMsg "Member of group? : " & objLocalGroup.IsMember(objDomainUser.Name)
	If not (objLocalGroup.IsMember(objDomainUser.ADsPath)) Then
		on error resume next
		objLocalGroup.Add(objDomainUser.ADsPath)
		DbgMsg "Adding user " & objDomainUser.ADsPath & " to " & objLocalGroup.Name & " group"
	End If
	Set objDomainUser = GetObject("WinNT://"& myadsite &"/CTRWorkstationAdmins,group")
	DbgMsg "Member of group? : " & objLocalGroup.IsMember(objDomainUser.Name)
	If not (objLocalGroup.IsMember(objDomainUser.ADsPath)) Then
		on error resume next
		objLocalGroup.Add(objDomainUser.ADsPath)
		DbgMsg "Adding user " & objDomainUser.ADsPath & " to " & objLocalGroup.Name & " group"
	End If
	Set objDomainUser = GetObject("WinNT://"& myadsite &"/EURWorkstationAdmins,group")
	DbgMsg "Member of group? : " & objLocalGroup.IsMember(objDomainUser.Name)
	If not (objLocalGroup.IsMember(objDomainUser.ADsPath)) Then
		on error resume next
		objLocalGroup.Add(objDomainUser.ADsPath)
		DbgMsg "Adding user " & objDomainUser.ADsPath & " to " & objLocalGroup.Name & " group"
	End If
	Set objDomainUser = GetObject("WinNT://"& myadsite &"/USPCAnywhereW2k,group")
	DbgMsg "Member of group? : " & objLocalGroup.IsMember(objDomainUser.Name)
	If not (objLocalGroup.IsMember(objDomainUser.ADsPath)) Then
		on error resume next
		objLocalGroup.Add(objDomainUser.ADsPath)
		DbgMsg "Adding user " & objDomainUser.ADsPath & " to " & objLocalGroup.Name & " group"
	End If
	
	
end sub
 
Sub DbgMsg(message)
	Wscript.echo Now() & ": " & message
End Sub

Function ADSite()
	Dim result: result = false
	Dim objADSysInfo: Set objADSysInfo = CreateObject("ADSystemInfo")
	result = objADSysInfo.SiteName
	ADSite = result
End Function

Function ADDomain()
	Dim result: result = false
	Dim objADSysInfo: Set objADSysInfo = CreateObject("ADSystemInfo")
	result = objADSysInfo.DomainDnsName
	ADDomain = result
End Function
