'// 2012-05-01
'// Description: Create 24 SCCM Advertisements schedued to run daily then dump collections over night
'//  12 Collections for every hour (e.g., 7am-7pm)
'//  12 Collecitons for every half hour (e.g., 6:30,7:30,8:30)
'//  or create 1 advertiements that runs every 15 minutes mandatory 
'//   2 scripts --> 1 to add the machine to its primary shopping collection  2nd to remove workstations from collection when build process completes successfully and machine images correctly.
'// 
'// 
'// 
'// 
'// test
' --- Set Variables
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
'// Create Log File
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 

'// Usage if arguments are not 2 (sccmserver hostname)
if not (wscript.arguments.length=2 or wscript.arguments.length=3) then 
	Wscript.echo "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 

Dim filename, Location,i,servername,sitecode,query,SMS_AdvertisementID,Advertisement,statement,count

Dim forceapproval :forceapproval=false
servername = wscript.arguments(0)
query = wscript.arguments(1)

if (wscript.arguments.length=3) then
	forceapproval=wscript.arguments(2)
	DbgMsg "forcing approval: " & forceapproval
end if

query = wscript.arguments(1)

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
		sitecode = Location.SiteCode
        Exit For 
    End If 
Next

Dim wmiconnection :set wmiconnection=swbemServices
'DbgMsg "Sitecode=" & sitecode
'DbgMsg "servername=" & servername
'DbgMsg "query=" & query

'////////////////////////////////////////////////////////////

'SMS_AdvertisementID = "tst20408"
Dim replacementScheduleArray()
Dim SMS_ScheduleToken

' --- Create Objects
Dim loc: Set loc = CreateObject("WbemScripting.SWbemLocator")
Set swbemServices = loc.ConnectServer(servername , "root\SMS\site_" & siteCode)


smsApproveClients GetResourceID(query),forceapproval
'smsQueryEverything()
'smsTaskSequenceInfo()
'smsQueryPackageAttrib()
'smsCreateAdvertisement()
'smsReadAdvertisement()
'smsUpdateAdvertisement()
'smsDeleteAdvertisement()

' --- Refresh Package
Function smsReadAdvertisement()
		statement = "Select * From SMS_Advertisement where AdvertisementName like '%" & query & "%' or AdvertisementID like '%" & query & "%'  or collectionID='" & query & "' "
		DbgMsg "statement=" & statement
		Dim colAdvertisements :Set colAdvertisements = swbemServices.ExecQuery(statement)
		count = 1
		DbgMsg "Count=" & colAdvertisements.Count
		For each Advertisement In colAdvertisements
			DbgMsg "[" & Advertisement.AdvertisementName & " (" & Advertisement.AdvertisementID & ")] "
			DbgMsg "-----------> Program: [" & Advertisement.ProgramName & " (" & Advertisement.PackageID & ")] "
			DbgMsg "-----------> Collection: [" & GetCollectionName(Advertisement.collectionID) & " (" & Advertisement.collectionID & ")] "
		Next 
End Function

Sub DbgMsg(strMessage)
	dim statement: statement = Now() & ": " & strMessage
	wscript.echo statement
	logfile.writeline statement
end sub

Function GetCollectionName(strCollID)
	Dim statement2,icollection
	GetCollectionName = null
	statement2 = "Select Name From SMS_Collection where CollectionID='" & strCollID & "' "
	Dim colCollections :Set colCollections = swbemServices.ExecQuery(statement2)
	For each icollection In colCollections
		GetCollectionName = icollection.Name
		'DbgMsg "GetCollectionName=" & GetCollectionName
	Next
End Function


'// query task sequence presence
' --- Refresh Package
Function smsQueryPackageAttrib()
		Dim pkg
		statement = "Select * From SMS_Package where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		DbgMsg "statement=" & statement
		Dim colPackageInfo :Set colPackageInfo = swbemServices.ExecQuery(statement)
		count = 1
		DbgMsg "Count=" & colPackageInfo.Count
		For each pkg In colPackageInfo
			DbgMsg "[" & pkg.Name & " (" & pkg.packageid & ")] = " & pkg.Description
			'DbgMsg "-----------> Program: [" & pkg.ProgramName & " (" & Advertisement.PackageID & ")] "
		Next 
End Function

Function smsTaskSequenceInfo()
		Dim pkg
		statement = "Select * From SMS_TaskSequencePackage  where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		DbgMsg "statement=" & statement
		Dim colPackageInfo :Set colPackageInfo = swbemServices.ExecQuery(statement)
		count = 1
		DbgMsg "Count=" & colPackageInfo.Count
		For each pkg In colPackageInfo
			DbgMsg "[" & pkg.Name & " (" & pkg.packageid & ")] = " & pkg.Description
			'DbgMsg "-----------> Program: [" & pkg.ProgramName & " (" & Advertisement.PackageID & ")] "
		Next 
End Function

Function smsQueryEverything()
		Dim count: count = 1
		Dim item
		
		'// task sequence
		statement = "Select * From SMS_TaskSequencePackage  where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		Dim results: Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			DbgMsg "no results from: " & statement
		else		
			For each item In results
				DbgMsg "SMS_TaskSequencePackage: [" & item.Name & " (" & item.packageid & ")] = " & item.Description
			Next 
		end if		
		
		'results = nothing
		
		'// package info
		statement = "Select * From SMS_Package where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			'DbgMsg "no results from: " & statement
		else		
			For each item In results
				DbgMsg "SMS_Package: " & item.Name & " [" & item.packageid & "] = Version " & item.StoredPkgVersion & ": " & item.Description
			Next 
		end if
		
		'results = nothing
		
		'// advertisements
		statement = "Select * From SMS_Advertisement where AdvertisementName like '%" & query & "%' or AdvertisementID='" & query & "'  or collectionID='" & query & "' "
		Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			'DbgMsg "no results from: " & statement
		else		
			For each item In results
			
			DbgMsg "SMS_Advertisement: " & item.ProgramName & " (" & item.PackageID & ")"
			DbgMsg "SMS_Advertisement: ----------> Advertisement Name: [" & item.AdvertisementName & " (" & item.AdvertisementID & ")"
			DbgMsg "SMS_Advertisement: ----------> Collection: [" & GetCollectionName(item.collectionID) & " (" & item.collectionID & ")] "
		Next
		end if	
		
		'results = nothing
End Function

Function GetResourceID(strSearchCriteria)
	dim item,statement
	'DbgMsg strSearchCriteria
	GetResourceID = "0"
	'statement = "Select * From SMS_R_System where MacAddresses like '%"& strSearchCriteria &"%' or ResourceID in '" & strSearchCriteria & "' or NetbiosName like '%" & strSearchCriteria & "%' or HardwareID like '%" & strSearchCriteria & "%' or SMSUniqueIdentifier like '%" & strSearchCriteria & "%' or SMBIOSGUID like '%" & strSearchCriteria & "%' or LastLogonUserName like '%" & strSearchCriteria & "%' or ADSiteName like '%" & strSearchCriteria & "%' order by AgentTime desc"
	statement = "Select * From SMS_R_System where MacAddresses like '%"& strSearchCriteria &"%' or NetbiosName like '%" & strSearchCriteria & "%' or SMSUniqueIdentifier like '%" & strSearchCriteria & "%' or SMBIOSGUID like '%" & strSearchCriteria & "%' or LastLogonUserName like '%" & strSearchCriteria & "%' or ADSiteName='" & strSearchCriteria & "' "
	'// & "' order by AgentTime desc"
	DbgMsg statement
	Dim results: Set results = swbemServices.ExecQuery(statement)
	DbgMsg "Records returned: " & results.count
	For each item In results
		GetResourceID = item.ResourceID
		DbgMsg "GetResourceID: found " & item.Name & " with resource id = " & item.ResourceID 
	Next 
End Function

Function smsApproveClients(intResourceID,bforceapproval)
		Dim count: count = 1
		Dim item
		Const AllSystemsCollID = "SMS00001"
		'statement = "Select * From SMS_FullCollectionMembership where CollectionID='SMS00001' and ResourceID='" & intResourceID & "' "
		statement = "Select * From SMS_FullCollectionMembership where ResourceID='" & intResourceID & "' "		
		Dim results: Set results = swbemServices.ExecQuery(statement)
		dim controlint :controlint=0
		dim recordcount,intArryresourceID,x
		recordcount = 0
		recordcount = results.count
		dim myarray(0)
		if (recordcount=0) then 
			DbgMsg "no results from: " & statement
			wscript.quit
		else		
			For each item In results
					myarray(controlint) = item.ResourceID
					'DbgMsg "SMS_FullCollectionMembership: " & item.Name & " (" & item.ResourceID & ") IsApproved=[" & item.IsApproved & "] in collection [" & GetCollectionName(item.CollectionID) & "] (" & item.CollectionID &")"
					DbgMsg item.Name & " (" & item.ResourceID & ") with status [" & item.IsApproved & "] in collection [" & GetCollectionName(item.CollectionID) & "] (" & item.CollectionID &") "
			Next 
		end if
		Dim objCollection :Set objCollection = wmiconnection.Get("SMS_Collection")
		Dim inParams :Set inParams = objCollection.Methods_("ApproveClients").InParameters.SpawnInstance_()
		inParams.Properties_.Item("ResourceIDs") = myarray
		inParams.Properties_.Item("Approved") = True
		if(bforceapproval) then
			Dim outParams :Set outParams = wmiconnection.ExecMethod("SMS_Collection", "ApproveClients", inParams)	
			DbgMsg "Autoapproval completed with exit code : " & err.number & " (" & err.description & ")"
		end if
		If Err.Number <> 0 Then 
			DbgMsg "Failed. Error "&Err.Number&" ("&Err.Description&")!"
			wscript.quit
		Else
			DbgMsg "Succeeded "& Err.Number&" ("&Err.Description&")!"
		End If	
End Function

Function GetCollectionName(search)
	dim item,statement
	statement="SELECT * FROM SMS_Collection WHERE CollectionID='" & search & "' "
	dim results: Set results = swbemServices.ExecQuery(statement)
	for each item In results
		GetCollectionName = item.Name
		'DbgMsg "GetCollectionName: Found collection [" & item.Name & "] "
	next 
End function


