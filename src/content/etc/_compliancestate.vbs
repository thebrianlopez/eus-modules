Option Explicit
Dim Location, newcode,command,strdeletecache
Dim sh :Set sh = CreateObject("Wscript.Shell") 
Dim reg :Set reg=GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\default:StdRegProv")
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim logfile :Set logfile = fso.OpenTextFile("c:\Progra~1\Nestle\Logs\SCCM_ClientReinstallation.log", 8, True, 0) 
Dim wmi :Set wmi = CreateObject("WbemScripting.SWbemLocator") 
Dim computername :computername = sh.ExpandEnvironmentStrings("%computername%")
Dim programs :programs = "c:\progra~1"
Dim defaultcachepath :defaultcachepath="D:\ESDCache\SMS\ccmcache"
Dim defaultcachesize :defaultcachesize="8192"
Dim inventorywaittime: inventorywaittime= 30000

DbgMsg "ForceComplianceUpdate: " & ForceComplianceUpdate

Function ForceComplianceUpdate
	Dim results: results = false
    Dim newCCMUpdatesStore : set newCCMUpdatesStore = CreateObject ("Microsoft.CCM.UpdatesStore")
    newCCMUpdatesStore.RefreshServerComplianceState :   DbgMsg "ForceComplianceUpdate complete with exit code " & err.number & " - " & err.description
	if not (err.number <>0) then results=true
	ForceComplianceUpdate = results
End Function

Function DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End Function