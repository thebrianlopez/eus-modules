'
'


'
'
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 

'// Usage if arguments are not 2 (sccmserver hostname)
if wscript.arguments.length <> 2 then 
	Wscript.echo "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
Dim filename, Location,i,hostname

hostname = ucase(wscript.arguments(1))

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
        Exit For 
    End If 
Next

'//Main method
QueryObsoleteClients swbemServices

'// Private Functions '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'// Query Function
Function QueryObsoleteClients(connection) 
    Dim resources 
    Dim resource,test,mymac,mac,query,count,ipaddr,myip,mydate
    Dim clientdatetime: Set clientdatetime = CreateObject("WbemScripting.SWbemDateTime")
	mac = ""
	query = "SELECT * FROM SMS_SystemResourceList where RoleName='SMS Distribution Point' "
	wscript.echo query
	err.clear
    Set resources = connection.ExecQuery(query)

    If Err.Number<>0 Then 
        Wscript.Echo "Couldn't get resources" 
        wscript.quit
    End If 

    wscript.echo resources.count

    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
	wscript.quit
    End If

	count = 1
	For Each resource In resources
		Wscript.echo resource.RoleName
		Wscript.echo resource.ServerName
		count = count + 1
	Next 

	query = "select * from SMS_DistributionPointInfo where IsPeerDP=0"
	Set resources = connection.ExecQuery(query)
	if not resources.count > 1 then
		Wscript.Echo "No resources found" 
		wscript.quit
	else
		For Each resource In resources
			wscript.echo " ***************************** "
	      		wscript.echo resource.ServerName & " server name"
	      		wscript.echo resource.SiteCode & " site code"
			count = count + 1
		Next 
	end if
End Function

'// Delete Function
Function DeleteResource (connection, resourceID) 
    Dim res :Set res = connection.Get("SMS_R_System.ResourceID='" & resourceID & "'") 
	err.clear
	'On error resume next
    'res.Delete_
	if(err.Number <> 0) then 
		DeleteResource = Err.Number	
	else
		DeleteResource = "Success!"
	end if
End Function

Function WMIDateStringToDate(dtmBootup)
    WMIDateStringToDate = CDate(Mid(dtmBootup, 5, 2) & "/" & _
         Mid(dtmBootup, 7, 2) & "/" & Left(dtmBootup, 4) _
         & " " & Mid (dtmBootup, 9, 2) & ":" & _
         Mid(dtmBootup, 11, 2) & ":" & Mid(dtmBootup, _
         13, 2))
End Function

