Option Explicit
'========================================================================== 
' NAME:    EnableMachines.vbs 
' AUTHOR:  Rajeev Nair
' DATE  :  25/06/2014 
' COMMENT: This script will enable computer accounts on the domain 
'          according to a supplied list of machines. 
'
'========================================================================== 
'Constants Declaration 
Const ForReading = 1 'For text file reading 
Dim InputFileName: InputFileName = "ohcrap.txt" 
Dim DomainString: DomainString = "netbiosdomainname" 
'========================================================================== 
'On Error Resume Next 

Dim canada: canada = "OU=EUC Workstations,OU=Canada,OU=AMS,OU=Organizations,DC=nestle,DC=com"
Dim nppc: nppc = "OU=EUC Workstations,OU=NPPC AMS,OU=AMS,OU=Organizations,DC=nestle,DC=com"
Dim nusa: nusa = "OU=EUC Workstations,OU=USA,OU=AMS,OU=Organizations,DC=nestle,DC=com"
Dim nwams: nwams = "OU=EUC Workstations,OU=Waters AMS,OU=AMS,OU=Organizations,DC=nestle,DC=com"
Dim wastebasket: wastebasket = "OU=Waste Basket,OU=GC AMS,OU=AMS,OU=Organizations,DC=nestle,DC=com"
' --- Change this here:
Dim market: market = wastebasket
' ---
Dim myFSO: set myFSO = CreateObject("Scripting.FileSystemObject") 
Dim myFile: set myFile = myfso.openTextFile(InputFileName, ForReading) 
Dim DomainObj: set DomainObj = GetObject("WinNT://" & DomainString) 
Dim strComputer
Dim objComputer
Dim ldap
Dim count: count = 1
Do 
    strComputer = myFile.readline 
    on error resume next
	ldap = "LDAP://CN=" & strComputer & "," & market
    set objComputer = GetObject(ldap) 
    objComputer.AccountDisabled = FALSE 
    objComputer.SetInfo 
    wscript.echo count & ": Enabled account " & ldap & " Completed with exit code " & err.number & " with description " & err.description
    on error goto 0
    count = count + 1
Loop Until myFile.AtEndOfStream 
Set DomainObj = Nothing 
myFile.Close