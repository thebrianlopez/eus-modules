'==========================================================================
'
' VBScript Source File -- Created with SAPIEN Technologies PrimalScript 4.1
'
' NAME: NomadFix
'
' AUTHOR: Edward Sassor 
' DATE  : 8/13/2013
'
' USAGE: from an elevated prompt type: cscript nomadfix.vbs assetnumber
'
' COMMENT: Used to stop Nomad Branch Service & set startup to disabled then restart the 
' SMS Agent Host service. Can be used as a Nomad workaround for issues with packages 
' not coming down or hash mismatch during download.
'
'==========================================================================
Option Explicit
Dim strService, strtext, strComputer, strStatus, objServiceTest   
Dim objWMIService, objService, colListOfServices, objShell, objExecObject, colServicesTest 
Const HKEY_LOCAL_MACHINE = &H80000002

If WScript.arguments.length <> 1 Then 
	WScript.echo "Usage: cscript " & WScript.scriptname & " workstationname (e.g. cscript " & WScript.scriptname & " US43-0abc123"
	WScript.quit
end If

strComputer = WScript.arguments(0)

'''''''''''''''
' Main
''''''''''''''
If IsReachable2(strComputer)then
	NomadBranch(strComputer)
	CCMExec(strComputer)
Else WScript.Echo strComputer & " is unreachable and the script will quit"
End If
WScript.Quit
''''''''''''''
' End Main
'''''''''''''''''''''

Function IsReachable2(strComputer)
	Set objShell = WScript.CreateObject("WScript.Shell")
	IsReachable2=False
	Set objExecObject = objShell.Exec("cmd /c ping -n 2 -w 1000 " & strComputer)
	Do While Not objExecObject.StdOut.AtEndOfStream
	    strtext = objExecObject.StdOut.ReadLine()
	    If Instr(strtext, "Reply") > 0 Then
	        IsReachable2=True
	        WScript.Echo strComputer & " is reachable"
	        Exit Do
	    End If
	Loop
End Function

Function NomadBranch(strComputer)
'strService = " 'NomadBranch' "
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colListOfServices = objWMIService.ExecQuery ("Select * from Win32_Service Where Name = 'NomadBranch'") 
For Each objService In colListOfServices
objService.StopService()
objService.ChangeStartMode("Disabled")
Next 
WScript.Echo "The NomadBranch service was Stopped and startup set to Disabled" 
End Function

Function CCMExec(strComputer)
'strService = " 'CcmExec' "
Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
Set colListOfServices = objWMIService.ExecQuery ("Select * from Win32_Service Where Name = 'CcmExec'") 
For Each objService In colListOfServices
objService.StopService()
WScript.Echo "Please wait while the SMS Agent Host is stopped and restarted...can take several minutes"
'wait for service to stop
Do Until strStatus = "Stopped"
Set colServicesTest = objWMIService.ExecQuery("Select * from Win32_Service Where Name = 'CcmExec'", "WQL", 48)
	For Each objServiceTest In colServicesTest
 	strStatus = objServiceTest.State
 	WScript.Echo "Service Status: " & strStatus
	WScript.Sleep(30000)
	Next
	Set colServicesTest = Nothing
Loop
 
  objService.StartService()
Next 
WScript.Echo "The SMS Agent Host service was restarted" 
End Function

WScript.Quit
