'
'


'
'
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 




'Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer("root\sms") 
'Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
'Dim filename, Location,i


'For Each Location In providerLoc 
'    If location.ProviderForLocalSite = True Then 
'        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
'        Exit For 
'    End If 
'Next

'//Main method
'QueryObsoleteClients swbemServices
QueryInstalledUpdates

'// Private Functions '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'// Query Function
Function QueryObsoleteClients(connection) 
    Dim resources 
    Dim resource,test,mymac,mac,query,count,ipaddr,myip,mydate
	Dim results: results = 0
	Dim UpdateAssignment,CINumOrg,ResultCIs
	Dim UpdateAssignmentLazy,CINumAfter
	Dim ContainsExpired
	
	'query = "Select * From SMS_SoftwareUpdate where isexpired=1" : DbgMsg query
	'Set resources = connection.ExecQuery(query)
	'DbgMsg "Count: " & resources.count

	
	query = "select * from SMS_UpdatesAssignment order by TargetCollectionID asc" : DbgMsg query
	Set resources = connection.ExecQuery(query)
	DbgMsg "Count: " & resources.count
	
	
	
    
    count=1
    For each UpdateAssignment in resources
        Set UpdateAssignmentLazy = connection.Get("SMS_UpdatesAssignment.AssignmentID=" & UpdateAssignment.AssignmentID)
        ContainsExpired = UpdateAssignmentLazy.ContainsExpiredUpdates
        'If (ContainsExpired = true and UpdateAssignment.TargetCollectionID <> "AMS0006A") Then
        If (ContainsExpired = true) Then
				DbgMsg count & " :" & UpdateAssignment.AssignmentID & " has expired updates targetting collection " & UpdateAssignment.TargetCollectionID & " named " & CollectionName(UpdateAssignment.TargetCollectionID,connection)
				CINumOrg = UBound(UpdateAssignmentLazy.AssignedCIs) + 1
				ResultCIs = UpdateAssignmentLazy.AssignedCIs
				'DbgMsg "CINumOrg: " & CINumOrg
				'DbgMsg "ResultCIs: " & typename(ResultCIs)
            'For each ExpiredCI in ExpiredCIs
             '   ResultCIs = Filter(ResultCIs,ExpiredCI.CI_ID,FALSE)
            'Next
            CINumAfter = UBound(ResultCIs) + 1
            'UpdateAssignmentLazy.AssignedCIs=ResultCIs
            'UpdateAssignmentLazy.Put_
	    count = count + 1
	    results = count
        End If
    Next
    QueryObsoleteClients = results
End Function

Function WMIDateStringToDate(dtmBootup)
    WMIDateStringToDate = CDate(Mid(dtmBootup, 5, 2) & "/" & _
         Mid(dtmBootup, 7, 2) & "/" & Left(dtmBootup, 4) _
         & " " & Mid (dtmBootup, 9, 2) & ":" & _
         Mid(dtmBootup, 11, 2) & ":" & Mid(dtmBootup, _
         13, 2))
End Function


Sub DbgMsg(strMessage)
	wscript.echo strMessage
	logfile.writeline Now() & ": " & strMessage
end sub

Function CollectionName(strID,connection)
	Dim results: results = false
	Dim collection
	Dim query : query = "SELECT * FROM SMS_Collection WHERE CollectionID='" & strID & "' "
	Dim listOfCollections: Set listOfCollections = connection.ExecQuery(query)
	For Each collection In listOfCollections     
			'DbgMsg "" & collection.Name & " -> " & collection.CollectionID 
			results = collection.Name
    Next
	CollectionName = results
End Function

Function QueryInstalledUpdates
	
	Dim connection :Set connection= swbemLocator.ConnectServer(".", "root\ccm\SoftwareUpdates\DeploymentAgent") 
	Dim filename, Location,i,results,collection

	
	Dim query : query = "select * from CCM_AssignmentCompliance" :: DbgMsg query
	Dim listOfCollections: Set listOfCollections = connection.ExecQuery(query) :: DbgMsg "Count: " & listOfCollections.count
	For Each collection In listOfCollections     
			DbgMsg "" & collection.AssignmentId   & " :: " & collection.IsCompliant  & " :: " & collection.Signature 
			''" :: " & collection.Sources
			'" :: " & collection.FullName &_
			'" :: " & collection.Name &_
			'" :: " & collection.UpdateID &_
			'" :: " & collection.URL 
			results = true
    Next
	
	
	Set connection= swbemLocator.ConnectServer(".", "Root\ccm\softwareupdates\updatesstore") 
	query = "select * from CCM_UpdateStatus" :: DbgMsg query
	Set listOfCollections = connection.ExecQuery(query) :: DbgMsg "Count: " & listOfCollections.count
	For Each collection In listOfCollections     
			DbgMsg "" & collection.Article   & " :: " & collection.Bulletin  & " :: " & collection.ScanTime 
			'DbgMsg collection.Sources
			'" :: " & collection.FullName &_
			'" :: " & collection.Name &_
			'" :: " & collection.UpdateID &_
			'" :: " & collection.URL 
			results = true
    Next
	QueryInstalledUpdates = results
End Function
