'
'


'
'
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 

'// Usage if arguments are not 2 (sccmserver hostname)
if wscript.arguments.length <> 2 then 
	DbgMsg "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
Dim filename, Location,i,hostname

hostname = ucase(wscript.arguments(1))

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
        Exit For 
    End If 
Next

'//Main method
QueryObsoleteClients swbemServices

'// Private Functions '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'// Query Function
Function QueryObsoleteClients(connection) 
    Dim resources 
    Dim resource,test,mymac,mac,query,count,ipaddr,myip,mydate
    Dim clientdatetime: Set clientdatetime = CreateObject("WbemScripting.SWbemDateTime")
	mac = ""
	query = "Select * From SMS_R_System where MacAddresses like '%" & ucase(hostname) & "%' or ResourceID like '%" & ucase(hostname) & "%' or NetbiosName like '%" & ucase(hostname)  & "%' or HardwareID like '"& hostname&"' or MacAddresses like '%" & ucase(hostname) & "%' or SMSUniqueIdentifier like '%" & ucase(hostname) & "%'  or SMBIOSGUID like '%" & ucase(hostname) & "%'  or LastLogonUserName like '%" & ucase(hostname) & "%' or ADSiteName='" & ucase(hostname) & "' order by AgentTime desc"
	'query = "Select * From SMS_R_System where LastLogonUserName like '%" & ucase(hostname) & "%' or ADSiteName like '%" & ucase(hostname) & "%' order by AgentTime desc"
	DbgMsg query
	err.clear
    Set resources = connection.ExecQuery(query)
    If Err.Number<>0 Then 
        DbgMsg "Couldn't get resources" 
        wscript.quit
    End If 
    DbgMsg resources.count
    If resources.Count=0 Then 
        DbgMsg "No resources found" 
		wscript.quit
    End If
	count = 1
	For Each resource In resources
		mac = ""
		ipaddr=""
		for each mac in resource.MacAddresses
			mymac = mac 
			DbgMsg mymac
		next
		for each ipaddr in resource.IPAddresses
			myip = ipaddr
			DbgMsg myip
			myip=0
			'DbgMsg resource.AgentTime.length
			for each mydate in resource.AgentTime
				clientdatetime.value = mydate
				DbgMsg "[" & count & "] " & resource.NetbiosName & " |" & resource.ResourceID & " |" & resource.SMSUniqueIdentifier & " |" & mymac & " |" & resource.LastLogonUserName & " |Obsolete=" & resource.obsolete & " |" & resource.ADSiteName & " |" & WMIDateStringToDate(clientdatetime.value) & " |" & resource.SMSAssignedSites(0) & " |" & ipaddr 
			
			next
		next
		DbgMsg "-> [" & count & "] " & resource.NetbiosName & " |" & resource.ResourceID & " |" & resource.SMSUniqueIdentifier & " |" & mymac & " |" & resource.LastLogonUserName & " |Obsolete=" & resource.obsolete & " |" & resource.ADSiteName & " |" & WMIDateStringToDate(clientdatetime.value) & " |" & resource.SMSAssignedSites(0) & " |" & ipaddr 
'		DbgMsg resource.NetbiosName & " |" & resource.ResourceID & " |" & resource.SMSUniqueIdentifier & " |" & " |Obsolete=" & resource.obsolete & " |" & resource.ADSiteName 
		count = count + 1
    Next 
	
	wscript.quit
End Function

Function WMIDateStringToDate(dtmBootup)
    WMIDateStringToDate = CDate(Mid(dtmBootup, 5, 2) & "/" & _
         Mid(dtmBootup, 7, 2) & "/" & Left(dtmBootup, 4) _
         & " " & Mid (dtmBootup, 9, 2) & ":" & _
         Mid(dtmBootup, 11, 2) & ":" & Mid(dtmBootup, _
         13, 2))
End Function


Sub DbgMsg(strMessage)
	wscript.echo strMessage
	logfile.writeline Now() & ": " & strMessage
end sub


