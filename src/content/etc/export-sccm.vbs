' Setup a connection to the local provider. 
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
Set swbemServices= swbemLocator.ConnectServer(InputBox("Enter sccm server:"), "root\sms") 
Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
Dim filename,mymac,fullmac,myuuid,fulluuid
For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
		Wscript.echo "Found " & Location.Machine & " at site " & Location.SiteCode
        Exit For 
    End If 
Next

filename = "C:\temp\SCCM_Export_" & Location.SiteCode & ".csv"

'Main call 
QueryObsoleteClients swbemServices
sh.run("excel.exe " & filename)
wscript.echo "Done!"

Sub QueryObsoleteClients(connection) 
'Queries for obsolete clients and for any it finds calls DeleteResource 
    'On Error Resume next
    Dim resources
    Dim resource
	Dim csvfile :Set csvfile = fso.CreateTextFile(filename, True)
    ' Run the query.
    Set resources = connection.ExecQuery("Select * From SMS_R_System where Name like 'us%' and Obsolete='1'")
    ' Set resources = connection.ExecQuery("Select * From SMS_R_System where Name like 'us16%'")
    If Err.Number<>0 Then 
        Wscript.Echo "Couldn't get resources" 
        Wscript.Quit 
    End If 
	i = 0
	csvfile.writeline "SCCM Export for " & Location.Machine & " at site " & Location.SiteCode
	csvfile.writeline "Resource ID, Name, Last Logged on User, OS, Obsolete,AD Site Name,"
    For Each resource In resources 
		i = i+1
		for each fullmac in resource.MACAddresses
			mymac = fullmac
			csvfile.writeline resource.ResourceID & ", " & resource.Name & ", " & resource.LastLogonUserName & ", " & resource.OperatingSystemNameandVersion & ", " & resource.obsolete & ", " & resource.ADSiteName & ", " & mymac  & ", "  & resource.SMSUniqueIdentifier
		next
		'for each fulluuid in resource.SMSUniqueIdentifier
		'	myuuid = fulluuid
		'next
        'csvfile.writeline resource.ResourceID & ", " & resource.Name & ", " & resource.LastLogonUserName & ", " & resource.OperatingSystemNameandVersion & ", " & resource.obsolete & ", " & resource.ADSiteName & ", " & mymac  & ", "  & resource.SMSUniqueIdentifier
    Next 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
    End If
End Sub

Sub DeleteResource (connection, resourceID) 
'Deletes a specific resource 
    On Error Resume Next 
    Dim resource 
    Wscript.echo "Attempting to delete resource with ID " & ResourceID
    Set resource = connection.Get("SMS_R_System.ResourceID='" & resourceID & "'") 
    If Err.Number<>0 Then 
        Wscript.Echo "Couldn't get resource " + resourceID 
        Exit Sub 
    End If 
    resource.Delete_ 
    WScript.Echo "Resource deleted" 
    If Err.Number<>0 Then 
        Wscript.Echo "Couldn't delete " + resourceID 
        Exit Sub 
    End If 
End Sub
'Add pause to end in case script was double clicked so we can review what happened 
