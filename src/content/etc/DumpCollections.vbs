Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim wmi :Set wmi = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
Dim ad :Set ad = CreateObject("ADSystemInfo")
Dim scriptversion : scriptversion = "0.20120919"
Dim globalDomainName : globalDomainName = GetDomainNames 
Dim scriptname : scriptname = wscript.scriptname
Dim usertemp :usertemp = sh.ExpandEnvironmentStrings("%localappdata%") & "\temp"
Dim loglocation: loglocation = sh.ExpandEnvironmentStrings("%programfiles%") 
Dim appdata: appdata = sh.ExpandEnvironmentStrings("%appdata%") 
Dim localappdata: localappdata = sh.ExpandEnvironmentStrings("%localappdata%") 
Dim computername: computername = sh.ExpandEnvironmentStrings("%computername%") 
Dim programdata: programdata = sh.ExpandEnvironmentStrings("%programdata%") 
Dim filename :filename = loglocation & "\Nestle\Logs\" & scriptname & ".log"
Dim logfile :Set logfile = fso.OpenTextFile(filename, 2, True, 0) 
Dim thisfolder: thisfolder = fso.GetFile(Wscript.ScriptFullName).ParentFolder & "\"
Dim defaultcachepath :defaultcachepath="D:\ESDCache\SMS"
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator")
Dim wksincollection :Set wksincollection = CreateObject ("System.Collections.ArrayList") '// workstations currently in collections
Dim wksmigrated :Set wksmigrated = CreateObject ("System.Collections.ArrayList") '// workstations currently migrated
Dim wkstoremove :Set wkstoremove = CreateObject ("System.Collections.ArrayList") '// workstations to remove
Dim defaultcachesize :defaultcachesize=8192
Dim inventorywaittime: inventorywaittime= 30000
Dim sProviderServer
Dim sSiteCode
Dim sglobalDomainName
Dim sMDTSQLServer
Dim sMDTDataBase
Dim sMDTUser
Dim sMDTPassword
Dim sMDTUser2
Dim sMDTPassword2
Dim strComputerName
Dim strUserName
Dim strPassword
Dim boolWinPE
Dim oSMS
Dim adUsername
Dim adKeyword
Dim smsServerName
Dim smsSiteCode
Dim smsnsuri
Dim smsadsitename
Dim smssUsername2
Dim smssKeyword2
Dim DebugEnabled :DebugEnabled = true
Dim strTaskID
Dim strProgramName
Dim intTimeoutValue :intTimeoutValue = 900000 '// 15 minutes time out value
Dim waitinterval : waitinterval = 30000
Dim sdomain2
Dim ssdomain2
Dim Location
Dim globalConfigFile: globalConfigFile = thisfolder & "config.ini"
Dim n: n = 0
'// Main Execution Block //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

sProviderServer=Base64Decode(ConfigFile("sProviderServer"))
sSiteCode=Base64Decode(ConfigFile("sSiteCode"))
sMDTSQLServer=Base64Decode(ConfigFile("sMDTSQLServer"))
sMDTDataBase=Base64Decode(ConfigFile("sMDTDataBase"))
sMDTUser=Base64Decode(ConfigFile("sMDTUser"))
sMDTPassword=Base64Decode(ConfigFile("sMDTPassword"))
sMDTUser2=Base64Decode(ConfigFile("sMDTUser2"))
sMDTPassword2=Base64Decode(ConfigFile("sMDTPassword2"))
adUsername=Base64Decode(ConfigFile("adUsername"))
adKeyword=Base64Decode(ConfigFile("adKeyword"))
smsServerName=Base64Decode(ConfigFile("smsServerName"))
smssUsername2=Base64Decode(ConfigFile("smssUsername2"))
smssKeyword2=Base64Decode(ConfigFile("smssKeyword2"))

Dim iRetVal  :iRetVal = DumpCollections


'// End Main ///////////////////////////////////////////////////////////////////////////////////////////
'// Private fucntions ////////////////////////////////////////////////////////////////////////////////
Function DumpCollections()
	DbgMsg "Starting Dumpcollections"
	Dim results: results = false
	Dim smsserver: smsserver = WScript.Arguments.Named("server") : if not (Defined(smsserver)) then smsserver="usphxm0100"
	Dim collection: collection = WScript.Arguments.Named("collection") : if not (Defined(collection)) then collection="SNOW5 New Build Office-Prod"
	Dim forcedelete: forcedelete = WScript.Arguments.Named("forcedelete") : if not (Defined(forcedelete)) then forcedelete="false"
	Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(smsserver, "root\sms")
	Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation")
	Dim item
	For Each Location In providerLoc
		If location.ProviderForLocalSite = True Then
			Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode)
			Exit For
		End If
	Next
	
	
	
	'// set values from artguemyns arraws
	
	
	'// populate list of clients currently in collection

	
	'// get operating system of each client
	
	'// if os is windows7 populate arrayy
	
	'// remove all workstations in this array
	

	if(GetCollectionMembers(swbemServices,collection,forcedelete)) then
		DbgMsg "No items removed."
	else
		DbgMsg "Deletion was successful"
	end if
	DumpCollections = results
End Function 


Function AutoRoleUpdater()
	Dim item
	Dim strRole1
	Dim strRole2
	Dim pintMdtId
	Dim strPlatformCode
	DbgMsg sMDTSQLServer
	if (wscript.arguments.count<>7) then
		DbgMsg "Incorrrect number of arguments."
		DbgMsg "Usage: " & chr(34) &  wscript.scriptfullname & chr(34) & " " & chr(34) & "S5-GLB: Office Workstations" & chr(34) &  " " & chr(34) & "S5-US: Office Workstations" & chr(34) & " " & chr(34) & "S5" & chr(34) &  " " & chr(34) & "SNOW5 New Build Office-Migration" & chr(34) &  " " & chr(34) & "GLB0169B * true "
		wscript.quit(888)
	end if
	DbgMsg "----- Starting AutoRoleUpdater -----"
	Dim gstrHostname : gstrHostname = strHostname
	Dim gstrwmiMacAddress : gstrwmiMacAddress = strwmiMacAddress
	Dim gstrwmiUUID : gstrwmiUUID = strwmiUUID
	Dim gstrwmiSerialNo : gstrwmiSerialNo = strwmiSerialNo 
	Dim gbAutoConfigure : gbAutoConfigure = false '// True=Do Active Directory update and MDt update, False=Not auto configure
	Dim gintMdtMachineID :gintMdtMachineID=intMDTRecordID(gstrwmiMacAddress,gstrwmiUUID,gstrwmiSerialNo,gstrHostname,gbAutoConfigure)
	Dim strArgs : strArgs=""
	Dim strCollectionName
	DbgMsg "Current Sitecode: " & GetLocalSiteCode
	DbgMsg "Current Management Point: " & GetLocalMP
	DbgMsg "Local Cache Location: " & GetCacheLocation
	DbgMsg "Local Cache Size: " & GetCacheSize
	DbgMsg "Current Ad Site: " & ADSite
	DbgMsg ValidateBoundary(ADSite,GetLocalSiteCode)
	'SetCache defaultcachepath,defaultcachesize 
	Dim xstring :xstring="Provider=sqloledb;Data Source=" & sMDTSQLServer & ";" & "Initial Catalog=" & sMDTDataBase  & ";User Id=" & sMDTUser2 & ";Password=" & sMDTPassword2 & ";"
	DbgMsg "Args: " & wscript.arguments.count
	
	if (wscript.arguments.count<>7) then
		DbgMsg "Incorrrect number of arguments."
		DbgMsg "Usage: " & chr(34) &  wscript.scriptfullname & chr(34) & " " & chr(34) & "S5-GLB: Office Workstations" & chr(34) &  " " & chr(34) & "S5-US: Office Workstations" & chr(34) & " " & chr(34) & "S5" & chr(34) &  " " & chr(34) & "SNOW5 New Build Office-Migration" & chr(34) &  " " & chr(34) & "GLB0169B * true "
		wscript.quit(888)
	end if
	
	for each item in wscript.arguments
		strArgs=strArgs & item & " "
	next
	DbgMsg "FixWin32EncryptableVolume Successful?: "  & FixWin32EncryptableVolume
	DbgMsg "Statestore deleted?: " & DeleteFolder("C:\StateStore",true)
	DbgMsg "Launching script " & chr(34) &  wscript.scriptfullname & chr(34) &  " " & strArgs
	DbgMsg "Found mdt record " & gintMdtMachineID

	'else
	strRole1=wscript.arguments(0)
	strRole2=wscript.arguments(1)
	strPlatformCode=wscript.arguments(2)
	strCollectionName=wscript.arguments(3)
	strTaskID=wscript.arguments(4)
	strProgramName=wscript.arguments(5)
	gbAutoConfigure=wscript.arguments(6)

	pintMdtId=gintMdtMachineID
	Dim vbEvaluateMDTRecord :vbEvaluateMDTRecord=bEvaluateMDTRecord(strRole1,strRole2,pintMdtId,strPlatformCode,gbAutoConfigure)
	Dim vbEvaluateMachineOU :vbEvaluateMachineOU=bEvaluateMachineOU(gstrwmiMacAddress,gstrwmiUUID,gstrwmiSerialNo,gstrHostname,gintMdtMachineID,gbAutoConfigure)
	DbgMsg "vbEvaluateMachineOU=" & vbEvaluateMachineOU
	DbgMsg "vbEvaluateMDTRecord=" & vbEvaluateMDTRecord
	
	if (vbEvaluateMachineOU) and (vbEvaluateMDTRecord) then '// Machine is in the correct OU and appears in MDT correctly
		DbgMsg "Machine OU Check Passed."
		if(SCCMConfigurationValidated) then
			DbgMsg "Adding to new build collection"
			if(AddWorkstationToCollection(gstrHostname,strCollectionName,gbAutoConfigure)) then '// Machine added to collection correctly
				DbgMsg "[warn] Workstation added to the collection"
				n = 0
				Do While not SearchAvailibleSMSPrograms(strTaskID,strProgramName)
					n = n + waitinterval
					DbgMsg "SearchAvailibleSMSPrograms = " & SearchAvailibleSMSPrograms(strTaskID,strProgramName) & " .... Waiting "  & n & "/" & intTimeoutValue
					InitiateDiscoveryPolicies
					wscript.sleep waitinterval
					if (n = intTimeoutValue) then exit do
				Loop
				if(SearchAvailibleSMSPrograms(strTaskID,strProgramName)) then 
					'// workstation was able to find task sequence and launch it as soon as it arrived
					DbgMsg "Waited and was able to locate task sequence " & strTaskID & " (" & strProgramName & ") . "
					'// System Context
					Dim installdir: installdir="C:\Program Files\Nestle\Scripts\"
					Dim shortcutdir: shortcutdir = programdata & "\Microsoft\Windows\Start Menu\Programs\"
					DbgMsg "Installing SNOW5 Migration Assistant shortcut: " & InstallShortCut(thisfolder & "SNOW5 Migration Assistant.lnk",shortcutdir)
					DbgMsg "Installing Scripts: " & InstallShortCut(thisfolder & "Trigger_MigWiz.wsf",installdir)
					DbgMsg "Installing Icons: " & InstallShortCut(thisfolder & "*.ico",installdir)
					DbgMsg "Script completed succesully with exit code " & err.number
					wscript.quit(0)
				else
					'// Workstation was unable to see task sequence arrive
					DbgMsg "Waited for task sequence  to arrive but it timed out."
					wscript.quit(999)
				end if
			else
				DbgMsg "Machine was not added to collection succsessfully" ' // Machine was not added to collection succsessfully 
				wscript.quit(999)
			end if
		else
			DbgMsg "SCCM Configuration not validated."
			wscript.quit(999)
		end if
	else
		DbgMsg "Machine OU Incorrect. Failing script"
		wscript.quit(999)
	end if
	DbgMsg "----- Ending AutoRoleUpdater -----"
End Function

Sub DbgMsg(message)
	if(lcase(TypeName(message)) = "string" or lcase(TypeName(message) = "long")) then
		logfile.writeline Now() & ": " & message
		if(DebugEnabled) then WScript.Echo Now() & ": " & message
	else
		wscript.echo TypeName(message) & ": " & message
	end if
End sub

Function intMDTRecordID(strMacAddress,strUUID,strSerialNo,strHostname,gbAutoConfigure)
	Dim result :result = 0
	dim oconn :Set oconn = CreateObject("ADODB.Connection")
	dim ocmd :Set ocmd = CreateObject("ADODB.Command")
	dim xstring :xstring="Provider=sqloledb;Data Source=" & sMDTSQLServer & ";" & "Initial Catalog=" & sMDTDataBase  & ";User Id=" & sMDTUser2 & ";Password=" & sMDTPassword2 & ";"
	dim query,retRole
	Dim retId,n
	'DbgMsg "Starting intMDTRecordID"
	'DbgMsg xstring
	oconn.ConnectionString = xstring
	if not (IsConnected(sMDTSQLServer)) then
		DbgMsg "Not connected to server, closing script"
	end if
	oconn.open
	ocmd.ActiveConnection = oconn
	
	'// query get machine record id
	query = "SELECT top 1 * FROM ComputerIdentity ci inner join Settings st on ci.id=st.id WHERE ci.MacAddress='" &strMacAddress& "' and ci.UUID='" &strUUID& "' and ci.SerialNumber='"&strSerialNo&"' and st.ComputerName='"&strHostname&"' order by ci.id desc"
	DbgMsg "" & query
	Dim recordcount :recordcount = 0
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Dim records :Set records = oCmd.Execute
	If records.EOF Then 
		DbgMsg "No mdt records found. Failing the task sequence gracefully"
	Else
		n=0
		Do while (Not records.EOF)
			retId= records("ID")
			result = retId
			records.movenext
			n=n+1
		loop
	End if
	if n > 1 then
		'DbgMsg "[warning] Multipe records detected. Please delete the uneeded records from MDT"
		wscript.quit(999)
	else
		'DbgMsg "Found " & n & " records in MDT."
		'DbgMsg "Record Found: " & result
	end if
	intMDTRecordID = result
	'DbgMsg "intMDTRecordID completed with result " & result
End Function

Function mMoveMachineObject(gstrHostname,pstrMachineDN,pstrDestOU,gbAutoConfigure)
	Dim result :result = false
	Dim pstrHostname :pstrHostname=gstrHostname
	Dim objResult,sUsername2,sKeyword2,dstou
	DbgMsg "Starting mMoveMachineObject" 

	if (Len(pstrDestOU)=0) then
		DbgMsg "No OU defined on role. Exiting script."
		wscript.quit(2012)
	end if

	'// ad creds
	sUsername2         	= adusername
	sKeyword2          	= adkeyword

	Dim objOU :Set objOU = GetObject("LDAP:")
	Dim objAD :set objAD = objOU.OpenDSObject("LDAP://" & pstrDestOU,sUsername2,sKeyword2,1)
	
	if(gbAutoConfigure) then 
		DbgMsg "[warning] Auto configure is ENABLED. Updating AD OU from [" & strMachineObjectOU(pstrHostname) & "] to [" & pstrDestOU & "]"
		set objResult = objAD.MoveHere("LDAP://" & pstrMachineDN, vbNullString)
	else
		DbgMsg "[warning] Auto configure is disabled. Will not update active directory ou."
	End if
		
	'DbgMsg "Completed with exit code: " & err.number & " :: " & err.description
	if err then
		'DbgMsg "failed"
	else
		'DbgMsg "Successful"
		result = true
	end if
	mMoveMachineObject = result
	DbgMsg "mMoveMachineObject=" & result
End Function

Function mUpdateRole(strNewRole,intMDTRecordID)
	Dim result :result = false
	mUpdateRole = result
End Function

Function strwmiMacAddress
	Dim result :result = ""
	Dim objItem
	Dim filter_wireless :filter_wireless = "caption like '%wireless%' or caption like '%wi-fi%' or caption like '%wifi%' or caption like '%wlan%' or caption like '%b/g%' or caption like '%802%' or caption like '%6200 agn%' or caption like '%advanced-n%' or caption like '%ultimate-n%' "
    Dim filter_nonwifi :filter_nonwifi = "caption like '%pantech%' or caption like '%bluetooth%' or caption like '%microsoft%'"
    Dim filter_virtualadp :filter_virtualadp = "caption like '%vpn%' or caption like '%secureclient%' or caption like '%check point%' or caption like '%miniport%' or caption like '%async%' or caption like '%virtual%' or caption like '%1394%' or caption like '%tether%' or caption like '%broadband%' "
	Dim qryphysical :qryphysical = "SELECT * FROM Win32_NetworkAdapterConfiguration WHERE MacAddress is not null and Not(" + filter_virtualadp + ")"
    Dim qrywired :qrywired = "SELECT * FROM Win32_NetworkAdapterConfiguration where MacAddress is not null AND NOT (" + filter_wireless + ") and not (" + filter_nonwifi + ") and not(" + filter_virtualadp + ")"
    Dim qrywireless :qrywireless = "SELECT * FROM Win32_NetworkAdapterConfiguration where MacAddress is not null AND (" + filter_wireless + ") and not (" + filter_nonwifi + ") and not(" + filter_virtualadp + ")"
	Dim qryvirtualadp :qryvirtualadp = "SELECT * FROM Win32_NetworkAdapterConfiguration where MacAddress is not null AND (" + filter_virtualadp + ") and not(" + filter_wireless + ") and not(" + filter_nonwifi + ")"
	Dim qryalldapters :qryalldapters = "SELECT * FROM Win32_NetworkAdapterConfiguration where MACAddress is not null"
    Dim query: query = qrywired
	Dim strComputer
	'DbgMsg query
	strComputer = "."
	Dim objWMIService :Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	Dim IPConfigSet :Set IPConfigSet = objWMIService.ExecQuery(query,,48)
	For Each objItem in IPConfigSet
		DbgMsg "Found adapter " & objItem.caption & " [" & objItem.macaddress & "] "
		result = objItem.macaddress
	Next
	strwmiMacAddress=result
End Function

Function strwmiUUID
	Dim result :result = ""
	Dim objItem
    Dim query: query = "select * from Win32_ComputerSystemProduct"
	'DbgMsg query
	Dim objWMIService :Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	For Each objItem in objWMIService.ExecQuery(query,,48)
		DbgMsg "Found UUID "  & " [" & objItem.uuid & "] "
		result = objItem.uuid
	Next
	strwmiUUID = result
End Function

Function strwmiSerialNo
	Dim result :result = ""
	Dim objItem
    Dim query: query = "select * from Win32_BIOS"
	'DbgMsg query
	Dim objWMIService :Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	For Each objItem in objWMIService.ExecQuery(query,,48)
		DbgMsg "Found SerialNumber "  & " [" & objItem.serialnumber & "] "
		result = objItem.serialnumber
	Next
	strwmiSerialNo = result
End Function

Function strHostname
	Dim result :result = ""
	Dim objItem
    Dim query: query = "select * from Win32_ComputerSystem "
	'DbgMsg query
	Dim objWMIService :Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")
	For Each objItem in objWMIService.ExecQuery(query,,48)
		DbgMsg "Found Hostname "  & " [" & objItem.name & "] "
		result = objItem.name
	Next
	strHostname = ucase(result)
End Function

Function bEvaluateMachineOU(gstrwmiMacAddress,gstrwmiUUID,gstrwmiSerialNo,gstrHostname,gintMdtMachineID,gbAutoConfigure)
	Dim result :result = false
	Dim pstrMacAddress :pstrMacAddress=gstrwmiMacAddress
	Dim pstrUUID :pstrUUID=gstrwmiUUID
	Dim pstrSerialNo :pstrSerialNo=gstrwmiSerialNo
	Dim pstrHostname :pstrHostname=gstrHostname
	Dim pintMdtID :pintMdtID=gintMdtMachineID
	Dim pstrMachineDN :pstrMachineDN=strMachineDN(pstrHostname)
	Dim pstrMdtRole2OU :pstrMdtRole2OU=strMDTRoleOU(pintMdtID,2)
	Dim pstrCurrentMachineOU :pstrCurrentMachineOU=strMachineObjectOU(pstrHostname)
	DbgMsg "Starting bEvaluateMachineOU" 
	'DbgMsg "MDT Role 2 Configured OU: " & pstrMdtRole2OU
	DbgMsg pstrCurrentMachineOU & " <= Current OU"
	DbgMsg pstrMdtRole2OU & " <= Configured in MDT"
	'DbgMsg pstrMachineDN & " <= Machine DN"
	if(pstrCurrentMachineOU <> pstrMdtRole2OU) then
		DbgMsg "AD info is different. Moving workstation to correct role"
		'DbgMsg "Attempting to move machine " & pstrMachineDN & " to " & pstrMdtRole2OU
		Dim bMovesuccessful :bMovesuccessful = mMoveMachineObject(pstrHostname,pstrMachineDN,pstrMdtRole2OU,gbAutoConfigure)
			if(bMovesuccessful) then
				DbgMsg "Move Succeeded with code " & err.number
				result = true
			else
				DbgMsg "Move failed with code " & err.number
			end if
	else
		DbgMsg "Machine is a member of the OU configured in MDT. Continuing on."
		result = true
	end if
	'DbgMsg "Ou good ?: " & result
	bEvaluateMachineOU = result
	DbgMsg "bEvaluateMachineOU=" & bEvaluateMachineOU
End Function

function strUserDN(strActiveDirectoryObject)
end function

function strGroupDN(strActiveDirectoryObject)
end function

function strMachineDN(strActiveDirectoryObject)
	Dim result :result = ""
	DbgMsg "Starting strMachineDN with argument: " & strActiveDirectoryObject

	'// force usphx
	'Dim gstrglobalDomainName :gstrglobalDomainName = GetObject("LDAP://usphxb0017.domain.com/RootDSE").Get("defaultNamingContext")
	'Dim mydc :mydc = GetObject("LDAP://usphxb0017.domain.com/RootDSE").Get("dnshostname")
	
	'// use local
	Dim gstrglobalDomainName :gstrglobalDomainName = GetObject("LDAP://RootDSE").Get("defaultNamingContext")
	Dim mydc :mydc = GetObject("LDAP://RootDSE").Get("dnshostname")
	DbgMsg "connected to : dc " & mydc 
	
	Dim oConnection :Set oConnection = CreateObject("ADODB.Connection")
	Dim objWks,strParentOU,strADsPath
	Dim oCont :Set oCont = GetObject("GC:")
	Dim strQuery: strQuery = "<LDAP://" & gstrglobalDomainName & ">;(&(objectClass=Computer)(cn=" & strActiveDirectoryObject & "));distinguishedName;subtree"
	
	DbgMsg "strMachineObjectOU: " & strQuery & ": " & err.number & " " & err.description
	oConnection.Provider = "ADsDSOObject"  'The ADSI OLE-DB provider
	oConnection.Open "ADs Provider"
		
	'// Option 1
	Dim ocmd :Set ocmd = CreateObject("ADODB.Command") 
	ocmd.ActiveConnection = oConnection 
	ocmd.Properties("Size Limit")=100000
	ocmd.Properties("Page Size")=100000
	ocmd.Properties("Sort on") = "sAMAccountName"
	ocmd.commandtext = strQuery
	Dim oRecordset  :Set oRecordset = ocmd.Execute
	
	'// Option 2
	'Dim oRecordset  :Set oRecordset = oConnection.Execute(strQuery) ':Set oRecordset = CreateObject("ADODB.Recordset")
	
	If (Err.Number = "-2147217865") or (oRecordset.EOF AND oRecordset.BOF) Then
		result = "NOTFOUND"
	Else
		While Not oRecordset.EOF
			set objWks = GetObject("LDAP://" & oRecordset.Fields("distinguishedName"))
			strParentOU=Replace(objWks.distinguishedName,"LDAP://","")
			result = strParentOU
			oRecordset.MoveNext
		Wend		
	End If
	If Err Then result = "ERROR: " & Err.Description
	DbgMsg "strMachineDN = " & ucase(result)
	strMachineDN = ucase(result)
end function

function strGetDNAttribute(strObjectDN,strADAttrib)
end function

function strObjectOU(strDN)
end function

Function strMDTRoleOU(intMdtId,intRoleSequence) '// 2 int values
	Dim oconn :Set oconn = CreateObject("ADODB.Connection")
	Dim ocmd :Set ocmd = CreateObject("ADODB.Command")
	Dim xstring :xstring="Provider=sqloledb;Data Source=" & sMDTSQLServer & ";" & "Initial Catalog=" & sMDTDataBase  & ";User Id=" & sMDTUser2 & ";Password=" & sMDTPassword2 & ";"
	Dim result :result = ""
	Dim records,query,retRole,recordcount,retId,retMDTOU
	'DbgMsg "Starting strMDTRoleOU"
	'DbgMsg xstring
	oconn.ConnectionString = xstring
	oconn.open
	ocmd.ActiveConnection = oconn
	
	'// query get role id
	query = "SELECT top 1 * FROM Settings_Roles WHERE ID='" & intMdtId & "' and Sequence='" & intRoleSequence & "' " '// Get record ID
	'DbgMsg query
	recordcount = 0
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Set records = oCmd.Execute
	If records.EOF Then 
		'DbgMsg "This machine record is not configured with a second role. Exiting script."
		wscript.quit(999)
	Else '// records exisst therefore lets see how many
		Do while (Not records.EOF)
			retRole = records("Role")
			retId= records("ID")
			'DbgMsg "role=" & retRole
			'DbgMsg "id=" & retId
			records.movenext
		loop	
	End if
	
	'// query get role id
	query = "SELECT top 1 * FROM RoleIdentity ri where ri.Role ='" & retRole & "' "
	'DbgMsg query
	recordcount = 0
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Set records = oCmd.Execute
	If records.EOF Then 
		'DbgMsg "No records found. Failing the task sequence gracefully"
	Else '// records exisst therefore lets see how many
		Do while (Not records.EOF)
			retId= records("ID")
			'DbgMsg "id=" & retId
			records.movenext
		loop	
	End if
	
	'// query role settings
	query = "SELECT top 1 * FROM Settings st where st.Type='R' and st.id='" & retId & "' "
	'DbgMsg query
	recordcount = 0
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Set records = oCmd.Execute
	If records.EOF Then 
		'DbgMsg "No records found. Failing the task sequence gracefully"
	Else '// records exisst therefore lets see how many
		Do while (Not records.EOF)
			retMDTOU = records("MachineObjectOU")
			'DbgMsg "retMDTOU=" & retMDTOU
			records.movenext
			result = retMDTOU
		loop	
	End if
	oconn.close
	strMDTRoleOU = ucase(result)
end function

function intValidateRoles
end function

function strMachineObjectOU(pstrComputerName)
	Dim result :result = ""
	Dim pstrglobalDomainName :pstrglobalDomainName = GetObject("LDAP://RootDSE").Get("DefaultNamingContext")
	Dim oConnection :Set oConnection = CreateObject("ADODB.Connection")
	Dim strQuery :strQuery = "<GC://" & pstrglobalDomainName & ">;(&(objectClass=Computer)(cn=" & pstrComputerName & "));distinguishedName;subtree"
	Dim objWks,strParentOU
	DbgMsg "Starting strMachineObjectOU"
	'DbgMsg strQuery
	oConnection.Provider = "ADsDSOObject"  'The ADSI OLE-DB provider
	oConnection.Open "ADs Provider"
	Dim oRecordset  :Set oRecordset = oConnection.Execute(strQuery) ':Set oRecordset = CreateObject("ADODB.Recordset")
	If (Err.Number = "-2147217865") or (oRecordset.EOF AND oRecordset.BOF) Then
		result = "NOTFOUND"
	Else
		While Not oRecordset.EOF
			set objWks = GetObject("LDAP://" & oRecordset.Fields("distinguishedName"))
			strParentOU=Replace(objWks.Parent,"LDAP://","")
			result = ucase(strParentOU)
			'DbgMsg result
			oRecordset.MoveNext
		Wend		
	End If

	If Err Then result = Ucase("ERROR: " & Err.Description)
	strMachineObjectOU = result
	DbgMsg "strMachineObjectOU=" & result
End Function

Function bEvaluateMDTRecord(strRole1,strRole2,intMachineID,strPlatformCode,gbAutoConfigure)
	DbgMsg "Starting bEvaluateMDTRecord"
	Dim result : result=false
	Dim pstrRole1 : pstrRole1=strRole1
	Dim pstrRole2 : pstrRole2=strRole2
	Dim pstriMDTRole1
	Dim pstriMDTRole2
	Dim pIntRecordID :pIntRecordID = intMachineID
	Dim retRole
	Dim retId
	Dim query
	Dim records
	Dim oconn :Set oconn = CreateObject("ADODB.Connection")
	Dim ocmd :Set ocmd = CreateObject("ADODB.Command")
	Dim xstring :xstring="Provider=sqloledb;Data Source=" & sMDTSQLServer & ";" & "Initial Catalog=" & sMDTDataBase  & ";User Id=" & sMDTUser2 & ";Password=" & sMDTPassword2 & ";"
	oconn.ConnectionString = xstring
	oconn.open
	ocmd.ActiveConnection = oconn
	
	'// query role 1
	query = "SELECT top 1 * FROM Settings_Roles WHERE ID='" & pIntRecordID & "' and Sequence='1' "
	'DbgMsg query
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Set records = oCmd.Execute
	If records.EOF Then 
		DbgMsg "This machine record is not configured with a primary role. Exiting script."
		wscript.quit(999)
	Else '// records exisst therefore lets see how many
		Do while (Not records.EOF)
			pstriMDTRole1 = records("Role")
			retId=records("ID")
			records.movenext
		loop	
	End if
	
	'// query role 2
	query = "SELECT top 1 * FROM Settings_Roles WHERE ID='" & pIntRecordID & "' and Sequence='2' "
	'DbgMsg query
	oCmd.CommandText = query
	oCmd.CommandType = 1
	Set records = oCmd.Execute
	If records.EOF Then 
		DbgMsg "This machine record is not configured with a second role. Exiting script."
		wscript.quit(999)
	Else '// records exisst therefore lets see how many
		Do while (Not records.EOF)
			pstriMDTRole2 = records("Role")
			retId=records("ID")
			records.movenext
		loop	
	End if
	
	dim bRolesAreSame: bRolesAreSame = false
	'// validate mdtrole1=newrole1
	if(pstriMDTRole1=pstrRole1) and (pstriMDTRole2=pstrRole2) then bRolesAreSame=true
	
	
	if(bRolesAreSame) then
		DbgMsg "Roles are the same. Machine is configured with " & pstrRole1 & " and role is configured with " & pstriMDTRole1 
		DbgMsg "Roles are the same. Machine is configured with " & pstrRole2 & " and role is configured with " & pstriMDTRole2 
	else
		DbgMsg "Role 1 will be updated from [" & pstriMDTRole1 & "] to [" & pstrRole1 & "]  for record [" & pIntRecordID & "]"
		DbgMsg "Role 2 will be updated from [" & pstriMDTRole2 & "] to [" & pstrRole2 & "]  for record [" & pIntRecordID & "]"
	end if
	
	'// validate mdtrole2=newrole2
	DbgMsg "Checking for autoconfig flag. gbAutoConfigure="  & gbAutoConfigure
	
	'// update role 1 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	query = "update settings_roles set role='" + strRole1 + "' where sequence='1' and id='" & intMachineID & "'"
	'DbgMsg query
	oCmd.CommandText = query
	oCmd.CommandType = 1
	if(gbAutoConfigure and bRolesAreSame=false) then 
		DbgMsg "[warning] Auto configure is ENABLED. Updating roles..." & query
		Set records = oCmd.Execute
	elseif (gbAutoConfigure=false) then
		DbgMsg "Auto configure is disabled. Will not update roles."
	End if

	'// update role 2 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	query = "update settings_roles set role='" + strRole2 + "' where sequence='2' and id='" & intMachineID & "'"
	'DbgMsg query
	oCmd.CommandText = query
	oCmd.CommandType = 1
	if(gbAutoConfigure and bRolesAreSame=false) then 
		DbgMsg "[warning] Auto configure is ENABLED. Updating roles..." & query
		Set records = oCmd.Execute
	else
		'DbgMsg "[warning] Auto configure is disabled. Will not update roles"
	End if
	
	'// update EUCSnowPlatformCode //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	query  = "update settings set EUCSnowPlatformCode='" + strPlatformCode + "' where id='" & intMachineID & "'  "
	'DbgMsg query
	oCmd.CommandText = query
	oCmd.CommandType = 1
	if(gbAutoConfigure and bRolesAreSame=false) then 
		DbgMsg "[warning] Auto configure is ENABLED. Updating roles..." & query
		Set records = oCmd.Execute
	else
		DbgMsg "Auto configure is disabled. Will not update eucsnowplatformcode."
	End if
	
	'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	If Err Then 
		result = Ucase("ERROR: " & Err.Description)
	else
		result = true
	end if
	DbgMsg "Completing bEvaluateMDTRecord with exit code: " & result
	bEvaluateMDTRecord=result
End Function

Function AddWorkstationToCollection(strHostname,strCollectionName,gbAutoConfigure)
	err.number = 0
	Dim result: result = false
	Dim mp,cmd,file,sUsername2,sKeyword2,nsuri,query,item,presourceid,servername,sitecode,adsitename,gService,clientsitecode,primarysiteserver
	Dim oSMSClient :Set oSMSClient = CreateObject ("Microsoft.SMS.Client") '// Creates SMS object to query the closest DP
	Dim lLocator :Set lLocator = CreateObject("WbemScripting.SWbemLocator")
	Dim ad :Set ad = CreateObject("ADSystemInfo")
	ServerName = smsservername
	SiteCode = sSiteCode

	If Err.Number <>0 Then 
		DbgMsg "Could not create SMS Client Object - quitting"
	End If
	
	clientsitecode = oSMSClient.GetAssignedSite
	adsitename =  ad.SiteName
	
	nsuri = "Root\SMS\Site_" & SiteCode
	DbgMsg "ServerName: " & ServerName
	DbgMsg "SiteCode: " & sitecode
	DbgMsg "nsuri: " & nsuri
	DbgMsg "Current site name: " & adsitename
	DbgMsg "clientsitecode=" & clientsitecode
	DbgMsg "Connecting to Central Site to get local Primary Site Server.."

	sUsername2=smssUsername2
	sKeyword2=smsskeyword2

	'// connect to central site and get primary from AD site ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	DbgMsg "Connecting to provider " & ServerName & "," & nsuri  & "," & sUsername2  
	Set gService = lLocator.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "SELECT ServerName FROM SMS_SystemResourceList WHERE RoleName = 'SMS Site Server' AND SiteCode = '" & clientsitecode & "'"
	DbgMsg query
	for each item in gService.ExecQuery(query)
		DbgMsg item.ServerName
		primarysiteserver = item.ServerName
	next
	
	DbgMsg "Site primary is " & primarysiteserver & "[" & clientsitecode & "] "
	
	'// connect to local site primary //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	servername = primarysiteserver
	sitecode = clientsitecode
	nsuri = "Root\SMS\Site_" & sitecode
	DbgMsg "Connecting to provider " & ServerName & "," & nsuri  & "," & sUsername2 
	Set gService = lLocator.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "Select * From SMS_R_System where Obsolete='0' and NetbiosName='" & ucase(strHostname)  & "' "
	DbgMsg query
	for each item in gService.ExecQuery(query)
		presourceid = item.resourceid
		DbgMsg "Found " & item.Name & " with " & item.SMSUniqueIdentifier & " and resource id " & presourceid
	next
	'wscript.quit
		
	'// connect to sccm and get collection ID from name
	query = "Select * From SMS_Collection Where Name ='" + ucase(strCollectionName) + "' AND OwnedByThisSite=1"
	Dim oCollectionSet :Set oCollectionSet = gService.ExecQuery(query)
	Dim CollListItem,CollectionRule
	DbgMsg query & " : Number of results " & oCollectionSet.count
	if (oCollectionSet.count=0) then
		DbgMsg "Unable to locate collection, closing script with exit code 300"
		wscript.quit(300)
	end if
	DbgMsg "Checking for autoconfig flag. gbAutoConfigure="  & gbAutoConfigure
	For Each CollListItem In oCollectionSet
		DbgMsg "Found collection: " & CollListItem.Name & " with ID=" & CollListItem.CollectionID
		If ucase(CollListItem.Name) = ucase(strCollectionName) Then
			'***************************************************************************
			'**** Create a direct membership rule (spawn a blank instance) to add to an existing collection.
			'**** Then give that instance the values it needs - this is REQUIRED
			'***************************************************************************
			if(gbAutoConfigure) then 
				DbgMsg "[warning] Auto configure is ENABLED. Adding machine to collection."
				DbgMsg "Err Value: " & err.number
				Set CollectionRule = gService.Get("SMS_CollectionRuleDirect").SpawnInstance_()
				CollectionRule.ResourceClassName = "SMS_R_System"
				CollectionRule.RuleName = "ResourceID=" & presourceid
				CollectionRule.ResourceID = presourceid
				CollListItem.AddMembershipRule CollectionRule
				DbgMsg "Added resourceid " & presourceid & " to collection with exitcode: " & err.Number & " - " & err.description
				If Err.Number = 0 Then ' --- If Error = 0, then adding membership was successful.
					result = True ' --- Set flag to "True" to indicate memb. rule added Successfully
					DbgMsg "Addition of membership rule with Resource ID " & presourceid & " to Collection " & CollListItem.Name & " was successful."
					DbgMsg "Perfoming policy discovery ... " & InitiateDiscoveryPolicies
				Else
					DbgMsg "Addition of membership rule with Resource ID " & presourceid & " to Collection " & CollListItem.Name & " was NOT successful!"
					WScript.Quit(1)
				End If
				
			else
				DbgMsg "Auto configure is disabled. Will not update collection " & CollListItem.Name & " with ID=" & CollListItem.Collectionid
			end if
		End If
	Next
	
	'// add resource id to new build collection to run wizard
	DbgMsg "AddWorkstationToCollection=" & result
	AddWorkstationToCollection = result
End Function

Function SMSClientID(strHostname)
	Dim result : result = ""
	Dim smsClient :Set smsClient = GetObject("winmgmts://" & strHostname & "/root/ccm:CCM_Client=@")
	
	If Err.Number <> 0 Then 
		DbgMsg "Could not create CCM Client Object - quitting"
		WScript.Quit(1)
	else
		result = smsClient.ClientID
	End If
	SMSClientID = result
End Function

Function SMSClientSiteCode(strHostname)
	Dim result : result = ""
	Dim smsClient :Set smsClient = GetObject("winmgmts://" & strHostname & "/root/ccm:CCM_Client=@")
	
	If Err.Number <> 0 Then 
		DbgMsg "Could not create CCM Client Object - quitting"
		WScript.Quit(1)
	else
		result = smsClient.GetAssignedSite
	End If
	SMSClientID = result
End Function

Function FixWin32EncryptableVolume
	Dim results: results=false
	Dim sh: set sh = CreateObject("WScript.Shell")
	Dim cmd: cmd = "C:\Windows\System32\wbem\mofcomp.exe C:\Windows\System32\wbem\win32_encryptablevolume.mof"
	sh.run cmd,0,True
	if(err.number = 0) then results = true
	DbgMsg "FixWin32Encryptablevolume: Completed with exit code " & err.Number & " : " & err.Description
	FixWin32EncryptableVolume = results
End Function

'// 2012.10.04 - Added function to perform machine policy update after adding workstation to the collection

Function InitiateDiscoveryPolicies
	Dim results: results= false
	Dim wmi :Set wmi = CreateObject("WbemScripting.SWbemLocator") 
	Dim sccmAction,action,srv,actions,strHardwareInventory,strSoftwareInventory,strMachinePolicyEvaluation,strSoftwareUpdateDeployment,strSoftwareUpdateScan
	Dim strRefreshDefaultMP,strRefreshLocation,strRefreshProxyMP
	'// List all client actions availible
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
			if(action.Name = "Request & Evaluate Machine Policy") then
				DbgMsg "Running ConfigMgr action: " & action.Name
				action.PerformAction
				results = true
				'wscript.sleep 3000 
			end if
	Next
	InitiateDiscoveryPolicies = results
End Function

Function SearchAvailibleSMSPrograms(strPackageID,strProgramName)
	Dim results: results = false
	'DbgMsg "Creating UIResource.UIResourceMgr object."
	Dim uiResource : Set uiResource = CreateObject("UIResource.UIResourceMgr")
	DbgMsg "Searching for package " & strPackageID & " and program name " & strProgramName
	Dim programList : Set programList = uiResource.GetAvailableApplications
	Dim p
	For each p in programList
		DbgMsg "Found program: " & p.Name & " ("  & p.PackageID & ") "
		If ((p.PackageID = strPackageID) and (p.Name = strProgramName)) then
			'on error resume next
			DbgMsg "[warn] Match found! Found program: " & p.Name & " ("  & p.PackageID & ") "
			results = true
			Exit For
		End If	
	Next
	SearchAvailibleSMSPrograms = results
End Function

Function InstallShortCut(strFilename,strDestFolder)
	Dim results: results= false
	DbgMsg "Installing shortcut " & strFilename & " to folder " & strDestFolder
	on error resume next
	fso.copyfile strfilename,strDestFolder,true
	if(err.number = 0) then results = true
	InstallShortCut = results
	on error goto 0
End Function

Function DeleteFolder(pfolderpath,boolOverride)
	Dim results: results = false
	err.clear
	on error resume next
	if(fso.folderexists(pfolderpath)) then
		fso.deletefolder pfolderpath,boolOverride
		if not (fso.folderexists(pfolderpath)) then
			results = true
			DbgMsg "Veriftying " & pfolderpath & " has been deleted..."
			DbgMsg pfolderpath & " folder has been deleted with exit code " & err.number & " - " & err.description
		else
			DbgMsg "Problem deleteing folder, closing task seuqence gracefully with exit code " & err.number & " - " & err.description
			wscript.quit(err.number)
		end if
	else
		DbgMsg "Folder " & pfolderpath & " not present, continuing on..."
		results = true
	end if
	on error goto 0
	DeleteFolder = results
End Function

Function ConfigFile(myKey)
	Dim myFilePath : myFilePath = globalConfigFile
	Dim mySection: mySection = globalDomainName
	'DbgMsg "mkey=" & mykey
    Const ForReading   = 1
    Const ForWriting   = 2
    Const ForAppending = 8
    Dim intEqualPos
    Dim objFSO, objIniFile
    Dim strFilePath, strKey, strLeftString, strLine, strSection
    Set objFSO = CreateObject( "Scripting.FileSystemObject" )
    ConfigFile     = ""
    strFilePath = Trim(myFilePath)
    strSection  = Trim(mySection)
    strKey      = Trim( myKey )
	'DbgMsg "Checking for " & strFilePath
    If objFSO.FileExists( strFilePath ) Then
        Set objIniFile = objFSO.OpenTextFile( strFilePath, ForReading, False )
        Do While objIniFile.AtEndOfStream = False
            strLine = Trim( objIniFile.ReadLine )

            ' Check if section is found in the current line
            If LCase( strLine ) = "[" & LCase( strSection ) & "]" Then
                strLine = Trim( objIniFile.ReadLine )

                ' Parse lines until the next section is reached
                Do While Left( strLine, 1 ) <> "["
                    ' Find position of equal sign in the line
                    intEqualPos = InStr( 1, strLine, "=", 1 )
                    If intEqualPos > 0 Then
                        strLeftString = Trim( Left( strLine, intEqualPos - 1 ) )
                        ' Check if item is found in the current line
                        If LCase( strLeftString ) = LCase( strKey ) Then
                            ConfigFile = Trim( Mid( strLine, intEqualPos + 1 ) )
                            ' In case the item exists but value is blank
                            If ConfigFile = "" Then
                                ConfigFile = " "
                            End If
                            ' Abort loop when item is found
                            Exit Do
                        End If
                    End If

                    ' Abort if the end of the INI file is reached
                    If objIniFile.AtEndOfStream Then Exit Do

                    ' Continue with next line
                    strLine = Trim( objIniFile.ReadLine )
                Loop
            Exit Do
            End If
        Loop
        objIniFile.Close
    Else
        WScript.Echo strFilePath & " doesn't exists. Exiting..."
        Wscript.Quit 1
    End If
End Function

Function ADSiteCode(connection,adsite) 
    'on error resume next
    Dim resources, resource 
    Set resources = connection.ExecQuery("Select SiteCode FROM SMS_Boundary WHERE DisplayName = '" & adsite & "'")
    If Err.Number<>0 Then 
		Wscript.Echo "Couldn't get resources"
	End if
    For Each resource In resources 
		ADSiteCode = resource.SiteCode 
    Next 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
    End If
End Function

Function ADSiteSCCMServer(connection,code) 
    'on error resume next
    Dim resources, resource 
    Set resources = connection.ExecQuery("SELECT ServerName FROM SMS_SystemResourceList WHERE RoleName = 'SMS Site Server' AND SiteCode = '" & code & "' ")
    If Err.Number<>0 Then 
		Wscript.Echo "Couldn't get resources"
	End if
    For Each resource In resources 
		ADSiteSCCMServer = resource.ServerName 
    Next 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
    End If
End Function

Function ADSite
	Dim objADSysInfo :Set objADSysInfo = CreateObject("ADSystemInfo")
	ADSite = objADSysInfo.SiteName
End Function

Function IsRunning(processname)
	Dim service :set service = GetObject ("winmgmts:")
	Dim Process
	IsRunning = false
	for each Process in Service.InstancesOf ("Win32_Process")
		If Process.Name = processname then
			IsRunning = true
		End If
	Next
End Function

Function CopySource
	dim src,dst
	src = "\\usglnw0012.domain.com\localinstall\Tools\SCCMClient\package"
	dst = "c:\progra~1\Nestle\Support\SCCMRefresh"
	
	if not fso.folderexists(programs) then
		fso.createfolder programs
	end if
	if not fso.folderexists(programs & "\Nestle") then
		fso.createfolder programs & "\Nestle"
	end if
	if not fso.folderexists(programs & "\Nestle\Support") then
		fso.createfolder programs & "\Nestle\Support"
	end if
	if not fso.folderexists(programs & "\Nestle\Support\SCCMRefresh") then
		fso.createfolder programs & "\Nestle\Support\SCCMRefresh"
	end if	
	if not fso.folderexists(dst) then
		fso.createfolder dst
	end if
	sh.run "robocopy.exe /v /e /z " & src & " " & dst,0
	DbgMsg("robocopy.exe /v /e /z " & src & " " & dst)
	WaitWhileThisIsFinishesRunning("robocopy.exe")
End Function

Function RemoveClient
	Dim pwd,cmd
	cmd = programs & globalccmsetuppath
	DbgMsg("Removing client with command line: " & cmd)
	sh.run cmd,0
	WaitWhileThisIsFinishesRunning("ccmsetup.exe")
	DbgMsg("Removal complete")
End function

Function WaitWhileThisIsFinishesRunning(psCmd)
	DbgMsg("Waiting for " & psCmd & " to finish running...")
	Do While IsRunning(psCmd)
		'DbgMsg "*"
		Wscript.sleep 3000
	Loop
End Function

Function BlowOutWMI
	Dim pwd,cmd
	DbgMsg("Blowing out wmi...")
	pwd = "C:\Windows\System32\wbem\"
	cmd = "WinMgmt.exe /resetrepository"
	DbgMsg("Current diectory: " & pwd)
	DbgMsg("Commandline: " & cmd)
	sh.CurrentDirectory = pwd
	sh.run cmd,0
	pwd = "C:\windows\system32\"
	cmd = "reg delete " & chr(34) & "HKLM\SOFTWARE\Microsoft\SMS\Mobile Client\Software Distribution\State" & chr(34) & " /v Paused /f"
	DbgMsg("Current diectory: " & pwd)
	DbgMsg("Commandline: " & cmd)
	sh.CurrentDirectory = pwd
	sh.run cmd,0	
	DbgMsg("Finished rebuiling wmi...")
End Function

Function RecompileMOFS
	Dim pwd,cmd,Folder,Files,file,ext
	DbgMsg("Recompiling mof files...")
	pwd = "C:\Windows\System32\wbem\"
	ext = ".dll"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "c:\Windows\System32\regsvr32.exe /s " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
		end if
	next
	pwd = "C:\Windows\System32\wbem\"
	ext = ".mof"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "mofcomp.exe " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
			WaitWhileThisIsFinishesRunning("mofcomp.exe")
		end if
	next
	pwd = "C:\Windows\System32\wbem\"
	ext = ".mfl"
	for each file in fso.getfolder(pwd).files
		if instr(1,file.Name,ext,1) then
			pwd = "C:\windows\system32\wbem\"
			cmd = "mofcomp.exe " & file
			DbgMsg("Commandline: " & cmd)
			sh.CurrentDirectory = pwd
			sh.run cmd,0
			WaitWhileThisIsFinishesRunning("mofcomp.exe")
		end if
	next
	DbgMsg("Finished recompiling mof files")
End Function


Sub DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End sub

Function DeleteSCCMRecord
	Dim pwd,cmd
	DbgMsg("Deleting the sccm records.")
	DeleteMe("usphxm0110")
	DeleteMe("usphxm0100")
	DeleteMe("usglnm0000")
	DeleteMe("ussolm0000")
	DbgMsg("Deletion complete.")
End function

Function DeleteMe(psServer)
	Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
	Dim serverattrb,resources,resource,test,mymac,mac,query
	serverattrb = Chr(105) & Chr(120) & Chr(51) & Chr(48) & Chr(85) & Chr(85) & Chr(51) & Chr(120) & Chr(57) & Chr(56)
	DbgMsg("Deleting from " & psServer)
	Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(psServer, "root\sms", ,,128) 
	Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
	Dim filename, Location,i,hostname
	hostname = sh.ExpandEnvironmentStrings("%computername%")
	For Each Location In providerLoc 
		If location.ProviderForLocalSite = True Then 
			Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode,, ,,128)
			Exit For 
		End If 
	Next
	mac = ""
	query = "Select * From SMS_R_System where NetbiosName like '" & ucase(hostname)  & "' or MacAddresses like '" & ucase(hostname) & "' or SMSUniqueIdentifier like '" & ucase(hostname) & "' or ResourceID like '" & ucase(hostname) & "'"
	DbgMsg("WMI Query: " & query)
	err.clear
	Set resources = swbemServices.ExecQuery(query)
	If Err.Number<>0 Then 
		DbgMsg "Couldn't get resources" 
	End If 
	If resources.Count=0 Then 
		DbgMsg "No resources found" 
	End If
	For Each resource In resources 
		for each mac in resource.MacAddresses
			mymac = mac 
		next
		DbgMsg "Found [" & resource.NetbiosName & "] :: [" & resource.ResourceID & "] with ID [" & resource.SMSUniqueIdentifier & "] and mac [" & mymac & "] at site [" & Location.SiteCode & "] [" & Location.Machine & "] :: " & DeleteResource(swbemServices,resource.ResourceID) 
	Next 
End function

Function DeleteResource (connection, resourceID) 
		Dim res :Set res = connection.Get("SMS_R_System.ResourceID='" & resourceID & "'") 
		err.clear
		'on error resume next
		res.Delete_
		DeleteResource = "Completed with exitcode: " & Err.Number & " #notarealwarning"
End Function
	
Function InitiateDiscoveryPolicies
	Dim sccmAction,action,srv,actions,strHardwareInventory,strSoftwareInventory,strMachinePolicyEvaluation,strSoftwareUpdateDeployment,strSoftwareUpdateScan
	Dim strRefreshDefaultMP,strRefreshLocation,strRefreshProxyMP
	Dim wmi :Set wmi = CreateObject("WbemScripting.SWbemLocator") 
	'// *********** SCCM Actions ******************
	strHardwareInventory = "{00000000-0000-0000-0000-000000000001}"
	'//	   strSoftwareInventory = "{00000000-0000-0000-0000-000000000002}"
	'//    strDataDiscovery = "{00000000-0000-0000-0000-000000000003}"
	'//    strMachinePolicyAssignmentRequest = "{00000000-0000-0000-0000-000000000021}"
	strMachinePolicyEvaluation = "{00000000-0000-0000-0000-000000000022}"
	strRefreshDefaultMP = "{00000000-0000-0000-0000-000000000023}"
	strRefreshLocation = "{00000000-0000-0000-0000-000000000024}"
	'//    strSoftwareMeteringReporting = "{00000000-0000-0000-0000-000000000031}"
	'//    strSourcelistUpdateCycle = "{00000000-0000-0000-0000-000000000032}"
	strRefreshProxyMP = "{00000000-0000-0000-0000-000000000037}"
	'//    strCleanuppolicy = "{00000000-0000-0000-0000-000000000040}"
	'//    strValidateAssignments = "{00000000-0000-0000-0000-000000000042}"
	'//    strCertificateMaintenance = "{00000000-0000-0000-0000-000000000051}"
	'//    strBranchDPScheduledMaintenance = "{00000000-0000-0000-0000-000000000061}"
	'//    strBranchDPProvisioningStatu Reporting = "{00000000-0000-0000-0000-000000000062}"
	'//	   strSoftwareUpdateDeployment = "{00000000-0000-0000-0000-000000000108}"
	'//    strStateMessageUpload = "{00000000-0000-0000-0000-000000000111}"
	'//    strStateMessageCacheCleanup = "{00000000-0000-0000-0000-000000000112}"
	strSoftwareUpdateScan = "{00000000-0000-0000-0000-000000000113}"
	'//    strSoftwareUpdateDeploymentReEval = "{00000000-0000-0000-0000-000000000114}"
	'//    strOOBSDiscovery = "{00000000-0000-0000-0000-000000000120}"

	'// List all client actions availible
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	
	if err.number<>0 then wscript.sleep(inventorywaittime)
	For Each action In actions
			DbgMsg "-> Availible actions: " & action.Name
			action.PerformAction
	Next
	
	'// Request & Evaluate Machine Policy
	sccmAction = strMachinePolicyEvaluation
	Set srv = wmi.ConnectServer(".","root\ccm\invagt") 
	on error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Request & Evaluate Machine Policy") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next

	'// Hardware Inventory Collection Cycle
	sccmAction = strHardwareInventory
	Set srv = wmi.ConnectServer( , "root\ccm\invagt") 
	on error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Hardware Inventory Collection Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
	
	'// Software Updates Assignments Evaluation Cycle
	sccmAction = strSoftwareUpdateScan
	Set srv = wmi.ConnectServer( , "root\ccm\invagt") 
	on error resume next
	srv.Delete "InventoryActionStatus.InventoryActionID=""" & sccmAction & """" 
	wscript.sleep 3000 
	Set actions = CreateObject("CPAPPLET.CPAppletMgr").GetClientActions 
	For Each action In actions 
		If Instr(action.Name,"Software Updates Assignments Evaluation Cycle") > 0 Then 
			'DbgMsg "Performing action => " & action.Name & " #notarealwarning"
			'action.PerformAction
		End If 
	Next
End Function 

Function GetLocalMP
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	mp = "fspserv.ams.domain.com"
	sitecode = "U01"
	if err.number = 0 then
		mp = sms.GetCurrentManagementPoint
		sitecode = sms.GetAssignedSite	
	end if
	GetLocalMP = mp
    Set sms=Nothing
End Function

Function GetLocalSiteCode
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	mp = "fspserv.ams.domain.com"
	sitecode = "U01"
	if err.number = 0 then
		mp = sms.GetCurrentManagementPoint
		sitecode = sms.GetAssignedSite	
	end if
	GetLocalSiteCode = sitecode
    Set sms=Nothing
End Function


Function GetCacheLocation
	Dim mp,results
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		results = ocache.Location
	else
		results = "** unknown **"
	end if
	GetCacheLocation = results
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function GetCacheSize
	Dim mp
	Dim results: results = 0
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		results = ocache.TotalSize
	else
		results = "** unknown **"
	end if
	GetCacheSize = cint(results)
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function SetCacheLocation(cachelocation)
	Dim mp,results
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	if err.number = 0 then
		ocache.Location = cachelocation
		results = cachelocation
	else
		results = "** error **"
	end if
	SetCacheLocation = results
    set sms=nothing
    set orm=nothing
    set sms=nothing
	set ocache = nothing
End Function

Function ForceHardwareInv
	dim cmd
	cmd = "WMIC.exe /node:localhost /namespace:\\root\ccm\invagt path inventoryActionStatus where InventoryActionID=" & chr(34) & "{00000000-0000-0000-0000-000000000001}" & chr(34) & " DELETE /NOINTERACTIVE"
	sh.run cmd 
	DbgMsg "Executed: " & cmd & " with exit code " & err.description & " (" & err.number & ") "
	cmd = "WMIC.exe /node:localhost /namespace:\\root\ccm\invagt path inventoryActionStatus where InventoryActionID=" & chr(34) & "{00000000-0000-0000-0000-000000000002}" & chr(34) & " DELETE /NOINTERACTIVE"
	sh.run cmd 
	DbgMsg "Executed: " & cmd & " with exit code " & err.description & " (" & err.number & ") "
End Function

Function ValidateBoundary(pstrADSite,pstrSiteCode)
	Dim results : results = false
	Dim oSMSClient :Set oSMSClient = CreateObject ("Microsoft.SMS.Client") '// Creates SMS object to query the closest DP
	Dim lLocator :Set lLocator = CreateObject("WbemScripting.SWbemLocator")
	Dim ad :Set ad = CreateObject("ADSystemInfo")
	Dim ServerName :ServerName = smsservername
	Dim SiteCode :SiteCode = sSiteCode
	Dim sUsername2 :sUsername2=smssUsername2
	Dim sKeyword2 :sKeyword2=smsskeyword2
	Dim clientsitecode :clientsitecode = oSMSClient.GetAssignedSite
	Dim desiredsite: desiredsite = pstrSiteCode
	Dim adsitename :adsitename =  pstrADSite
	Dim nsuri :nsuri = "Root\SMS\Site_" & SiteCode
	Dim item,sc,sHostSitecode,oPSC,ReportingSiteCode
	Dim icount: icount = 0
	
	If Err.Number <>0 Then 
		DbgMsg "Could not create SMS Client Object - quitting"
	End If
	
	Dim gservices: Set gservices = lLocator.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	 '// query central server for sitecode of boundary
	Dim query: query = "SELECT SiteCode FROM SMS_Boundary WHERE DisplayName = '"& adsitename &"' "
	DbgMsg query & " : Count " & gservices.ExecQuery(query).count

	
	For each item in gservices.ExecQuery(query)
		icount = iCount + 1
		sHostSitecode = item.SiteCode
		DbgMsg "Found " & adsitename & " as belonging to " & sHostSitecode
	Next

	'// 2012-12-19: Checking if site is a secondary site ////////////////////////////////////////////////////////////////////////
	Set gservices = lLocator.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	 '// query central server for sitecode of boundary
	query = "SELECT ReportingSiteCode FROM SMS_Site WHERE SiteCode = '"& sHostSitecode &"' AND Type = 1"
	DbgMsg query & " : Count " & gservices.ExecQuery(query).count
	For each item in gservices.ExecQuery(query)
		icount = iCount + 1
		ReportingSiteCode = item.ReportingSiteCode
		DbgMsg "Found reporting site code as " & ReportingSiteCode
		sHostSitecode=ReportingSiteCode
	Next
	'// if reporting site code is defined then set it to reporting site else lease site code as is 
	'// 2012-12-19: //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	if icount = 0 Then
		DbgMsg "Error No ConfigMgr site exists for ADSite: " & adsitename
		Exit Function
	else
		If sHostSitecode = "" Then
			DbgMsg "Error SiteCode is blank"
			exit function
		Else
			DbgMsg "Sitecode for ADSite: " & adsitename & " is " & sHostSiteCode
		End If
	end if
	
	DbgMsg "Check if client site matches boundary site"
	if(clientsitecode <> sHostSitecode) then
		DbgMsg "Error, client site does not match actual site. Moving to " & sHostSiteCode
		if(SetSiteCode(sHostSiteCode)) then 
			DbgMsg "Site reassignment successful. Running inventory trigger schedules"
			DbgMsg "Inventory sucessful: " & InitiateDiscoveryPolicies
			results = true
		else
			DbgMsg "Site reassignment failed. "
			wscript.quit(999)
		end if
	else
		DbgMsg "Sitecode is ok"
		results = true
	end if
	results = QueryResource(computername,sHostSiteCode)
	DbgMsg "Resource appears in site database: " & results
	ValidateBoundary = results
End Function

Function SCCMConfigurationValidated
	Dim results: results = true
'	
'	DbgMsg "Starting SCCMConfigurationValidated"
'	if(GetCacheLocation <> defaultcachepath) then 
'		DbgMsg "Changing cache location"
'		if(SetCacheLocation(defaultcachepath)) then InitiateDiscoveryPolicies
'	end if
'	if (GetCacheSize<>defaultcachesize) then 
'		DbgMsg "Changing cache size: " & SetCacheSize(defaultcachesize)
'	end if
'
'	if(ValidateBoundary(ad.SiteName,GetLocalSiteCode)) then
'		DbgMsg "SCCM Configuration has been validated: " & ad.SiteName & " " & GetLocalSiteCode
'		results = true
'	end if
	SCCMConfigurationValidated = results
End Function


Function QueryResource(pHostname,pSitecode)
	Dim results : results = false
	Dim oSMSClient :Set oSMSClient = CreateObject ("Microsoft.SMS.Client") '// Creates SMS object to query the closest DP
	Dim wbem :Set wbem = CreateObject("WbemScripting.SWbemLocator") ' -- replace by wbem global object
	Dim ServerName :ServerName = smsservername
	Dim SiteCode :SiteCode = sSiteCode
	Dim sUsername2 :sUsername2=smssUsername2
	Dim sKeyword2 :sKeyword2=smsskeyword2
	Dim desiredsite: desiredsite = pSitecode
	Dim nsuri :nsuri = "Root\SMS\Site_" & SiteCode
	Dim item,sc,sHostSitecode,oPSC,gservices,query,icount,primarysiteserver
	DbgMsg "Connecting to " & ServerName & " " & nsuri & " " & sUsername2 	'// query site server using sitecode
	Set gservices = wbem.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "SELECT ServerName FROM SMS_SystemResourceList WHERE RoleName = 'SMS Site Server' AND SiteCode = '" & desiredsite & "'"
	DbgMsg query & " : Count " & gservices.ExecQuery(query).count
	icount = 0
	For each item in gservices.ExecQuery(query)
		icount = iCount + 1
		primarysiteserver = item.ServerName
		DbgMsg "Found " & primarysiteserver & " as belonging to " & desiredsite
	Next
	
	ServerName = primarysiteserver
	nsuri = "Root\SMS\Site_" & desiredsite
	
	'// query clients in site database
	DbgMsg "Connecting to " & ServerName & "\" & nsuri & " as " & sUsername2
	Dim cmprov: Set cmprov = wbem.ConnectServer(ServerName, nsuri, sUsername2, sKeyword2,,,128)	
	query = "SELECT * FROM SMS_R_System WHERE NetbiosName like '%"& pHostname &"%' "
	DbgMsg query & " : Count " & cmprov.ExecQuery(query).count
	
	Do While (cmprov.ExecQuery(query).count <> 1)
		n = n + waitinterval
		InitiateDiscoveryPolicies
		DbgMsg "Sleeping " & waitinterval & "/" & n
		wscript.sleep waitinterval
		if (n = intTimeoutValue) then exit do
	Loop

	if(cmprov.ExecQuery(query).count < 1)  then
		DbgMsg "Unable to locate hostname in database"
	else
		DbgMsg "Found hostname "& pHostname &" in database"
		results = true
	end if
	QueryResource = results
End Function

sub break
	wscript.quit
end sub

'Function SetCacheSize(cachesize)
'	Dim mp,results	
'	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
'	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
'	Dim ocache :Set ocache=orm.GetCacheInfo()
'	Dim oldsize :oldsize=cint(ocache.TotalSize)
'	Dim oldpath :oldpath = ocache.Location
'	Dim newsize :newsize = cint(cachesize)
'	'Dim newpath :newpath = cachepath
'	if err.number <> 0 then 
'		DbgMsg "Unable to connect to local SMS client object. Quiting"
'		results = false
'		wscript.quit
'	end if
'	if (newsize=oldsize) then
'		DbgMsg "Cache is correctly configured to " & oldpath & " with size " & oldsize 
'		results = true
'	else
'		DbgMsg "Configuring cache size to size " & newsize 
'		ocache.TotalSize = newsize
'		ocache.Location = 
'		results = true
'	end if
'	SetCacheSize = results
'    set sms=nothing
'    set orm=nothing
'	set ocache = nothing
'End Function

Function SetSiteCode(argSiteCode)
	Dim oldsite, newsite
	Dim results : results = false
	Dim mp,sitecode
	Dim sms :Set sms = CreateObject ("Microsoft.SMS.Client")
	on error resume next
	newsite = argSiteCode
	oldsite = sms.GetAssignedSite
	DbgMsg "Oldsite=" & oldsite & " and newsite=" & newsite
	'// compare
	if(oldsite = newsite) then
		'// do nothing
		DbgMsg "Site are the same, doing nothing."
		'wscript.quit
	else
		'// reassign to new site
		DbgMsg "Reassigning..."
		sms.SetAssignedSite(argSiteCode)
		wscript.sleep(inventorywaittime)
		if(lcase(sms.GetAssignedSite)=lcase(argSiteCode)) then
			DbgMsg "Previous site was " & sms.GetAssignedSite & ". New site is " & argSiteCode & " complete with exit code " & err.number & " " & err.description
		end if
		
		if err.number = 0 then
			oldsite = sms.GetAssignedSite
			DbgMsg "Reassignment successful. Initiating inventory after 30 seconds."
			wscript.sleep(inventorywaittime)
			ForceHardwareInv
		else
			DbgMsg "Reassignment failed"
			wscript.quit
		end if
		results = true
	end if
	SetSiteCode = results
    Set sms=Nothing
End Function

Function SetCache(cachepath,cachesize)
	Dim mp,results	
	Dim sms :set sms = CreateObject ("Microsoft.SMS.Client")
	Dim orm :set orm = CreateObject("UIResource.UIResourceMgr")
	Dim ocache :Set ocache=orm.GetCacheInfo()
	Dim oldsize :oldsize=cint(ocache.TotalSize)
	Dim oldpath :oldpath = Left(ocache.Location,15)
	Dim newsize :newsize = cint(cachesize)
	Dim newpath :newpath = cachepath
	Dim cmd
	
	if err.number <> 0 then 
		DbgMsg "Unable to connect to local SMS client object. Quiting"
		results = false
		wscript.quit
	end if
	DbgMsg "oldpath " & oldpath
	DbgMsg "newpath " & newpath
	
	if ((oldpath=newpath) and (newsize=oldsize))then '// 2013-01-09: Check cache settings -BL
		DbgMsg "Cache is correctly configured to " & oldpath & " with size " & oldsize
		results = true
	else
		DbgMsg "[warning] ConfigMgr cache settings are incorrect. Continuing on..."
		'ocache.Location = newpath
		'ocache.TotalSize = newsize
		'cmd = "net stop ccmexec" : DbgMsg(cmd) : sh.run(cmd) 
		'wscript.sleep 10000
		'cmd = "net start ccmexec" : DbgMsg(cmd) : sh.run(cmd) 
		'wscript.sleep 10000
		results = true
	end if
	
	SetCache = results
    set sms=nothing
    set orm=nothing
	set ocache = nothing
End Function

Function HexToText(strHex)
    ' Function to convert a string of hexadecimal bytes into a text string.
    Dim strChar, k

    HexToText = ""
    For k = 1 To Len(strHex) Step 2
        strChar = Mid(strHex, k, 2)
        HexToText = HexToText & Chr("&H" & strChar)
    Next
End Function

Function Base64ToHex(strValue)
	
    ' Function to convert a base64 encoded string into a hex string.
    Dim lngValue, lngTemp, lngChar, intLen, k, j, intTerm, strHex,objChars
	Set objChars = CreateObject("Scripting.Dictionary")
	objChars.CompareMode = vbBinaryCompare
	
    intLen = Len(strValue)

    ' Check padding.
    intTerm = 0
    If (Right(strValue, 1) = "=") Then
        intTerm = 1
    End If
    If (Right(strValue, 2) = "==") Then
        intTerm = 2
    End If

    ' Parse into groups of 4 6-bit characters.
    j = 0
    lngValue = 0
    Base64ToHex = ""
    For k = 1 To intLen
        j = j + 1
        ' Calculate 24-bit integer.
        lngValue = (lngValue * 64) + objChars(Mid(strValue, k, 1))
        If (j = 4) Then
            ' Convert 24-bit integer into 3 8-bit bytes.
            lngTemp = Fix(lngValue / 256)
            lngChar = lngValue - (256 * lngTemp)
            strHex = Right("00" & Hex(lngChar), 2)
            lngValue = lngTemp

            lngTemp = Fix(lngValue / 256)
            lngChar = lngValue - (256 * lngTemp)
            strHex = Right("00" & Hex(lngChar), 2) & strHex
            lngValue = lngTemp

            lngTemp = Fix(lngValue / 256)
            lngChar = lngValue - (256 * lngTemp)
            strHex = Right("00" & Hex(lngChar), 2) & strHex

            Base64ToHex = Base64ToHex & strHex
            j = 0
            lngValue = 0
        End If
    Next
    ' Account for padding.
    Base64ToHex = Left(Base64ToHex, Len(Base64ToHex) - (intTerm * 2))

End Function

Sub LoadChars()
    ' Subroutine to load dictionary object with information to convert
    ' Base64 characters into base 64 index integers.
    ' Object reference objChars has global scope.

    objChars.Add "A", 0
    objChars.Add "B", 1
    objChars.Add "C", 2
    objChars.Add "D", 3
    objChars.Add "E", 4
    objChars.Add "F", 5
    objChars.Add "G", 6
    objChars.Add "H", 7
    objChars.Add "I", 8
    objChars.Add "J", 9
    objChars.Add "K", 10
    objChars.Add "L", 11
    objChars.Add "M", 12
    objChars.Add "N", 13
    objChars.Add "O", 14
    objChars.Add "P", 15
    objChars.Add "Q", 16
    objChars.Add "R", 17
    objChars.Add "S", 18
    objChars.Add "T", 19
    objChars.Add "U", 20
    objChars.Add "V", 21
    objChars.Add "W", 22
    objChars.Add "X", 23
    objChars.Add "Y", 24
    objChars.Add "Z", 25
    objChars.Add "a", 26
    objChars.Add "b", 27
    objChars.Add "c", 28
    objChars.Add "d", 29
    objChars.Add "e", 30
    objChars.Add "f", 31
    objChars.Add "g", 32
    objChars.Add "h", 33
    objChars.Add "i", 34
    objChars.Add "j", 35
    objChars.Add "k", 36
    objChars.Add "l", 37
    objChars.Add "m", 38
    objChars.Add "n", 39
    objChars.Add "o", 40
    objChars.Add "p", 41
    objChars.Add "q", 42
    objChars.Add "r", 43
    objChars.Add "s", 44
    objChars.Add "t", 45
    objChars.Add "u", 46
    objChars.Add "v", 47
    objChars.Add "w", 48
    objChars.Add "x", 49
    objChars.Add "y", 50
    objChars.Add "z", 51
    objChars.Add "0", 52
    objChars.Add "1", 53
    objChars.Add "2", 54
    objChars.Add "3", 55
    objChars.Add "4", 56
    objChars.Add "5", 57
    objChars.Add "6", 58
    objChars.Add "7", 59
    objChars.Add "8", 60
    objChars.Add "9", 61
    objChars.Add "+", 62
    objChars.Add "/", 63

End Sub

Sub Syntax()
    ' Subroutine to display syntax message.
    Wscript.Echo "Syntax:"
    Wscript.Echo "  cscript //nologo Base64ToText.vbs <Base64 string>"
    Wscript.Echo "where:"
    Wscript.Echo "  <Base64 string> is a Base64 encoded string."
    Wscript.Echo "For example:"
    Wscript.Echo "  cscript //nologo Base64ToText.vbs VGhpcyBpcyBhIHRlc3Q="
End Sub

Function Base64Encode(inData)
  'rfc1521
  '2001 Antonin Foller, Motobit Software, http://Motobit.cz
  Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
  Dim cOut, sOut, I
  'DbgMsg "Encodinig " & inData
  'For each group of 3 bytes
  For I = 1 To Len(inData) Step 3
    Dim nGroup, pOut, sGroup
    
    'Create one long from this 3 bytes.
    nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
      &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))
    
    'Oct splits the long To 8 groups with 3 bits
    nGroup = Oct(nGroup)
    
    'Add leading zeros
    nGroup = String(8 - Len(nGroup), "0") & nGroup
    
    'Convert To base64
    pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
      Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)
    
    'Add the part To OutPut string
    sOut = sOut + pOut
    
    'Add a new line For Each 76 chars In dest (76*3/4 = 57)
    'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf
  Next
  Select Case Len(inData) Mod 3
    Case 1: '8 bit final
      sOut = Left(sOut, Len(sOut) - 2) + "=="
    Case 2: '16 bit final
      sOut = Left(sOut, Len(sOut) - 1) + "="
  End Select
  Base64Encode = sOut
End Function

Function MyASC(OneChar)
  If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
End Function

Function Base64Decode(ByVal base64String)
	'DbgMsg "DEcoding: " & base64String
	'rfc1521
	'1999 Antonin Foller, Motobit Software, http://Motobit.cz
	Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	Dim dataLength, sOut, groupBegin
  
	'remove white spaces, If any
	base64String = Replace(base64String, vbCrLf, "")
	base64String = Replace(base64String, vbTab, "")
	base64String = Replace(base64String, " ", "")
  
  'The source must consists from groups with Len of 4 chars
  dataLength = Len(base64String)
  If dataLength Mod 4 <> 0 Then
    Err.Raise 1, "Base64Decode", "Bad Base64 string."
    Exit Function
  End If

  
  ' Now decode each group:
  For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, nGroup, pOut
    ' Each data group encodes up To 3 actual bytes.
    numDataBytes = 3
    nGroup = 0

    For CharCounter = 0 To 3
      ' Convert each character into 6 bits of data, And add it To
      ' an integer For temporary storage.  If a character is a '=', there
      ' is one fewer data byte.  (There can only be a maximum of 2 '=' In
      ' the whole string.)

      thisChar = Mid(base64String, groupBegin + CharCounter, 1)

      If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
      Else
        thisData = InStr(1, Base64, thisChar, vbBinaryCompare) - 1
      End If
      If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
      End If

      nGroup = 64 * nGroup + thisData
    Next
    
    'Hex splits the long To 6 groups with 4 bits
    nGroup = Hex(nGroup)
    
    'Add leading zeros
    nGroup = String(6 - Len(nGroup), "0") & nGroup
    
    'Convert the 3 byte hex integer (6 chars) To 3 characters
    pOut = Chr(CByte("&H" & Mid(nGroup, 1, 2))) + _
      Chr(CByte("&H" & Mid(nGroup, 3, 2))) + _
      Chr(CByte("&H" & Mid(nGroup, 5, 2)))
    
    'add numDataBytes characters To out string
    sOut = sOut & Left(pOut, numDataBytes)
  Next

  Base64Decode = sOut
  'DbgMsg sOut
End Function

Function DumpVariables
Dim item
item="sProviderServer"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sSiteCode"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTSQLServer"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTDataBase"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTUser"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTPassword"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTUser2"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="sMDTPassword2"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="adUsername"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="adKeyword"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="smsServerName"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="smssUsername2"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

item="smssKeyword2"
DbgMsg item & "=" & Base64Encode(ConfigFile(item))

End Function


Function IsConnected(strhost)
	IsConnected = true
End Function

Function GetDomainNames()
	  Dim objSysinfo
	  Dim strDNSDomainName
	  Dim strNetBIOSDomainName
	  GetDomainNames=False
	   'Create the ADSystemInfo object
	  'On Error Resume Next
	  Err.Clear
	  Set objSysinfo = CreateObject("ADSystemInfo")
	  If Err.Number = 0 Then
		if(debugenabled) then 
			strDNSDomainName=sdomain2
			strNetBIOSDomainName=ssdomain2
		else
			strDNSDomainName=objSysinfo.DomainDNSName
			strNetBIOSDomainName=objSysinfo.DomainShortName
		end if
	    GetDomainNames=True
	  Else
	    strDNSDomainName=""
	    strNetBIOSDomainName=""
	  End If
	  On Error Goto 0
	  Set objSysinfo=Nothing
	  
	  GetDomainNames=strDNSDomainName
End Function

Function RemoveWorkstationFromCollection(connection,presourceid,pcollectionname,booldelete)
	dim ind,collection,sublist,item,rume,ruleset,results
	Dim query :query = "SELECT * FROM SMS_Collection WHERE OwnedByThisSite='1' and Name like '%" & pcollectionname & "%' OR CollectionID Like '" & pcollectionname & "' "
	'DbgMsg query
	Dim listOfCollections :set listOfCollections = connection.ExecQuery(query)
	for each collection In listOfCollections
			'DbgMsg "" & collection.Name & " ("& collection.CollectionID & ")"
			set sublist = connection.Get("SMS_Collection='" & collection.CollectionID & "'")
			ruleset = sublist.CollectionRules
			if(TypeName(sublist.CollectionRules) = "Variant()") then
				for each item in ruleset
					if (item.Path_.Class = "SMS_CollectionRuleDirect" and item.ResourceID=presourceid) then
						DbgMsg "Found " & MachineName(item.ResourceID,connection) &" in [" & collection.Name & " ("& collection.CollectionID & ")] Delete: " & booldelete
						if(booldelete) then
							sublist.DeleteMembershipRule item
							results= true
						end if
					end if
				next
			end if
    next
	RemoveWorkstationFromCollection = results
End function

Function MachineName(strResourceID,xcon)
	Dim query,count,resource,results,smshostname,SMSUniqueIdentifier,installedos
	query = "Select * From SMS_R_System where ResourceID='" & ucase(strResourceID) & "'"
	count = 1
	Dim resources: Set resources = xcon.ExecQuery(query)
    If (Err.Number<>0 or resources.Count=0)  Then 
        'DbgMsg "Couldn't get resources" 
    else
		'DbgMsg "Found: " & resources.count
		For Each resource In resources
			smshostname = resource.Netbiosname
			SMSUniqueIdentifier = resource.SMSUniqueIdentifier
			installedos = resource.OperatingSystemNameandVersion
			if(instr(installedos,"Microsoft Windows NT Workstation 6.0")) then
				installedos = "SNOW4"
			else
				installedos = "SNOW5"
			end if
			results = smshostname & "("& installedos &") [" & SMSUniqueIdentifier & "," & resource.resourceid & "]"
			count = count + 1
		Next 
	end if
	MachineName = results
End Function

Sub DbgMsg(message)
	logfile.writeline Now() & ": " & message
	Wscript.echo Now() & ": " & message
End sub

Function GetResourceID(pconnection,strSearchCriteria)
	dim item
	GetResourceID = "0"
	Dim query: query = "Select * From SMS_R_System where MacAddresses like '%"& strSearchCriteria &"%' or NetbiosName like '%" & strSearchCriteria & "%' or SMSUniqueIdentifier like '%" & strSearchCriteria & "%' or SMBIOSGUID like '%" & strSearchCriteria & "%' or LastLogonUserName like '%" & strSearchCriteria & "%' or ADSiteName='" & strSearchCriteria & "' "
	'DbgMsg query
	Dim results :set results = pconnection.ExecQuery(query)
	'DbgMsg "Records returned: " & results.count
	For each item In results
		GetResourceID = item.ResourceID
		DbgMsg "GetResourceID: found " & item.Name & " with resource id = " & item.ResourceID 
	Next
End Function

Function GetCollectionMembers(connection,pcollectionname,booldelete)
	Dim collection,sublist,item,ruleset,hostname
	Dim results : results = false
	dim ind : ind = 1
	Dim query :query = "SELECT * FROM SMS_Collection WHERE OwnedByThisSite='1' and Name like '%" & pcollectionname & "%' OR CollectionID Like '" & pcollectionname & "' "
	DbgMsg query
	Dim listOfCollections :set listOfCollections = connection.ExecQuery(query)
	
	for each collection In listOfCollections
			DbgMsg "" & collection.Name & " ("& collection.CollectionID & ")"
			set sublist = connection.Get("SMS_Collection='" & collection.CollectionID & "'")
			ruleset = sublist.CollectionRules
			if(TypeName(sublist.CollectionRules) = "Variant()") then
				for each item in ruleset
					if (item.Path_.Class = "SMS_CollectionRuleDirect") then
						hostname = MachineName(item.ResourceID,connection)
						if(booldelete = "true" or booldelete=true) then
							DbgMsg "(" & ind & ") Deleteing " &  hostname &" from [" & collection.Name & " ("& collection.CollectionID & ")] " & item.ResourceID
							sublist.DeleteMembershipRule item
						else
							DbgMsg "(" & ind & ") Found " &  hostname &" in [" & collection.Name & " ("& collection.CollectionID & ")] " & item.ResourceID
							results = true
						end if
						ind = ind + 1
					end if
				next
			end if
    next
	GetCollectionMembers = results
End function


Function Defined(item)
	Defined=true
	if(IsEmpty(item) or len(item)=0) then Defined=false
	'DbgMsg "Checking if string is defined: '" & item & "' " & Defined
End Function
