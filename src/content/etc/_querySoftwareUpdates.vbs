'// 2012-05-01
'// Description: Create 24 SCCM Advertisements schedued to run daily then dump collections over night
'//  12 Collections for every hour (e.g., 7am-7pm)
'//  12 Collecitons for every half hour (e.g., 6:30,7:30,8:30)
'//  or create 1 advertiements that runs every 15 minutes mandatory 
'//   2 scripts --> 1 to add the machine to its primary shopping collection  2nd to remove workstations from collection when build process completes successfully and machine images correctly.
'// 
'// 
'// http://msdn.microsoft.com/en-us/library/cc146462.aspx - SMS_UpdateDeploymentSummary 
'// http://msdn.microsoft.com/en-us/library/cc143895.aspx - SMS_CIUpdateSources 
'// http://msdn.microsoft.com/en-us/library/cc144678.aspx - SMS_CIToContent 
'// http://msdn.microsoft.com/en-us/library/cc145597.aspx - SMS_SoftwareUpdatesPackage 
'// http://msdn.microsoft.com/en-us/library/cc144546.aspx - SMS_SoftwareUpdate 
'// http://msdn.microsoft.com/en-us/library/cc143796.aspx - SMS_ConfigurationItemBaseClass 
'// http://msdn.microsoft.com/en-us/library/cc143856.aspx - SMS_SoftwareUpdateSource 
'// http://msdn.microsoft.com/en-us/library/cc144546.aspx - SMS_SoftwareUpdate 
'// 
'// 
'// 
'// 
'// 
'// 
'// 
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 
'// Create Log File
Dim logfile :Set logfile = fso.OpenTextFile("c:\temp\" & wscript.scriptname & ".log", 2, True, 0) 

'// Usage if arguments are not 2 (sccmserver hostname)
if wscript.arguments.length <> 2 then 
	Wscript.echo "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 

Dim filename, Location,i,servername,sitecode,query,SMS_AdvertisementID,Advertisement,statement,count

servername = wscript.arguments(0)
query = wscript.arguments(1)

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
		sitecode = Location.SiteCode
        Exit For 
    End If 
Next

'DbgMsg "Sitecode=" & sitecode
'DbgMsg "servername=" & servername
'DbgMsg "query=" & query

'////////////////////////////////////////////////////////////

'SMS_AdvertisementID = "tst20408"
'Dim replacementScheduleArray()
'Dim SMS_ScheduleToken

' --- Create Objects
Dim loc: Set loc = CreateObject("WbemScripting.SWbemLocator")
Set swbemServices = loc.ConnectServer(servername , "root\SMS\site_" & siteCode)

smsQueryEverything()

' --- Refresh Package
Function smsReadAdvertisement()
		statement = "Select * From SMS_Advertisement where AdvertisementName like '%" & query & "%' or AdvertisementID like '%" & query & "%'  or collectionID='" & query & "' "
		DbgMsg "statement=" & statement
		Dim colAdvertisements :Set colAdvertisements = swbemServices.ExecQuery(statement)
		count = 1
		DbgMsg "Count=" & colAdvertisements.Count
		For each Advertisement In colAdvertisements
			DbgMsg "[" & Advertisement.AdvertisementName & " (" & Advertisement.AdvertisementID & ")] "
			DbgMsg "-----------> Program: [" & Advertisement.ProgramName & " (" & Advertisement.PackageID & ")] "
			DbgMsg "-----------> Collection: [" & GetCollectionName(Advertisement.collectionID) & " (" & Advertisement.collectionID & ")] "
		Next 
End Function

Sub DbgMsg(strMessage)
	wscript.echo strMessage
	logfile.writeline Now() & ": " & strMessage
end sub

Function GetCollectionName(strCollID)
	Dim statement2,icollection
	GetCollectionName = null
	statement2 = "Select Name From SMS_Collection where CollectionID='" & strCollID & "' "
	Dim colCollections :Set colCollections = swbemServices.ExecQuery(statement2)
	For each icollection In colCollections
		GetCollectionName = icollection.Name
		'DbgMsg "GetCollectionName=" & GetCollectionName
	Next
End Function
'

'// query task sequence presence
' --- Refresh Package
'Function smsQueryPackageAttrib()
'		Dim pkg
'		statement = "Select * From SMS_Package where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
'		DbgMsg "statement=" & statement
'		Dim colPackageInfo :Set colPackageInfo = swbemServices.ExecQuery(statement)
'		count = 1
'		DbgMsg "Count=" & colPackageInfo.Count
'		For each pkg In colPackageInfo
'			DbgMsg "[" & pkg.Manufacturer & " " & pkg.Name & " (" & pkg.packageid & ")] = " & pkg.Description
'			DbgMsg "-----------> Program: [" & pkg.ProgramName & " (" & Advertisement.PackageID & ")] "
'		Next 
'End Function
'
Function smsTaskSequenceInfo()
		Dim pkg
		statement = "Select * From SMS_TaskSequencePackage  where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		DbgMsg "statement=" & statement
		Dim colPackageInfo :Set colPackageInfo = swbemServices.ExecQuery(statement)
		count = 1
		DbgMsg "Count=" & colPackageInfo.Count
		For each pkg In colPackageInfo
			DbgMsg "[" & pkg.Name & " (" & pkg.packageid & ")] = " & pkg.Description
			'DbgMsg "-----------> Program: [" & pkg.ProgramName & " (" & Advertisement.PackageID & ")] "
		Next 
End Function

Function GetProgramName(strpackageid)
		Dim pkg
		statement = "Select * From SMS_Program  where PackageID='" & strpackageid & "' "
		'DbgMsg "statement=" & statement
		Dim colPackageInfo :Set colPackageInfo = swbemServices.ExecQuery(statement)
		count = 1
		'DbgMsg "Count=" & colPackageInfo.Count
		For each pkg In colPackageInfo
			'DbgMsg "[" & pkg.Name & " (" & pkg.packageid & ")] = " & pkg.Description 
			DbgMsg "    " & "Program " & count & ": " & pkg.ProgramName & " cmd: " & pkg.CommandLine 
			count = count + 1
		Next 
End Function


Function smsQueryEverything()
		Dim count: count = 1
		Dim item, results, n
		
		'// task sequence
		'statement = "Select * From SMS_TaskSequencePackage  where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		'DbgMsg statement
		'Dim results: Set results = swbemServices.ExecQuery(statement)
		'if (results.Count=0) then 
		'	DbgMsg "SMS_TaskSequencePackage: no results " & statement
		'else		
		'	For each item In results
		'		DbgMsg "SMS_TaskSequencePackage: [" & item.Name & " (" & item.packageid & ")] " & item.Description
		'	Next 
		'end if		
		
		'// packages
		'statement = "Select * From SMS_Package where Name like '%" & query & "%' or PackageID like '%" & query & "%'  or Description like '%" & query & "%'  "
		'Set results = swbemServices.ExecQuery(statement)
		'if (results.Count=0) then 
		'	'DbgMsg "no results from: " & statement
		'else		
		'	For each item In results
		'		DbgMsg "SMS_Package: ->[" & item.Manufacturer & " " & item.Name & " " & item.version & " (" & item.packageid & " v" & item.StoredPkgVersion & ")]"
		'		DbgMsg "    " & "Description: " & item.description 
		'		DbgMsg "    " & "Source: " & item.PkgSourcePath
		'		GetProgramName(item.packageid)
		'	Next 
		'end if

		'// advertisements
		'statement = "Select * From SMS_Advertisement where AdvertisementName like '%" & query & "%' or AdvertisementID='" & query & "'  or collectionID='" & query & "' "
		'Set results = swbemServices.ExecQuery(statement)
		'if (results.Count=0) then 
		'	DbgMsg "no results from: " & statement
		'else		
		'	For each item In results
		'		DbgMsg "SMS_Advertisement: " & item.ProgramName & " (" & item.PackageID & ")"
		'		DbgMsg "SMS_Advertisement: ----------> Advertisement Name: [" & item.AdvertisementName & " (" & item.AdvertisementID & ")"
		'		DbgMsg "SMS_Advertisement: ----------> Collection: [" & GetCollectionName(item.collectionID) & " (" & item.collectionID & ")] "
		'	Next
		'end if	
		
		'// SMS_SoftwareUpdate
		'statement = "Select * From SMS_SoftwareUpdate where IsEnabled='true' "
		'dim n: n = 1
		'Set results = swbemServices.ExecQuery(statement)
		'if (results.Count=0) then 
		'	'DbgMsg "no results from: " & statement
		'else		
		'	For each item In results
		'		'DbgMsg "SMS_SoftwareUpdateSource: ->[" & item.Manufacturer & " " & item.Name & " " & item.version & " (" & item.packageid & " v" & item.StoredPkgVersion & ")]"
		'		DbgMsg "(" & n & ") " & "ArticleID: " & item.ArticleID & " " &_
		'		"BulletinID: " & item.BulletinID & " " &_
		'		"CI_ID: " & item.CI_ID & " " &_
		'		"CI_UniqueID: " & item.CI_UniqueID & " " &_
		'		"CIType_ID: " & item.CIType_ID & " " &_
		'		"CIVersion: " & item.CIVersion & " " &_
		'		"EffectiveDate: " & item.EffectiveDate & " "
		'		n = n + 1
		'	Next 
		'end if
		
		'// SMS_SoftwareUpdatesPackage
		statement = "Select * From SMS_SoftwareUpdatesPackage where Name like '%" & query & "%' or PackageID like '%" & query & "%'  "
		n = 1
		Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			'DbgMsg "no results from: " & statement
		else		
			For each item In results
				'DbgMsg "SMS_SoftwareUpdateSource: ->[" & item.Manufacturer & " " & item.Name & " " & item.version & " (" & item.packageid & " v" & item.StoredPkgVersion & ")]"
				'DbgMsg "SMS_SoftwareUpdatesPackage: " & item.Name & " (" & item.PackageID & " v" & item.SourceVersion & ") " 
				'DbgMsg "    " & "PkgSourcePath: " & item.PkgSourcePath
				'DbgMsg "    " & "SourceDate: " & item.SourceDate
				'n = n + 1
			Next 
		end if
		
		'// SMS_CIContentFiles  
		statement = "Select * From SMS_CIToContent where CI_ID=75883"
		DbgMsg statement
		n = 1
		Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			DbgMsg "no results from: " & statement
		else		
			For each item In results
				DbgMsg "SMS_CIToContent  : " 
				DbgMsg "    " & "CI_ID: " & item.CI_ID
				DbgMsg "    " & "ContentDescription: " & item.ContentDescription
				DbgMsg "    " & "ContentDownloaded: " & item.ContentDownloaded
				DbgMsg "    " & "ContentHash: " & item.ContentHash
				DbgMsg "    " & "ContentID: " & item.ContentID
				DbgMsg "    " & "ContentUniqueID: " & item.ContentUniqueID
				n = n + 1
				if (n = 100) then wscript.quit
			Next 
		end if
		
		'// SMS_UpdateDeploymentSummary 
		statement = "Select * From SMS_UpdateDeploymentSummary where CI_ID=75883 and AssignmentName like '%" & query & "%'  "
		n = 1 : Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			DbgMsg "no results from: " & statement
		else		
			For each item In results
				DbgMsg "SMS_UpdateDeploymentSummary: " & "AssignmentName: " & item.AssignmentName
				DbgMsg "    " & "AssignmentID: " & item.AssignmentID
				DbgMsg "    " & "AssignmentUniqueID: " & item.AssignmentUniqueID
				DbgMsg "    " & "CI_ID: " & item.CI_ID
				DbgMsg "    " & "CollectionID: " & item.CollectionID
				'DbgMsg "    " & "CollectionName: " & item.CollectionName
				DbgMsg "    " & "NumNotApplicable: " & item.NumNotApplicable
				DbgMsg "    " & "NumInstalled: " & item.NumInstalled
				DbgMsg "    " & "NumTotal: " & item.NumTotal
				n = n + 1
				if (n = 100) then wscript.quit
			Next 
		end if
		
		'// SMS_CIUpdateSources   
		statement = "Select * From SMS_CIUpdateSources where CI_ID=75883"
		DbgMsg statement
		n = 1
		Set results = swbemServices.ExecQuery(statement)
		if (results.Count=0) then 
			DbgMsg "no results from: " & statement
		else		
			For each item In results
				DbgMsg "SMS_CIUpdateSources  : " 
				DbgMsg "    " & "CI_ID: " & item.CI_ID
				DbgMsg "    " & "UpdateSource_ID: " & item.UpdateSource_ID
				n = n + 1
				if (n = 100) then wscript.quit
			Next 
		end if
		
End Function
