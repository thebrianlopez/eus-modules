strfile = wscript.arguments(0)

GetFileSize(strfile)

Function GetFileSize(strpath)
	Dim pstrpath
	Dim results
	Dim pfso: set pfso = CreateObject("Scripting.FileSystemObject")
	Dim pfile: set pfile = pfso.getfile(strpath)
	results = clng(pfile.Size)
	GetFileSize = results
	set pfso = nothing
	
	DbgMsg "GetFileSize: " & strpath & " = " & results & " typename=" & typename(results)
End Function

Function DbgMsg(strMessage)
	dim test: test = len(strMessage)
	'strMessage = strMessage & " [" & test & "]"
	'logfile.writeline Now() & ": " & strMessage
	wscript.echo Now() & ": " & strMessage
End Function