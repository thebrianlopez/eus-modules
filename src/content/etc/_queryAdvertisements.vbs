'// 2012-05-01
'// Description: Create 24 SCCM Advertisements schedued to run daily then dump collections over night
'//  12 Collections for every hour (e.g., 7am-7pm)
'//  12 Collecitons for every half hour (e.g., 6:30,7:30,8:30)
'//  or create 1 advertiements that runs every 15 minutes mandatory 
'//   2 scripts --> 1 to add the machine to its primary shopping collection  2nd to remove workstations from collection when build process completes successfully and machine images correctly.
'// 
'// 
'// 
'// 
'//
' --- Set Variables
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 

'// Usage if arguments are not 2 (sccmserver hostname)
if wscript.arguments.length <> 2 then 
	Wscript.echo "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 

Dim filename, Location,i,servername,sitecode,query,SMS_AdvertisementID,Advertisement,statement,count

servername = wscript.arguments(0)
query = wscript.arguments(1)

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
		sitecode = Location.SiteCode
        Exit For 
    End If 
Next

'DbgMsg "Sitecode=" & sitecode
'DbgMsg "servername=" & servername
'DbgMsg "query=" & query

'////////////////////////////////////////////////////////////

'SMS_AdvertisementID = "tst20408"
Dim replacementScheduleArray()
Dim SMS_ScheduleToken

' --- Create Objects
Dim loc: Set loc = CreateObject("WbemScripting.SWbemLocator")
Set swbemServices = loc.ConnectServer(servername , "root\SMS\site_" & siteCode)


'smsCreateAdvertisement()
smsReadAdvertisement()
'smsUpdateAdvertisement()
'smsDeleteAdvertisement()

' --- Refresh Package
Function smsReadAdvertisement()
		statement = "Select * From SMS_Advertisement where AdvertisementName like '%" & query & "%' or AdvertisementID like '%" & query & "%'  or collectionID='" & query & "' "
		'statement = "Select * From SMS_Advertisement where collectionID='U04001EE' "
'		'%"& query &"%' "	
		DbgMsg "statement=" & statement
		'Dim colAdvertisements :Set colAdvertisements = swbemServices.ExecQuery("Select * From SMS_Advertisement where AdvertisementID='" & SMS_AdvertisementID & "'")
		Dim colAdvertisements :Set colAdvertisements = swbemServices.ExecQuery(statement)
		'Dim objAdvertisement :Set objAdvertisement = swbemServices.Get("SMS_Advertisement.AdvertisementID='" & SMS_AdvertisementID & "'")
		'Dim Advertisement_Properties :Set Advertisement_Properties = objAdvertisement.Properties_ 'get property Set
		'Dim AssignedScheduleArray :set AssignedScheduleArray = Advertisement_Properties.Item("AssignedSchedule")
		count = 1
		'DbgMsg "-----------------------------------------------------------------------------------------------------------"
		DbgMsg "Count=" & colAdvertisements.Count
		For each Advertisement In colAdvertisements
			'Get Info
			'DbgMsg "Program [" & Advertisement.ProgramName & " (" & Advertisement.PackageID & ")] is being advertied to the colection [" & GetCollectionName(Advertisement.collectionID) & " (" & Advertisement.collectionID & ")]  via the advertisement [" & Advertisement.AdvertisementName & " (" & Advertisement.AdvertisementID & ")] "
			DbgMsg "[" & Advertisement.AdvertisementName & " (" & Advertisement.AdvertisementID & ")] "
			DbgMsg "-----------> Program: [" & Advertisement.ProgramName & " (" & Advertisement.PackageID & ")] "
			DbgMsg "-----------> Collection: [" & GetCollectionName(Advertisement.collectionID) & " (" & Advertisement.collectionID & ")] "
			'DbgMsg "PresentTime=" & Advertisement.PresentTime
			'DbgMsg "PresentTimeEnabled=" & Advertisement.PresentTimeEnabled
			'DbgMsg "TimeFlags=" & Advertisement.TimeFlags
			'DbgMsg "AssignedScheduleEnabled=" & Advertisement.AssignedScheduleEnabled
			'DbgMsg "ExpirationTime=" & Advertisement.ExpirationTime
			'DbgMsg "ExpirationTimeEnabled=" & Advertisement.ExpirationTimeEnabled
			
			 

			'on error resume next
		'	For i = 0 to UBound(AssignedScheduleArray)
		'			Set SMS_ScheduleToken = AssignedScheduleArray(i)
		'			DbgMsg AssignedScheduleArray.Name & " " & i & ":" & SMS_ScheduleToken.GetObjectText_()
		'			DbgMsg "Assignment:" & SMS_ScheduleToken.StartTime
		'	Next
			'DbgMsg "-----------------------------------------------------------------------------------------------------------"
			
			'begin modifying:

			'strNewComment = "Updated by vbs"
			'Advertisement.Comment = strNewComment

			'This next line will modify the Advertisment with settings set in variables above.
			
			'Advertisement.collectionID="U04001EF" 'SNOW5 New Build LMS-PILOT -> U04001EF
			'Advertisement.collectionID="U04001EE" 'SNOW5 New Build LMS-PROD -> U04001EE
			
			'Advertisement.collectionID="U04001EB" 'SNOW5 New Build OFFICE-PROD -> U04001EB
			'Advertisement.collectionID="U04001ED" 'SNOW5 New Build OFFICE-PILOT -> U04001ED
			
			'Advertisement.collectionID="U04001F0" 'SNOW5 New Build KIOSK-PROD -> U04001F0
			'Advertisement.collectionID="U04001F1" 'SNOW5 New Build KIOSK-PILOT -> U04001F1
			
			'Advertisement.Put_
			'DbgMsg "-----------> Program: [" & Advertisement.ProgramName & " (" & Advertisement.PackageID & ")] "
			'DbgMsg "-----------> Collection: [" & GetCollectionName(Advertisement.collectionID) & " (" & Advertisement.collectionID & ")] "
		Next 
End Function

Sub DbgMsg(strMessage)
	wscript.echo strMessage
end sub

Function GetCollectionName(strCollID)
	Dim statement2,icollection
	GetCollectionName = null
	statement2 = "Select Name From SMS_Collection where CollectionID='" & strCollID & "' "
	Dim colCollections :Set colCollections = swbemServices.ExecQuery(statement2)
	For each icollection In colCollections
		GetCollectionName = icollection.Name
		'DbgMsg "GetCollectionName=" & GetCollectionName
	Next
End Function


