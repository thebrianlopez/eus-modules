'
'


'
'
Option Explicit
Dim fso :set fso = CreateObject("Scripting.FilesystemObject")
Dim sh :set sh = CreateObject("WScript.Shell")
Dim swbemLocator :Set swbemLocator = CreateObject("WbemScripting.SWbemLocator") 

'// Usage if arguments are not 2 (sccmserver hostname)
if wscript.arguments.length <> 2 then 
	Wscript.echo "Usage: " & wscript.scriptname & " %sccmserver% %hostname% (e.g., " & wscript.scriptname & " usglnm0000 us16-xxxxxxx "
	wscript.quit
end if

Dim swbemServices :Set swbemServices= swbemLocator.ConnectServer(wscript.arguments(0), "root\sms") 
Dim providerLoc :Set providerLoc = swbemServices.InstancesOf("SMS_ProviderLocation") 
Dim filename, Location,i,hostname

hostname = ucase(wscript.arguments(1))

For Each Location In providerLoc 
    If location.ProviderForLocalSite = True Then 
        Set swbemServices = swbemLocator.ConnectServer(Location.Machine, "root\sms\site_" + Location.SiteCode) 
        Exit For 
    End If 
Next

'//Main method
QueryObsoleteClients swbemServices

'// Private Functions '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'// Query Function
Function QueryObsoleteClients(connection) 
    Dim resources 
    Dim resource,test,mymac,mac,query
	mac = ""
	'query = "Select * From SMS_R_System where SMSUniqueIdentifier='" & ucase(hostname)  & "' "
	'query = "Select * From SMS_R_System where MacAddresses like '%" & ucase(hostname) & "%' or ResourceID='155579' or NetbiosName like '%" & ucase(hostname)  & "%' or HardwareID like '"& hostname&"' or MacAddresses like '%" & ucase(hostname) & "%' or SMSUniqueIdentifier like '%" & ucase(hostname) & "%'  or SMBIOSGUID like '%" & ucase(hostname) & "%'  or LastLogonUserName like '%" & ucase(hostname) & "%' or ADSiteName='" & ucase(hostname) & "' order by AgentTime desc"
	query = "Select * From SMS_R_System where MacAddresses like '%" & ucase(hostname) & "%' or ResourceID like '%" & ucase(hostname) & "%' or NetbiosName like '%" & ucase(hostname)  & "%' or HardwareID like '"& hostname&"' or MacAddresses like '%" & ucase(hostname) & "%' or SMSUniqueIdentifier like '%" & ucase(hostname) & "%'  or SMBIOSGUID like '%" & ucase(hostname) & "%'  or LastLogonUserName like '%" & ucase(hostname) & "%' or ADSiteName='" & ucase(hostname) & "' order by AgentTime desc"
	wscript.echo query
	err.clear
    Set resources = connection.ExecQuery(query)
    If Err.Number<>0 Then 
        Wscript.Echo "Couldn't get resources" 
        wscript.quit
    End If 
    If resources.Count=0 Then 
        Wscript.Echo "No resources found" 
		wscript.quit
    End If
	For Each resource In resources 
		'for each mac in resource.MacAddresses
			'mymac = mac 
		'next
		'Wscript.echo "-->[" & resource.NetbiosName & "] ID/" & resource.ResourceID & " guid/" & resource.SMSUniqueIdentifier & " MAC/[" & mymac & "] Site/" & Location.SiteCode & " Obsolete=[" & resource.obsolete & "] Server/" & Location.Machine & " UUID/" & resource.SMBIOSGUID & " > " & DeleteResource(connection,resource.ResourceID) 
		Wscript.echo "-->[" & resource.NetbiosName & "] ID/" & resource.ResourceID & " guid/" & resource.SMSUniqueIdentifier & " exit=" & DeleteResource(connection,resource.ResourceID) 
    Next 
	
	wscript.quit
End Function

'// Delete Function
Function DeleteResource (connection, resourceID) 
    Dim res :Set res = connection.Get("SMS_R_System.ResourceID='" & resourceID & "'") 
	err.clear
	On error resume next
    res.Delete_
	if(err.Number <> 0) then 
		DeleteResource = Err.Number	
	else
		DeleteResource = "Success!"
	end if
End Function

