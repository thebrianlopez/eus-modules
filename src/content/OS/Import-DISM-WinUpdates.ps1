# Script Name: Import-DISM-WinUpdates.ps1
# Date Created: 2014-04-23 14:51PM PST
# Description: Script polls a list of client workstations for offline sync status and logs all data to a database
# Revisions: 
#    2014-04-23: Created. 
#    2014-05-05: Working now.
#
#
# ToDo:
#    2014-05-05: Add logging support with NLog to ensure standardized logging tools (maybe create a ps1m library for reuse)
#
#
# Psuedo code: Automatic script to
# * Query SCCM for latest wim file
# * Copy wim locally
# * mount wim file
# * download patches from Microsoft.com
# * install patches to wim file
# * update build number in wim file
# * commit


# Global vairables
$scriptpath = $MyInvocation.MyCommand.Definition
$scriptname = $MyInvocation.MyCommand.Name
$Logfile =  Join-Path ($env:temp) ($scriptname +".log")

Function LogWrite
{
   Param ([string]$logstring)
   $timestamp = Get-Date -Format o
   $logstring = $timestamp + " : " + $logstring
   Write-Host  $logstring
   Add-content $Logfile -value $logstring
}

Function Wim-Utility
{
  param( 
        [Parameter(Mandatory=$true)][string]$action
        ) 

  $loglevel = "WarningsInfo"
  # $operation = "mount"
  $root = "wim-maintenance"
  $pwd = Split-Path -Parent -Path $scriptpath | Split-Path -Parent
  $pwd = Join-Path $pwd  $root -resolve
  $repo = Join-Path -path $pwd -childpath "repo"
  $mount = Join-Path $pwd "mount" -resolve
  $logpath = Join-Path $pwd "log" -resolve
  $WimFolder = Join-Path $pwd "wim" -resolve
  $scratch = Join-Path $pwd "tmp" -resolve
  $WimFile = Join-Path $WimFolder "*wim" -resolve
  $wimindex = 1

  LogWrite "Action: $action"
  LogWrite "MountPoint: $mount"
  LogWrite "UpdatesRepo: $repo"
  LogWrite "WimFile: $WimFile"
  LogWrite "Logfile: $Logfile"
  LogWrite "Scratch: $scratch"

  #Validate WIM exists in the infra
  try{
    Get-WindowsImage -ImagePath $WimFile
  }
  catch{
    $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
    LogWrite $message
    exit
  }


  

  ##############################################################
  ### Mount the wim file 
  ##############################################################
  if($action -eq "mount")
  {
    LogWrite "Performing mount"
    # try{Mount-WindowsImage -Imagepath $WimFile -index 1 -path $mount -LogLevel $loglevel -Optimize}
    try{ Mount-WindowsImage -Imagepath $WimFile -index 1 -path $mount -LogLevel $loglevel -Optimize}
    catch{
      $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
      LogWrite $message
      exit
    }
    LogWrite "Mount successful"
    
  }

  ##############################################################
  ### Discard the wim file 
  ##############################################################
  if($action -eq "discard"){
    LogWrite "Unmounting wim..."
    try{Dismount-WindowsImage -Discard -path $mount -LogLevel $loglevel}
    catch{
      $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
      LogWrite $message
      exit
    }
    LogWrite "Unmount complete"
  }

  ##############################################################
  ### Save the wim file 
  ##############################################################
  if($action -eq "save"){
    LogWrite "Unmounting wim..."
    try{Dismount-WindowsImage -save -path $mount -LogLevel $loglevel}
    catch{
      $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
      LogWrite $message
      exit
    }
    LogWrite "Unmount complete"
  }

  ##############################################################
  ### Update wim with all patches in the repo folder
  ##############################################################
  if($action -eq "update"){
    # Detect list of patches that are required 
    LogWrite "Patch folder set to: $repo"
    try{
      Add-WindowsPackage -LogLevel $loglevel -path $mount -Packagepath $repo -scratchdirectory $scratch
    }
    catch{
      $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
      LogWrite $message
      exit
    }
    
    LogWrite "Done adding to mountpoint"
  }
  

  ##############################################################
  ###List all patches currently in the wim file 
  ##############################################################
  if($action -eq "list"){
    # Detect list of patches that are required 
    LogWrite "Listing installed updates in image"
    # LogWrite "Path exists? $(Test-Path $mount)"

    try{
      # Get-WindowsPackage -path $mount | Export-Csv -NoTypeInformation -path (Join-Path ($logpath) ("Installed_Patches.csv"))
      $cmd = "dism /image:$mount /get-packages /format:table /English > Join-Path ($logpath) (Packages.txt)"
      LogWrite "cmd: $cmd"
      # Invoke-Command $cmd
    }
    catch{
      $message = "TRAPPED: {0}: '{1}'" -f ($_.Exception.GetType().FullName), ($_.Exception.Message)
      LogWrite $message
      exit
    }
    LogWrite "List operation complete"
  }
  LogWrite "All done"
}


$amount = (Measure-Command{
    # Wim-Utility -action mount
    Wim-Utility -action list
    # Wim-Utility -action update
    # Wim-Utility -action list
    # Wim-Utility -action save
    # Wim-Utility -action discard
}).TotalSeconds


LogWrite "Process Completed in $amount seconds"



