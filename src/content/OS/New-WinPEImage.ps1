:: Drivers to install
3Com 10/100/1000 PCI 10.26.0.0
3Com Dual Port 1000-SX PCI-X Server NIC 10.9.0.0
AMD PCNET Family PCI Ethernet Adapter 4.38.0.0
Intel(R) 82567LM-2 Gigabit Network Connection 9.52.9.0
Intel(R) 82567LM-3 Gigabit Network Connection 11.5.10.0
Intel(R) 82579LM Gigabit Network Connection 11.12.36.0
vmxnet3 Ethernet Adapter 1.1.6.0
Intel(R) 82579LM Gigabit Network Connection 11.15.16.0
Intel(R) ICH7R/DH SATA RAID Controller 11.0.0.1032
Intel(R) Ethernet Connection I217-LM 12.6.47.0


:: WinPE CReate a Boot CD - Requires ADK 8.1 Installed - http://technet.microsoft.com/en-us/library/dn293200.aspx
set arch=x86
set basebcd=\\hqaaam0011\Packages$\Global\EN\1EPXEServerTFPRoot\V1.0\Images\DV00173F\boot.DV00173F.bcd
set solpath=C:\temp\WinPE\_%arch%_source
set tftppath=%solpath%\tftp
set winpepath=C:\Program Files (x86)\Windows Kits\8.1\Assessment and Deployment Kit\Windows Preinstallation Environment
set pkgpath=C:\Program Files (x86)\Windows Kits\8.1\Assessment and Deployment Kit\Windows Preinstallation Environment\%arch%\WinPE_OCs
set filespath=C:\Program Files (x86)\Windows Kits\8.1\Assessment and Deployment Kit\Deployment Tools
set mntpnt=%solpath%\mount
set media=%solpath%\media
set bootimgpkgid=D000005E
set dstbcd=%tftppath%\Images\%bootimgpkgid%\boot.%bootimgpkgid%.bcd
set dstwim=%tftppath%\Images\%bootimgpkgid%\boot.%bootimgpkgid%.wim

:: Copy WinPESource
copype "%arch%" "%solpath%"

:: Mount Image - http://technet.microsoft.com/en-us/library/hh824972.aspx
Dism /Mount-Image /ImageFile:"%solpath%\media\sources\boot.wim" /index:1 /MountDir:"%mntpnt%"

:: Copy 1e boot components
md %tftppath%
md %tftppath%\Boot
md %tftppath%\Boot\Fonts
md %tftppath%\Boot\%arch%
md %tftppath%\Config
md %tftppath%\Images
md %tftppath%\Images\%bootimgpkgid%

:: Copy files from WinPE image into TFTP Root Package
copy "%mntpnt%\Windows\Boot\PXE\*" "%tftppath%\Boot\%arch%\" /y
copy "%winpepath%\%arch%\Media\Boot\boot.sdi" "%tftppath%\Boot\%arch%" /y
copy "%winpepath%\%arch%\Media\Boot\boot.sdi" "%tftppath%\" /y
copy "%winpepath%\%arch%\Media\Boot\Fonts\*" "%tftppath%\Boot\Fonts\" /y


:: Install Packages
cd /d "%pkgpath%"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-WMI.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-WMI_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-Scripting.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-Scripting_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-HTA.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-HTA_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-NetFx.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-NetFx_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-SecureStartup.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-SecureStartup_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-PowerShell.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-PowerShell_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-Dot3Svc.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-Dot3Svc_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-StorageWMI.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-StorageWMI_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-EnhancedStorage.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-EnhancedStorage_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-SecureBootCmdlets.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-DismCmdlets.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-DismCmdlets_en-us.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\WinPE-MDAC.cab"
dism /image:%solpath%\mount /add-package /PackagePath:"%pkgpath%\en-us\WinPE-MDAC_en-us.cab"

:: Unmount - http://technet.microsoft.com/en-us/library/hh824972.aspx
Dism /Unmount-Image /MountDir:"%mntpnt%" /commit

:: Copy source file into PXELite Server package
copy "%media%\sources\boot.wim" "%dstwim%" /y
copy "%basebcd%" "%dstbcd%" /y

bcdedit /store "%dstbcd%" /set {ramdiskoptions} description "Ramdisk Options"
::bcdedit /store "%dstbcd%" /set {ramdiskoptions} ramdisktftpblocksize 16384
::bcdedit /store "%dstbcd%" /set {ramdiskoptions} ramdisksdidevice boot
::bcdedit /store "%dstbcd%" /set {ramdiskoptions} ramdisksdipath \boot.sdi
bcdedit /store "%dstbcd%" /set {default} device ramdisk=[boot]\Images\%bootimgpkgid%\boot.%bootimgpkgid%.wim,{ramdiskoptions}
bcdedit /store "%dstbcd%" /set {default} path \Windows\System32\boot\winload.exe
bcdedit /store "%dstbcd%" /set {default} description "WinPE 5.1 x86"
bcdedit /store "%dstbcd%" /set {default} osdevice ramdisk=[boot]\Images\%bootimgpkgid%\boot.%bootimgpkgid%.wim,{ramdiskoptions}
bcdedit /store "%dstbcd%" /set {default} systemroot \Windows
bcdedit /enum all /store "%dstbcd%" > %tftppath%\Images\%bootimgpkgid%\boot.%bootimgpkgid%.bcd.log


:: Make an ISO File - http://technet.microsoft.com/en-us/library/dn293200.aspx
:: MakeWinPEMedia /ISO %solpath% %solpath%\WinPE_%arch%.iso
