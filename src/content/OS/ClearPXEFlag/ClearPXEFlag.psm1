$thismoddir = (split-path -parent $($MyInvocation.MyCommand.Definition))


 # Ideally we should keep the environmental configuration as limited as possible -- maybe detecting from a trusted CA would be a better approach
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}

. "$thismoddir\Get-PXEFlag.ps1"
. "$thismoddir\Set-PXEFlag.ps1"
. "$thismoddir\Clear-PXEFlag.ps1"

New-alias -name cpxe -value Clear-PXEFlag
New-alias -name clearpxe -value Clear-PXEFlag
New-alias -name clearpxeflag -value Clear-PXEFlag
New-alias -name setpxe -value Set-PXEFlag
New-alias -name getpxe -value Get-PXEFlag

Export-ModuleMember -Function * -Alias *
