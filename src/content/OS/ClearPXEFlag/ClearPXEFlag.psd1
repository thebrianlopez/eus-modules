@{
    ModuleToProcess = 'ClearPXEFlag.psm1'
    ModuleVersion = '1.0'
    GUID = 'ffd63615-1c83-4a6b-95ce-77982566878d'
    Author = 'testlopez1'
    CompanyName = 'GTS'
    Copyright = '(c) 2015 testlopez1. All rights reserved.'
    Description = 'PowerShell module to interact with the eucwebservices'
    FunctionsToExport = '*'
    CmdletsToExport = '*'
    VariablesToExport = '*'
    AliasesToExport = '*'
    FileList = @(
        'ClearPXEFlag.psm1',
        'Get-PXEFlag.ps1',
        'Set-PXEFlag.ps1'
    )
}

