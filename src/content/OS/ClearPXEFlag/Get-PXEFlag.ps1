function Get-PXEFlag{
    <#
    .SYNOPSIS 
    Gets the build status of a SNOW Workstation (i.e., the PXE Flag)

    .DESCRIPTION
    Version 1.0

    Version History
        2015-05-16  Initial creation

    This function can take the list of computers and clear the pxe flag to allow SNOW imaging.
    This function replaces the main functionality of the ClearPXE tool by utilizing the EUC Webservice to make calls to the MDT and SCCM databases.

    .INPUTS
    Accepts input from the pipeline as a [string] for the ComputerName property

    .OUTPUTS
    String. 

    .EXAMPLE
    C:\PS> Get-PXEFlag us16-0g32w33 
  
    .EXAMPLE
    C:\PS> Get-PXEFlag us16-0g32w33,us16-0iy373c | Clear-PXEFlag

    .EXAMPLE
    C:\PS> Get-PXEFlag us16-0g32w33,us16-0iy373c | Set-PXEFlag

    .LINK
    http://www.nestle.com

    #>   
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,HelpMessage="Enter the computer name or asset tag.",ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [Alias("FQDN","Computer","Machine")]
        [String[]] $ComputerName,
        [parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$uri="https://eucwebserv/eucwebserv",
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )]
        [System.Management.Automation.PSCredential] $credential = $null,                                # Not srue if this is really needed as we dont care who clearxe flags
        [Parameter(Mandatory=$false)][switch]$async                                                     # Todo someday
        )

    BEGIN{
        $mdt=(New-WebServiceProxy -uri "$uri/mdt.asmx")
    }
    PROCESS{
        $returnstatus = @()

        foreach($computer in $computername){
            $wsdlresponse = $null

            write-verbose "Step 1 - get the UUID of the computer"
            $wsdlresponse = $mdt.GetComputerSettingsProperty("$computer")
            $uuid = $wsdlresponse.ComputerSettingResult.uuid
            $name = $wsdlresponse.ComputerSettingResult.ComputerName


            if($wsdlresponse.Status -ne "Success"){
                $PXEFlag = "NotFound"
            }
            else{
                $wsdlresponse = $mdt.GetComputerSettingsProperty($uuid)
                $ServerB = $wsdlresponse.ComputerSettingResult.ServerB
                $EUCSnowRebuildCode = $wsdlresponse.ComputerSettingResult.EUCSnowRebuildCode

                write-verbose "Step 3 - Determine if the flag status is set or clear"
                if($($ServerB -eq "INSTALLED") -and $($EUCSnowRebuildCode -eq "")){
                    write-verbose "PXEflag is set, No SNOW Rebuild can occur"
                    $PXEFlag = "Set"
                }
                else{
                    write-verbose "PXEFlag is clear. Machine is ready to be built."
                    $PXEFlag = "Clear"
                }
            }


            write-verbose "Found the name $name with uuid $uuid"
            write-verbose "Step 2 - Use the UUID of the computer to get the flag status"

            write-verbose "Step 4 - Create a property hastable to populate or response object"
            $props = @{
                "ComputerName"=$computer;
                "ServerB"=$ServerB;
                "EUCSnowRebuildCode"=$EUCSnowRebuildCode;
                "PXEFlag"=$PXEFlag;
            }
            $row = New-Object -TypeName PSOBject -Property $props

            write-verbose "Add the property hash to out new object"


            write-verbose "Step 6: Push our new object to the global object"
            $returnstatus += $row
        }

        write-verbose "Return the global object containing the status for all computers"
        Write-Output $returnstatus
    }
}