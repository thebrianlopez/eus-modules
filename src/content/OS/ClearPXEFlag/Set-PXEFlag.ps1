function Set-PXEFlag{
    <#
    .SYNOPSIS 
    Sets the build status of a SNOW Workstation (i.e., the PXE Flag)

    .DESCRIPTION
    Version 1.0

    Version History
        2015-05-16  Initial creation

    This function can take the list of computers and clear the pxe flag to allow SNOW imaging.
    This function replaces the main functionality of the ClearPXE tool by utilizing the EUC Webservice to make calls to the MDT and SCCM databases.

    .INPUTS
    Accepts input from the pipeline as a [string] for the ComputerName property

    .OUTPUTS
    String. 

    .EXAMPLE
    C:\PS> Set-PXEFlag us16-0g32w33 
  
    .EXAMPLE
    C:\PS> Set-PXEFlag us16-0g32w33,us16-0iy373c | Get-PXEFlag

    .EXAMPLE
    C:\PS> Set-PXEFlag us16-0g32w33,us16-0iy373c | Clear-PXEFlag

    .LINK
    http://www.nestle.com

    #>   
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,HelpMessage="Enter the computer name or asset tag.",ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [Alias("FQDN","Computer","Machine")]
        [String[]] $ComputerName,
        [parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$uri="https://eucwebserv/eucwebserv",
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )]
        [System.Management.Automation.PSCredential] $credential = $null,                                # Not srue if this is really needed as we dont care who clearxe flags
        [Parameter(Mandatory=$false)][switch]$async,                                                    # Todo someday
        [Parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$ServerB="INSTALLED",
        [Parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$EUCSnowRebuildCode=""
        )

    BEGIN{
        $mdt=(New-WebServiceProxy -uri "$uri/mdt.asmx")
    }
    PROCESS{
        $returnstatus = @()

        foreach($computer in $computername){
            try{


                $res = $null

                write-verbose "Step 1 - Get the UUID of the computer"
                $res = $mdt.GetComputerSettingsProperty("$computer")
                switch($res.Status){
                    "Exception" {write-error "$($res.ErrorMessage)" -ea Stop;}
                    "Success"  {write-verbose "Query succeeded."}
                    "Failure" {write-error "$($res.ErrorMessage)";}
                    "RecordsForKeyNotFound"{$PXEFlag = "No records found";write-verbose $pxeflag;throw}
                    # default {write-error "Webservice returned an error." -ea Stop;throw "XXX";}
                }
                #$boom = 0
                

                $uuid = $res.ComputerSettingResult.uuid
                $name = $res.ComputerSettingResult.ComputerName
                # $boom = 0
                $res = $null
                if($uuid -eq $null){
                    $pxeflag = "No uuid returned from the database. "
                    write-verbose $pxeflag
                    throw
                }

                write-verbose "Found uuid of $uuid and machine name of $name"

                write-verbose "Step 2: Setting legacy ServerB flag to $ServerB"
                $res = $mdt.SetComputerSettingsProperty($uuid,"ServerB",$ServerB)
                switch($res.Status){
                    "Exception" {write-error "$($res.ErrorMessage)" -ea Stop;}
                    "Success"  {write-verbose "Query succeeded."}
                    "Failure" {write-error "$($res.ErrorMessage)";}
                    "RecordsForKeyNotFound"{write-verbose "No records found for this computer.";}
                    "Exception" {write-error "$($res.ErrorMessage)";}
                    # default {write-error "Webservice returned an error." -ea Stop;throw "XXX";}
                }
                $res = $null

                write-verbose "Step 3: Setting EUCSnowRebuildCode to $EUCSnowRebuildCode"
                $res = $mdt.SetComputerSettingsProperty($uuid,"EUCSnowRebuildCode",$EUCSnowRebuildCode)
                switch($res.Status){
                    "Exception" {write-error "$($res.ErrorMessage)" -ea Stop;}
                    "Success"  {write-verbose "Query succeeded.";$PXEFlag="Set Successfully"}
                    "Failure" {write-error "$($res.ErrorMessage)";}
                    "RecordsForKeyNotFound"{write-verbose "No records found for this computer.";}
                    "Exception" {write-error "$($res.ErrorMessage)";}
                    # default {write-error "Webservice returned an error." -ea Stop;throw "XXX";}
                }
                $res = $null 
            }
            catch{
                write-verbose $_.Exception
                $EUCSnowRebuildCode=""
                $ServerB = ""
            }           

            write-verbose "Step 4 - Return the string"
            $props = @{
                "ComputerName"=$computer;
                "ServerB"=$ServerB;
                "EUCSnowRebuildCode"=$EUCSnowRebuildCode;
                "PXEFlag"=$PXEFlag;
            }
            $row = New-Object -TypeName PSOBject -Property $props
            $returnstatus += $row
        }

        Write-Output  $returnstatus
    }
}
