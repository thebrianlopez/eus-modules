function Clear-PXEFlag 
{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,HelpMessage="Enter the computer name or asset tag.",ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [Alias("FQDN","Computer","Machine")][String[]] $ComputerName,
        [parameter(Mandatory=$false,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]
        [string]$uri="https://eucwebserv/eucwebserv",
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null,
        [Parameter(Mandatory=$false)][switch]$async,
        [Parameter(Mandatory=$false)]
        [string]$ServerB="",
        [Parameter(Mandatory=$false)]
        [string]$EUCSnowRebuildCode="INSTALL"
        )

    BEGIN {
        $mdt=(New-WebServiceProxy -uri "$uri/mdt.asmx")
        $returnstatus = @()
    }

    PROCESS {
        foreach($computer in $computername){

            $params = @{
                "ComputerName"=$computer;
                "uri"=$uri;
                "credential"=$credential;
                "async"=$async;
                "ServerB"=$ServerB;
                "EUCSnowRebuildCode"=$EUCSnowRebuildCode;
            }
            
            $returnstatus += $(Set-PXEFlag @params)

        }
        write-output $returnstatus
    }
    
}