$Logfile = "c:\Program Files\Nestle\Logs\Uninstall_$(gc env:computername).log"

Function LogWrite
{
   Param ([string]$logstring)
   $timestamp = Get-Date -Format o
   $logstring = $timestamp + " : " + $logstring
   write-host $logstring
   Add-content $Logfile -value $logstring
}

function Uninstall
{
  Param ([string]$appname)
  LogWrite "Starting program"
  LogWrite "Searching for the application name $appname"
  $xstring = "name like '%$appname%'" 
  LogWrite "WMI Query: $xstring"
  $app=Get-WmiObject -class Win32_Product -filter $xstring
  $count = $app.count


  if($count){
  LogWrite "Found $count items installed"  
  }

  foreach($item in $app){
    if($item.Name){
      $name = $item.Name
      $code = $item.PackageCode
      LogWrite "Found $name with code $code"
      $item.uninstall()
    }
  }

  # after
  $app=Get-WmiObject -class Win32_Product -filter $xstring
  $count = $app.count
  foreach($item in $app){
    if($item.Name){
      $name = $item.Name
      $code = $item.PackageCode
      LogWrite "Found $name with code $code"
    }
  }
  LogWrite "Script complete"  
}

function uninstallreg
{
  Param ([string]$appname)
  # LogWrite $appname
  # $reg = Get-ItemProperty "HKLM:\software\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate, UninstallString, InstallLocation
  # $reg = Get-ItemProperty "HKLM:\software\Microsoft\Windows\CurrentVersion\Uninstall\*" | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate, UninstallString, InstallLocation | Where-Object {$_.DisplayName -like '*Microsoft*' }
  $reg = Get-ItemProperty "HKLM:\software\Microsoft\Windows\CurrentVersion\Uninstall\*" | Where-Object {$_.DisplayName -like "*$appname*" } | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate, UninstallString, InstallLocation

  foreach($rg in $reg)
  {
    LogWrite $rg.DisplayName
    LogWrite $rg.DisplayVersion
    LogWrite $rg.UninstallString
    # LogWrite $sreg
  }
  
}

# Uninstall -appname "VLC"
# Uninstall -appname "videolan"
uninstallreg -appname "adobe"