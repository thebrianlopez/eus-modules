<# 
.SYNOPSIS 
Starts RDP and all pre-requisite services on a speficifed computer(s)
.DESCRIPTION 
Starts the Remote Registry service if not already started, starts the remote desktop service and enables remote desktop, then add 'Client Management' group to the local 'Remote Desktop Users Group' 
.INPUTS 
Object 
    Computername string array, can also be specified from the pipeline in a text file
    Credential - Mandatory parameter that prompts for creds to configure the remote components
.OUTPUTS 
   None
.EXAMPLE 
  Start-RDP -computername us16-0g32v32
  This example configures and starts RDP on a remote workstation
.EXAMPLE 
  Start-RDP -computername (Get-content .\computer.txt)
  This example configures and starts RDP on a list of remote workstations from a text file

.NOTES 
  Todo: Detect and prompt for admin rights to ensure we dont annoy people that already have admin rights.
  Testing: 
    Need to test this against Get-ADUser pipeline output to ensure we can run this using AD as an input
    Need to test this against a WMI query from SCCM to ensure we can enable rdp for a list of machines in a collection.
.LINK 
  http://snowwiki.nestle.com/wiki/index.php/Client_SWD_Scripts
  #> 
  function Start-RDP{
    [CmdletBinding()] 
    param(
      [Parameter(Mandatory=$false, ValueFromPipeline = $true)] [string[]]$computername=$env:computername,
      [Parameter(Mandatory=$true, ValueFromPipeline = $true)] [System.Management.Automation.PSCredential]$credential,
      [Parameter(Mandatory=$false)][string]$domain="nestle.com"
      ) 
    process{
      foreach ($computer in $computername) {

        # Start Remote Registry Service
        $service = "RemoteRegistry"
        $params = @{
          "Namespace"="root\cimv2";
          "credential"=$credential;
          "computername"=$computer;
          "Class"="Win32_Service";
          "filter"="Name='$service'";
        }

        write-verbose "Attempting to start service $service"
        try{
          $obj = (Get-WmiObject @params)
          write-verbose "Current state of service is $($obj.State) "
          if($obj.State -eq "Stopped"){
            write-verbose "Attmepting to start the service $service on computer $computer"
            $results = $obj | invoke-wmimethod -Name StartService
            write-verbose "$service method exited with code $($results.ReturnValue) "
          }
          else{
            Write-Verbose "$service is already running on $computer, skipping"
          }
        }
        catch
        {
          write-error $_.Exception.Message
          throw $_.Exception.Message
        }


        # Enable WinRM
        write-verbose "Enabling WinRM"
        $cmd = reg add \\$computer\hklm\SOFTWARE\Policies\Microsoft\Windows\WinRM\Service /v AllowNegotiate /t REG_DWORD /d 1 /f
        write-verbose "WinRM hopefully enabled"

        $service = "Win32_TerminalServiceSetting"
        $params = @{
          "Namespace"="root\cimv2\TerminalServices";
          "credential"=$credential;
          "computername"=$computer;
          "authentication"="PacketPrivacy";
          "impersonation"="Impersonate"
          "Class"=$service;
        }

        write-verbose "Attempting to start service $service"

        try{
          $obj = (Get-WmiObject @params)
          if($obj.AllowTSConnections -eq 1){
            write-verbose "Remote Desktop is already enabled on $computer, skipping"
          }
          else{
            write-verbose "Enabling Remote Desktop on $computer"
            $item = $obj.SetAllowTSConnections(1)
            write-verbose "$service method exited with code $($item.ReturnValue) "
          }
        }
        catch
        {
          throw $_.Exception.Message
          write-error $_.Exception.Message

        }

        #$state = (Get-WmiObject -credential $credential -computername $computername -authentication PacketPrivacy -impersonation Impersonate $service).State
        #write-verbose "Service is currently set to $state"

        # Add users to local 'Remote Users' group.
        write-verbose "Attempting to add group $group to $computer" 
        Add-DomainUserToLocalGroup -computername $computer -domain $domain -credential $credential -group "amsnaclimanag"

      }
    }
  }

  Function Add-DomainUserToLocalGroup 
  { 
    [cmdletBinding()] 
    Param( 
      [Parameter(Mandatory=$false)][string[]]$computername=$env:computername, 
      [Parameter(Mandatory=$false)][string]$localgroup="Remote Desktop Users", 
      [Parameter(Mandatory=$false)][string]$domain="nestle.com", 
      [Parameter(Mandatory=$false)][Alias("group")][string]$adgroup="amsnaclimanag",
      [Parameter(Mandatory=$true, ValueFromPipeline = $true)] [System.Management.Automation.PSCredential]$credential
      )

    foreach($computer in $computername) {
      write-verbose "Adding the group $adgroup to the local group $localgroup"

      $de = [ADSI]"WinNT://$computername/$localgroup,group"

      try{

        write-verbose "Attempting to add '$adgroup' to the local group '$localgroup' on '$computer'"
        $de.psbase.UserName=$credential.UserName
        $de.psbase.Password=$credential.GetNetworkCredential().password
        $de.psbase.Invoke("Add",([ADSI]"WinNT://$domain/$adgroup").path)
        write-verbose "Action completed successfully"
      }
      catch{
        $ex = $_.Exception
        Write-verbose $ex.Message
      }


    }
    } #end function Add-DomainUserToLocalGroup

