# ============================================================================= 
# NAME: ServciceImage.ps1 
#  
# AUTHOR: Ben Pearce, Jason Stiff, Premier Field Engineering, Microsoft UK 
# DATE  : May 2010 
# Version: 1.0 
#  
# COMMENT: Script to automate the servicing of a .WIM or .VHD Windows 7/Windows  
# Server 2008 R2 offline image. 
# 
# 
# This script sample is provided "AS IS" with no warranties, confers no rights and  
# is not supported by the authors or Microsoft Corporation.  
# Use of this script sample is subject to the terms specified at  
# http://www.microsoft.com/info/cpyright.mspx. 
# ============================================================================= 
 
# ============================================================================= 
# Common Functions 
# ============================================================================= 
 
# ============================================================================= 
# Generate the dism command line to inject packages into a mounted .WIM or .VHD 
# ============================================================================= 
function gendismcmd($cabs,$img) 
{ 
 
    $cmdline = "DISM /image:" + $img + " /Add-Package"  
    foreach($cab in $cabs) 
    { 
        $cmdline += " /PackagePath:" + "'" + $cab.fullname + "'" 
    } 
    return $cmdline 
 
} 
 
# ============================================================================= 
# Generate the DiskPart scripts required to mount a .vhd image 
# ============================================================================= 
function gendpmountscript($drive, $scriptpath, $vhd) 
{ 
    $dpscript = @() 
    $dpscript += ("select vdisk file=" + "'" + $vhd +"'") 
    $dpscript += "attach vdisk" 
    $dpscript += "exit" 
    set-content -path "$scriptpath\dpmount1.txt" -value $dpscript 
     
    $dpscript = @() 
    $dpscript += ("select vdisk file=" + "'" + $vhd +"'") 
    $dpscript += "Select partition 2" 
    $dpscript += ("assign letter=" + $drive) 
    $dpscript += "exit"  
    set-content -path "$scriptpath\dpmount2.txt" -value $dpscript 
} 
 
# ============================================================================= 
# Generate and invoke the DISM command to display information of a .wim image 
# ============================================================================= 
function showwimindex($wim) 
{ 
    $showindex = "Dism /Get-WimInfo /WimFile:" + "'" + $wim + "'" 
    Invoke-Expression $showindex 
    echo " " 
} 
 
# ============================================================================= 
# Generate and invoke the DISM command to mount the .wim image 
# ============================================================================= 
function invokewimmount($wim, $mountindex, $mountdir) 
{ 
    $mountwim = "DISM /Mount-Wim /WimFile:" + "'" + $wim + "'" + " /index:" + $mountindex + " /MountDir:" + "'" + $mountdir + "'" 
    Invoke-Expression $mountwim 
} 
 
# ============================================================================= 
# Generate and invoke DISM command to unmount the .wim image 
# ============================================================================= 
function wimunmount($mountdir) 
{ 
    $unmountwim = "DISM /Unmount-Wim /MountDir:" + "'" + $mountdir + "'" + " /commit" 
    Invoke-Expression $unmountwim 
} 
 
# ============================================================================= 
# Generate the DiskPart script required to unmount a .vhd image 
# ============================================================================= 
function gendpUnmountscript($scriptpath, $vhd) 
{ 
    $dpscript = @() 
    $dpscript += ("select vdisk file=" + "'" + $vhd +"'") 
    $dpscript += "detach vdisk" 
    $dpscript += "exit"  
    set-content -path "$scriptpath\dpunmount.txt" -value $dpscript 
} 
 
# ============================================================================= 
# Main() 
# ============================================================================= 
# Common Processing 
# ============================================================================= 
 
# ============================================================================= 
# Generate Required Variables 
# ============================================================================= 
$sourcepack = read-host "Enter the directory containing packages to inject" 
$imageloc = read-host "Enter the full path to the .vhd or .wim file to be mounted" 
 
if($sourcepack -eq "" -or $imageloc -eq "") 
{ 
    "Please supply valid image location and package directory"  
    exit 1 
} 
$imageloc = $imageloc.ToLower() 
 
$cabs = get-childitem -path $sourcepack| where{($_.extension -eq ".msu") -or($_.extension -eq ".cab")} | select fullname 
if ($cabs -eq $null) 
{ 
    "No packages found in package directory"  
    exit 1 
} 
$dpscriptpath = ".\" 
 
 
# ============================================================================= 
# Call required .VHD servicing functions 
# ============================================================================= 
if ($imageloc.EndsWith(".vhd")) 
{ 
    $mountdrive = Read-Host "Enter drive letter to temporarly mount .vhd drive. e.g R" 
    gendpmountscript $mountdrive $dpscriptpath $imageloc 
    gendpunmountscript $dpscriptpath $imageloc 
    $mountloc = $mountdrive + ":\" 
    $dismcmdline = gendismcmd $cabs $mountloc 
     
    "Attaching VDisk ...." 
    diskpart /s "$dpscriptpath\dpmount1.txt" 
    start-sleep -Seconds 4 
    "Assigning Drive Letter ...." 
    diskpart /s "$dpscriptpath\dpmount2.txt" 
    "Running DISM command line ...." 
    start-sleep -seconds 2 
    invoke-expression $dismcmdline 
    "Unmounting VDisk ...."  
    diskpart /s "$dpscriptpath\dpunmount.txt" 
} 
 
# ============================================================================= 
# Call required .WIM servicing functions 
# ============================================================================= 
elseif ($imageloc.EndsWith(".wim")) 
{ 
    showwimindex $imageloc 
    $index = Read-Host "Enter the .wim index number" 
    $mountloc = Read-Host "Enter the full path to an empty mount directory" 
    if ($mountloc -eq "") 
    { 
        "Please supply a valid path to an empty mount directory" 
        exit 1 
    } 
    "Mounting WIM .... " 
    invokewimmount $imageloc $index $mountloc 
    $dismcmdline = gendismcmd $cabs $mountloc 
    "Running DISM command line ...." 
    invoke-expression $dismcmdline 
    "Unmounting WIM .... " 
    wimunmount $mountloc 
} 
"Script has completed running"