$wks = Get-Content .\Computers.txt
$service = "NomadBranch"

Function Ping-Host {
  PROCESS {
    $errorActionPreference="SilentlyContinue"
    $wmi = get-wmiobject -query "SELECT * FROM Win32_PingStatus WHERE Address = '$_'"
    if ($wmi.StatusCode -eq 0) {
      $_
    }
  }
}


ForEach ($item in $wks){
  $ping = new-object System.Net.NetworkInformation.Ping
  $errorActionPreference="SilentlyContinue"
  $reply = $ping.send($item,1000)
  if($reply.status -eq "Success"){
    write-host "$item is online"
    $result = Get-service -computername $item $service | select Name,Status,MachineName
    $one = Get-service -computername $item $service | Stop-Service -PassThru 
    $two = Get-service -computername $item $service | Set-Service -StartupType disabled 
  }
  else{
    write-host "$item is OFFLINE"
  }

} 
