# Name: Report-UNCPath-LastModified.ps1
# Description: Checks the last modified date of a UNC path in UTC Time and logs status to database.
# Changelog:
#   2014.04.22: Created



# Step 1 - Retrieve array of UNC paths to check from sql table (active directory)
# Step 2 - Iterate through list and gather last modified date
# Step 2.1 - VAlidate folder actually exists on server, if not then raise error
# Step 3 - Log date into SQL database
# Step 4 - Rinse and repeat
