$mi = $MyInvocation.MyCommand.Definition
$pwd = (split-path -parent $mi)

. "$pwd\content\Get-CMVariables.ps1"
. "$pwd\content\Invoke-SNOWMediaHook.ps1"
. "$pwd\content\Set-CMVariables.ps1"
. "$pwd\content\Invoke-Prereqcheck.ps1"

[long]$start = (get-date).Ticks
[long]$future = ((get-date).AddSeconds(600).Ticks)
[long]$s = $start - $start
[long]$f = $future - $start
[long]$c = (((get-date).Ticks) - $start)

function write-executionstatus{
    param(
        [Parameter(Mandatory=$true,Position=0)][string]$message,
        [Parameter(Mandatory=$false)][io.directoryinfo]$log="$env:temp\SMSTSLOG\SNOWMediahook.log"
        )
    [bool]$err=$false
    
    if(-not ([io.directoryinfo]$log.parent).exists){
        mkdir ($log.parent).fullname | out-null
    }   

    $version = [string]((Get-module "eus-modules") | select -first 1 version).Version
    
    $now = (get-date).Ticks
    $end = $f
    $currentpercentage = (($now - $start) / $end) * 100
    if($currentpercentage -gt 99){$currentpercentage=99}
    $params = @{
        "activity"="SNOW Mediahook $version"
        "status"=$message
        "PercentComplete"=$currentpercentage
    }

    # put error checking first, then update progress status
    echo "$([DateTime]::Now) : $error : $message" 5>&1 >> $log
    if($message.tolower().contains("error")){$err=$true}
    if($message.tolower().contains("fail")){$err=$true}
    if($message.tolower().contains("unable")){$err=$true}
    if($message.tolower().contains("invalid")){$err=$true}

    if($err){
        write-error "$message" -erroraction "Continue";
        write-progress @params
        start-sleep 60;
        throw;
    }
    else{
        write-progress @params    
    }
}

function start-cmosd {

    write-executionstatus "Checking network connectivity"
    [uri]$w="https://eucwebserv/eucwebserv/"
    
    $i=1
    $maxretry=60
    do{write-executionstatus "Waiting for an ip address, attempt $i / $maxretry";$i++;start-sleep -Milliseconds 750;}
    while((-not([Net.NetworkInformation.NetworkInterface]::GetIsNetworkAvailable()))-and($i -lt $maxretry))
    if(-not([Net.NetworkInformation.NetworkInterface]::GetIsNetworkAvailable())){
        write-executionstatus "Failed to connect to network" -erroraction Stop;
        start-sleep 600;
    }

    # get hostname
    try{
        # also retry DNS resolution
        $boolResolvedName = $false
        for($i=1;$i -le $maxretry;$i++){
            write-executionstatus "Resolving $($w.host) from DNS, attempt $i of $maxretry"
            try{
                [System.Net.Dns]::GetHostAddresses($w.host).IPAddressToString | out-null
                $boolResolvedName = $true
            }
            catch{}
            if($boolResolvedName){break;}
            if($i -eq $maxretry){
                write-executionstatus "Failed to resolve '$($w.host)' after $maxretry attempts"
            }
            start-sleep -Milliseconds 750
        }

        $i = [System.Net.Dns]::GetHostAddresses($w.host).IPAddressToString
        $h = [System.Net.Dns]::GetHostbyAddress($i).hostname
        [uri]$u = "$($w.Scheme)://$h$($w.PathAndQuery)"
        [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
        $one = [Net.WebRequest]::Create($u).GetResponseAsync()
        while(-not ($one.IsCompleted)){
            write-executionstatus "Checking network connectivity to $u"
            Start-Sleep -Milliseconds 750;
        }
        $r = (new-object net.webclient).downloadstring($w)
    }
    catch [System.Net.Sockets.SocketException]{write-executionstatus "Unable to resolve the the servername '$($w.host)' from DNS." }
    catch [System.Net.WebException]{write-executionstatus "Unable to contact the webservice $w" }
    catch [Exception]{write-executionstatus "$($_.Exception.GetType()) : $($_.Exception.Message)"}
    
    # write-executionstatus "OK so we've established that the computer is online, can ping the webservices server, and can connect over http."
    # write-executionstatus "Let's get on with it now."
    write-executionstatus "Opening Task Sequence Environment"
    $ts = (get-cmvariables)
    
    write-executionstatus "Checking if the ts variables are valid"
    if(($ts["pUserPassword"].length -eq 0) -or ($ts["pUserDomain"].length -eq 0) -or ($ts["pUserName"].length -eq 0)){
        write-executionstatus "Unable to read variables. Did we inject the variables.dat into this bootimage?" 
    }

    # check if these are valid bsae64 string

    write-executionstatus "Importing bootimage variables into the running environment"
    try{
        $n=[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($ts["pUserName"]))
        $sec=[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($ts["pUserPassword"]))
        $d=[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($ts["pUserDomain"]))
    }
    catch [System.FormatException]{write-executionstatus "Unable to de-obfuscate task sequence variables" }
    catch [Exception]{write-executionstatus "$($_.Exception.GetType()) : $($_.Exception.Message)"}

    write-executionstatus "Creating authenticated connection using imported variables"
    #$credential = New-Object System.Management.Automation.PSCredential ("$d\$n",(ConvertTo-SecureString $sec -AsPlainText -Force))
        
    # Using pscredential initialize web service calls.
    Invoke-SNOWMediaHook -uri $u -credential $credential

}

function New-CustomWebServiceProxy{
    [CmdletBinding()]
    param( 
        [Parameter(Mandatory=$true)][uri]$uri,
        [Parameter(Mandatory=$false)][System.Management.Automation.PSCredential]$credential,
        [Parameter(Mandatory=$false)][int]$maxretry=1860,
        [Parameter(Mandatory=$false)][int]$waittimeseconds=1,
        [Parameter(Mandatory=$false)][int]$consolenotificationtime=600,
        [Parameter(Mandatory=$false)][string]$allsystems="SMS00001",
        [Parameter(Mandatory=$false)][string]$build="D0000957"
    )
    
    $params = @{
        "uri"=$uri;
    }
    
    # If credential is defined then lets add it to the parameters
    if(-not([bool]($credential -eq $null))){
        $params.Add("credential",$credential)
    }

    return (new-webserviceproxy @params)
}