function Get-CMVariables{   
    $tsenv = @{}
    #write-executionstatus "Check if the environment is active"
    try{
        $TSEnvironment = New-Object -COMObject Microsoft.SMS.TSEnvironment
        $TSEnvironment.GetVariables() | % {$tsenv["$_"] = $($TSEnvironment.Value($_));}
    }
    catch {
        $tsenv["Exception"]=$_.GetType()
        $tsenv["ExceptionText"]=$_.Exception
        # $tsenv["pUserDomain"]="TkVTVExFVzJL"        # FAIL - should fail as it does not have suffix at the end
        $tsenv["pUserDomain"]="bmVzdGxlLncyaw=="        # SUCCESS - Should succeed as it has the dns suffix
        $tsenv["pUserName"]="R0xCRVVDV2ViU2Vydg=="      # UTF8 (Unicode encoding will fail here)
        $tsenv["pUserPassword"]="TmVzdGxlMTIz"  # UTF8 (Unicode encoding will fail here)
        
    }
    return $tsenv
}

# Example to convert a text string to a b64 encoded string
# $string="GLBEUCWebServ";$encoded=[System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($string));write-host "$string <> $encoded"
# Example to decode a base64 string
# $string="R0xCRVVDV2ViU2Vydg==";$decoded=[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($string));write-host "$string <> $decoded"
