function Set-CMVariables{
    [CmdletBinding()]
    param( 
        [Parameter(Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [String]$json
    )

    $result = ($json | convertfrom-json)
    $tso = New-Object -COMObject Microsoft.SMS.TSEnvironment

    foreach($item in $result.psobject.properties){
        $key = $item.Name
        $value = $item.Value

        # replace the 'C_SMSTS' prefix with '_SMSTS' 
        $key = $key.Replace("C_SMSTS","_SMSTS")
        #wr "Item is $key while value is $value"
        
        if($key.Contains("_SMS") -eq $true){
            $cmd = "x:\deploy\tsenv2.exe set $key=`"s$value`" "
            # wr "Setting the variable $key using the 1e tool $cmd"
            # iex $cmd
        }
        else{
            # wr "Setting the variable $key with the comobject Microsoft.SMS.TSEnvironment"
            $tso.Value($key)="$value"
        }
        $key = $null
        $value = $null    
    }
    return 0
}