function Invoke-SNOWMediaHook{
    [CmdletBinding()]
    param( 
        [Parameter(Mandatory=$true)][uri]$uri,
        [Parameter(Mandatory=$false)][System.Management.Automation.PSCredential]$credential,
        [Parameter(Mandatory=$false)][int]$maxretry=1860,
        [Parameter(Mandatory=$false)][int]$waittimeseconds=1,
        [Parameter(Mandatory=$false)][int]$consolenotificationtime=600,
        [Parameter(Mandatory=$false)][string]$allsystems="SMS00001"
    )

    write-executionstatus "Contacting eucwebservice redirector service"

    # write-executionstatus "Disable SSL Checking"
    # Ideally we should keep the environmental configuration as limited as possible -- maybe detecting from a trusted CA would be a better approach
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
    [string]$url = $($uri.AbsoluteUri);$uri = $null
    [string]$uri = $url
    $uri += "/redirector.asmx"
    $uri = $uri.Replace("//redirector.asmx","/redirector.asmx")
    
    $red=(New-CustomWebServiceProxy -uri "$uri" -credential $credential)
    write-executionstatus "Connection established to $uri"

    write-executionstatus "Gather system information from WMI"
    $csp = (get-wmiobject -class Win32_ComputerSystemProduct)
    $bios = (get-wmiobject -class Win32_BIOS)
    $cs = (get-wmiobject -class Win32_ComputerSystem)
    $nac = (get-wmiobject -class win32_NetworkAdapterConfiguration -filter 'ipenabled = "true"' | select -first 1)
    $na = (get-wmiobject -class Win32_NetworkAdapter -filter "ServiceName = '$($nac.ServiceName)' ")
    $se = (get-wmiobject -class Win32_SystemEnclosure)

    write-executionstatus "Found UUID of $($csp.uuid)"

    write-executionstatus "Retrieving webservice url from redirector $url using the uuid $($csp.uuid)"

    $res = $red.getWebServiceRedirectorURL($csp.uuid)
    switch($res.Status){
        "Success" {$webserviceuri = $($res.result);write-executionstatus "Using the webservice $webserviceuri";}
        "Failure" {write-executionstatus "Webservice reported a failure" }
        "NoRecordForUUID" { write-executionstatus "Unable to find the uuid $($csp.uuid) in the MDT database. does this computer exist in Asset Manager?"  }
        "PropertyNotSet" { write-executionstatus "Unable to find a valid webservice url for this copmuter. Maybe check roles?"  }
        "Exception" { write-executionstatus "$res.ErrorMessage"  }
    }
       
    write-executionstatus "Retrieved the eucwebservice url $webserviceuri for the computer record $($csp.uuid)"
    
    try{
        $m = ("$webserviceuri/mdt.asmx").Replace("//mdt.asmx","/mdt.asmx")
        $a = ("$webserviceuri/ad.asmx").Replace("//ad.asmx","/ad.asmx")
        $s = ("$webserviceuri/sccm.asmx").Replace("//sccm.asmx","/sccm.asmx")

        write-executionstatus "Establishing connection with $m"
        $mdt=(New-CustomWebServiceProxy -uri $m -credential $credential)
        
        write-executionstatus "Establishing connection with $a"
        $ad=(New-CustomWebServiceProxy -uri $a -credential $credential )
        
        write-executionstatus "Establishing connection with $s"
        $sccm=(New-CustomWebServiceProxy -uri $s -credential $credential )
    }
    catch{
        switch($_.Exception.GetType().FullName){
            "System.UriFormatException"{write-executionstatus "Unable to find a valid Webservice URL assigned to the configured MDT role." }
            default{write-executionstatus "Exception occured: $($_.Exception.GetType().FullName) :: $($_.Exception.Message)" }
        }
    }

    $ip = $($nac.ipaddress[0])
    write-executionstatus "Contact webservice uri"
    write-executionstatus "My ipaddress is $ip"
    write-executionstatus "Call GetMarket Server and set local variables"
    
    write-executionstatus "Running webservice method GetMarketServer with parameter $ip"
    $gms = $sccm.GetMarketServer($ip)
    write-executionstatus "Running webservice method GetPrimaryServer with parameter $ip"
    $gps = $sccm.GetPrimaryServer($ip)

    if($gms.status -ne "Success"){write-executionstatus "$($gms.Status) : $($gms.ErrorMessage)" }
    if($gpr.status -ne "Success"){write-executionstatus "$($gms.Status) : $($gms.ErrorMessage)" }

    write-executionstatus "GetMarketServer returned the $($gms.ServerName) and $($gms.SiteCode)"
    write-executionstatus "Call GetMDTProperties variable and set items"
    write-executionstatus "Running webservice method GetProperties_Priority with parameters $($csp.uuid),$($gms.SiteCode)"

    try{
        $gmp = $mdt.GetProperties_Priority($csp.uuid,$gms.SiteCode)
    }
    catch{
        write-executionstatus "Failed to run GetProperties_Priority with parameters $($csp.uuid),$($gms.SiteCode)"
    }
    
    if($gmp.status -ne "Success"){
        write-executionstatus "$($gmp.Status) : $($gmp.ErrorMessage)" 
    }

    write-executionstatus "Using results of get mdt properties go ahead and set TS variables"

    if($gmp.pxebootresult -eq $null){
        write-executionstatus "Unable to locate PXEBoot variables, is this subnet and site code defined in SCCM ?"
        write-executionstatus "$($gmp.Status) : $($gmp.ErrorMessage)" 
    }

    write-executionstatus "Convert the returned webservice type to smsts object"
    try{

        if($gmp.SettingResult.eucsnowbootimage.length -eq 0){
            write-executionstatus "Failed: EUCSnowBootImage is not defined. Maybe check the MDT roles?"
        }

        $build = (($gmp.SettingResult.eucsnowbootimage).Split("#")[1])
        if($build.length -eq 0){
            write-executionstatus "Failed: Collectionid is not defined. Maybe check the MDT roles?"
        }
        

        write-executionstatus "Set any custom task sequence variables"
        $gmp.pxebootresult | convertto-json | Set-CMVariables | out-null
        $gmp.SettingResult | convertto-json | Set-CMVariables | out-null
        $tso = New-Object -COMObject Microsoft.SMS.TSEnvironment
        $tso.Value("OSDComputerName")=$gmp.SettingResult.computername
        $tso.Value("ServerName")=$gms.ServerName
        $tso.Value("SMSTSMP")=$gms.ServerName
        $tso.Value("MacAddress")=$na.MacAddress
        $tso.Value("SerialNumber")=$se.SerialNumber
        $tso.Value("UUID")=$csp.uuid
        $tso.Value("OSDSiteCode")=$gms.SiteCode
        write-executionstatus "Was able to successfully set all custom task sequence variables"
    }
    catch [System.Runtime.InteropServices.COMException]{
        write-executionstatus "Was not able to access task sequence environment."
    }
    catch {
        write-executionstatus "Error: $($_.Exception.getType().FullName)"
        throw
    }

    write-executionstatus "Re-read all environment variables"
    $tse = (Get-CMVariables)
    $mac=$na.MacAddress
    $uuid=$csp.uuid
    $name=$gmp.SettingResult.computername
    
    write-executionstatus "Purging all computer records from each primary in hierarchy."
    @($sccm.GetProviderServer().Servers) | %{
        write-executionstatus "Cleaning up any previous computer records from server $_";
        do{$sccm.removecomputerrecord($_,$mac,$uuid,$name) | out-null;start-sleep 30;}
        while($sccm.getcomputerrecord($_,$mac,$uuid,$name).Status -eq "Success")
    }
    
    write-executionstatus "Creating a new record";
    $resourceid = $sccm.newcomputerrecord($gps.servername,$mac,$uuid,$name,$true)
    $resourceid = $resourceid.resourceid

    if([int]$resourceid -eq -1){
        write-executionstatus "Error provisioning ConfigMgr record"
    }

    do{write-executionstatus "Waiting for ConfigMgr to provision record $resourceid";start-sleep 30;}
    while($sccm.getcomputerrecord($gps.servername,$mac,$uuid,$name).Status -ne "Success")

    do{write-executionstatus "Waiting for ConfigMgr to add computer $resourceid to collection $allsystems";start-sleep 30;}
    while($sccm.GetCollectionMembership($gps.servername,$resourceid,$allsystems).Status -ne "Success")

    write-executionstatus "Adding computer $resourceid to collection $build"
    $sccm.NewCollectionMembership($gps.servername,$resourceid,$build) | out-null

    do{write-executionstatus "Waiting for ConfigMgr to add computer $resourceid to collection $build";start-sleep 30;}
    while($sccm.GetCollectionMembership($gps.servername,$resourceid,$build).Status -ne "Success")

    do{write-executionstatus "Waiting for PXE Advertisement to become availible to computer";start-sleep 30;}
    while($sccm.GetPXEAdvertisement($gps.servername,$resourceid).Status -ne "Success")

    do{write-executionstatus "Waiting for policy to become availible before starting task sequence";start-sleep 30;}
    while($sccm.GetTaskSequencePolicy($gps.servername,$resourceid,$build).Status -ne "Success")

    write-executionstatus "Script completed successfully. Starting task sequence."
    [System.Threading.Thread]::Sleep(30000)
}
