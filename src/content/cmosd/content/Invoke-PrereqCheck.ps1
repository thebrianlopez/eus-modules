function Invoke-PreReqCheck{
    <#
    .SYNOPSIS 
    Performs hardware, infra, and package availability checks before starting the SNOW Build process.

    .DESCRIPTION
    Version 1.0

    Version History
        2015-06-05  Initial creation

    This function can take the list of computers and check pre-reqs for SNOW imaging.

    .INPUTS
    Accepts input from the pipeline as a [string] for the ComputerName property

    .OUTPUTS
    String. 

    .EXAMPLE
    C:\PS> Invoke-PreReqCheck us16-0g32w33 
  
    .EXAMPLE
    C:\PS> Invoke-PreReqCheck us16-0g32w33,us16-0iy373c

    .EXAMPLE
    C:\PS> Invoke-PreReqCheck us16-0g32w33,us16-0iy373c

    .LINK
    http://www.nestle.com

    #>   
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][String[]]$ComputerName,
        [parameter(Mandatory=$true)][string]$uri,
        [parameter(Mandatory=$true)][string]$make,
        [parameter(Mandatory=$true)][string]$model,
        [parameter(Mandatory=$true)][string]$uuid,
        [parameter(Mandatory=$true)][string]$prefix,
        [parameter(Mandatory=$true)][string]$sitecode,
        [parameter(Mandatory=$true)][string]$ipaddress,
        [parameter(Mandatory=$true)][string]$userdnsdomain,
        [parameter(Mandatory=$true)][string]$diskdrivesize,
        [Parameter(Mandatory=$false,HelpMessage="Credentials to use" )][System.Management.Automation.PSCredential] $credential = $null    # Not sure if this is really needed as we dont care who clearxe flags
        )

    BEGIN{
        write-prereqstatus "Starting Function"
        # Ideally we should keep the environmental configuration as limited as possible -- maybe detecting from a trusted CA would be a better approach
        [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}

        try{
            $mdt=(New-WebServiceProxy -uri "$uri/mdt.asmx" -errorAction Stop)
            $ad=(New-WebServiceProxy -uri "$uri/ad.asmx" -errorAction Stop)
            $sccm=(New-WebServiceProxy -uri "$uri/sccm.asmx" -errorAction Stop)
        }
        catch{
            write-prereqstatus $_.Exception -errorAction Stop
        }
    }
    PROCESS{

        $returnstatus = @()

        foreach($computer in $ComputerName){

            [bool]$MakeModelSupport = $false
            [bool]$PlatformCode = $false
            [bool]$MDTRoles = $false
            [bool]$OUAssignment = $false
            [bool]$Packages = $false
            [bool]$DiskSize = $false
            [bool]$prereqcheck = $false

            try{

                # Make model support
                $res = $mdt.CheckMakeAndModel($make,$model)
                switch($res.Status){
                    "RecordsForKeyNotFound" {write-prereqstatus "CheckMakeAndModel found no results."}
                    "Success" {write-prereqstatus "CheckMakeAndModel Succeeded";$MakeModelSupport=$true}
                    default {write-prereqstatus "[warn] CheckMakeAndModel did not succeed."}
                }
                $res = $null
                
                # PlatformCode
                $res = $mdt.GetComputerSettingsProperty($uuid)
                switch($res.Status){
                    "RecordsForKeyNotFound" {write-prereqstatus "GetComputerSettingsProperty found no results."}
                    "Success" {write-prereqstatus "GetComputerSettingsProperty Succeeded";$pltformcd = $res.ComputerSettingResult.EUCSNOWPlatformCode}
                    default {write-prereqstatus "[warn] Retrieving PlatformCode did not succeed.";}
                }

                if($res.Status -eq "Success"){
                    switch($pltformcd){
                        "S5"{write-prereqstatus "GetComputerSettingsProperty found EUCSNOWPlatformCode set to S5";$PlatformCode=$true}
                        default {write-prereqstatus "[warn] EUCSNOWPlatformCode is set to $pltformcd";}
                    }
                }                
                $res = $null
                
                # MDT Roles
                $res = $mdt.CheckRoles($pltformcd,$uuid)
                switch($res.Status){
                    "RecordsForKeyNotFound" {write-prereqstatus "CheckRoles found no results."}
                    "Success" {write-prereqstatus "CheckRoles Succeeded";$MDTRoles=$true}
                    default {write-prereqstatus "[warn] CheckRoles did not succeed."}
                }
                $res = $null

                # Get market server to retrieve the sitecode
                $res = $sccm.GetMarketServer($ipaddress)
                switch($res.Status){
                    "Success" {write-prereqstatus "GetMarketServer Succeeded";}
                    default {write-prereqstatus "[warn] GetMarketServer did not succeed."}
                }
                $stcd = $res.SiteCode
                $res = $null

                # OUAssignment
                $res = $mdt.GetProperties_Priority($uuid,$stcd)
                switch($res.Status){
                    "Success" {write-prereqstatus "GetProperties_Priority Succeeded";}
                    default {write-prereqstatus "[warn] GetProperties_Priority did not succeed."}
                }

                # Get MachineObjectOU from MDT
                $mdtou = $res.SettingResult.MachineObjectOU
                $res = $ad.CheckAndMoveComputerOU($ipaddress,$computer,$mdtou)
                switch($res.Status){
                    "Success" {write-prereqstatus "CheckAndMoveComputerOU Succeeded $($res.ErrorMessage)";$OUAssignment=$true}
                    default {write-prereqstatus "[warn] CheckAndMoveComputerOU did not succeed."}
                }

                # Get all mdt packages
                $res = $mdt.GetMDTPackages($uuid,$make,$model,$false)
                switch($res.Status){
                    "Success" {write-prereqstatus "GetMDTPackages Succeeded $($res.ErrorMessage)"}
                    default {write-prereqstatus "[warn] GetMDTPackages did not succeed."}
                }
                $mdtpackages = ($res.PackagesResult | select PackageID)

                $res = $ad.GetADSiteManual($ipaddress)
                switch($res.Status){
                    "Success" {write-prereqstatus "GetADSiteManual Succeeded $($res.ErrorMessage)"}
                    default {write-prereqstatus "[warn] GetADSiteManual did not succeed."}
                }

                # Get ADSite in order to get replication status
                $adsite = $res.Result

                # Get Package replication status
                $res = $sccm.GetPackageReplicationStatus($mdtpackages.packageid,$adsite)
                #$res
                
                switch($res.Status){
                    "Success" {write-prereqstatus "GetPackageReplicationStatus Succeeded $($res.ErrorMessage)";$Packages=$true}
                    "Failure" {write-prereqstatus "[warn] GetPackageReplicationStatus did not succeed."}
                }

                write-prereqstatus "Disksize is $($diskdrivesize / 1GB) GB"
                
                switch(($diskdrivesize / 1GB) -gt 1000){
                    $false{write-prereqstatus "Disksize is under 1TB";$DiskSize = $true}
                    $true {write-prereqstatus "[warn] diskdrivesize did not succeed."}
                }
                
                # Get overall result and provide back to pipeline
                if($MakeModelSupport -and $PlatformCode -and $MDTRoles -and $OUAssignment -and $Packages -and $DiskSize){
                    write-prereqstatus "Pre-req check completed successfully."
                    $prereqcheck = $true
                }
                else{
                    write-prereqstatus "[warn] Pre-req check did not succeed."
                }

            }
            catch{write-prereqstatus "$($_.Exception)";throw;}
           
            $props = @{
                "PREREQ-HWSUPPORT"=$MakeModelSupport;
                "PREREQ-S5CODE"=$PlatformCode;
                "PREREQ-S5ROLES"=$MDTRoles;
                "PREREQ-CORRECTOU"=$OUAssignment;
                "PREREQ-PKGSONDP"=$Packages;
                "PREREQ-DISKSIZE"=$DiskSize;
                "ComputerName"=$computer;
                "Result"=$prereqcheck;
            }

            $row = New-Object -TypeName PSOBject -Property $props
            $returnstatus += $row
            write-prereqstatus "Completed Function"
        }
        
    }

    END{
        
        if([bool]$returnstatus.Result){
            Write-Output  $returnstatus
        }
        else{
            write-error "Prereqcheck failed" -ea Stop
        }
    }
}

function Test-PreReqCheck{
    $params = @{
        "uri"="https://eucwebserv.nestle.w2k/eucwebserv"
        "computername"="ch82-svmcm01"
        "make"="VMWare, Inc."
        "model"="VMWare Virtual Platform"
        "uuid"="968E3242-7113-08F5-BCAD-248D60672A94"
        "prefix"="S5"
        "sitecode"="D01"
        "ipaddress"="10.54.255.106"
        "userdnsdomain"="nestle.w2k"
        "diskdrivesize"="500GB"
    }
    Invoke-PreReqCheck @params
}


function write-prereqstatus{
    param(
        [Parameter(Mandatory=$true,Position=0)][string]$message
        )
    write-executionstatus -log "$env:temp\SMSTSLOG\ZTISNOW5-PrereqCheck.log" $message
}
