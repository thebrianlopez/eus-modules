# Name: Nestle-Modules
# Author: Brian Lopez (brian.lopez@purina.nestle.com)
# Description: Performs helper options used for managing the nestle.cloud environment
# Added pending reboot script
# $thispath=Get-Location
$global:scriptpath = $MyInvocation.MyCommand.Definition
$global:scriptname = $MyInvocation.MyCommand.Name
$global:thismoddir = (split-path -parent $global:scriptpath)

# ServerManager
# Get-Module -ListAvailable | where {$_.Name -eq "ServerManager"} | Import-Module # This works on Windows Server 2008 R2

# Active Directory
#Get-Module -ListAvailable | where {$_.Name -eq "ActiveDirectory"} | Import-Module # This works on Windows Server 2008 R2

# ConfigMgr
# $mp="C:\Program Files (x86)\Microsoft Configuration Manager\AdminConsole\bin\ConfigurationManager.psd1";if(test-path -path "$mp") {Import-Module "$mp"}

# ConfigMgr
# $mp="F:\SMS\AdminConsole\bin\ConfigurationManager.psd1";if(test-path -path "$mp") {Import-Module "$mp"}

# ConfigMgr2012
# $mp="F:\SMS\bin\ConfigurationManager.psd1";if(test-path -path "$mp") {Import-Module "$mp"}

Import-Module "$global:thismoddir\content\CreateDatFile\CreateDatFile.psm1"
Import-Module "$global:thismoddir\content\cmgopher\cmgopher.psm1"
if($psversiontable.PSVersion.Major -gt 2){Import-Module "$global:thismoddir\content\cmosd\cmosd.psm1"}
Import-Module "$global:thismoddir\content\OneClickDomain\OneClickDomain.psm1"
Import-Module "$global:thismoddir\content\OS\ClearPXEFlag\ClearPXEFlag.psm1"
Import-module "$global:thismoddir\content\cPackageManagement\cPackageManagement.psm1"
. "$global:thismoddir\content\OS\Get-PendingReboot.ps1"
. "$global:thismoddir\content\Event\Push-WinEvents.ps1"
. "$global:thismoddir\content\Azure\Remove-BlobsFromAllContainers.ps1"
. "$global:thismoddir\content\DTU-SCCM\CreateIISWebsite.ps1"
. "$global:thismoddir\content\Data\Invoke-SQLQuery.ps1"
. "$global:thismoddir\content\IO\ZipFile.ps1"
. "$global:thismoddir\content\Custom\Aliases.ps1"



function Install-cPatchDirectory
{
    param( 
        [Parameter(Mandatory=$true)][string]$folder
    ) 
    $results = $false
    write-verbose "Installing hotfixes from directory $folder"
    $hotfixes = (get-childitem $folder -include *.msu -recurse)
    $archarray = @{"AMD64"="$env:systemroot\system32";"x86"="$env:systemroot\syswow64";}
    $wusa = "$($archarray.$env:PROCESSOR_ARCHITECTURE)\wusa.exe"
    if(test-path $wusa){
        write-verbose "ag"
    }
    else{
        throw;
    }
    foreach ($hotfix in $hotfixes) 
    {
        $cmd="`"$wusa`""
        $arg += " "
        $arg += "$hotfix"
        $arg += " "
        $arg += "/quiet"
        $arg += " "
        $arg += "/norestart"
        $arg += " "
        $StartInfo = new-object System.Diagnostics.ProcessStartInfo
        $StartInfo.FileName = "$cmd"
        $StartInfo.Arguments = "$arg"
        $StartInfo.UseShellExecute = $false
        $StartInfo.WorkingDirectory = $thisdir
        write-verbose "Running: $cmd$arg"
        [int]$exitCode = [Diagnostics.Process]::Start($StartInfo).WaitForExit();$arg = $null 

        if($exitCode -eq 0){
            write-verbose "command complete successfully."
            $results = $true
        }
        else{
            write-error "command existed with code $($_.Exception.Message.toString())" -ErrorAction Stop
            throw
        }
    }
    write-verbose "hotfix installation complete"
    return $results
}

function Enable-cOptionalFeature
{
  param(
    [Parameter(Mandatory=$true)][array]$feature
    )
  write-verbose "Enabling windows optional feature."
  foreach($item in $feature)
  {
    if((Get-WindowsOptionalFeature -online -featurename $item).State -eq "Enabled"){
      write-verbose "Optional feature $item is already enabled."
    }
    else {
      write-verbose "Installing $item" 
      Enabled-WindowsOptionalFeature -online -featurename $item -all -norestart  
    }
  }
  write-verbose "Function completed with exit code $lasterrorcode"
}

function Enable-cWindowsFeature
{
  param(
    [Parameter(Mandatory=$true)][array]$feature
    )

  write-verbose "Enabling Windows Feature"
  if(Get-Command Get-WindowsFeature -erroraction silentlycontinue)
  {
    foreach($item in $feature)
    {
      if((Get-WindowsFeature -name $item).Installed){
        write-verbose "Windows feature $item is already enabled."
      }
      else {
        if(Get-Command Install-WindowsFeature -erroraction silentlycontinue){ # Server 2012
          write-verbose "Using the item Install-Windows feature to install $item" 
          Install-WindowsFeature -name $item -restart -includeallsubfeature  
        }
        else { # Server 2008
          write-verbose "Using the item Add-Windows feature to install $item" 
          Add-WindowsFeature -name $item -restart -includeallsubfeature  
        }
      }
    }   
  }
  else {
    write-verbose "Feature not found"
  }
  write-verbose "Function completed with exit code $lasterrorcode"
}


function Send-HTTPPost {
  param (
    [string]$url,
    [string]$payload
    )
  
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($payload)
  $web = [System.Net.WebRequest]::Create($url)
  $web.Method = "GET"
  $web.ContentLength = $bytes.Length
  $web.ContentType = "application/json"
  $stream = $web.GetRequestStream()
  $stream.Write($bytes,0,$bytes.Length)
  $stream.close()

  $reader = New-Object System.IO.Streamreader -ArgumentList $web.GetResponse().GetResponseStream()
  $reader.ReadToEnd()
  $reader.Close()

}

function New-VSORepo
{
  $payload='{"name": "brianlopez-2142341","project": {"id": "6ce954b1-ce1f-45d1-b94d-e6bf2464ba2c"}}'
  $account="gtseus"
  $repo="euc-snow-webservice"
  $url="https://$account.visualstudio.com/defaultcollection/_apis/git/repositories/"
  $url="$url" + "$repo"
  $url="$url" + "?api-version=1.0-preview"
  Send-HTTPPost -url $url -payload $payload
}


function Expand-File {

<#
.SYNOPSIS
   Expand-File is a function which extracts the contents of a zip file.

.DESCRIPTION
   Expand-File is a function which extracts the contents of a zip file specified via the -File parameter to the
location specified via the -Destination parameter. This function first checks to see if the .NET Framework 4.5
is installed and uses it for the unzipping process, otherwise COM is used.

.PARAMETER File
    The complete path and name of the zip file in this format: C:\zipfiles\myzipfile.zip 
 
.PARAMETER Destination
    The destination folder to extract the contents of the zip file to. If a path is no specified, the current path
is used.

.PARAMETER ForceCOM
    Switch parameter to force the use of COM for the extraction even if the .NET Framework 4.5 is present.

.EXAMPLE
   Expand-File -File C:\zipfiles\AdventureWorks2012_Database.zip -Destination C:\databases\

.EXAMPLE
   Expand-File -File C:\zipfiles\AdventureWorks2012_Database.zip -Destination C:\databases\ -ForceCOM

.EXAMPLE
   'C:\zipfiles\AdventureWorks2012_Database.zip' | Expand-File

.EXAMPLE
    Get-ChildItem -Path C:\zipfiles | ForEach-Object {$_.fullname | Expand-File -Destination C:\databases}

.INPUTS
   String

.OUTPUTS
   None

.NOTES
   Author:  Mike F Robbins
   Website: http://mikefrobbins.com
   Twitter: @mikefrobbins

   #>

   [CmdletBinding()]
   param (
    [Parameter(Mandatory=$true, 
     ValueFromPipeline=$true)]
    [ValidateScript({
      If ((Test-Path -Path $_ -PathType Leaf) -and ($_ -like "*.zip")) {
        $true
      }
      else {
        Throw "$_ is not a valid zip file. Enter in 'c:\folder\file.zip' format"
      }
      })]
    [string]$File,

    [ValidateNotNullOrEmpty()]
    [ValidateScript({
      If (Test-Path -Path $_ -PathType Container) {
        $true
      }
      else {
        Throw "$_ is not a valid destination folder. Enter in 'c:\destination' format"
      }
      })]
    [string]$Destination = (Get-Location).Path,

    [switch]$ForceCOM,
    [switch]$force=$false
    )


   If (-not $ForceCOM -and ($PSVersionTable.PSVersion.Major -ge 3) -and
     ((Get-ItemProperty -Path "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Full" -ErrorAction SilentlyContinue).Version -like "4.5*" -or
       (Get-ItemProperty -Path "HKLM:\Software\Microsoft\NET Framework Setup\NDP\v4\Client" -ErrorAction SilentlyContinue).Version -like "4.5*")) {

    write-verbose -Message "Attempting to Unzip $File to location $Destination using .NET 4.5"

    try {
      [System.Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
      [System.IO.Compression.ZipFile]::ExtractToDirectory("$File", "$Destination")
    }
    catch {
      Write-Warning -Message "Unexpected Error. Error details: $_.Exception.Message"
    }


  }
  else {

    write-verbose -Message "Attempting to Unzip $File to location $Destination using COM"

    try {
      $shell = New-Object -ComObject Shell.Application
      $shell.Namespace($destination).copyhere(($shell.NameSpace($file)).items(),16)
    }
    catch {
      Write-Warning -Message "Unexpected Error. Error details: $_.Exception.Message"
    }

  }

}

function Export-Archive{
  # [CmdletBindings]
  param(
    [string] $packageName,
    [string] $url,
    [string] $unzipLocation,
    [string] $extension="zip",
    [switch] $force=$false
    )

  $outfile="$packageName.$extension"
  write-verbose "Extracting $url to $outfile"
  # Using the system temp foldet
  $global:systemtemp = "$([Environment]::GetEnvironmentVariable("TMP","MACHINE"))"

  # Downloading the source
  $client = new-object System.Net.WebClient
  $client.DownloadFile("$url","$global:systemtemp\$outfile")

  # using com
  # $shell_app=new-object -com shell.application
  # $zip_file = $shell_app.namespace("$global:systemtemp\$outfile")
  # write-verbose "Extracting $global:systemtemp\$outfile to $unzipLocation : test good: $(Test-Path -path $unzipLocation )" 
  # $destination = $shell_app.namespace("$unzipLocation")
  # $destination.Copyhere($zip_file.items(),16)
  if($force){
    write-verbose "Checking if the directoy $unzipLocation exists ..."
    if(test-path -path $unzipLocation){
      write-verbose "Path already exists"
    }
    else{
      write-verbose "Making the dircetory $unzipLocation"  
      mkdir $unzipLocation
    }
    
  }
  # using Expand-File.ps1
  $params = @{
    "force"=$force;
    "file"="$global:systemtemp\$outfile";
    "destination"="$unzipLocation";
    "forcecom"=$true;
  }
  Expand-File @params

}

function Rename-Computer
{
  param ( 
    [Parameter(Mandatory=$true)][string]$oldname,
    [Parameter(Mandatory=$true)][string]$newname,
    [Parameter(Mandatory=$true)][string]$domainname,
    [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$credential
    )

  process
  {
    try
    {
      # $credential = Get-Credential
      $computer = Get-WmiObject -Class Win32_ComputerSystem -Authentication 6
      $result = $computer.Rename("$newname",$credential.GetNetworkCredential().Password,$credential.Username)
      
      switch($result.ReturnValue)
      {       
        0 { write-verbose "Success" }
        5 
        {
          write-verbose "You need administrative rights to execute this cmdlet" 
          exit
        }
        default 
        {
          write-verbose "Error - return value of " $result.ReturnValue
          exit
        }
      }
    }
    catch
    {
      write-verbose "Unexpected Error. Error details: $_.Exception.Message"
    }
  }
}

function Start-filedownload{
    param(
        [parameter(Position=0,Mandatory=$true)][string]$url,
        [parameter(Mandatory=$false)][string]$proxyserver=$env:http_proxy,
        [parameter(Position=1,Mandatory=$false)][string]$outfile
    )
    $wc = new-object system.net.webclient
    if($proxyserver.length -gt 0){
        write-verbose "proxy server is $proxyserver"
        $proxy = New-Object System.Net.WebProxy("$proxyserver")
        $proxy.useDefaultCredentials = $true
        $wc.proxy = $proxy
    }

    $uri = [System.Uri]"$url"
    $count = $uri.PathAndQuery.split("/").count
    if($outfile){
        $filename=$outfile
    }
    else{
        $filename = $uri.PathAndQuery.split("/")[$count -1]
    }

    write-verbose "download $url to $filename"

    try {
        $wc.downloadfile($url,$filename)
    }
    catch {
        write-verbose "$_.Exception.Message"
    }
}

function Remove-cAzureDisks{
    Get-AzureDisk | where { $_.AttachedTo -eq $null } | Remove-AzureDisk -DeleteVHD
}


Function Set-CustomProxy{
  [CmdletBinding()]
  param(
        [parameter(Mandatory=$false)][switch]$infonet=$false,
        [parameter(Mandatory=$false)][switch]$local=$false,
        [parameter(Mandatory=$false)][switch]$zscaler=$false,
        [parameter(Mandatory=$false)][switch]$disable=$false,
        [parameter(Mandatory=$false)][string]$proxy=$false
    )

  begin{
    write-verbose "Starting toggle proxy: $($PSBoundParameters.count)"
  }

  process{
    $config = $((Get-Content "$global:thismoddir\config.json") -join "`n" | convertfrom-json).Proxies
    
    $http = $env:http_proxy
    $https = $env:https_proxy
    $parameters = $PSBoundParameters
    

    # $hproxy.Keys | % { "key = $_ , value = " + $hproxy.Item($_) }

    if($proxy -eq $false){
      # echo "selected item = $($config.Name)"
      # echo "value of selected item = $($config.Value)"
      # echo "selected item's correcponding value in hashtable = $($config[$($PSBoundParameters.keys | select -first 1)])"
      $selectedkey = ($parameters.keys | select -first 1)
      $value = ($config | where {$_.Name -eq "$selectedkey"}).Value
      $http=$value
      $https=$http
    }else {
      $http="http://$proxy"
      $https="http://$proxy"
    }


    $env:http_proxy=$http
    $env:https_proxy=$https
    [Environment]::SetEnvironmentVariable | out-null
    
    setx http_proxy "$http"
    setx https_proxy "$http"

    ("http_proxy", $http, [System.EnvironmentVariableTarget]::Machine) | out-null
    ("https_proxy", $https, [System.EnvironmentVariableTarget]::Machine) | out-null
    ("http_proxy", $http, [System.EnvironmentVariableTarget]::User) | out-null
    ("https_proxy", $https, [System.EnvironmentVariableTarget]::User) | out-null
    
    
    write-verbose "Completed toggle proxy to $($env:http_proxy)"
  }

}

Function Get-JSONFile{
  [CmdletBinding()]
  param(
        [parameter(Mandatory=$true)][string]$path,
        [parameter(Mandatory=$false)][string]$outfile=$false
    )

  $results = (Get-Content $path) -join "`n" 
  $results = $results | convertfrom-json

  if($outfile -ne $false){
    write-verbose "outfile is defined"
    $results | ConvertTo-json | out-file "$outfile"

  }
  else{
    write-verbose "outfile is not defined"
  }
  
  return $results
}

function New-CustomNuSpec{
  [CmdletBinding()]
  param(
        [parameter(Mandatory=$false,Position=0)][string]$path="$(get-location)\Package",
        [parameter(Mandatory=$false)][string]$id="my_cool_module",
        [parameter(Mandatory=$false)][string]$version="1.0.2014",
        [parameter(Mandatory=$false)][string]$authors="$env:username",
        [parameter(Mandatory=$false)][string]$owners="$env:username",
        [parameter(Mandatory=$false)][string]$projectUrl="http://snowwiki.nestle.com/wiki/index.php/$id",
        [parameter(Mandatory=$false)][string]$licenseUrl=$projectUrl,
        [parameter(Mandatory=$false)][string]$iconUrl=$projectUrl,
        [parameter(Mandatory=$false)][string]$requireLicenseAcceptance="false",
        [parameter(Mandatory=$false)][string]$description="Standard PowerShell Module",
        [parameter(Mandatory=$false)][string]$releaseNotes="CI build created $((get-date).DateTime) $(([TimeZoneInfo]::Local).DisplayName)",
        [parameter(Mandatory=$false)][string]$copyright="false",
        [parameter(Mandatory=$false)][string]$tags="false"
    )

  
  nuget spec $path -f

  $fullpath = $path + ".nuspec"  

  write-verbose "Using the file $fullpath"
  [xml]$newfile = Get-Content "$fullpath"
  $newfile.package.ChildNodes.Item(0).id=$id
  $newfile.package.ChildNodes.Item(0).version=$version
  $newfile.package.ChildNodes.Item(0).authors=$authors
  $newfile.package.ChildNodes.Item(0).owners=$owners
  $newfile.package.ChildNodes.Item(0).licenseUrl=$licenseUrl
  $newfile.package.ChildNodes.Item(0).projectUrl=$projectUrl
  $newfile.package.ChildNodes.Item(0).iconUrl=$iconUrl
  $newfile.package.ChildNodes.Item(0).requireLicenseAcceptance=$requireLicenseAcceptance
  $newfile.package.ChildNodes.Item(0).description=$description
  $newfile.package.ChildNodes.Item(0).releaseNotes=$releaseNotes
  $newfile.package.ChildNodes.Item(0).copyright=$copyright
  $newfile.package.ChildNodes.Item(0).tags=$tags

  # Remove all dependendies using a where clause
  $basedep = $newfile.package.metadata.dependencies.dependency | where {$_.id -eq "SampleDependency"}

  # Remove the sample dependency created by Nuget
  $newfile.package.metadata.dependencies.RemoveChild($basedep) | out-null

  # Save File  
  $newfile.Save("$fullpath")
  nuget pack $fullpath -basepath (split-path -parent $path) -OutputDirectory (split-path -parent $path)

}

function Test-Administrator
{  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)  
}

Export-ModuleMember -Function * -Alias *