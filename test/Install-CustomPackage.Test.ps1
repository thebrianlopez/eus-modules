$mi = $MyInvocation.MyCommand.Definition
[io.directoryinfo]$src = "$($([io.directoryinfo]$mi).parent.parent)\src"
$module = (resolve-path "$($src.fullname)\*.*" | select -first 1).path
import-module $module -verbose
set-psdebug -trace 0
$VerbosePreference="Continue"

Measure-Command {

    write-debug "Step 1: Confirm the module is loaded"
    get-command -name "Install-CustomPackage" -modules "eus-modules";if($error){write-error "Test Failed" -ea Stop;throw;}
    get-command -name "inst" -modules "eus-modules";if($error){write-error "Test Failed" -ea Stop;throw;}


    write-debug "Step 2: Delete all old packlages from system"
    if(test-path "$env:appdata\nuget\programs\7zip"){rm -rec -for "$env:appdata\nuget\programs\7zip"}
    if(test-path "$env:appdata\nuget\programs\npp"){rm -rec -for "$env:appdata\nuget\programs\npp"}
    
    if($error){write-error "Test Failed" -ea Stop;throw;}

    write-debug "Step 3: Install and validate 7zip"
    
    inst 7zip,npp

    if(-not (test-path -path "$env:appdata\nuget\programs\7zip\7z.exe")){write-error "Unable to find executable" -erroraction Stop}
      
}
$error