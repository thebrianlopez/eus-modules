$mi = $MyInvocation.MyCommand.Definition
[io.directoryinfo]$src = "$($([io.directoryinfo]$mi).parent.parent)\src"
$module = (resolve-path "$($src.fullname)\*.*" | select -first 1).path
import-module $module -verbose
set-psdebug -trace 0
$VerbosePreference="Continue"

Measure-Command {

    write-debug "Step 1: Confirm the module is loaded"

    get-command -name "Install-CustomModule" -modules "eus-modules";if($error){write-error "Test 1 Failed" -ea Stop;throw;}
    get-command -name "pinst" -modules "eus-modules";if($error){write-error "Test 1 Failed" -ea Stop;throw;}
    get-command -name "modinst" -modules "eus-modules";if($error){write-error "Test 1 Failed" -ea Stop;throw;}
    get-command -name "psinst" -modules "eus-modules";if($error){write-error "Test 1 Failed" -ea Stop;throw;}
    get-command -name "psminst" -modules "eus-modules";if($error){write-error "Test 1 Failed" -ea Stop;throw;}

    $user = "$env:userprofile\Documents\WindowsPowerShell\Modules\eus-modules"
    $system = "$env:programfiles\WindowsPowerShell\Modules\eus-modules"

    write-debug "Step 2: Check execution runs in user context using module name"
        if(test-path $user){rm -rec -for $user} # Remove all existing instances of eus-modules
        Install-CustomModule eus-modules        # Install the module
        # Test the module was installed at the correct location
        if(-not (test-path -path $user)){write-error "Unable to find module at $user" -erroraction Stop;}else{write-host "Test 2 Passed" -foregroundcolor Green;}


    write-debug "Step 3: Check execution runs in user context using alias name"
        if(test-path $user){rm -rec -for $user} # Remove all existing instances of eus-modules
        minst eus-modules                       # Install the module with the alias
        # Test the module was installed at the correct location
        if(-not (test-path -path $user)){write-error "Unable to find module at $user" -erroraction Stop;}else{write-host "Test 3 Passed" -foregroundcolor Green;}


    write-debug "Step 4: Test in system contexts, assuming we are running elevated"
    if(test-administrator){

        # Remove all existing instances of eus-modules
        if(test-path $system){rm -rec -for $system}

        # Install the module with the alias
        minst eus-modules -system

        # Test the module was installed at the correct location
        if(-not (test-path -path $system)){write-error "Unable to find module at $system" -erroraction Stop;}else{write-host "Test 4 Passed" -foregroundcolor Green;}
    }

    write-debug "Step 5: Force Nuget operation"
        if(test-path $user){rm -rec -for $user} # Remove all existing instances of eus-modules
        minst eus-modules -forcenuget                       # Install the module with the alias
        # Test the module was installed at the correct location
        if(-not (test-path -path $user)){write-error "Unable to find module at $user" -erroraction Stop;}else{write-host "Test 5 Passed" -foregroundcolor Green;}
    
    # minst eus-modules -user -system
    minst eus-modules -user
    if(-not (test-path -path $user)){write-error "Unable to find module at $user" -erroraction Stop;}else{write-host "Test 6 Passed" -foregroundcolor Green;}

    
    minst eus-modules
    if(-not (test-path -path $user)){write-error "Unable to find module at $user" -erroraction Stop;}else{write-host "Test 7 Passed" -foregroundcolor Green;}
    


    

    if($error){write-error "Test Failed" -ea Stop;throw;}

}
