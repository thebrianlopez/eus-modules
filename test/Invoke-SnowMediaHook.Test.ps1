set-psdebug -trace 1
set-psbreakpoint -variable boom 
$mi = $MyInvocation.MyCommand.Definition
[io.directoryinfo]$src = "$($([io.directoryinfo]$mi).parent.parent)\src"
$module = (resolve-path "$($src.fullname)\*.psm1" | select -first 1).path
import-module $module -verbose

write-debug "Step 1: Confirm the module is loaded"
get-command -name "Invoke-SnowMediaHook" -modules "eus-modules"
if($error){write-error "Test Failed" -ea Stop;throw;}

write-debug "Step 2: Start OSD"
get-command -name "start-cmosd" -modules "eus-modules"
if($error){write-error "Test Failed" -ea Stop;throw;}

Measure-Command {
    start-cmosd;
}
