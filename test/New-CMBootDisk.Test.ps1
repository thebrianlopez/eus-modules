set-psdebug -trace 1
$VerbosePreference="Continue"
$mi = $MyInvocation.MyCommand.Definition
[io.directoryinfo]$src = "$($([io.directoryinfo]$mi).parent.parent)\src"
$module = (resolve-path "$($src.fullname)\*.*" | select -first 1).path
import-module $module -verbose

write-debug "Test 1: Check if commandlet is visible"
get-command -name "New-CMBootDisk" -modules "eus-modules"
if($error){
    write-error "Test Failed" -ea Stop 
    throw;
}

write-debug "Test 2: Make sure it works in the dev environment"
Measure-Command{
    $two = @{
        "cas"="hqaaam0300";
        "DPContainingBootImage"="hqaaam0322.nestle.w2k"
        "bootimageid"="D000028D";
        #"preexecutionpackage"="D0000225" # MDT Files.Webservices package
        "encryptedUserName"="bmVzdGxlLncyaw=="
        "encryptedPassword"="TmVzdGxlMTIz"
        "encryptedDomain"="bmVzdGxlLncyaw=="
        "destination"="$($env:userprofile)\desktop\brian.iso"
    }
    New-CMBootDisk @two -verbose
}