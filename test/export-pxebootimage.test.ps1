# eus-modules\test\export-pxebootimage.test.ps1
set-psdebug -trace 1
$VerbosePreference="Continue"
$mi = $MyInvocation.MyCommand.Definition
[io.directoryinfo]$src = "$($([io.directoryinfo]$mi).parent.parent)\src"
$module = (resolve-path "$($src.fullname)\*.*" | select -first 1).path


Measure-Command{
    import-module $module -verbose

    write-debug "Test 1: Check if commandlet is visible"
    get-command -name "Export-PXEBootImage" -modules "eus-modules"
    if($error){
        write-error "Test Failed" -ea Stop 
        throw;
    }

    write-debug "Test 2: Export-PXEBootImage"

    $dev = @{
            "cas"="hqaaam0300.nestle.w2k";
            "DPContainingBootImage"="hqaaam0322.nestle.w2k"
            "preexecutionpackage"="D0000225" # MDT Files.Webservices package
            "encryptedUserName"="bmVzdGxlLncyaw=="
            "encryptedPassword"="TmVzdGxlMTIz"
            "encryptedDomain"="bmVzdGxlLncyaw=="
            "destination"="c:\temp\pxeexport";
    }

    Export-PXEBootImage @dev -bootimageid D0000290,D0000291

}