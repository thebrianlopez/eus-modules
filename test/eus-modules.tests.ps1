$modulename = "eus-modules"

Describe "if $modulename is resolveable" {
    It "has a valid module" {
        (get-module $modulename).Name | should not be null
    }

    It "has a valid manifest" {
        {
            $script:manifest = Test-ModuleManifest -Path (resolve-path "$modulename\*.psd1").path -ErrorAction Stop -WarningAction SilentlyContinue
        } | Should Not Throw
    }

    It "has valid commandlets"{
        (get-command test-buildpackages).name | should not be null
    }
}